import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

// DARK MODE COLOR
// Kuning
const Color primaryColor = Color(0xffEDA923);
// Abu
const Color secondaryColor = Color(0xffBFBFBF);
// Cream
const Color thirdColor = Color(0xffECE5DD);
// Abu Gelap
const Color fourthColor = Color(0xff616161);
// Kuning 2
const Color fiveColor = Color(0xffFED27C);
// Label
const Color sixColor = Color(0xffECEB98);
// Warning
const Color sevenColor = Color(0xffFF502A);
// Success
const Color eightColor = Color(0xff25D366);
// Hitam
const Color ninthColor = Color(0xff404040);
// Putih
const Color tenthColor = Color(0xffF4F4F4);
//Abu Terang Banget
const Color elveColor = Color(0xffDDE5E9);
const Color backgroundColor = Color(0xff181818);

TextStyle primaryTextStyle = GoogleFonts.notoSans();
TextStyle secondaryTextStyle = GoogleFonts.openSans();
TextStyle thirdTextStyle = GoogleFonts.poppins();

const FontWeight light = FontWeight.w300;
const FontWeight reguler = FontWeight.w400;
const FontWeight medium = FontWeight.w500;
const FontWeight semiBold = FontWeight.w600;
const FontWeight bold = FontWeight.w700;
