import 'package:flutter/material.dart';
import 'package:initial_folder/models/user_info_model.dart';
import 'package:initial_folder/services/user_info_service.dart';

enum ResultState { Loading, NoData, HasData, Error }

class UserInfoProvider with ChangeNotifier {
  final UserInfoService userInfoService;

  UserInfoProvider({
    required this.userInfoService,
  });

  UserInfoModel? _userInfoModel;

  ResultState? _state;

  String _message = '';

  UserInfoModel? get result => _userInfoModel;

  ResultState? get state => _state;

  String get message => _message;

  set userInfo(UserInfoModel userInfo) {
    _userInfoModel = userInfo;
    notifyListeners();
  }

  Future<dynamic> getUserInfo(_email) async {
    try {
      _state = ResultState.Loading;
      notifyListeners();
      UserInfoModel userInfo = await userInfoService.getUserInfo(_email);
      //print(userInfo.data[0].id_user);
      if (userInfo.data.isEmpty) {
        _state = ResultState.NoData;
        notifyListeners();
        return _message = 'Empty Data';
      } else {
        _state = ResultState.HasData;
        notifyListeners();
        return _userInfoModel = userInfo;
      }
    } catch (e) {
      _state = ResultState.Error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }
}
