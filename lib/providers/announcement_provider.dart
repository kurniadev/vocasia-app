import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:initial_folder/models/announcement_model.dart';
import 'package:initial_folder/services/announcement_service.dart';
import 'package:initial_folder/services/course_service.dart';

enum ResultState { loading, noData, hasData, error }
enum ResultStateLike { loading, error }

class AnnouncementProvider with ChangeNotifier {
  final AnnouncementService announcementService;
  final String id;

  AnnouncementProvider({required this.announcementService, required this.id}) {
    getAnnouncement(id);
  }
  String _message = '';
  String get message => _message;
  ResultState? _state;
  ResultState? get state => _state;
  AnnouncementModel? _announcementModel;
  AnnouncementModel? get result => _announcementModel;
  set annoucement(AnnouncementModel? announcementModel) {
    _announcementModel = announcementModel;
    notifyListeners();
  }

  Future<dynamic> getAnnouncement(String _id) async {
    try {
      _state = ResultState.loading;
      notifyListeners();
      AnnouncementModel announcementModel =
          await announcementService.getAnnouncement(_id);
      if (announcementModel.data[0].isEmpty) {
        _state = ResultState.noData;
        notifyListeners();
        return _message = 'Tidak ada Data';
      } else {
        _state = ResultState.hasData;
        notifyListeners();
        return _announcementModel = announcementModel;
      }
    } catch (e) {
      _state = ResultState.error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }
}
