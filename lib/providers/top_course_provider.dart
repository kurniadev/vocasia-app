import 'package:flutter/material.dart';
import 'package:initial_folder/models/course_model.dart';
import 'package:initial_folder/services/course_service.dart';

enum ResultState { Loading, NoData, HasData, Error }

class TopCourseProvider with ChangeNotifier {
  final CourseService courseService;
  TopCourseProvider({required this.courseService}) {
    getTopCourse();
  }

  List<CourseModel> _course = [];

  ResultState? _state;

  String _message = '';

  List<CourseModel> get result => _course;

  ResultState? get state => _state;

  String get message => _message;

  set course(List<CourseModel> course) {
    _course = course;
    notifyListeners();
  }

  Future<dynamic> getTopCourse() async {
    try {
      _state = ResultState.Loading;
      notifyListeners();
      List<CourseModel> course = await courseService.getTopCourse();
      if (course.isEmpty) {
        _state = ResultState.NoData;
        notifyListeners();
        return _message = 'Empty Data';
      } else {
        _state = ResultState.HasData;
        notifyListeners();
        return _course = course;
      }
    } catch (e) {
      _state = ResultState.Error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }
}
