import 'package:flutter/material.dart';
import 'package:initial_folder/models/carts_model.dart';
import 'package:initial_folder/services/cart_service.dart';

enum ResultState { Loading, NoData, HasData, Error }

class CartsProvider with ChangeNotifier {
  final CartService cartService;
  CartsProvider({required this.cartService}) {
    getCarts();
  }
  CartsModel? _cartsModel;

  ResultState? _state;

  String _message = '';

  CartsModel? get result => _cartsModel;

  ResultState? get state => _state;

  String get message => _message;
  int _length = 0;
  int get lenght => _length;
  set lengthCarts(int v) {
    _length = v;
    notifyListeners();
  }

  List _data = [];
  List get data => _data;

  set carts(CartsModel carts) {
    _cartsModel = carts;
    notifyListeners();
  }

  void clear() {
    _length = 0;
    notifyListeners();
  }

  Future<dynamic> getCarts() async {
    try {
      _state = ResultState.Loading;
      notifyListeners();
      CartsModel carts = await cartService.getCarts();
      _length = carts.data.length;
      notifyListeners();
      if (carts.data.isEmpty) {
        _state = ResultState.NoData;
        notifyListeners();
        return _message = 'Empty Data';
      } else {
        _data = carts.data.map((e) => e.courseId).toList();
        _state = ResultState.HasData;
        notifyListeners();
        return _cartsModel = carts;
      }
    } catch (e) {
      _state = ResultState.Error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }
}
