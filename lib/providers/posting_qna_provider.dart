import 'package:flutter/material.dart';
import 'package:initial_folder/models/qna_model.dart';
import 'package:initial_folder/services/qna_service.dart';

enum ResultState { uninitilized, loading, failed, success }

class PostingQnaProvider with ChangeNotifier {
  QnaDataModel? _qnaDataModel;
  QnaDataModel? get wishlistPostModel => _qnaDataModel;

  setwishlistPostModel(QnaDataModel? qnaDataModel) {
    _qnaDataModel = qnaDataModel;
    notifyListeners();
  }

  ResultState _state = ResultState.uninitilized;

  ResultState get state => _state;

  //Posting QNA
  Future<bool> postingQna(String quest, String idCourse) async {
    try {
      _state = ResultState.loading;
      notifyListeners();

      bool response = await QnaService().postingQna(quest, idCourse);
      if (response) {
        _state = ResultState.success;
        notifyListeners();
        return true;
      } else {
        _state = ResultState.failed;
        notifyListeners();
        return false;
      }
    } catch (e) {
      _state = ResultState.failed;
      notifyListeners();
      return false;
    }
  }

  //Update QNA
  Future<bool> editQna(String quest, int idQna, String idCourse) async {
    try {
      _state = ResultState.loading;
      notifyListeners();

      bool response = await QnaService().updateQna(idQna, quest, idCourse);
      if (response) {
        _state = ResultState.success;
        notifyListeners();
        return true;
      } else {
        _state = ResultState.failed;
        notifyListeners();
        return false;
      }
    } catch (e) {
      _state = ResultState.failed;
      notifyListeners();
      return false;
    }
  }

  //Delete QNA
  Future<bool> deleteQna(int idQna) async {
    try {
      _state = ResultState.loading;
      notifyListeners();

      bool response = await QnaService().deleteQna(idQna);
      if (response) {
        _state = ResultState.success;
        notifyListeners();
        return true;
      } else {
        _state = ResultState.failed;
        notifyListeners();
        return false;
      }
    } catch (e) {
      _state = ResultState.failed;
      notifyListeners();
      return false;
    }
  }
}
