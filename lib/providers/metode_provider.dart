import 'package:flutter/material.dart';

enum MetodePembayaran { kreditdebit, mandiri, bni, permata, lainnya, gopay }

class MetodeProvider with ChangeNotifier {
  MetodePembayaran? _character = MetodePembayaran.kreditdebit;

  MetodePembayaran? get character => _character;

  set character(MetodePembayaran? value) {
    _character = value;
    notifyListeners();
  }
}
