import 'package:flutter/cupertino.dart';
import 'package:initial_folder/models/course_model.dart';
import 'package:initial_folder/services/search_service.dart';

enum ResultState { loading, noData, hasData, error }

class FilterCourseProvider with ChangeNotifier {
  final SearchService searchService;
  FilterCourseProvider({required this.searchService});

  List<CourseModel> _searchCourse = [];
  ResultState? _state;
  String _message = '';
  String _search = '';

  List<CourseModel> get result => _searchCourse;
  ResultState? get state => _state;
  String get message => _message;
  String get search => _search;

  set filterSearchText(String text) {
    _search = text;
    notifyListeners();
  }

  String _currentIndex = '';
  String get currentIndex => _currentIndex;
  set currentIndex(String index) {
    _currentIndex = index;
    notifyListeners();
  }

  String _currentIndexRating = '';
  String get currentIndexRating => _currentIndexRating;
  set currentIndexRating(String index) {
    _currentIndexRating = index;
    notifyListeners();
  }

  String _currentIndexRadio = '';
  String get currentIndexRadio => _currentIndexRadio;
  set currentIndexRadio(String index) {
    _currentIndexRadio = index;
    notifyListeners();
  }

  String _nameCurrentIndexRadio = '';
  String get nameCurrentIndexRadio => _nameCurrentIndexRadio;
  set nameCurrentIndexRadio(String index) {
    _nameCurrentIndexRadio = index;
    notifyListeners();
  }

  String _currentIndexLevelCheckBox = '';
  String get currentIndexLevelCheckBox => _currentIndexLevelCheckBox;
  set currentIndexLevelCheckBox(String index) {
    _currentIndexLevelCheckBox = index;
    notifyListeners();
  }

  void reset() {
    _currentIndexLevelCheckBox = '';
    _currentIndexRadio = '';
    _currentIndexRating = '';
    _currentIndex = '';
    notifyListeners();
  }

  bool _isSearch = false;
  bool get isSearch => _isSearch;
  void isSearchsTrue() {
    _isSearch = true;
    notifyListeners();
  }

  void isSearchsFalse() {
    _isSearch = false;
    notifyListeners();
  }

  Future<dynamic> filterCourse({
    String price = '',
    String level = '',
    String rating = '',
    String subCategory = '',
    String namePrice = '',
    String nameLevel = '',
    String nameRating = '',
    String nameSubcategory = '',
  }) async {
    try {
      // _state = ResultState.loading;
      // notifyListeners();
      var course = await searchService.filter(
        nameLevel: nameLevel,
        namePrice: namePrice,
        nameRating: nameRating,
        nameSubcategory: nameSubcategory,
        price: price,
        level: level,
        rating: rating,
        subCategory: subCategory,
      );
      print(course);
      return _message = course;
    } catch (e) {
      _state = ResultState.error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }
}
