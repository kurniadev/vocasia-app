import 'package:flutter/material.dart';
import 'package:initial_folder/models/course_model.dart';
import 'package:initial_folder/services/course_service.dart';

enum ResultState { Loading, NoData, HasData, Error }

class OthersCourseProvider with ChangeNotifier {
  final CourseService otherCourseService;
  OthersCourseProvider({required this.otherCourseService}) {
    getOthersCourse();
  }
  List<CourseModel> _othersCourse = [];

  ResultState? _state;

  String _message = '';

  List<CourseModel> get result => _othersCourse;

  ResultState? get state => _state;

  String get message => _message;
  int _page = 1;
  int get page => _page;
  bool _loading = true;
  bool get loading => _loading;
  set course(List<CourseModel> course) {
    _othersCourse = course;
    notifyListeners();
  }

  Future<dynamic> getOthersCourse() async {
    try {
      _state = ResultState.Loading;
      notifyListeners();
      List<CourseModel> course =
          await otherCourseService.getOthersCourse(_page);

      if (course.isEmpty) {
        _state = ResultState.NoData;
        notifyListeners();
        return _message = 'Empty Data';
      } else {
        _state = ResultState.HasData;
        notifyListeners();
        return _othersCourse = course;
      }
    } catch (e) {
      _state = ResultState.Error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }

  Future<dynamic> getOthersCourses() async {
    try {
      List<CourseModel> res =
          await otherCourseService.getOthersCourse(_page + 1);
      if (res.isNotEmpty) {
        _page += 1;
        notifyListeners();
        return _othersCourse.addAll(res);
      } else {
        _loading = false;
        notifyListeners();
      }
    } catch (e) {
      _state = ResultState.Error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }
}
