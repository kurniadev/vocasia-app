import 'package:flutter/material.dart';
import 'package:initial_folder/models/wishlist_model.dart';
import 'package:initial_folder/services/wishlist_service.dart';

enum ResultState { Loading, NoData, HasData, Error }

class WishlistProvider with ChangeNotifier {
  WishlistProvider() {
    getWishlist();
  }
  WishlistModel? _wishlistModel;
  ResultState? _state;

  String _message = '';
  List _data = [];
  List get data => _data;
  set dataWishlist(List value) {
    _data = value;
    notifyListeners();
  }

  WishlistModel? get result => _wishlistModel;

  ResultState? get state => _state;

  String get message => _message;

  set wishlistModel(WishlistModel wishlist) {
    _wishlistModel = wishlist;
    notifyListeners();
  }

  Future<dynamic> getWishlist() async {
    try {
      _state = ResultState.Loading;
      notifyListeners();

      WishlistModel wishlist = await WishlistService().getWishlist();
      if (wishlist.data.isEmpty) {
        _data = [];
        _state = ResultState.NoData;
        notifyListeners();
        return _message = 'Empty Data';
      } else {
        _data = wishlist.data[0].map((e) => e.courseId).toList();
        _state = ResultState.HasData;
        notifyListeners();
        return _wishlistModel = wishlist;
      }
    } catch (e) {
      _state = ResultState.Error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }
}
