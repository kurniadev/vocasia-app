import 'package:flutter/material.dart';
import 'package:initial_folder/helper/user_info.dart';
import 'package:initial_folder/models/counter_qna_comment_model.dart';
import 'package:initial_folder/models/qna_model.dart';
import 'package:initial_folder/services/qna_service.dart';

enum ResultState { loading, noData, hasData, error }
enum ResultStateLike { loading, error }

class QnaProvider with ChangeNotifier {
  final String idCourse;

  QnaProvider({required this.idCourse}) {
    getQna(idCourse);
  }
  String _message = '';
  String get message => _message;
  ResultState? _state;
  ResultState? get state => _state;
  QnaModel? _qnaModel;
  QnaModel? get result => _qnaModel;
  set qnaModel(QnaModel? qnaModel) {
    _qnaModel = qnaModel;
    notifyListeners();
  }

  //Get QNA
  Future<dynamic> getQna(String _idCourse) async {
    try {
      _state = ResultState.loading;
      notifyListeners();
      QnaModel qnaModel = await QnaService().getMyQna(idCourse);
      if (qnaModel.data[0].isEmpty) {
        _state = ResultState.noData;
        notifyListeners();
        return _message = 'Tidak ada Data';
      } else {
        _state = ResultState.hasData;
        notifyListeners();
        return _qnaModel = qnaModel;
      }
    } catch (e) {
      _state = ResultState.error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }
}
