import 'package:flutter/material.dart';
import 'package:initial_folder/services/qna_service.dart';

enum ResultState { uninitilized, loading, failed, success }

class PostingQnaReplyProvider with ChangeNotifier {
  ResultState _state = ResultState.uninitilized;

  ResultState get state => _state;

  //Post Qna Reply
  Future<bool> postQnaReply(String text_rep, String id_qna) async {
    try {
      _state = ResultState.loading;
      notifyListeners();

      bool response = await QnaService().postQnaReply(text_rep, id_qna);
      if (response) {
        _state = ResultState.success;
        notifyListeners();
        return true;
      } else {
        _state = ResultState.failed;
        notifyListeners();
        return false;
      }
    } catch (e) {
      _state = ResultState.failed;
      notifyListeners();
      return false;
    }
  }

  //Update QNA Reply
  Future<bool> editQnaReply(String text_rep, int id_rep, String id_qna) async {
    try {
      _state = ResultState.loading;
      notifyListeners();

      bool response = await QnaService().editQnaReply(id_rep, text_rep, id_qna);
      if (response) {
        _state = ResultState.success;
        notifyListeners();
        return true;
      } else {
        _state = ResultState.failed;
        notifyListeners();
        return false;
      }
    } catch (e) {
      _state = ResultState.failed;
      notifyListeners();
      return false;
    }
  }

  // Delete Qna Reply
  Future<bool> deleteReplyQna(int id_rep) async {
    try {
      _state = ResultState.loading;
      notifyListeners();

      bool response = await QnaService().deleteReplyQna(id_rep);
      if (response) {
        _state = ResultState.success;
        notifyListeners();
        return true;
      } else {
        _state = ResultState.failed;
        notifyListeners();
        return false;
      }
    } catch (e) {
      _state = ResultState.failed;
      notifyListeners();
      return false;
    }
  }
}
