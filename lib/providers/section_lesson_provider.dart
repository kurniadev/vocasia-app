import 'package:flutter/foundation.dart';

class SectionLessonProvider with ChangeNotifier {
  bool isExpanded;
  bool isCheckBoxLesson;
  bool isOutcomes;
  bool isDescription;
  bool isDescriptionInstruktur;
  SectionLessonProvider({
    this.isExpanded = false,
    this.isCheckBoxLesson = false,
    this.isOutcomes = false,
    this.isDescription = true,
    this.isDescriptionInstruktur = false,
  });

  void expanded() {
    isExpanded = !isExpanded;
    notifyListeners();
  }

  void checkBoxLesson() {
    isCheckBoxLesson = !isCheckBoxLesson;
    notifyListeners();
  }

  void detailPlayCourseOutcomes() {
    isOutcomes = !isOutcomes;
    notifyListeners();
  }

  void descriptionPlayCourse() {
    isDescription = !isDescription;
    notifyListeners();
  }

  void descriptionInstrukturPlayCourse() {
    isDescriptionInstruktur = !isDescriptionInstruktur;
    notifyListeners();
  }
}
