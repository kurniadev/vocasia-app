import 'package:flutter/material.dart';
import 'package:initial_folder/models/qna_model.dart';
import 'package:initial_folder/services/qna_service.dart';

enum ResultState { loading, noData, hasData, error }
enum ResultStateLike { loading, error }

class ReplyQnaProvider with ChangeNotifier {
  final String idCourse;
  final int index;

  ReplyQnaProvider({required this.idCourse, required this.index}) {
    getQna(idCourse, index);
  }
  String _message = '';
  String get message => _message;
  ResultState? _state;
  ResultState? get state => _state;
  QnaModel? _qnaModel;
  QnaModel? get result => _qnaModel;
  set qnaModel(QnaModel? qnaModel) {
    _qnaModel = qnaModel;
    notifyListeners();
  }

  Future<dynamic> getQna(String _idCourse, int index) async {
    try {
      _state = ResultState.loading;
      notifyListeners();
      QnaModel qnaModel = await QnaService().getMyQna(idCourse);
      if (qnaModel.data[0][index].comment.isEmpty) {
        _state = ResultState.noData;
        notifyListeners();
        return _message = 'Tidak ada Data';
      } else {
        _state = ResultState.hasData;
        notifyListeners();
        return _qnaModel = qnaModel;
      }
    } catch (e) {
      _state = ResultState.error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }
}
