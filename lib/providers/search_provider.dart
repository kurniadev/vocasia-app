import 'package:flutter/cupertino.dart';
import 'package:initial_folder/models/course_model.dart';
import 'package:initial_folder/services/search_service.dart';

enum ResultState { loading, noData, hasData, error }

class SearchProvider with ChangeNotifier {
  final SearchService searchService;
  SearchProvider({required this.searchService});

  List<CourseModel> _searchCourse = [];

  ResultState? _state;

  String _message = '';

  List<CourseModel> get result => _searchCourse;

  ResultState? get state => _state;

  String get message => _message;

  String _search = '';

  String get search => _search;

  set searchText(String text) {
    _search = text;
    notifyListeners();
    searchCourse(text);
  }

  void clearSearchBox() {
    _search = '';
    notifyListeners();
  }

  Future<dynamic> searchCourse(_judul) async {
    try {
      _state = ResultState.loading;
      notifyListeners();
      List<CourseModel> course = await searchService.search(_judul);

      if (course.isEmpty) {
        _state = ResultState.noData;
        notifyListeners();
        return _message = 'Empty Data';
      } else {
        _state = ResultState.hasData;
        notifyListeners();
        return _searchCourse = course;
      }
    } catch (e) {
      _state = ResultState.error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }
}
