import 'package:flutter/material.dart';
import 'package:initial_folder/models/data_diri_model.dart';
import 'package:initial_folder/services/user_info_service.dart';

enum ResultState { loading, succes }

class UpdateDataDiriProvider with ChangeNotifier {
  DataDiriModel? _dataDiriModel;

  DataDiriModel? get dataDiriModel => _dataDiriModel;
  ResultState? _state;
  ResultState? get state => _state;
  set dataDiriModel(DataDiriModel? _dataDiriModel) {
    _dataDiriModel = dataDiriModel;
    notifyListeners();
  }

  Future<bool> dataDiriUpdate(
      {String? fullname,
      String? biograph,
      String? twitter,
      String? facebook,
      String? linkedin,
      String? instagram,
      String? datebirth,
      String? phone,
      String? gender,
      String? heading}) async {
    try {
      _state = ResultState.loading;
      notifyListeners();
      bool dataDiriModel = await UserInfoService().updateDataDiri(
          fullname: fullname,
          biograph: biograph,
          phone: phone,
          datebirth: datebirth,
          gender: gender,
          heading: heading,
          facebook: facebook,
          linkedin: linkedin,
          twitter: twitter,
          instagram: instagram);
      if (dataDiriModel) {
        _state = ResultState.succes;
        notifyListeners();
        return true;
      }

      return false;
    } catch (e) {
      print("EXception: $e");
      return false;
    }
  }
}
