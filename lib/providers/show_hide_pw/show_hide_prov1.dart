import 'package:flutter/cupertino.dart';

class ShowHidePassword1 with ChangeNotifier {
  bool password;
  ShowHidePassword1({this.password = false});

  void showPassword() {
    password = !password;
    notifyListeners();
  }
}
