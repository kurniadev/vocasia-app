import 'package:flutter/cupertino.dart';

class ShowHidePassword with ChangeNotifier {
  bool password;
  ShowHidePassword({this.password = false});

  void showPassword() {
    password = !password;
    notifyListeners();
  }
}
