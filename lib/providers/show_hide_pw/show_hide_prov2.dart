import 'package:flutter/cupertino.dart';

class ShowHidePassword2 with ChangeNotifier {
  bool password;
  ShowHidePassword2({this.password = false});

  void showPassword() {
    password = !password;
    notifyListeners();
  }
}
