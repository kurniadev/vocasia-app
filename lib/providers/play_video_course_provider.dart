import 'package:flutter/cupertino.dart';

class PlayVideoCourseProvider with ChangeNotifier {
  String _url = '';

  String get url => _url;

  set uri(String urlVideo) {
    _url = urlVideo;
    notifyListeners();
  }

  indexUri(String indexUri) {
    _url = indexUri;
    notifyListeners();
  }
}
