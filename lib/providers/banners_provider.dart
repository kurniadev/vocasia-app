import 'package:flutter/material.dart';
import 'package:initial_folder/models/banners_model.dart';
import 'package:initial_folder/services/banners_service.dart';

enum ResultState { Loading, NoData, HasData, Error }

class BannersProvider with ChangeNotifier {
  final BannersService bannersService;
  BannersProvider({required this.bannersService}) {
    getAllBanners();
  }
  List<BannersModel> _banners = [];

  ResultState? _state;

  String _message = '';

  List<BannersModel> get result => _banners;

  ResultState? get state => _state;

  String get message => _message;

  set banners(List<BannersModel> banners) {
    _banners = banners;
    notifyListeners();
  }

  Future<dynamic> getAllBanners() async {
    try {
      _state = ResultState.Loading;
      notifyListeners();
      List<BannersModel> banners = await bannersService.getAllBanners();
      if (banners.isEmpty) {
        _state = ResultState.NoData;
        notifyListeners();
        return _message = 'Empty Data';
      } else {
        _state = ResultState.HasData;
        notifyListeners();
        return _banners = banners;
      }
    } catch (e) {
      _state = ResultState.Error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }
}
