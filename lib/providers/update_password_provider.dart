import 'package:flutter/material.dart';
import 'package:initial_folder/models/update_password_model.dart';
import 'package:initial_folder/services/user_info_service.dart';

class UpdatePasswordProvider with ChangeNotifier {
  UpdatePasswordModel? _updatePasswordModel;

  UpdatePasswordModel? get updatePasswordModel => _updatePasswordModel;

  set updatePasswordModel(UpdatePasswordModel? _updatePasswordModel) {
    _updatePasswordModel = updatePasswordModel;
    notifyListeners();
  }

  Future<bool> passwordUpdate({
    required idUser,
    required String? email,
    required String? old_password,
    required String? password,
    required String? new_password_confirm,
  }) async {
    try {
      UpdatePasswordModel? updatePasswordModel = await UserInfoService()
          .updatePassword(
              idUser: idUser,
              email: email,
              old_password: old_password,
              password: password,
              new_password_confirm: new_password_confirm);

      _updatePasswordModel = updatePasswordModel;
      //print(user);
      return true;
    } catch (e) {
      print("EXception: $e");
      return false;
    }
  }
}
