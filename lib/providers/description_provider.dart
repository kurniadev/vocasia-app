import 'package:flutter/material.dart';

class DescriptionProvider with ChangeNotifier {
  bool isExpanded;

  DescriptionProvider({this.isExpanded = true});

  void expanded() {
    isExpanded = !isExpanded;
    notifyListeners();
  }
}
