import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:initial_folder/models/wishlist_model.dart';
import 'package:initial_folder/services/wishlist_service.dart';

class WishlistPostProvider with ChangeNotifier {
  WishlistPostModel? _wishlistPostModel;
  WishlistPostModel? get wishlistPostModel => _wishlistPostModel;

  setwishlistPostModel(WishlistPostModel? wishlistPostModel) {
    _wishlistPostModel = wishlistPostModel;
    notifyListeners();
  }

  Future<bool> addWishlist(int wishlistItem) async {
    try {
      WishlistPostModel? wishlistPostModel =
          await WishlistService().addWishlist(wishlistItem);
      _wishlistPostModel = wishlistPostModel;
      return true;
    } on SocketException {
      return false;
    } catch (e) {
      print("Exception: $e");
      return false;
    }
  }

  Future<bool> deleteWishlist(int wishlistItem) async {
    try {
      WishlistPostModel? wishlistPostModel =
          await WishlistService().deleteWishlist(wishlistItem);
      _wishlistPostModel = wishlistPostModel;
      return true;
    } on SocketException {
      return false;
    } catch (e) {
      print("Exception: $e");
      return false;
    }
  }
}
