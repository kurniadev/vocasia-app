import 'package:flutter/foundation.dart';
import 'package:initial_folder/models/section_lesson_model.dart';
import 'package:initial_folder/services/section_lesson_service.dart';

enum ResultState { Loading, HasData, Error, NoData }

class SectionLessonCourseProvider with ChangeNotifier {
  final SectionLessonService sectionLessonService;
  final String id;
  SectionLessonCourseProvider(
      {required this.sectionLessonService, required this.id}) {
    getDetailCourse(id);
  }

  SectionLessonModel? _sectionLessonModel;

  SectionLessonModel? get result => _sectionLessonModel;

  ResultState? _state;

  String _message = '';

  ResultState? get state => _state;

  String get message => _message;

  set detailCourse(SectionLessonModel detail) {
    _sectionLessonModel = detail;
    notifyListeners();
  }

  Future<dynamic> getDetailCourse(_id) async {
    try {
      _state = ResultState.Loading;
      notifyListeners();
      SectionLessonModel detail =
          await sectionLessonService.getSectionLessonCourse(_id);
      if (detail.data![0].isEmpty) {
        _state = ResultState.NoData;
        notifyListeners();
        return _message = 'Tidak ada data';
      } else {
        _state = ResultState.HasData;
        notifyListeners();
        return _sectionLessonModel = detail;
      }
    } catch (e) {
      _state = ResultState.Error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }
}
