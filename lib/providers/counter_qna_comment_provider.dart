// import 'package:flutter/material.dart';
// import 'package:initial_folder/models/counter_qna_comment_model.dart';
// import 'package:initial_folder/services/qna_service.dart';

// enum ResultState { uninitilized, loading, failed, success }

// class CounterQnaCommentProvider with ChangeNotifier {
//   ResultState _state = ResultState.uninitilized;

//   ResultState get state => _state;

// //Get Counter Comment QNA
//   Future<bool> getCounterComment(String idQna) async {
//     try {
//       _state = ResultState.loading;
//       notifyListeners();

//       CounterCommentModel counterCommentModel =
//           await QnaService().getCounterComment(idQna);
//       if (counterCommentModel.data != null) {
//         _state = ResultState.success;
//         notifyListeners();
//         return true;
//       } else {
//         _state = ResultState.failed;
//         notifyListeners();
//         return false;
//       }
//     } catch (e) {
//       _state = ResultState.failed;
//       notifyListeners();
//       return false;
//     }
//   }
// }

import 'package:flutter/material.dart';
import 'package:initial_folder/models/counter_qna_comment_model.dart';
import 'package:initial_folder/services/qna_service.dart';

enum ResultState { loading, noData, hasData, error }
enum ResultStateLike { loading, error }

class CounterQnaCommentProvider with ChangeNotifier {
  final String idQna;

  CounterQnaCommentProvider({required this.idQna}) {
    getCounterComment(idQna);
  }
  String _message = '';
  String get message => _message;
  ResultState? _state;
  ResultState? get state => _state;
  CounterCommentModel? _counterCommentModel;
  CounterCommentModel? get result => _counterCommentModel;
  set counterComment(CounterCommentModel? counterCommentModel) {
    _counterCommentModel = counterCommentModel;
    notifyListeners();
  }

  //Get Counter Comment QNA
  Future<dynamic> getCounterComment(String idQna) async {
    try {
      _state = ResultState.loading;
      notifyListeners();
      CounterCommentModel counterCommentModel =
          await QnaService().getCounterComment(idQna);
      print("Ini Cunter : ${counterCommentModel.data}");
      // ignore: unnecessary_null_comparison
      if (counterCommentModel.data != null) {
        print("ADA DATA");
        _state = ResultState.hasData;
        notifyListeners();
        return _counterCommentModel = counterCommentModel;
      } else {
        print("TIDAK ADA DATA");
        _state = ResultState.noData;
        notifyListeners();
        return _message = 'Tidak ada Data';
      }
    } catch (e) {
      _state = ResultState.error;
      print(e);
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }
}
