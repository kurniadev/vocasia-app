import 'package:flutter/foundation.dart';
import 'package:initial_folder/models/detail_rating_course_model.dart';
import 'package:initial_folder/services/course_service.dart';

enum ResultState { Loading, HasData, Error, NoData }

class DetailRatingCourseProvider with ChangeNotifier {
  final CourseService courseService;
  final String id;
  DetailRatingCourseProvider({required this.courseService, required this.id}) {
    getDetailCourse(id);
  }

  RatingCourseDetailModel? _detailRatingCourse;

  RatingCourseDetailModel? get result => _detailRatingCourse;

  ResultState? _state;

  String _message = '';

  ResultState? get state => _state;

  String get message => _message;

  set detailCourse(RatingCourseDetailModel detail) {
    _detailRatingCourse = detail;
    notifyListeners();
  }

  int _currentIndex = 0;

  int get currentIndex => _currentIndex;

  set currentIndex(int index) {
    _currentIndex = index;
    notifyListeners();
  }

  Future<dynamic> getDetailCourse(_id) async {
    try {
      _state = ResultState.Loading;
      notifyListeners();
      RatingCourseDetailModel detail =
          await courseService.getRatingDetailCourse(_id);

      _state = ResultState.HasData;
      notifyListeners();
      return _detailRatingCourse = detail;
    } catch (e) {
      _state = ResultState.Error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }
}
