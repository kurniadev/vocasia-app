import 'package:flutter/cupertino.dart';
import 'package:initial_folder/models/detail_course_model.dart';
import 'package:initial_folder/screens/splash/splash_screen_login.dart';
import 'package:initial_folder/services/course_service.dart';

enum ResultState { Loading, HasData, Error, NoData }

class DetailCourseProvider with ChangeNotifier {
  final CourseService courseService;
  final String id;
  DetailCourseProvider({required this.courseService, required this.id}) {
    Condition.loginEmail ? getDetailCourseLogin(id) : getDetailCourse(id);
  }

  DetailCourseModel? _detailCourse;

  DetailCourseModel? get result => _detailCourse;

  ResultState? _state;

  String _message = '';

  ResultState? get state => _state;

  String get message => _message;

  set detailCourse(DetailCourseModel? detail) {
    _detailCourse = detail;
    notifyListeners();
  }

  Future<dynamic> getDetailCourse(_id) async {
    try {
      _state = ResultState.Loading;
      notifyListeners();
      DetailCourseModel? detail = await courseService.getDetailCourse(_id);

      if (detail.data[0].isEmpty) {
        _state = ResultState.NoData;
        notifyListeners();
        return _message = 'Tidak ada data';
      } else {
        _state = ResultState.HasData;
        notifyListeners();
        return _detailCourse = detail;
      }
    } catch (e) {
      _state = ResultState.Error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }

  Future<dynamic> getDetailCourseLogin(_id) async {
    try {
      _state = ResultState.Loading;
      notifyListeners();
      DetailCourseModel? detail = await courseService.getDetailCourseLogin(_id);

      if (detail.data[0].isEmpty) {
        _state = ResultState.NoData;
        notifyListeners();
        return _message = 'Tidak ada data';
      } else {
        _state = ResultState.HasData;
        notifyListeners();
        return _detailCourse = detail;
      }
    } catch (e) {
      _state = ResultState.Error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }
}
