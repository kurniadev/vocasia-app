import 'package:flutter/cupertino.dart';
import 'package:initial_folder/models/payments_model.dart';
import 'package:initial_folder/services/payment_service.dart';

enum ResultState { error, succes, gagal }
enum Process { uninitialized, loading }

class PaymentsProvider with ChangeNotifier {
  final PaymentServices paymentServices;
  PaymentsProvider({required this.paymentServices});

  // PaymentModel? _paymentModel;
  bool _paymentModel = false;
  ResultState? _state;
  Process _stateProcess = Process.uninitialized;
  bool get result => _paymentModel;
  // PaymentModel? get result => _paymentModel;
  ResultState? get state => _state;
  Process get stateProcess => _stateProcess;

  Future<bool> freeCourse(int _idCourse) async {
    try {
      _stateProcess = Process.loading;
      notifyListeners();
      bool result = await paymentServices.freeCoure(_idCourse);
      print(result);
      if (result) {
        _stateProcess = Process.uninitialized;
        notifyListeners();
        return true;
      } else {
        _state = ResultState.gagal;
        _stateProcess = Process.uninitialized;
        notifyListeners();
        return false;
      }
    } catch (e) {
      print('Eroorr -> $e');
      _state = ResultState.error;
      _stateProcess = Process.uninitialized;

      notifyListeners();
      return false;
    }
  }
}
