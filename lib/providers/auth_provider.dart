import 'package:flutter/material.dart';
import 'package:initial_folder/models/user_model.dart';
import 'package:initial_folder/services/auth_service.dart';

class AuthProvider with ChangeNotifier {
  UserModel? _user;
  UserModel? get user => _user;

  set user(UserModel? user) {
    _user = user;
    notifyListeners();
  }

  Future<bool> register({
    required String name,
    required String email,
    required String password,
  }) async {
    try {
      UserModel? user = await AuthService().register(
        name: name,
        email: email,
        password: password,
      );

      _user = user;
      //print(user);
      return true;
    } catch (e) {
      print("EXception: $e");
      return false;
    }
  }

  Future<bool> login({
    required String email,
    required String password,
  }) async {
    try {
      UserModel user = await AuthService().login(
        email: email,
        password: password,
      );

      _user = user;
      return true;
    } catch (e) {
      print("EXception: $e");
      return false;
    }
  }
}
