import 'package:collection/collection.dart';
import 'package:flutter/cupertino.dart';
import 'package:initial_folder/models/lesson_course_model.dart';
import 'package:initial_folder/services/lesson_course_service.dart';

enum ResultState { loading, hasData, noData, error }
// enum ResultStateUpdate { uninitialized, loading, success, eror }

class LessonCourseProvider with ChangeNotifier {
  final LessonCourseService lessonCourseService;
  final int id;
  LessonCourseProvider({required this.lessonCourseService, required this.id}) {
    getLessonCourse(id);
  }

  bool switchbutton = false;

  LessonCourseModel? _lessonCourseModel;
  ResultState? _state;
  // ResultStateUpdate _stateUpdate = ResultStateUpdate.uninitialized;
  String _message = '';

  LessonCourseModel? get result => _lessonCourseModel;

  ResultState? get state => _state;
  // ResultStateUpdate get stateUpdate => _stateUpdate;
  String get message => _message;

  String _url = '';

  String get url => _url;

  set uri(String urlVideo) {
    _url = urlVideo;
    notifyListeners();
  }

  indexUri(String indexUri) {
    _url = indexUri;
    notifyListeners();
  }

  set lessonCourse(LessonCourseModel lesson) {
    _lessonCourseModel = lesson;
    notifyListeners();
  }
  // set newMap(NewMap lesson) {
  //   _newMap = lesson;
  //   notifyListeners();
  // }

  void autoplay() {
    switchbutton = !switchbutton;
    notifyListeners();
  }

  Future<dynamic> getLessonCourse(int _id) async {
    try {
      _state = ResultState.loading;
      notifyListeners();
      LessonCourseModel lesson = await lessonCourseService.getLessonCourse(_id);

      if (lesson.data[0].isEmpty) {
        _state = ResultState.noData;
        notifyListeners();
        return _message = 'Empty Data';
      } else {
        _state = ResultState.hasData;

        notifyListeners();
        return _lessonCourseModel = lesson;
      }
    } catch (e) {
      _state = ResultState.error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }

  Future<bool> updateLessonCourse(String _courseId, String _lessonId) async {
    try {
      // _stateUpdate = ResultStateUpdate.loading;
      // notifyListeners();
      bool update =
          await lessonCourseService.updateLessonCourse(_courseId, _lessonId);

      if (update) {
        // _stateUpdate = ResultStateUpdate.success;
        // notifyListeners();
        return true;
      }
      return false;
    } catch (e) {
      // _stateUpdate = ResultStateUpdate.eror;
      return false;
    }
  }
}
