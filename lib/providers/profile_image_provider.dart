import 'dart:io';

import 'package:flutter/material.dart';
import 'package:initial_folder/models/profile_image_post_model.dart';
import 'package:initial_folder/services/profile_image_service.dart';

class ProfileImageProvider with ChangeNotifier {
  ProfileImagePostModel? _imageModel;

  ProfileImagePostModel? get imageModel => _imageModel;

  set imageModel(ProfileImagePostModel? _imageModel) {
    _imageModel = imageModel;
    notifyListeners();
  }

  Future<bool> addProfileImage({required File pckFile}) async {
    try {
      ProfileImagePostModel? imageModel =
          await ProfileImageService().addProfileImage(pckFile: pckFile);

      _imageModel = imageModel;
      //print(user);
      return true;
    } catch (e) {
      print("EXception: $e");
      return false;
    }
  }
}
