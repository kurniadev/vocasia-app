import 'package:flutter/material.dart';
import 'package:initial_folder/models/data_diri_model.dart';
import 'package:initial_folder/services/user_info_service.dart';

enum ResultStateData { Loading, NoData, HasData, Error }

class DataDiriProvider with ChangeNotifier {
  final UserInfoService userInfoService;

  DataDiriProvider({
    required this.userInfoService,
  }) {
    getDataDiri();
  }

  DataDiriModel? _dataDiriModel;

  ResultStateData? _state;

  String _message = '';
  String _dateBirth = '';

  DataDiriModel? get result => _dataDiriModel;

  ResultStateData? get state => _state;

  String get message => _message;
  String get dateBirth => _dateBirth;

  bool _isSelected = false;
  bool get isSelected => _isSelected;

  bool _newName = false;
  bool get newName => _newName;

  bool _newBiograpy = false;
  bool get newBiograpy => _newBiograpy;

  bool _newPhone = false;
  bool get newPhone => _newPhone;

  bool _newInstagram = false;
  bool get newInstagram => _newInstagram;

  bool _newTwitter = false;
  bool get newTwitter => _newTwitter;

  bool _newFacebook = false;
  bool get newFacebook => _newFacebook;

  bool _newLinkedin = false;
  bool get newLinkedin => _newLinkedin;

  set isSelected(bool value) {
    _isSelected = !_isSelected;
    notifyListeners();
  }

  set newName(bool value) {
    _newName = value;
    notifyListeners();
  }

  set newBiograpy(bool value) {
    _newBiograpy = value;
    notifyListeners();
  }

  set newPhone(bool value) {
    _newPhone = value;
    notifyListeners();
  }

  set newInstagram(bool value) {
    _newInstagram = value;
    notifyListeners();
  }

  set newTwitter(bool value) {
    _newTwitter = value;
    notifyListeners();
  }

  set newFacebook(bool value) {
    _newFacebook = value;
    notifyListeners();
  }

  set newLinkedin(bool value) {
    _newLinkedin = value;
    notifyListeners();
  }

  set datebirth(String value) {
    _dateBirth = value;
    notifyListeners();
  }

  set dataDiri(DataDiriModel dataDiri) {
    _dataDiriModel = dataDiri;
    notifyListeners();
  }

  Future<dynamic> getDataDiri() async {
    try {
      _state = ResultStateData.Loading;
      notifyListeners();
      DataDiriModel dataDiri = await userInfoService.getDataDiri();

      _state = ResultStateData.HasData;
      notifyListeners();
      return _dataDiriModel = dataDiri;
    } catch (e) {
      _state = ResultStateData.Error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }
}
