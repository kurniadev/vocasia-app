import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:initial_folder/models/cart_model.dart';
import 'package:initial_folder/services/cart_service.dart';

enum ResultState { loading, succes, failed }

class CartProvider with ChangeNotifier {
  final CartService cartService;
  CartProvider({required this.cartService});
  CartModel? _cartModel;
  CartModel? get cartModel => _cartModel;
  ResultState? _state;
  ResultState? get state => _state;
  set cartModel(CartModel? cartModel) {
    _cartModel = cartModel;
    notifyListeners();
  }

  Future<bool> addCart(_idCourse) async {
    try {
      _state = ResultState.loading;
      notifyListeners();
      bool cart = await cartService.addCart(_idCourse);
      if (cart) {
        _state = ResultState.succes;
        notifyListeners();
      } else if (!cart) {
        _state = ResultState.failed;
        notifyListeners();
      }
      return true;
    } on SocketException {
      return false;
    } catch (e) {
      _state = ResultState.failed;
      notifyListeners();
      print("Exceptions: $e");
      return false;
    }
  }

  Future<bool> deleteCart(_idCart) async {
    try {
      bool cart = await cartService.deleteCart(_idCart);
      if (cart) {
        _state = ResultState.succes;
        notifyListeners();
      } else if (!cart) {
        _state = ResultState.failed;
        notifyListeners();
      }
      return true;
    } on SocketException {
      return false;
    } catch (e) {
      print("Exceptions: $e");
      return false;
    }
  }
}
