import 'package:flutter/widgets.dart';
import 'package:initial_folder/models/history_transaction_model.dart';
import 'package:initial_folder/services/history_transactions_service.dart';

enum ResultState { loading, noData, hasData, error }

class HistoryTranscationsProvider with ChangeNotifier {
  final HistoryTransactionService historyTransactionService;
  HistoryTranscationsProvider({required this.historyTransactionService}) {
    getHistoryTransaction();
  }

  HistoryTransactionsModel? _historyTransactionsModel;
  HistoryTransactionsModel? get result => _historyTransactionsModel;

  ResultState? _state;
  ResultState? get state => _state;

  String _message = '';
  String get message => _message;

  set histtoryTranscation(HistoryTransactionsModel historyTr) {
    _historyTransactionsModel = historyTr;
    notifyListeners();
  }

  Future<dynamic> getHistoryTransaction() async {
    try {
      _state = ResultState.loading;
      notifyListeners();
      HistoryTransactionsModel result =
          await historyTransactionService.historyTransactions();
      if (result.data[0].isEmpty) {
        _state = ResultState.noData;

        notifyListeners();
        return _message = 'Tidak Ada data';
      } else {
        _state = ResultState.hasData;
        notifyListeners();
        return _historyTransactionsModel = result;
      }
    } catch (e) {
      _state = ResultState.error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }
}
