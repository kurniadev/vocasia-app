import 'package:flutter/material.dart';
import 'package:initial_folder/models/my_course_model.dart';
import 'package:initial_folder/services/course_service.dart';

enum ResultState { Loading, NoData, HasData, Error }

class MyCourseProvider with ChangeNotifier {
  final CourseService courseService;
  MyCourseProvider({required this.courseService}) {
    getMyCourse();
  }
  MyCourseModel? _myCourseModel;

  ResultState? _state;

  String _message = '';

  MyCourseModel? get result => _myCourseModel;

  ResultState? get state => _state;

  String get message => _message;

  set myCourse(MyCourseModel myCourse) {
    _myCourseModel = myCourse;
    notifyListeners();
  }

  Future<dynamic> getMyCourse() async {
    try {
      _state = ResultState.Loading;
      notifyListeners();
      MyCourseModel myCourse = await courseService.getMyCourse();

      if (myCourse.data[0].isEmpty) {
        _state = ResultState.NoData;
        notifyListeners();
        return _message = 'Empty Data';
      } else {
        _state = ResultState.HasData;
        notifyListeners();
        return _myCourseModel = myCourse;
      }
    } catch (e) {
      _state = ResultState.Error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }
}
