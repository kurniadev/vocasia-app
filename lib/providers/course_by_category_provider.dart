import 'package:flutter/material.dart';
import 'package:initial_folder/models/course_model.dart';
import 'package:initial_folder/services/course_by_category_service.dart';

enum ResultState { Loading, NoData, HasData, Error }

class CourseByCategoryProvider with ChangeNotifier {
  final CourseByCategoryService courseByCategoryService;
  final String id;
  CourseByCategoryProvider(
      {required this.courseByCategoryService, required this.id}) {
    getCourseByCategory(id);
  }
  List<CourseModel> _courseByCategory = [];

  ResultState? _state;

  String _message = '';

  List<CourseModel> get result => _courseByCategory;

  ResultState? get state => _state;

  String get message => _message;

  set courseByCategory(List<CourseModel> courseByCategory) {
    _courseByCategory = courseByCategory;
    notifyListeners();
  }

  Future<dynamic> getCourseByCategory(_id) async {
    try {
      _state = ResultState.Loading;
      notifyListeners();
      List<CourseModel> courseByCategory =
          await courseByCategoryService.getCourseByCategory(_id);
      if (courseByCategory.isEmpty) {
        _state = ResultState.NoData;
        notifyListeners();
        return _message = 'Empty Data';
      } else {
        _state = ResultState.HasData;
        notifyListeners();
        return _courseByCategory = courseByCategory;
      }
    } catch (e) {
      _state = ResultState.Error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }
}
