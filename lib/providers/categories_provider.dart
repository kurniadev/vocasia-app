import 'package:flutter/material.dart';
import 'package:initial_folder/models/catagories_model.dart';
import 'package:initial_folder/services/categories_service.dart';

enum ResultState { Loading, NoData, HasData, Error }

class CategoriesProvider with ChangeNotifier {
  final CategoriesService categoriesService;
  CategoriesProvider({required this.categoriesService}) {
    getAllCategories();
  }
  List<CategoriesModel> _categories = [];

  ResultState? _state;

  String _message = '';

  List<CategoriesModel> get result => _categories;

  ResultState? get state => _state;

  String get message => _message;

  set categories(List<CategoriesModel> categories) {
    _categories = categories;
    notifyListeners();
  }

  Future<dynamic> getAllCategories() async {
    try {
      _state = ResultState.Loading;
      notifyListeners();
      List<CategoriesModel> categories =
          await categoriesService.getAllCategories();
      if (categories.isEmpty) {
        _state = ResultState.NoData;
        notifyListeners();
        return _message = 'Empty Data';
      } else {
        _state = ResultState.HasData;
        notifyListeners();
        return _categories = categories;
      }
    } catch (e) {
      _state = ResultState.Error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }
}
