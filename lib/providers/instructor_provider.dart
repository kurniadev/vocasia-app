import 'package:flutter/cupertino.dart';
import 'package:initial_folder/models/instructor_model.dart';
import 'package:initial_folder/services/instructor_service.dart';

enum ResultState { Loading, HasData, NoData, Error }

class InstructorProvider with ChangeNotifier {
  final InstructorService instructorService;
  final int id;
  InstructorProvider({required this.instructorService, required this.id}) {
    getProfileInstructor(id);
  }
  InstructorModel? _instructorModel;
  ResultState? _state;
  String _message = '';
  InstructorModel? get result => _instructorModel;
  ResultState? get state => _state;
  String get message => _message;
  set instruktur(InstructorModel instruktur) {
    _instructorModel = instruktur;
    notifyListeners();
  }

  Future<dynamic> getProfileInstructor(_id) async {
    try {
      _state = ResultState.Loading;
      notifyListeners();
      InstructorModel instruktur =
          await instructorService.getInstructorProfile(_id);
      if (instruktur.data.isEmpty) {
        _state = ResultState.NoData;
        notifyListeners();
        return _message = 'Tidak Ada Data';
      } else {
        _state = ResultState.HasData;
        notifyListeners();
        return _instructorModel = instruktur;
      }
    } catch (e) {
      _state = ResultState.Error;
      notifyListeners();
      return _message = 'Error --> $e';
    }
  }
}
