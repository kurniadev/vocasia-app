import 'package:flutter/material.dart';
import 'package:initial_folder/services/course_service.dart';

enum ResultState { uninitilized, loading, failed, success }

class PostingReviewProvider with ChangeNotifier {
  final CourseService courseService;
  PostingReviewProvider({required this.courseService});

  ResultState _state = ResultState.uninitilized;

  ResultState get state => _state;

  Future<bool> postingReview(
      String _review, int _courseId, int _valueRating) async {
    try {
      _state = ResultState.loading;
      notifyListeners();
      bool response = await courseService.postingReviewCourse(
          _review, _courseId, _valueRating);
      if (response) {
        _state = ResultState.success;
        notifyListeners();
        return true;
      } else {
        _state = ResultState.failed;
        notifyListeners();
        return false;
      }
    } catch (e) {
      _state = ResultState.failed;
      notifyListeners();
      return false;
    }
  }
}
