import 'package:flutter/material.dart';
import 'package:initial_folder/models/comment_qna_model.dart';
import 'package:initial_folder/models/qna_model.dart';
import 'package:initial_folder/providers/posting_qna_reply_provider.dart';
import 'package:initial_folder/widgets/edit_reply_qna_user.dart';
import 'package:provider/provider.dart';

import '../size_config.dart';
import '../theme.dart';

class ReplyQnaUser extends StatefulWidget {
  const ReplyQnaUser(
      {Key? key,
      required this.qnaDataModel,
      required this.divider,
      required this.comment})
      : super(key: key);

  final Widget? divider;
  final Comment comment;
  final QnaDataModel qnaDataModel;
  @override
  State<ReplyQnaUser> createState() => _ReplyQnaUserState();
}

class _ReplyQnaUserState extends State<ReplyQnaUser> {
  @override
  Widget build(BuildContext context) {
    PostingQnaReplyProvider deleteReplyQnaProvider =
        Provider.of<PostingQnaReplyProvider>(context);

    deleteReplyQna() async {
      print(widget.comment.id_rep);
      if (await deleteReplyQnaProvider
          .deleteReplyQna(int.parse(widget.comment.id_rep.toString()))) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            duration: Duration(seconds: 2),
            backgroundColor: primaryColor,
            content: Text(
              'Pertanyaan berhasil dihapus',
              style: primaryTextStyle.copyWith(
                color: backgroundColor,
              ),
            ),
            behavior: SnackBarBehavior.floating,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
          ),
        );
        return Navigator.pop(context);
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            duration: Duration(seconds: 2),
            backgroundColor: primaryColor,
            content: Text(
              'Terjadi kesalahan',
              style: primaryTextStyle.copyWith(
                color: backgroundColor,
              ),
            ),
            behavior: SnackBarBehavior.floating,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
          ),
        );
      }
    }

    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: getProportionateScreenWidth(16),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  CircleAvatar(
                    backgroundColor: primaryColor,
                    backgroundImage: widget.comment.foto_profile == null
                        ? AssetImage("assets/images/Profile Image.png")
                        : NetworkImage(widget.comment.foto_profile ?? '')
                            as ImageProvider,
                  ),
                  SizedBox(
                    width: getProportionateScreenWidth(8),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            widget.comment.username ?? '',
                            style: primaryTextStyle.copyWith(
                                fontSize: getProportionateScreenWidth(12),
                                color: tenthColor),
                          ),
                        ],
                      ),
                      Text(
                        widget.comment.create_at ?? '',
                        style: primaryTextStyle.copyWith(
                            fontSize: getProportionateScreenWidth(12),
                            color: secondaryColor),
                      ),
                    ],
                  ),
                  Expanded(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        PopupMenuButton(
                          child: Padding(
                            padding: EdgeInsets.only(right: 10),
                            child: Icon(
                              Icons.more_vert,
                              color: Colors.white,
                            ),
                          ),
                          itemBuilder: (context) => [
                            PopupMenuItem(
                              child: Container(
                                child: Text('Edit'),
                              ),
                              value: 'edit',
                            ),
                            PopupMenuItem(
                              child: Text('Hapus'),
                              value: 'hapus',
                            ),
                          ],
                          onSelected: (value) {
                            switch (value) {
                              case 'edit':
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => EditReplyQna(
                                      id_qna: widget.qnaDataModel.id_qna,
                                      text_rep: widget.comment.text_rep,
                                      id_rep: widget.comment.id_rep,
                                    ),
                                  ),
                                );
                                break;
                              case 'hapus':
                                print(widget.comment.id_rep);
                                deleteReplyQna();
                                break;
                            }
                          },
                        ),
                      ],
                    ),
                  )
                ],
              ),
              SizedBox(height: getProportionateScreenHeight(3)),
              Text(
                widget.comment.text_rep ?? '',
                style: secondaryTextStyle.copyWith(
                    color: Color(0xffFFFFFF),
                    letterSpacing: 1,
                    fontSize: SizeConfig.blockHorizontal! * 3.4),
              ),
              SizedBox(height: getProportionateScreenHeight(16)),
            ],
          ),
          SizedBox(
            height: getProportionateScreenWidth(13),
          ),
          SizedBox(
            child: widget.divider,
          )
        ],
      ),
    );
  }
}
