import 'package:flutter/material.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';

class NotifikasiList extends StatelessWidget {
  const NotifikasiList({Key? key, this.berhasil = true, this.baru = false})
      : super(key: key);
  final bool berhasil;
  final bool baru;
  @override
  Widget build(BuildContext context) {
    return Container(
      color: (baru == false) ? Color(0xFF181818) : Color(0xFF212121),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(
              left: getProportionateScreenWidth(16),
              right: getProportionateScreenWidth(16),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 12),
                (berhasil == true)
                    ? Row(
                        children: [
                          Container(
                            alignment: Alignment.center,
                            width: getProportionateScreenWidth(120),
                            height: getProportionateScreenWidth(24),
                            child: Text(
                              'Berhasil Dibayar',
                              style: primaryTextStyle.copyWith(
                                  letterSpacing: 0.5,
                                  color: Colors.green[900],
                                  fontSize: SizeConfig.blockHorizontal! * 2.6,
                                  fontWeight: semiBold),
                            ),
                            decoration: BoxDecoration(
                              color: Colors.green[300],
                              borderRadius: BorderRadius.circular(5),
                              //border: Border.all(color: kPrimaryColor),
                            ),
                          ),
                          SizedBox(width: getProportionateScreenWidth(8)),
                          Icon(Icons.lens_rounded,
                              color: Color(0xffc4c4c4),
                              size: getProportionateScreenWidth(7)),
                          SizedBox(width: getProportionateScreenWidth(8)),
                          Text(
                            "09:53",
                            style: primaryTextStyle.copyWith(
                              color: secondaryColor,
                              fontWeight: reguler,
                              fontSize: getProportionateScreenWidth(11),
                              letterSpacing: 0.5,
                            ),
                          ),
                          SizedBox(width: getProportionateScreenWidth(8)),
                          (baru == true)
                              ? Icon(Icons.lens_rounded,
                                  color: Color(0xffCD2228),
                                  size: getProportionateScreenWidth(12))
                              : SizedBox(height: 0),
                        ],
                      )
                    : Row(
                        children: [
                          Container(
                            alignment: Alignment.center,
                            width: getProportionateScreenWidth(165),
                            height: getProportionateScreenWidth(24),
                            child: Text(
                              'Menunggu Pembayaran',
                              style: primaryTextStyle.copyWith(
                                  letterSpacing: 0.5,
                                  color: Colors.amber[900],
                                  fontSize: SizeConfig.blockHorizontal! * 2.6,
                                  fontWeight: semiBold),
                            ),
                            decoration: BoxDecoration(
                              color: Colors.amber[200],
                              borderRadius: BorderRadius.circular(5),
                              //border: Border.all(color: kPrimaryColor),
                            ),
                          ),
                          SizedBox(width: getProportionateScreenWidth(8)),
                          Icon(Icons.lens_rounded,
                              color: Color(0xffc4c4c4),
                              size: getProportionateScreenWidth(7)),
                          SizedBox(width: getProportionateScreenWidth(8)),
                          Text(
                            "09:53",
                            style: primaryTextStyle.copyWith(
                              color: secondaryColor,
                              fontWeight: reguler,
                              fontSize: getProportionateScreenWidth(11),
                              letterSpacing: 0.5,
                            ),
                          ),
                          SizedBox(width: getProportionateScreenWidth(8)),
                          (baru == true)
                              ? Icon(Icons.lens_rounded,
                                  color: Color(0xffCD2228),
                                  size: getProportionateScreenWidth(12))
                              : SizedBox(height: 0),
                        ],
                      ),
                SizedBox(height: 9),
                Text(
                  "428 Menit Menjadi Pengusaha Sukses",
                  style: primaryTextStyle.copyWith(
                    fontWeight: reguler,
                    fontSize: getProportionateScreenWidth(13),
                    letterSpacing: 0.5,
                  ),
                  maxLines: 2,
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(height: 9),
                (berhasil == true)
                    ? Text(
                        "Pembayaran berhasil dilakukan, kamu dapat mengikuti kursus sekarang!",
                        style: primaryTextStyle.copyWith(
                          color: secondaryColor,
                          fontWeight: reguler,
                          fontSize: getProportionateScreenWidth(11),
                          letterSpacing: 0.5,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      )
                    : Text(
                        "Selesaikan pembayaran sebelum Kamis, 19 Oktober 2021 untuk mulai kursus",
                        style: primaryTextStyle.copyWith(
                          color: secondaryColor,
                          fontWeight: reguler,
                          fontSize: getProportionateScreenWidth(11),
                          letterSpacing: 0.5,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                SizedBox(height: 13),
              ],
            ),
          ),
          Divider(
            height: 1,
            color: fourthColor,
          ),
        ],
      ),
    );
  }
}
