import 'package:flutter/material.dart';
import 'package:initial_folder/helper/user_info.dart';
import 'package:initial_folder/providers/qna_provider.dart';
import 'package:initial_folder/widgets/qna_user.dart';
import 'package:provider/provider.dart';

import '../theme.dart';

class QnaUserPage extends StatefulWidget {
  const QnaUserPage({Key? key, required this.idCourse}) : super(key: key);
  final idCourse;

  @override
  State<QnaUserPage> createState() => _QnaUserPageState();
}

class _QnaUserPageState extends State<QnaUserPage> {
  int? userId = 0;

  void getUserId() async {
    userId = await UsersInfo().getIdUser();
  }

  @override
  void initState() {
    // TODO: implement initState
    getUserId();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => QnaProvider(idCourse: widget.idCourse),
      child: Consumer<QnaProvider>(builder: (context, state, _) {
        if (state.state == ResultState.loading) {
          return Center(
            child: CircularProgressIndicator(
              color: primaryColor,
              strokeWidth: 2,
            ),
          );
        } else if (state.state == ResultState.noData) {
          return Center(
            child: Text(
              'belum ada pertanyaan',
              style: thirdTextStyle,
            ),
          );
        } else if (state.state == ResultState.hasData) {
          return ListView.builder(
              itemCount: state.result!.data[0].length,
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              itemBuilder: (context, index) {
                var qnauser = state.result!.data[0][index];
                return QnaUser(
                  divider: Divider(),
                  qnaDataModel: qnauser,
                  id: widget.idCourse,
                  index: index,
                  userId: userId!,
                );
              });
        } else if (state.state == ResultState.error) {
          return Center(
              child: Column(
            children: [
              Text(
                'Terjadi Kesalahan Coba Lagi',
                style: thirdTextStyle,
              ),
            ],
          ));
        }
        return Center(
          child: Text(
            'Terjadi Kesalahan',
            style: thirdTextStyle,
          ),
        );
      }),
    );
  }
}

// class QnaUserPage extends StatelessWidget {
//   const QnaUserPage({
//     Key? key,
//     required this.idCourse,
//   }) : super(key: key);
//   final idCourse;

//   @override
//   Widget build(BuildContext context) {
    
//   }
// }
