import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';

class CustomTextField extends StatelessWidget {
  CustomTextField({
    Key? key,
    this.length = 999,
    this.minLines = 1,
    this.maxLines = 1,
    this.height = 13,
    this.noTitle = false,
    this.prefix,
    this.suffix,
    this.color = secondaryColor,
    this.borderColor = Colors.white,
    this.keyboardType = TextInputType.text,
    this.title = '',
    this.obscuretext = false,
    this.auto = false,
    this.validate,
    this.enable = true,
    required this.pad,
    required this.controler,
    required this.hinttext,
  }) : super(key: key);
  final int length;
  final Color color;
  final int minLines;
  final int maxLines;
  final bool noTitle;
  final Color borderColor;
  final TextInputType keyboardType;
  final bool auto;
  final String title;
  final bool obscuretext;
  final TextEditingController controler;
  final String hinttext;
  final double pad;
  final double height;
  final FormFieldValidator<String>? validate;
  final Widget? prefix;
  final Widget? suffix;
  final bool enable;
  // final String val;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: pad),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          noTitle == false
              ? Text(
                  title,
                  style: secondaryTextStyle.copyWith(
                      fontWeight: semiBold,
                      fontSize: getProportionateScreenWidth(12),
                      color: secondaryColor,
                      letterSpacing: 0.5),
                )
              : SizedBox(height: 0),
          noTitle == false
              ? SizedBox(
                  height: getProportionateScreenHeight(4),
                )
              : SizedBox(height: 0),
          Theme(
            data: ThemeData.dark().copyWith(errorColor: sevenColor),
            child: TextFormField(
              // initialValue: val,
              enabled: enable,
              minLines: minLines,
              maxLines: maxLines,
              inputFormatters: [
                LengthLimitingTextInputFormatter(length),
              ],
              keyboardType: keyboardType,
              autofocus: auto,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: validate,
              obscureText: obscuretext,
              controller: controler,
              style: primaryTextStyle.copyWith(
                fontSize: getProportionateScreenWidth(14),
                letterSpacing: 0.5,
              ),
              cursorColor: secondaryColor,
              decoration: InputDecoration(
                errorStyle: primaryTextStyle,
                errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: sevenColor),
                    borderRadius: BorderRadius.circular(10)),
                suffixIcon: suffix,
                prefixIcon: prefix,
                contentPadding: EdgeInsets.only(
                    left: getProportionateScreenWidth(15),
                    top: getProportionateScreenHeight(height),
                    bottom: getProportionateScreenHeight(height)),
                hintText: hinttext,
                hintStyle: primaryTextStyle.copyWith(
                    fontSize: getProportionateScreenWidth(12),
                    color: color,
                    letterSpacing: 0.5),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(
                    10,
                  ),
                  borderSide: BorderSide(
                    color: borderColor,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(
                    10,
                  ),
                  borderSide: BorderSide(
                    color: borderColor,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            height: getProportionateScreenHeight(16),
          ),
        ],
      ),
    );
  }
}
