import 'package:flutter/material.dart';
import '../../theme.dart';
import '../../size_config.dart';

class DefaultIconButton extends StatelessWidget {
  const DefaultIconButton(
      {Key? key,
      required this.icon,
      required this.iconWidth,
      required this.iconHeight,
      this.text = '',
      this.press,
      this.color})
      : super(key: key);
  final String icon;
  final String text;
  final VoidCallback? press;
  final double iconHeight;
  final double iconWidth;
  final Color? color;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: getProportionateScreenWidth(40),
      child: TextButton.icon(
        style: TextButton.styleFrom(
          shape: RoundedRectangleBorder(
              borderRadius:
                  BorderRadius.circular(getProportionateScreenWidth(10))),
          primary: Colors.white,
          backgroundColor: tenthColor,
        ),
        onPressed: press,
        icon: Padding(
          padding: EdgeInsets.only(right: getProportionateScreenWidth(15.0)),
          child: Image.asset(
            icon,
            height: getProportionateScreenWidth(iconHeight),
            width: getProportionateScreenWidth(iconWidth),
            color: color,
          ),
        ),
        label: Text(
          text,
          style: thirdTextStyle.copyWith(
              fontSize: getProportionateScreenWidth(14),
              fontWeight: reguler,
              color: ninthColor,
              letterSpacing: 0.1),
        ),
      ),
    );
  }
}
