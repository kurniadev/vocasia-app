import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';

class CustomProfileTextField extends StatefulWidget {
  CustomProfileTextField({
    Key? key,
    this.length = 999,
    this.minLines = 1,
    this.maxLines = 1,
    this.height = 13,
    this.noTitle = false,
    this.prefix,
    this.suffix,
    this.color = secondaryColor,
    this.borderColor = Colors.white,
    this.keyboardType = TextInputType.text,
    this.title = '',
    this.obscuretext = false,
    this.auto = false,
    this.validate,
    this.enable = true,
    this.onChanged,
    required this.pad,
    required this.text,
    required this.hinttext,
  }) : super(key: key);
  final int length;
  final Color color;
  final int minLines;
  final int maxLines;
  final bool noTitle;
  final Color borderColor;
  final TextInputType keyboardType;
  final bool auto;
  final String title;
  final bool obscuretext;
  final String text;
  final String hinttext;
  final double pad;
  final double height;
  final FormFieldValidator<String>? validate;
  final Widget? prefix;
  final Widget? suffix;
  final bool enable;
  final Function(String)? onChanged;

  @override
  State<CustomProfileTextField> createState() => _CustomProfileTextFieldState();
}

class _CustomProfileTextFieldState extends State<CustomProfileTextField> {
  late final TextEditingController controller;

  @override
  void initState() {
    super.initState();

    controller = TextEditingController(text: widget.text);
  }

  @override
  void dispose() {
    controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: widget.pad),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          widget.noTitle == false
              ? Text(
                  widget.title,
                  style: secondaryTextStyle.copyWith(
                      fontWeight: semiBold,
                      fontSize: getProportionateScreenWidth(12),
                      color: secondaryColor,
                      letterSpacing: 0.5),
                )
              : SizedBox(height: 0),
          widget.noTitle == false
              ? SizedBox(
                  height: getProportionateScreenHeight(4),
                )
              : SizedBox(height: 0),
          Theme(
            data: ThemeData.dark().copyWith(errorColor: sevenColor),
            child: TextFormField(
              // initialValue: val,
              enabled: widget.enable,
              minLines: widget.minLines,
              maxLines: widget.maxLines,
              inputFormatters: [
                LengthLimitingTextInputFormatter(widget.length),
              ],
              keyboardType: widget.keyboardType,
              autofocus: widget.auto,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: widget.validate,
              obscureText: widget.obscuretext,
              controller: controller,
              onChanged: widget.onChanged,
              style: primaryTextStyle.copyWith(
                fontSize: getProportionateScreenWidth(14),
                letterSpacing: 0.5,
              ),
              cursorColor: secondaryColor,
              decoration: InputDecoration(
                errorStyle: primaryTextStyle,
                errorBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: sevenColor),
                    borderRadius: BorderRadius.circular(10)),
                suffixIcon: widget.suffix,
                prefixIcon: widget.prefix,
                contentPadding: EdgeInsets.only(
                    left: getProportionateScreenWidth(15),
                    top: getProportionateScreenHeight(widget.height),
                    bottom: getProportionateScreenHeight(widget.height)),
                hintText: widget.hinttext,
                hintStyle: primaryTextStyle.copyWith(
                    fontSize: getProportionateScreenWidth(12),
                    color: widget.color,
                    letterSpacing: 0.5),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(
                    10,
                  ),
                  borderSide: BorderSide(
                    color: widget.borderColor,
                  ),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(
                    10,
                  ),
                  borderSide: BorderSide(
                    color: widget.borderColor,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(
            height: getProportionateScreenHeight(16),
          ),
        ],
      ),
    );
  }
}
