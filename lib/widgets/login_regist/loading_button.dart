import 'package:flutter/material.dart';

import '../../theme.dart';
import '../../size_config.dart';

class LoadingButton extends StatelessWidget {
  const LoadingButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: getProportionateScreenWidth(44),
      child: TextButton(
        style: TextButton.styleFrom(
          shape: RoundedRectangleBorder(
              borderRadius:
                  BorderRadius.circular(getProportionateScreenWidth(10))),
          primary: Colors.white,
          backgroundColor: primaryColor,
        ),
        onPressed: () {},
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              width: 17,
              height: 17,
              child: CircularProgressIndicator(
                strokeWidth: 2,
                valueColor: AlwaysStoppedAnimation(
                  Color(0xff050505),
                ),
              ),
            ),
            SizedBox(
              width: getProportionateScreenWidth(6),
            ),
            Text(
              'Loading',
              style: thirdTextStyle.copyWith(
                  fontSize: getProportionateScreenWidth(14),
                  fontWeight: semiBold,
                  color: Color(0xff050505),
                  letterSpacing: 0.3),
            ),
          ],
        ),
      ),
    );
  }
}
