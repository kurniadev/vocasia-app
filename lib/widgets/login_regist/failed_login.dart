import 'package:flutter/material.dart';
import 'package:initial_folder/size_config.dart';

class FailedLogin extends StatelessWidget {
  const FailedLogin(
      {Key? key, this.text = '', required this.style, this.jarak = 8})
      : super(key: key);
  final String text;
  final TextStyle style;
  final double jarak;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset(
          'assets/images/VOCASIA logo.png',
          width: getProportionateScreenWidth(150),
          height: getProportionateScreenHeight(50),
        ),
        SizedBox(height: getProportionateScreenHeight(jarak)),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(16)),
          child: Text(text,
              maxLines: 2, textAlign: TextAlign.center, style: style),
        ),
      ],
    );
  }
}
