import 'package:flutter/material.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';

class Header extends StatelessWidget {
  const Header(
      {Key? key,
      this.text = '',
      this.text2 = '',
      required this.style,
      this.jarak = 8,
      this.title = true})
      : super(key: key);
  final String text;
  final String text2;
  final TextStyle style;
  final double jarak;
  final bool title;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        title == true
            ? Image.asset(
                'assets/images/VOCASIA logo.png',
                width: getProportionateScreenWidth(150),
                height: getProportionateScreenHeight(50),
              )
            : Text(text2,
                textAlign: TextAlign.center,
                style: secondaryTextStyle.copyWith(
                    color: tenthColor,
                    fontWeight: reguler,
                    fontSize: getProportionateScreenWidth(20),
                    letterSpacing: 0.23)),
        SizedBox(height: getProportionateScreenHeight(jarak)),
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(16)),
          child: Text(text,
              maxLines: 2, textAlign: TextAlign.center, style: style),
        ),
      ],
    );
  }
}
