import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

IconData fontAwesomeIconsFromString(String name) {
  switch (name) {
    case "FontAwesomeIcons.moneyBillAlt":
      return FontAwesomeIcons.moneyBillAlt;

    case "FontAwesomeIcons.desktop":
      return FontAwesomeIcons.desktop;

    case "FontAwesomeIcons.music":
      return FontAwesomeIcons.music;

    case "FontAwesomeIcons.pencilAlt":
      return FontAwesomeIcons.pencilAlt;

    case "FontAwesomeIcons.pencilRuler":
      return FontAwesomeIcons.pencilRuler;

    case "FontAwesomeIcons.shoppingBag":
      return FontAwesomeIcons.shoppingBag;

    case "FontAwesomeIcons.chartLine":
      return FontAwesomeIcons.chartLine;

    case "FontAwesomeIcons.clipboardList":
      return FontAwesomeIcons.clipboardList;

    case "FontAwesomeIcons.chess":
      return FontAwesomeIcons.chess;

    case "FontAwesomeIcons.cameraRetro":
      return FontAwesomeIcons.cameraRetro;

    case "FontAwesomeIcons.graduationCap":
      return FontAwesomeIcons.graduationCap;

    case "FontAwesomeIcons.accessibleIcon":
      return FontAwesomeIcons.accessibleIcon;

    case "FontawesomeIcons.accusoft":
      return FontAwesomeIcons.accusoft;

    case "FontAwesomeIcons.acquisitionIncorporated":
      return FontAwesomeIcons.acquisitionsIncorporated;

    case "FontAwesomeIcons.ad":
      return FontAwesomeIcons.ad;

    case "FontAwesomeIcons.addressBook":
      return FontAwesomeIcons.addressBook;

    case "FontAwesomeIcons.addressCard":
      return FontAwesomeIcons.addressCard;

    case "FontAwesomeIcons.adjust":
      return FontAwesomeIcons.adjust;

    case "FontAwesomeIcons.adn":
      return FontAwesomeIcons.adn;

    case "FontAwesomeIcons.adversal":
      return FontAwesomeIcons.adversal;

    case "FontAwesomeIcons.affiliatetheme":
      return FontAwesomeIcons.affiliatetheme;

    case "FontAwesomeIcons.airFreshener":
      return FontAwesomeIcons.airFreshener;

    case "FontAwesomeIcons.airbnb":
      return FontAwesomeIcons.airbnb;

    case "FontAwesomeIcons.algolia":
      return FontAwesomeIcons.algolia;

    case "FontAwesomeIcons.alignCenter":
      return FontAwesomeIcons.alignCenter;

    case "FontAwesomeIcons.alignJustify":
      return FontAwesomeIcons.alignJustify;

    case "FontAwesomeIcons.alignLeft":
      return FontAwesomeIcons.alignLeft;

    case "FontAwesomeIcons.alignRight":
      return FontAwesomeIcons.alignRight;

    case "FontAwesomeIcons.alipay":
      return FontAwesomeIcons.alipay;

    case "FontAwesomeIcons.allergies":
      return FontAwesomeIcons.allergies;

    case "FontAwesomeIcons.amazon":
      return FontAwesomeIcons.amazon;

    case "FontAwesomeIcons.amazonPay":
      return FontAwesomeIcons.amazonPay;

    case "FontAwesomeIcons.ambulance":
      return FontAwesomeIcons.ambulance;

    case "FontAwesomeIcons.americanSignLanguageInterpreting":
      return FontAwesomeIcons.americanSignLanguageInterpreting;

    case "FontAwesomeIcons.amilia":
      return FontAwesomeIcons.amilia;

    case "FontAwesomeIcons.anchor":
      return FontAwesomeIcons.anchor;

    case "FontAwesomeIcons.android":
      return FontAwesomeIcons.android;

    case "FontAwesomeIcons.angellist":
      return FontAwesomeIcons.angellist;

    case "FontAwesomeIcons.angleDoubleDown":
      return FontAwesomeIcons.angleDoubleDown;

    case "FontAwesomeIcons.angleDoubleLeft":
      return FontAwesomeIcons.angleDoubleLeft;

    case "FontAwesomeIcons.angleDoubleRight":
      return FontAwesomeIcons.angleDoubleRight;

    case "FontAwesomeIcons.angleDoubleUp":
      return FontAwesomeIcons.angleDoubleUp;

    case "FontAwesomeIcons.angleDown":
      return FontAwesomeIcons.angleDown;

    case "FontAwesomeIcons.angleLeft":
      return FontAwesomeIcons.angleLeft;

    case "FontAwesomeIcons.angleRight":
      return FontAwesomeIcons.angleRight;

    case "FontAwesomeIcons.angleUp":
      return FontAwesomeIcons.angleUp;

    case "FontAwesomeIcons.angry":
      return FontAwesomeIcons.angry;

    case "FontAwesomeIcons.angrycreative":
      return FontAwesomeIcons.angrycreative;

    case "FontAwesomeIcons.angular":
      return FontAwesomeIcons.angular;

    case "FontAwesomeIcons.ankh":
      return FontAwesomeIcons.ankh;

    case "FontAwesomeIcons.appStore":
      return FontAwesomeIcons.appStore;

    case "FontAwesomeIcons.appStoreIos":
      return FontAwesomeIcons.appStoreIos;

    case "FontAwesomeIcons.apper":
      return FontAwesomeIcons.apper;

    case "FontAwesomeIcons.apple":
      return FontAwesomeIcons.apple;

    case "FontAwesomeIcons.appleAlt":
      return FontAwesomeIcons.appleAlt;

    case "FontAwesomeIcons.applePay":
      return FontAwesomeIcons.applePay;

    case "FontAwesomeIcons.archive":
      return FontAwesomeIcons.archive;

    case "FontAwesomeIcons.archway":
      return FontAwesomeIcons.archway;

    case "FontAwesomeIcons.arrowAltCircleDown":
      return FontAwesomeIcons.arrowAltCircleDown;

    case "FontAwesomeIcons.arrowAltCircleLeft":
      return FontAwesomeIcons.arrowAltCircleLeft;

    case "FontAwesomeIcons.arrowAltCircleRight":
      return FontAwesomeIcons.arrowAltCircleRight;

    case "FontAwesomeIcons.arrowAltCircleUp":
      return FontAwesomeIcons.arrowAltCircleUp;

    case "FontAwesomeIcons.arrowCircleDown":
      return FontAwesomeIcons.arrowCircleDown;

    case "FontAwesomeIcons.arrowCircleLeft":
      return FontAwesomeIcons.arrowCircleLeft;

    case "FontAwesomeIcons.arrowCircleRight":
      return FontAwesomeIcons.arrowCircleRight;

    case "FontAwesomeIcons.arrowCircleUp":
      return FontAwesomeIcons.arrowCircleUp;

    case "FontAwesomeIcons.arrowDown":
      return FontAwesomeIcons.arrowDown;

    case "FontAwesomeIcons.arrowLeft":
      return FontAwesomeIcons.arrowLeft;

    case "FontAwesomeIcons.arrowRight":
      return FontAwesomeIcons.arrowRight;

    case "FontAwesomeIcons.arrowUp":
      return FontAwesomeIcons.arrowUp;

    case "FontAwesomeIcons.arrowsAlt":
      return FontAwesomeIcons.arrowsAlt;

    case "FontAwesomeIcons.arrowsAltH":
      return FontAwesomeIcons.arrowsAltH;

    case "FontAwesomeIcons.arrowsAltV":
      return FontAwesomeIcons.arrowsAltV;

    case "FontAwesomeIcons.artstation":
      return FontAwesomeIcons.artstation;

    case "FontAwesomeIcons.assistiveListeningSystems":
      return FontAwesomeIcons.assistiveListeningSystems;

    case "FontAwesomeIcons.asterisk":
      return FontAwesomeIcons.asterisk;

    case "FontAwesomeIcons.asymmetrik":
      return FontAwesomeIcons.asymmetrik;

    case "FontAwesomeIcons.at":
      return FontAwesomeIcons.at;

    case "FontAwesomeIcons.atlas":
      return FontAwesomeIcons.atlas;

    case "FontAwesomeIcons.atlassian":
      return FontAwesomeIcons.atlassian;

    case "FontAwesomeIcons.atom":
      return FontAwesomeIcons.atom;

    case "FontAwesomeIcons.audible":
      return FontAwesomeIcons.audible;

    case "FontAwesomeIcons.audioDescription":
      return FontAwesomeIcons.audioDescription;

    case "FontAwesomeIcons.autoprefixer":
      return FontAwesomeIcons.autoprefixer;

    case "FontAwesomeIcons.avianex":
      return FontAwesomeIcons.avianex;

    case "FontAwesomeIcons.aviato":
      return FontAwesomeIcons.aviato;

    case "FontAwesomeIcons.award":
      return FontAwesomeIcons.award;

    case "FontAwesomeIcons.aws":
      return FontAwesomeIcons.aws;

    case "FontAwesomeIcons.baby":
      return FontAwesomeIcons.baby;

    case "FontAwesomeIcons.babyCarriage":
      return FontAwesomeIcons.babyCarriage;

    case "FontAwesomeIcons.backspace":
      return FontAwesomeIcons.backspace;

    case "FontAwesomeIcons.backward":
      return FontAwesomeIcons.backward;

    case "FontAwesomeIcons.bacon":
      return FontAwesomeIcons.bacon;

    case "FontAwesomeIcons.bahai":
      return FontAwesomeIcons.bahai;

    case "FontAwesomeIcons.balanceScale":
      return FontAwesomeIcons.balanceScale;

    case "FontAwesomeIcons.balanceScaleLeft":
      return FontAwesomeIcons.balanceScaleLeft;

    case "FontAwesomeIcons.balanceScaleRight":
      return FontAwesomeIcons.balanceScaleRight;

    case "FontAwesomeIcons.ban":
      return FontAwesomeIcons.ban;

    case "FontAwesomeIcons.bandAid":
      return FontAwesomeIcons.bandAid;

    case "FontAwesomeIcons.bandcamp":
      return FontAwesomeIcons.bandcamp;

    case "FontAwesomeIcons.barcode":
      return FontAwesomeIcons.barcode;

    case "FontAwesomeIcons.bars":
      return FontAwesomeIcons.bars;

    case "FontAwesomeIcons.baseballBall":
      return FontAwesomeIcons.baseballBall;

    case "FontAwesomeIcons.basketballBall":
      return FontAwesomeIcons.basketballBall;

    case "FontAwesomeIcons.bath":
      return FontAwesomeIcons.bath;

    case "FontAwesomeIcons.batteryEmpty":
      return FontAwesomeIcons.batteryEmpty;

    case "FontAwesomeIcons.batteryFull":
      return FontAwesomeIcons.batteryFull;

    case "FontAwesomeIcons.batteryHalf":
      return FontAwesomeIcons.batteryHalf;

    case "FontAwesomeIcons.batteryQuarter":
      return FontAwesomeIcons.batteryQuarter;

    case "FontAwesomeIcons.batteryThreeQuarters":
      return FontAwesomeIcons.batteryThreeQuarters;

    case "FontAwesomeIcons.battleNet":
      return FontAwesomeIcons.battleNet;

    case "FontAwesomeIcons.bed":
      return FontAwesomeIcons.bed;

    case 'FontAwesomeIcons.beer':
      return FontAwesomeIcons.beer;

    case "FontAwesomeIcons.behance":
      return FontAwesomeIcons.behance;

    case "FontAwesomeIcons.behanceSquare":
      return FontAwesomeIcons.behanceSquare;

    case "FontAwesomeIcons.bell":
      return FontAwesomeIcons.bell;

    case "FontAwesomeIcons.bellSlash":
      return FontAwesomeIcons.bellSlash;

    case "FontAwesomeIcons.bezierCurve":
      return FontAwesomeIcons.bezierCurve;

    case "FontAwesomeIcons.bible":
      return FontAwesomeIcons.bible;

    case "FontAwesomeIcons.bicycle":
      return FontAwesomeIcons.bicycle;

    case "FontAwesomeIcons.biking":
      return FontAwesomeIcons.biking;

    case "FontAwesomeIcons.bimobject":
      return FontAwesomeIcons.bimobject;

    case "FontAwesomeIcons.binoculars":
      return FontAwesomeIcons.binoculars;

    case "FontAwesomeIcons.biohazard":
      return FontAwesomeIcons.biohazard;

    case "FontAwesomeIcons.birthdayCake":
      return FontAwesomeIcons.birthdayCake;

    case "FontAwesomeIcons.bitbucket":
      return FontAwesomeIcons.bitbucket;

    case "FontAwesomeIcons.bitcoin":
      return FontAwesomeIcons.bitcoin;

    case "FontAwesomeIcons.bity":
      return FontAwesomeIcons.bity;

    case "FontAwesomeIcons.blackTie":
      return FontAwesomeIcons.blackTie;

    case "FontAwesomeIcons.blackberry":
      return FontAwesomeIcons.blackberry;

    case "FontAwesomeIcons.blender":
      return FontAwesomeIcons.blender;

    case "FontAwesomeIcons.blenderPhone":
      return FontAwesomeIcons.blenderPhone;

    case "FontAwesomeIcons.blind":
      return FontAwesomeIcons.blind;

    case "FontAwesomeIcons.blog":
      return FontAwesomeIcons.blog;

    case "FontAwesomeIcons.blogger":
      return FontAwesomeIcons.blogger;

    case "FontAwesomeIcons.bloggerB":
      return FontAwesomeIcons.bloggerB;

    case "FontAwesomeIcons.bluetooth":
      return FontAwesomeIcons.bluetooth;

    case "FontAwesomeIcons.bluetoothB":
      return FontAwesomeIcons.bluetoothB;

    case "FontAwesomeIcons.bold":
      return FontAwesomeIcons.bold;

    case "FontAwesomeIcons.bolt":
      return FontAwesomeIcons.bolt;

    case "FontAwesomeIcons.bomb":
      return FontAwesomeIcons.bomb;

    case "FontAwesomeIcons.bone":
      return FontAwesomeIcons.bone;

    case "FontAwesomeIcons.bong":
      return FontAwesomeIcons.bong;

    case "FontAwesomeIcons.book":
      return FontAwesomeIcons.book;

    case "FontAwesomeIcons.bookDead":
      return FontAwesomeIcons.bookDead;

    case "FontAwesomeIcons.bookMedical":
      return FontAwesomeIcons.bookMedical;

    case "FontAwesomeIcons.bookOpen":
      return FontAwesomeIcons.bookOpen;

    case "FontAwesomeIcons.bookReader":
      return FontAwesomeIcons.bookReader;

    case "FontAwesomeIcons.bookmark":
      return FontAwesomeIcons.bookmark;

    case "FontAwesomeIcons.bootstrap":
      return FontAwesomeIcons.bootstrap;

    case "FontAwesomeIcons.borderAll":
      return FontAwesomeIcons.borderAll;

    case "FontAwesomeIcons.borderNone":
      return FontAwesomeIcons.borderNone;

    case "FontAwesomeIcons.borderStyle":
      return FontAwesomeIcons.borderStyle;

    case "FontAwesomeIcons.bowlingBall":
      return FontAwesomeIcons.bowlingBall;

    case "FontAwesomeIcons.box":
      return FontAwesomeIcons.box;

    case "FontAwesomeIcons.boxOpen":
      return FontAwesomeIcons.boxOpen;

    case "FontAwesomeIcons.boxTissue":
      return FontAwesomeIcons.boxTissue;

    case "FontAwesomeIcons.boxes":
      return FontAwesomeIcons.boxes;

    case "FontAwesomeIcons.braille":
      return FontAwesomeIcons.braille;

    case "FontAwesomeIcons.brain":
      return FontAwesomeIcons.brain;

    case "FontAwesomeIcons.breadSlice":
      return FontAwesomeIcons.breadSlice;

    case "FontAwesomeIcons.briefcase":
      return FontAwesomeIcons.briefcase;

    case "FontAwesomeIcons.briefcaseMedical":
      return FontAwesomeIcons.briefcaseMedical;

    case "FontAwesomeIcons.broadcastTower":
      return FontAwesomeIcons.broadcastTower;

    case "FontAwesomeIcons.broom":
      return FontAwesomeIcons.broom;

    case "FontAwesomeIcons.brush":
      return FontAwesomeIcons.brush;

    case "FontAwesomeIcons.btc":
      return FontAwesomeIcons.btc;

    case "FontAwesomeIcons.buffer":
      return FontAwesomeIcons.buffer;

    case "FontAwesomeIcons.bug":
      return FontAwesomeIcons.bug;

    case "FontAwesomeIcons.building":
      return FontAwesomeIcons.building;

    case "FontAwesomeIcons.bullhorn":
      return FontAwesomeIcons.bullhorn;

    case "FontAwesomeIcons.bullseye":
      return FontAwesomeIcons.bullseye;

    case "FontAwesomeIcons.burn":
      return FontAwesomeIcons.burn;

    case "FontAwesomeIcons.buromobelexperte":
      return FontAwesomeIcons.buromobelexperte;

    case "FontAwesomeIcons.bus":
      return FontAwesomeIcons.bus;

    case "FontAwesomeIcons.busAlt":
      return FontAwesomeIcons.busAlt;

    case "FontAwesomeIcons.businessTime":
      return FontAwesomeIcons.businessTime;

    case "FontAwesomeIcons.buyNLarge":
      return FontAwesomeIcons.buyNLarge;

    case "FontAwesomeIcons.buysellads":
      return FontAwesomeIcons.buysellads;

    case "FontAwesomeIcons.calculator":
      return FontAwesomeIcons.calculator;

    case "FontAwesomeIcons.calendar":
      return FontAwesomeIcons.calendar;

    case "FontAwesomeIcons.calendarAlt":
      return FontAwesomeIcons.calendarAlt;

    case "FontAwesomeIcons.calendarCheck":
      return FontAwesomeIcons.calendarCheck;

    case "FontAwesomeIcons.calendarDay":
      return FontAwesomeIcons.calendarDay;

    case "FontAwesomeIcons.calendarMinus":
      return FontAwesomeIcons.calendarMinus;

    case "FontAwesomeIcons.calendarPlus":
      return FontAwesomeIcons.calendarPlus;

    case "FontAwesomeIcons.calendarTimes":
      return FontAwesomeIcons.calendarTimes;

    case "FontAwesomeIcons.calendarWeek":
      return FontAwesomeIcons.calendarWeek;

    case "FontAwesomeIcons.camera":
      return FontAwesomeIcons.camera;

    case "FontAwesomeIcons.campground":
      return FontAwesomeIcons.campground;

    case "FontAwesomeIcons.canadianMapleLeaf":
      return FontAwesomeIcons.canadianMapleLeaf;

    case "FontAwesomeIcons.candyCane":
      return FontAwesomeIcons.candyCane;

    case "FontAwesomeIcons.cannabis":
      return FontAwesomeIcons.cannabis;

    case "FontAwesomeIcons.capsules":
      return FontAwesomeIcons.capsules;

    case "FontAwesomeIcons.car":
      return FontAwesomeIcons.car;

    case "FontAwesomeIcons.carAlt":
      return FontAwesomeIcons.carAlt;

    case "FontAwesomeIcons.carBattery":
      return FontAwesomeIcons.carBattery;

    case "FontAwesomeIcons.carCrash":
      return FontAwesomeIcons.carCrash;

    case "FontAwesomeIcons.carSide":
      return FontAwesomeIcons.carSide;

    case "FontAwesomeIcons.caravan":
      return FontAwesomeIcons.caravan;

    case "FontAwesomeIcons.caretDown":
      return FontAwesomeIcons.caretDown;

    case "FontAwesomeIcons.caretLeft":
      return FontAwesomeIcons.caretLeft;

    case "FontAwesomeIcons.caretRight":
      return FontAwesomeIcons.caretRight;

    case "FontAwesomeIcons.caretSquareDown":
      return FontAwesomeIcons.caretSquareDown;

    case "FontAwesomeIcons.caretSquareLeft":
      return FontAwesomeIcons.caretSquareLeft;

    case "FontAwesomeIcons.caretSquareRight":
      return FontAwesomeIcons.caretSquareRight;

    case "FontAwesomeIcons.caretSquareUp":
      return FontAwesomeIcons.caretSquareUp;

    case "FontAwesomeIcons.caretUp":
      return FontAwesomeIcons.caretUp;

    case "FontAwesomeIcons.carrot":
      return FontAwesomeIcons.carrot;

    case "FontAwesomeIcons.cartArrowDown":
      return FontAwesomeIcons.cartArrowDown;

    case "FontAwesomeIcons.cartPlus":
      return FontAwesomeIcons.cartPlus;

    case "FontAwesomeIcons.cashRegister":
      return FontAwesomeIcons.cashRegister;

    case "FontAwesomeIcons.cat":
      return FontAwesomeIcons.cat;

    case "FontAwesomeIcons.ccAmazonPay":
      return FontAwesomeIcons.ccAmazonPay;

    case "FontAwesomeIcons.ccAmex":
      return FontAwesomeIcons.ccAmex;

    case "FontAwesomeIcons.ccApplePay":
      return FontAwesomeIcons.ccApplePay;

    case "FontAwesomeIcons.ccDinersClub":
      return FontAwesomeIcons.ccDinersClub;

    case "FontAwesomeIcons.ccDiscover":
      return FontAwesomeIcons.ccDiscover;

    case "FontAwesomeIcons.ccJcb":
      return FontAwesomeIcons.ccJcb;

    case "FontAwesomeIcons.ccMastercard":
      return FontAwesomeIcons.ccMastercard;

    case "FontAwesomeIcons.ccPaypal":
      return FontAwesomeIcons.ccPaypal;

    case "FontAwesomeIcons.ccStripe":
      return FontAwesomeIcons.ccStripe;

    case "FontAwesomeIcons.ccVisa":
      return FontAwesomeIcons.ccVisa;

    case "FontAwesomeIcons.centercode":
      return FontAwesomeIcons.centercode;

    case "FontAwesomeIcons.centos":
      return FontAwesomeIcons.centos;

    case "FontAwesomeIcons.certificate":
      return FontAwesomeIcons.certificate;

    case "FontAwesomeIcons.chair":
      return FontAwesomeIcons.chair;

    case "FontAwesomeIcons.chalkboard":
      return FontAwesomeIcons.chalkboard;

    case "FontAwesomeIcons.chalkboardTeacher":
      return FontAwesomeIcons.chalkboardTeacher;

    case "FontAwesomeIcons.chargingStation":
      return FontAwesomeIcons.chargingStation;

    case "FontAwesomeIcons.chartArea":
      return FontAwesomeIcons.chartArea;

    case "FontAwesomeIcons.chartBar":
      return FontAwesomeIcons.chartBar;

    case "FontAwesomeIcons.chartLine":
      return FontAwesomeIcons.chartLine;

    case "FontAwesomeIcons.chartPie":
      return FontAwesomeIcons.chartPie;

    case "FontAwesomeIcons.check":
      return FontAwesomeIcons.check;

    case "FontAwesomeIcons.checkCircle":
      return FontAwesomeIcons.checkCircle;

    case "FontAwesomeIcons.checkDouble":
      return FontAwesomeIcons.checkDouble;

    case "FontAwesomeIcons.checkSquare":
      return FontAwesomeIcons.checkSquare;

    case "FontAwesomeIcons.cheese":
      return FontAwesomeIcons.cheese;

    case "FontAwesomeIcons.chessBishop":
      return FontAwesomeIcons.chessBishop;

    case "FontAwesomeIcons.chessBoard":
      return FontAwesomeIcons.chessBoard;

    case "FontAwesomeIcons.chessKing":
      return FontAwesomeIcons.chessKing;

    case "FontAwesomeIcons.chessKnight":
      return FontAwesomeIcons.chessKnight;

    case "FontAwesomeIcons.chessPawn":
      return FontAwesomeIcons.chessPawn;

    case "FontAwesomeIcons.chessQueen":
      return FontAwesomeIcons.chessQueen;

    case "FontAwesomeIcons.chessRook":
      return FontAwesomeIcons.chessRook;

    case "FontAwesomeIcons.chevronCircleDown":
      return FontAwesomeIcons.chevronCircleDown;

    case "FontAwesomeIcons.chevronCircleLeft":
      return FontAwesomeIcons.chevronCircleLeft;

    case "FontAwesomeIcons.chevronCircleRight":
      return FontAwesomeIcons.chevronCircleRight;

    case "FontAwesomeIcons.chevronCircleUp":
      return FontAwesomeIcons.chevronCircleUp;

    case "FontAwesomeIcons.chevronDown":
      return FontAwesomeIcons.chevronDown;

    case "FontAwesomeIcons.chevronLeft":
      return FontAwesomeIcons.chevronLeft;

    case "FontAwesomeIcons.chevronRight":
      return FontAwesomeIcons.chevronRight;

    case "FontAwesomeIcons.chevronUp":
      return FontAwesomeIcons.chevronUp;

    case "FontAwesomeIcons.child":
      return FontAwesomeIcons.child;

    case "FontAwesomeIcons.chrome":
      return FontAwesomeIcons.chrome;

    case "FontAwesomeIcons.chromecast":
      return FontAwesomeIcons.chromecast;

    case "FontAwesomeIcons.church":
      return FontAwesomeIcons.church;

    case "FontAwesomeIcons.circle":
      return FontAwesomeIcons.circle;

    case "FontAwesomeIcons.circleNotch":
      return FontAwesomeIcons.circleNotch;

    case "FontAwesomeIcons.city":
      return FontAwesomeIcons.city;

    case "FontAwesomeIcons.clinicMedical":
      return FontAwesomeIcons.clinicMedical;

    case "FontAwesomeIcons.clipboard":
      return FontAwesomeIcons.clipboard;

    case "FontAwesomeIcons.clipboardCheck":
      return FontAwesomeIcons.clipboardCheck;

    case "FontAwesomeIcons.clipboardList":
      return FontAwesomeIcons.clipboardList;

    case "FontAwesomeIcons.clock":
      return FontAwesomeIcons.clock;

    case "FontAwesomeIcons.clone":
      return FontAwesomeIcons.clone;

    case "FontAwesomeIcons.closedCaptioning":
      return FontAwesomeIcons.closedCaptioning;

    case "FontAwesomeIcons.cloud":
      return FontAwesomeIcons.cloud;

    case "FontAwesomeIcons.cloudDownloadAlt":
      return FontAwesomeIcons.cloudDownloadAlt;

    case "FontAwesomeIcons.cloudMeatball":
      return FontAwesomeIcons.cloudMeatball;

    case "FontAwesomeIcons.cloudMoon":
      return FontAwesomeIcons.cloudMoon;

    case "FontAwesomeIcons.cloudMoonRain":
      return FontAwesomeIcons.cloudMoonRain;

    case "FontAwesomeIcons.cloudRains":
      return FontAwesomeIcons.cloudRain;

    case "FontAwesomeIcons.cloudShowersHeavy":
      return FontAwesomeIcons.cloudShowersHeavy;

    case "FontAwesomeIcons.cloudSun":
      return FontAwesomeIcons.cloudSun;

    case "FontAwesomeIcons.cloudSunRain":
      return FontAwesomeIcons.cloudSunRain;

    case "FontAwesomeIcons.cloudUploadAlt":
      return FontAwesomeIcons.cloudUploadAlt;

    case "FontAwesomeIcons.cloudscale":
      return FontAwesomeIcons.cloudscale;

    case "FontAwesomeIcons.cloudsmith":
      return FontAwesomeIcons.cloudsmith;

    case "FontAwesomeIcons.cloudversify":
      return FontAwesomeIcons.cloudversify;

    case "FontAwesomeIcons.cocktail":
      return FontAwesomeIcons.cocktail;

    case "FontAwesomeIcons.code":
      return FontAwesomeIcons.code;

    case "FontAwesomeIcons.codeBranch":
      return FontAwesomeIcons.codeBranch;

    case "FontAwesomeIcons.codepen":
      return FontAwesomeIcons.codepen;

    case "FontAwesomeIcons.codiepie":
      return FontAwesomeIcons.codiepie;

    case "FontAwesomeIcons.coffee":
      return FontAwesomeIcons.coffee;

    case "FontAwesomeIcons.cog":
      return FontAwesomeIcons.cog;

    case "FontAwesomeIcons.cogs":
      return FontAwesomeIcons.cogs;

    case "FontAwesomeIcons.coins":
      return FontAwesomeIcons.coins;

    case "FontAwesomeIcons.columns":
      return FontAwesomeIcons.columns;

    case "FontAwesomeIcons.comment":
      return FontAwesomeIcons.comment;

    case "FontAwesomeIcons.commentAlt":
      return FontAwesomeIcons.commentAlt;

    case "FontAwesomeIcons.commentDollar":
      return FontAwesomeIcons.commentDollar;

    case "FontAwesomeIcons.commentDots":
      return FontAwesomeIcons.commentDots;

    case "FontAwesomeIcons.commentMedical":
      return FontAwesomeIcons.commentMedical;

    case "FontAwesomeIcons.commentSlash":
      return FontAwesomeIcons.commentSlash;

    case "FontAwesomeIcons.comments":
      return FontAwesomeIcons.comments;

    case "FontAwesomeIcons.commentsDollar":
      return FontAwesomeIcons.commentsDollar;

    case "FontAwesomeIcons.compactDisc":
      return FontAwesomeIcons.compactDisc;

    case "FontAwesomeIcons.compass":
      return FontAwesomeIcons.compass;

    case "FontAwesomeIcons.compress":
      return FontAwesomeIcons.compress;

    case "FontAwesomeIcons.compressAlt":
      return FontAwesomeIcons.compressAlt;

    case "FontAwesomeIcons.compressArrowsAlt":
      return FontAwesomeIcons.compressArrowsAlt;

    case "FontAwesomeIcons.conciergeBell":
      return FontAwesomeIcons.conciergeBell;

    case "FontAwesomeIcons.confluence":
      return FontAwesomeIcons.confluence;

    case "FontAwesomeIcons.connectdevelop":
      return FontAwesomeIcons.connectdevelop;

    case "FontAwesomeIcons.contao":
      return FontAwesomeIcons.contao;

    case "FontAwesomeIcons.cookie":
      return FontAwesomeIcons.cookie;

    case "FontAwesomeIcons.cookieBite":
      return FontAwesomeIcons.cookieBite;

    case "FontAwesomeIcons.copy":
      return FontAwesomeIcons.copy;

    case "FontAwesomeIcons.copyright":
      return FontAwesomeIcons.copyright;

    case "FontAwesomeIcons.cottonBureau":
      return FontAwesomeIcons.cottonBureau;

    case "FontAwesomeIcons.couch":
      return FontAwesomeIcons.couch;

    case "FontAwesomeIcons.cpanel":
      return FontAwesomeIcons.cpanel;

    case "FontAwesomeIcons.creativeCommons":
      return FontAwesomeIcons.creativeCommons;

    case "FontAwesomeIcons.creativeCommonsBy":
      return FontAwesomeIcons.creativeCommonsBy;

    case "FontAwesomeIcons.creativeCommonsNc":
      return FontAwesomeIcons.creativeCommonsNc;

    case "FontAwesomeIcons.creativeCommonsNcEu":
      return FontAwesomeIcons.creativeCommonsNcEu;

    case "FontAwesomeIcons.creativeCommonsNcJp":
      return FontAwesomeIcons.creativeCommonsNcJp;

    case "FontAwesomeIcons.creativeCommonsNd":
      return FontAwesomeIcons.creativeCommonsNd;

    case "FontAwesomeIcons.creativeCommonsPd":
      return FontAwesomeIcons.creativeCommonsPd;

    case "":
      return FontAwesomeIcons.creativeCommonsPdAlt;

    default:
      return FontAwesomeIcons.home;
  }
}
