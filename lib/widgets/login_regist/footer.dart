import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/widgets/terms_and_privacy.dart';

class Footer extends StatelessWidget {
  const Footer(
      {Key? key,
      required this.textOne,
      required this.textTwo,
      required this.route,
      this.height = 48.0})
      : super(key: key);
  final String textOne;
  final String textTwo;
  final String route;
  final double height;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          // padding: EdgeInsets.only(
          //     left: getProportionateScreenWidth(5),
          //     top: getProportionateScreenWidth(5)),
          alignment: Alignment.center,
          child: RichText(
            text: TextSpan(
              children: <TextSpan>[
                TextSpan(text: textOne),
                TextSpan(
                    text: textTwo,
                    style: primaryTextStyle.copyWith(color: primaryColor),
                    recognizer: TapGestureRecognizer()
                      ..onTap = () {
                        Navigator.of(context).pushNamedAndRemoveUntil(
                            route, (Route<dynamic> route) => false);
                        //Navigator.pushNamed(context, route);
                      }),
              ],
              style: primaryTextStyle.copyWith(
                color: tenthColor,
                fontWeight: reguler,
                fontSize: getProportionateScreenWidth(14),
              ),
            ),
          ),
        ),
        SizedBox(height: getProportionateScreenHeight(height)),
        Container(
            // padding: EdgeInsets.only(
            //     left: getProportionateScreenWidth(5),
            //     top: getProportionateScreenWidth(5)),
            alignment: Alignment.center,
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(30)),
            child: RichText(
                textAlign: TextAlign.center,
                maxLines: 2,
                text: TextSpan(
                    children: <TextSpan>[
                      TextSpan(text: 'Dengan membuat akun, anda menyetujui '),
                      TextSpan(
                          text: 'S&K',
                          style: primaryTextStyle.copyWith(color: primaryColor),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => TermsAndCondition(
                                      url:
                                          'https://vocasia.id/home/terms_and_condition',
                                      id: 'sk'),
                                ),
                              );
                            }),
                      TextSpan(text: ' dan '),
                      TextSpan(
                          text: 'Kebijakan Privasi',
                          style: primaryTextStyle.copyWith(color: primaryColor),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => TermsAndCondition(
                                      url:
                                          'https://vocasia.id/home/privacy_policy',
                                      id: 'prv'),
                                ),
                              );
                            }),
                    ],
                    style: primaryTextStyle.copyWith(
                        height: 2,
                        color: tenthColor,
                        fontWeight: reguler,
                        fontSize: getProportionateScreenWidth(14))))),
      ],
    );
  }
}
