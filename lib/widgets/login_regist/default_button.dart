import 'package:flutter/material.dart';

import '../../theme.dart';
import '../../size_config.dart';

class DefaultButton extends StatelessWidget {
  const DefaultButton({
    Key? key,
    this.text = '',
    this.weight = semiBold,
    this.press,
  }) : super(key: key);
  final String text;
  final FontWeight weight;
  final VoidCallback? press;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: getProportionateScreenWidth(44),
      child: TextButton(
        style: TextButton.styleFrom(
          shape: RoundedRectangleBorder(
              borderRadius:
                  BorderRadius.circular(getProportionateScreenWidth(10))),
          primary: Colors.white,
          backgroundColor: primaryColor,
        ),
        onPressed: press,
        child: Text(
          text,
          style: thirdTextStyle.copyWith(
              fontSize: getProportionateScreenWidth(14),
              fontWeight: weight,
              color: Color(0xff050505),
              letterSpacing: 0.3),
        ),
      ),
    );
  }
}
