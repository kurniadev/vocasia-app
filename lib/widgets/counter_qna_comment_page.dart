import 'package:flutter/material.dart';
import 'package:initial_folder/providers/counter_qna_comment_provider.dart';
import 'package:initial_folder/widgets/counter_qna_comment.dart';
// import 'package:initial_folder/widgets/qna_user.dart';
import 'package:provider/provider.dart';

import '../theme.dart';

class CounterQnaCommentPage extends StatelessWidget {
  const CounterQnaCommentPage({Key? key, required this.idQna})
      : super(key: key);

  final idQna;

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => CounterQnaCommentProvider(idQna: idQna),
      child: Consumer<CounterQnaCommentProvider>(builder: (context, state, _) {
        if (state.state == ResultState.loading) {
          print(idQna);
          return Center(
            child: CircularProgressIndicator(
              color: primaryColor,
              strokeWidth: 2,
            ),
          );
        } else if (state.state == ResultState.noData) {
          return Center(
            child: Text(
              'belum ada pertanyaan',
              style: thirdTextStyle,
            ),
          );
        } else if (state.state == ResultState.hasData) {
          var counterQna = state.result!.data;

          return CounterQnaComment(
            counterComment: counterQna!,
          );
          // return ListView.builder(
          //     itemCount: state.result!.data!.length,
          //     physics: NeverScrollableScrollPhysics(),
          //     shrinkWrap: true,
          //     scrollDirection: Axis.vertical,
          //     itemBuilder: (context, index) {
          //       var counterQna = state.result!.data;
          //       return CounterQnaComment(
          //         counterComment: counterQna!,
          //       );
          //     });
        } else if (state.state == ResultState.error) {
          return Center(
              child: Column(
            children: [
              Text(
                'Terjadi Kesalahan Coba Lagi',
                style: thirdTextStyle,
              ),
            ],
          ));
        }
        return Center(
          child: Text(
            'Terjadi Kesalahan',
            style: thirdTextStyle,
          ),
        );
      }),
    );
  }
}
