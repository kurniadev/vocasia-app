import 'package:flutter/material.dart';

import '../size_config.dart';
import '../theme.dart';

class PointInstruktur extends StatelessWidget {
  const PointInstruktur({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        SizedBox(
          width: getProportionateScreenWidth(11),
        ),
        CircleAvatar(
          backgroundColor: primaryColor,
          maxRadius: 2,
        ),
        SizedBox(
          width: getProportionateScreenWidth(10),
        ),
        Text(
          'Instruktur',
          style: primaryTextStyle.copyWith(fontSize: 10, color: primaryColor),
        )
      ],
    );
  }
}
