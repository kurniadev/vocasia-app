import 'package:flutter/material.dart';
import 'package:initial_folder/providers/posting_qna_provider.dart';
import 'package:initial_folder/providers/qna_provider.dart';
import 'package:provider/provider.dart';

import '../size_config.dart';
import '../theme.dart';

class EditQna extends StatefulWidget {
  const EditQna(
      {Key? key,
      required this.quest,
      required this.id_qna,
      required this.id_course})
      : super(key: key);

  final quest;
  final id_qna;
  final id_course;

  @override
  State<EditQna> createState() => _EditQnaState();
}

class _EditQnaState extends State<EditQna> {
  final _textControlPertanyaan = TextEditingController();
  double value = 0;

  @override
  void initState() {
    if (widget.quest != null) {
      _textControlPertanyaan.text = widget.quest ?? '';
    }
    super.initState();
  }

  @override
  void dispose() {
    _textControlPertanyaan.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    PostingQnaProvider editQnaProvider =
        Provider.of<PostingQnaProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Edit Pertanyaan',
          style: primaryTextStyle.copyWith(
            fontWeight: semiBold,
            fontSize: getProportionateScreenWidth(16),
            letterSpacing: 0.2,
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: getProportionateScreenWidth(16),
            ),
            child: Column(
              children: [
                TextField(
                  controller: _textControlPertanyaan,
                  cursorColor: secondaryColor,
                  scrollPadding: EdgeInsets.zero,
                  minLines: 2,
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  decoration: InputDecoration(
                    hintStyle: secondaryTextStyle.copyWith(
                      color: secondaryColor,
                      letterSpacing: 0.5,
                      fontSize: getProportionateScreenWidth(12),
                    ),
                    // hintText: 'Edit pertanyaanmu di sini...',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(
                        10,
                      ),
                      borderSide: BorderSide(
                        color: thirdColor,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(
                        10,
                      ),
                      borderSide: BorderSide(
                        color: secondaryColor,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(4),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: ElevatedButton(
                    onPressed: () async {
                      if (await editQnaProvider
                          .editQna(
                              _textControlPertanyaan.text,
                              int.parse(widget.id_qna.toString()),
                              widget.id_course)
                          .whenComplete(
                        () {
                          _textControlPertanyaan.clear();
                          return Navigator.pop(context);
                          // await Provider.of(context)<QnaProvider>(context,
                          //         listen: true)
                          //     .getQna(widget.id_course);
                        },
                      ))
                        ;
                      else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            duration: Duration(seconds: 2),
                            backgroundColor: primaryColor,
                            content: Text(
                              'Terjadi kesalahan',
                              style: primaryTextStyle.copyWith(
                                color: backgroundColor,
                              ),
                            ),
                            behavior: SnackBarBehavior.floating,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5),
                            ),
                          ),
                        );
                      }
                    },
                    child: Text(
                      'Edit Pertanyaan',
                      style: thirdTextStyle.copyWith(
                        color: Colors.black,
                        fontSize: SizeConfig.blockHorizontal! * 4,
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                      primary: primaryColor,
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
