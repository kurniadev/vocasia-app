import 'package:flutter/material.dart';
import 'package:initial_folder/helper/validator.dart';
import 'package:initial_folder/models/wishlist_model.dart';
import 'package:initial_folder/screens/detail_course/detail_course_screen.dart';
import 'package:initial_folder/screens/whislist/wishlist_card.dart';

class MyWishlistPage extends StatelessWidget {
  const MyWishlistPage({Key? key, required this.wishlistDataModel})
      : super(key: key);
  final DataWihslistModel wishlistDataModel;

  @override
  Widget build(BuildContext context) {
    return Container(
        child: WishlistCard(
      id: wishlistDataModel.wishlistId ?? '',
      thumbnail: wishlistDataModel.thumbnail ??
          'http://api.vocasia.pasia.id/uploads/courses_thumbnail/course_thumbnail_default_57.jpg',
      title: wishlistDataModel.title ?? '',
      numberOfRatings: wishlistDataModel.review[0].totalReview ?? '0',
      press: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DetailCourseScreen(
              idcourse: wishlistDataModel.courseId ?? '0',
            ),
          ),
        );
      },
      price: (wishlistDataModel.discountPrice == '0')
          ? 'Gratis'
          : numberFormat(wishlistDataModel.discountPrice),
      isTopCourse: '0',
      instructorName: wishlistDataModel.instructor ?? '',
      rating: wishlistDataModel.review[0].avgRating != null
          ? '${wishlistDataModel.review[0].avgRating}'
          : '5.0',
      realPrice: (wishlistDataModel.price == '0')
          ? ''
          : numberFormat(wishlistDataModel.price),
      specificRating: double.parse(wishlistDataModel.review[0].avgRating != null
              ? '${wishlistDataModel.review[0].avgRating}'
              : '0')
          .toString(),
    ));
  }
}
//  return ProductCard(
//                   id: othersCourse.idCourse,
//                   thumbnail: othersCourse.thumbnail ??
//                       'http://api.vocasia.pasia.id/uploads/courses_thumbnail/course_thumbnail_default_57.jpg',
//                   title: othersCourse.title,
//                   instructorName: othersCourse.instructorName,
//                   specificRating: double.parse(
//                           othersCourse.rating[0]!.avgRating != null
//                               ? '${othersCourse.rating[0]!.avgRating}'
//                               : '0')
//                       .toString(),
//                   rating: othersCourse.rating[0]!.avgRating != null
//                       ? '${othersCourse.rating[0]!.avgRating}'
//                       : '5.0',
//                   numberOfRatings: othersCourse.rating[0]!.totalReview ?? '0',
//                   isTopCourse: othersCourse.topCourse!,
// price: (othersCourse.discountPrice == '0')
//     ? 'Gratis'
//     : numberFormat(othersCourse.discountPrice),
//                   realPrice: (othersCourse.price == '0')
//                       ? ''
//                       : numberFormat(othersCourse.price),
//                   press: () {
//                     print(othersCourse.idCourse);
// Navigator.push(
//   context,
//   MaterialPageRoute(
//     builder: (context) => DetailCourseScreen(
//       idcourse: othersCourse.idCourse,
//     ),
//   ),
// );
//                   },
//                 );
