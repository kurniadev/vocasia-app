import 'package:flutter/material.dart';
import 'package:initial_folder/helper/user_info.dart';
import 'package:initial_folder/models/qna_model.dart';
import 'package:initial_folder/providers/posting_qna_provider.dart';
import 'package:initial_folder/screens/course/component/detail_quest_and_answer.dart';
import 'package:initial_folder/widgets/counter_qna_comment_page.dart';
import 'package:initial_folder/widgets/edit_qna_user.dart';
import 'package:provider/provider.dart';

import '../size_config.dart';
import '../theme.dart';

class QnaUser extends StatefulWidget {
  const QnaUser(
      {Key? key,
      required this.id,
      this.divider,
      required this.qnaDataModel,
      required this.index,
      required this.userId})
      : super(key: key);

  final Widget? divider;
  final id;
  final QnaDataModel qnaDataModel;
  final int index;
  final int userId;

  @override
  State<QnaUser> createState() => _QnaUserState();
}

class _QnaUserState extends State<QnaUser> {
  @override
  Widget build(BuildContext context) {
    PostingQnaProvider deleteQnaProvider =
        Provider.of<PostingQnaProvider>(context);

    deleteQna() async {
      if (await deleteQnaProvider
          .deleteQna(int.parse(widget.qnaDataModel.id_qna.toString()))) {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            duration: Duration(seconds: 2),
            backgroundColor: primaryColor,
            content: Text(
              'Pertanyaan berhasil dihapus',
              style: primaryTextStyle.copyWith(
                color: backgroundColor,
              ),
            ),
            behavior: SnackBarBehavior.floating,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            action: SnackBarAction(
              label: 'Lihat',
              onPressed: () {
                ScaffoldMessenger.of(context).hideCurrentSnackBar();
              },
            ),
          ),
        );
      } else {
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            duration: Duration(seconds: 2),
            backgroundColor: primaryColor,
            content: Text(
              'Terjadi kesalahan',
              style: primaryTextStyle.copyWith(
                color: backgroundColor,
              ),
            ),
            behavior: SnackBarBehavior.floating,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
          ),
        );
      }
    }

    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: getProportionateScreenWidth(16),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => DetailQuestAndAnswer(
                    qnaDataModel: widget.qnaDataModel,
                    id: widget.id,
                    index: widget.index,
                    userId: widget.userId,
                  ),
                ),
              );
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    CircleAvatar(
                      backgroundColor: primaryColor,
                      backgroundImage: widget.qnaDataModel.foto_profile == null
                          ? AssetImage("assets/images/Profile Image.png")
                          : NetworkImage(widget.qnaDataModel.foto_profile ?? '')
                              as ImageProvider,
                    ),
                    SizedBox(
                      width: getProportionateScreenWidth(8),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text(
                              widget.qnaDataModel.username ?? '',
                              style: primaryTextStyle.copyWith(
                                  fontSize: getProportionateScreenWidth(12),
                                  color: tenthColor),
                            ),
                          ],
                        ),
                        Text(
                          widget.qnaDataModel.date ?? '',
                          style: primaryTextStyle.copyWith(
                              fontSize: getProportionateScreenWidth(12),
                              color: secondaryColor),
                        ),
                      ],
                    ),
                    int.parse(widget.qnaDataModel.sender.toString()) ==
                            widget.userId
                        ? Expanded(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                PopupMenuButton(
                                  child: Padding(
                                    padding: EdgeInsets.only(right: 10),
                                    child: Icon(
                                      Icons.more_vert,
                                      color: Colors.white,
                                    ),
                                  ),
                                  itemBuilder: (context) => [
                                    PopupMenuItem(
                                      child: Container(
                                        child: Text('Edit'),
                                      ),
                                      value: 'edit',
                                    ),
                                    PopupMenuItem(
                                      child: Text('Hapus'),
                                      value: 'hapus',
                                    ),
                                  ],
                                  onSelected: (value) {
                                    switch (value) {
                                      case 'edit':
                                        Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) => EditQna(
                                              quest: widget.qnaDataModel.quest,
                                              id_qna:
                                                  widget.qnaDataModel.id_qna,
                                              id_course: widget.id,
                                            ),
                                          ),
                                        );
                                        break;
                                      case 'hapus':
                                        deleteQna();
                                        break;
                                    }
                                  },
                                )
                              ],
                            ),
                          )
                        : SizedBox(
                            height: 12,
                          ),
                  ],
                ),
                SizedBox(height: getProportionateScreenHeight(3)),
                Text(
                  widget.qnaDataModel.quest ?? '',
                  style: secondaryTextStyle.copyWith(
                      color: Color(0xffFFFFFF),
                      letterSpacing: 1,
                      fontSize: SizeConfig.blockHorizontal! * 3.4),
                ),
                SizedBox(height: getProportionateScreenHeight(16)),
              ],
            ),
          ),
          Row(
            children: [
              Icon(
                Icons.favorite_border,
                color: secondaryColor,
                size: 12,
              ),
              SizedBox(
                width: getProportionateScreenWidth(3),
              ),
              Text(
                widget.qnaDataModel.like ?? '',
                style: secondaryTextStyle.copyWith(
                    fontSize: getProportionateScreenWidth(10),
                    letterSpacing: 0.3),
              ),
              SizedBox(
                width: getProportionateScreenWidth(13),
              ),
              Icon(
                Icons.question_answer_rounded,
                color: secondaryColor,
                size: 12,
              ),
              SizedBox(
                width: getProportionateScreenWidth(3),
              ),
              CounterQnaCommentPage(idQna: widget.qnaDataModel.id_qna)
            ],
          ),
          SizedBox(
            height: getProportionateScreenWidth(13),
          ),
          SizedBox(
            child: widget.divider,
          )
        ],
      ),
    );
  }
}
