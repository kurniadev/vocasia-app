import 'package:flutter/material.dart';
import 'package:initial_folder/providers/posting_qna_reply_provider.dart';
import 'package:provider/provider.dart';

import '../size_config.dart';
import '../theme.dart';

class EditReplyQna extends StatefulWidget {
  const EditReplyQna(
      {Key? key,
      required this.id_qna,
      required this.text_rep,
      required this.id_rep})
      : super(key: key);

  final id_qna;
  final text_rep;
  final id_rep;

  @override
  State<EditReplyQna> createState() => _EditReplyQnaState();
}

class _EditReplyQnaState extends State<EditReplyQna> {
  final _textControlBalasan = TextEditingController();
  double value = 0;

  @override
  void initState() {
    if (widget.text_rep != null) {
      _textControlBalasan.text = widget.text_rep ?? '';
    }
    super.initState();
  }

  @override
  void dispose() {
    _textControlBalasan.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    PostingQnaReplyProvider editQnaReplyProvider =
        Provider.of<PostingQnaReplyProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Edit Balasan',
          style: primaryTextStyle.copyWith(
            fontWeight: semiBold,
            fontSize: getProportionateScreenWidth(16),
            letterSpacing: 0.2,
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: getProportionateScreenWidth(16),
            ),
            child: Column(
              children: [
                TextField(
                  controller: _textControlBalasan,
                  cursorColor: secondaryColor,
                  scrollPadding: EdgeInsets.zero,
                  minLines: 2,
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  decoration: InputDecoration(
                    hintStyle: secondaryTextStyle.copyWith(
                      color: secondaryColor,
                      letterSpacing: 0.5,
                      fontSize: getProportionateScreenWidth(12),
                    ),
                    // hintText: 'Edit pertanyaanmu di sini...',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(
                        10,
                      ),
                      borderSide: BorderSide(
                        color: thirdColor,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(
                        10,
                      ),
                      borderSide: BorderSide(
                        color: secondaryColor,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(4),
                ),
                Align(
                  alignment: Alignment.topRight,
                  child: ElevatedButton(
                    onPressed: () async {
                      if (await editQnaReplyProvider
                          .editQnaReply(
                              _textControlBalasan.text,
                              int.parse(widget.id_rep.toString()),
                              widget.id_qna)
                          .whenComplete(
                        () {
                          _textControlBalasan.clear();
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              duration: Duration(seconds: 2),
                              backgroundColor: primaryColor,
                              content: Text(
                                'Balasan berhasil diedit',
                                style: primaryTextStyle.copyWith(
                                  color: backgroundColor,
                                ),
                              ),
                              behavior: SnackBarBehavior.floating,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5),
                              ),
                            ),
                          );
                          return Navigator.pop(context);
                          // await Provider.of(context)<QnaProvider>(context,
                          //         listen: true)
                          //     .getQna(widget.id_course);
                        },
                      ))
                        ;
                      else {
                        ScaffoldMessenger.of(context).showSnackBar(
                          SnackBar(
                            duration: Duration(seconds: 2),
                            backgroundColor: primaryColor,
                            content: Text(
                              'Terjadi kesalahan',
                              style: primaryTextStyle.copyWith(
                                color: backgroundColor,
                              ),
                            ),
                            behavior: SnackBarBehavior.floating,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5),
                            ),
                          ),
                        );
                      }
                    },
                    child: Text(
                      'Edit Balasan',
                      style: thirdTextStyle.copyWith(
                        color: Colors.black,
                        fontSize: SizeConfig.blockHorizontal! * 4,
                      ),
                    ),
                    style: ElevatedButton.styleFrom(
                      primary: primaryColor,
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
