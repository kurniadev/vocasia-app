import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:initial_folder/helper/validator.dart';
import 'package:initial_folder/providers/filters_course_provider.dart'
    as filterCourseProv;
import 'package:initial_folder/providers/search_provider.dart';
import 'package:initial_folder/screens/detail_course/detail_course_screen.dart';
import 'package:initial_folder/screens/home/components/body_comp/product_card/product_card.dart';
import 'package:initial_folder/screens/search_course/component/filter.dart';
import 'package:provider/provider.dart';

import '../size_config.dart';
import '../theme.dart';

class SearchAndFilterCourse extends StatelessWidget {
  const SearchAndFilterCourse({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(getProportionateScreenWidth(57)),
          child: AppBar(
            leadingWidth: 30,
            actions: [
              IconButton(
                  padding: EdgeInsets.zero,
                  onPressed: () => Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Filter())),
                  icon: Icon(Icons.tune_rounded)),
            ],
            title: Container(
              height: 40,
              child: Consumer<SearchProvider>(
                builder: (context, state, _) => TextField(
                  autofocus: true,
                  onSubmitted: (value) {
                    Provider.of<filterCourseProv.FilterCourseProvider>(context,
                            listen: false)
                        .isSearchsFalse();
                    state.searchText = validatorSearch(value);
                  },
                  style: primaryTextStyle.copyWith(
                    fontSize: getProportionateScreenWidth(14),
                    letterSpacing: 0.5,
                  ),
                  cursorColor: secondaryColor,
                  decoration: InputDecoration(
                    errorBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: sevenColor),
                        borderRadius: BorderRadius.circular(10)),
                    contentPadding: EdgeInsets.only(
                      left: getProportionateScreenWidth(15),
                    ),
                    prefixIcon: Icon(
                      FeatherIcons.search,
                      size: 18,
                      color: secondaryColor,
                    ),
                    hintText: 'Cari Kursus',
                    hintStyle: primaryTextStyle.copyWith(
                        fontSize: getProportionateScreenWidth(12),
                        color: secondaryColor,
                        letterSpacing: 0.5),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(
                        10,
                      ),
                      borderSide: BorderSide(
                        color: secondaryColor,
                      ),
                    ),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(
                        10,
                      ),
                      borderSide: BorderSide(
                        color: secondaryColor,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
        body: Provider.of<filterCourseProv.FilterCourseProvider>(context)
                .isSearch
            ? Text('--')
            : Consumer<SearchProvider>(
                builder: (context, state, _) {
                  if (state.state == ResultState.loading) {
                    return Center(
                      child: CircularProgressIndicator(
                        strokeWidth: 2,
                        color: primaryColor,
                      ),
                    );
                  } else if (state.state == ResultState.hasData) {
                    return GridView.builder(
                      padding: EdgeInsets.only(
                          right: getProportionateScreenWidth(20)),
                      physics: ScrollPhysics(),
                      shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio: 2.8 / 4,
                        // crossAxisSpacing: 10,
                        //mainAxisSpacing: 20,
                        //mainAxisSpacing: 0,
                      ),
                      itemCount: state.result.length,
                      itemBuilder: (context, index) {
                        var othersCourse = state.result[index];
                        // var finalRating = double.parse(
                        //     (othersCourse.specificRating![0] / 20).toStringAsFixed(2));
                        return ProductCard(
                            id: othersCourse.idCourse,
                            thumbnail: othersCourse.thumbnail ??
                                'https://vocasia.id/uploads/thumbnails/course_thumbnails/course_thumbnail_default_63.jpg',
                            title: othersCourse.title,
                            instructorName: othersCourse.instructorName,
                            specificRating: double.parse(
                                    othersCourse.rating[0]!.avgRating != null
                                        ? '${othersCourse.rating[0]!.avgRating}'
                                        : '5.0')
                                .toString(),
                            rating: othersCourse.rating[0]!.avgRating != null
                                ? '${othersCourse.rating[0]!.avgRating}'
                                : '5.0',
                            numberOfRatings:
                                othersCourse.rating[0]!.totalReview ?? '0',
                            isTopCourse: othersCourse.topCourse ?? '0',
                            price: (othersCourse.discountPrice == '0')
                                ? 'Gratis'
                                : numberFormat(othersCourse.discountPrice),
                            realPrice: (othersCourse.price == '0')
                                ? ''
                                : numberFormat(othersCourse.price),
                            press: () {
                              // await Hive.openBox<Wishlist>("wishlist");
                              // await Hive.openBox('carts');

                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => DetailCourseScreen(
                                            idcourse: othersCourse.idCourse,
                                          )));
                            });
                      },
                    );
                  } else if (state.state == ResultState.noData) {
                    return Center(
                      child: Text('ooppss Kursus tidak ditemukans'),
                    );
                  } else if (state.state == ResultState.error) {
                    return Center(
                      child: Text(
                        'Error 404',
                        style: thirdTextStyle,
                      ),
                    );
                  } else {
                    return Center(child: Text(''));
                  }
                },
              ),
      ),
    );
  }
}
