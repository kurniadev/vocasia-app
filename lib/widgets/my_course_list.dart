import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/shims/dart_ui_real.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:initial_folder/models/my_course_model.dart';
import 'package:initial_folder/providers/detail_course_provider.dart';
import 'package:initial_folder/providers/instructor_provider.dart';
import 'package:initial_folder/providers/lesson_course_provider.dart';
import 'package:initial_folder/providers/my_course_provider.dart';
import 'package:initial_folder/providers/posting_review_provider.dart'
    as postReviewProvider;
import 'package:initial_folder/screens/course/play_course_page.dart';
import 'package:initial_folder/screens/my_course/success_free_course.dart';
import 'package:initial_folder/services/course_service.dart';
import 'package:initial_folder/services/instructor_service.dart';
import 'package:initial_folder/services/lesson_course_service.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import 'package:initial_folder/screens/detail_course/detail_course_screen.dart';

class MyCourseList extends StatefulWidget {
  const MyCourseList({
    Key? key,
    required this.dataMyCourseModel,
  }) : super(key: key);
  final DataMyCourseModel dataMyCourseModel;

  @override
  State<MyCourseList> createState() => _MyCourseListState();
}

class _MyCourseListState extends State<MyCourseList> {
  double value = 0;
  TextEditingController _controller = TextEditingController(text: '');
  // @override
  // void dispose() {
  //   _controller.dispose();
  //   super.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(
            left: getProportionateScreenWidth(16),
            right: getProportionateScreenWidth(16),
            top: getProportionateScreenWidth(10),
          ),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                      flex: 11,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CachedNetworkImage(
                            placeholder: (context, url) => Shimmer(
                                child: Container(
                                  color: Colors.black,
                                ),
                                gradient: LinearGradient(stops: [
                                  0.2,
                                  0.5,
                                  0.6
                                ], colors: [
                                  ninthColor,
                                  fourthColor,
                                  ninthColor
                                ])),
                            errorWidget: (context, url, error) =>
                                Icon(Icons.error),
                            imageBuilder: (context, imageUrl) => Container(
                              width: getProportionateScreenWidth(156),
                              height: getProportionateScreenWidth(88),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: imageUrl,
                                ),
                              ),
                            ),
                            imageUrl: widget.dataMyCourseModel.thumbnail ??
                                'http://api.vocasia.pasia.id/uploads/courses_thumbnail/course_thumbnail_default_57.jpg',
                          ),
                          SizedBox(
                            height: getProportionateScreenHeight(8),
                          ),
                        ],
                      )),
                  SizedBox(
                    width: getProportionateScreenWidth(9),
                  ),
                  Flexible(
                    flex: 10,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.dataMyCourseModel.title ?? ' ',
                          style: secondaryTextStyle.copyWith(
                            letterSpacing: 1,
                            fontWeight: semiBold,
                            fontSize: SizeConfig.blockHorizontal! * 3.5,
                          ),
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                        ),
                        SizedBox(
                          height: getProportionateScreenWidth(8),
                        ),
                        Text(
                          'Oleh ${widget.dataMyCourseModel.instructor}',
                          style: primaryTextStyle.copyWith(
                            color: secondaryColor,
                            fontSize: SizeConfig.blockHorizontal! * 3,
                          ),
                        ),
                        SizedBox(
                          height: getProportionateScreenWidth(6),
                        ),
                        Row(
                          children: [
                            RatingBarIndicator(
                              itemSize: SizeConfig.blockHorizontal! * 3.5,
                              rating: double.parse(widget
                                      .dataMyCourseModel.rating.isEmpty
                                  ? '0'
                                  : widget.dataMyCourseModel.rating[0].rating ??
                                      '5'),
                              direction: Axis.horizontal,
                              itemCount: 5,
                              //itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                              itemBuilder: (context, _) => FaIcon(
                                  FontAwesomeIcons.solidStar,
                                  color: primaryColor),
                            ),
                            SizedBox(
                              width: getProportionateScreenWidth(5),
                            ),
                            InkWell(
                              onTap: () => showDialog(
                                context: context,
                                builder: (context) => MultiProvider(
                                  providers: [
                                    ChangeNotifierProvider(
                                      create: (context) => postReviewProvider
                                          .PostingReviewProvider(
                                              courseService: CourseService()),
                                    )
                                  ],
                                  child: Consumer<
                                          postReviewProvider
                                              .PostingReviewProvider>(
                                      builder: (context, state, _) {
                                    if (state.state ==
                                        postReviewProvider
                                            .ResultState.uninitilized) {
                                      return AlertDialog(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(16)),
                                        contentPadding: EdgeInsets.symmetric(
                                            vertical: 10, horizontal: 10),
                                        insetPadding: EdgeInsets.all(1),
                                        backgroundColor: backgroundColor,
                                        title: Center(
                                          child: Text(
                                            'Berikan Ulasan',
                                            style: secondaryTextStyle.copyWith(
                                                fontSize: 20,
                                                letterSpacing: 0.2,
                                                color: primaryColor),
                                          ),
                                        ),
                                        content: SingleChildScrollView(
                                          scrollDirection: Axis.vertical,
                                          child: Column(
                                            // mainAxisSize: MainAxisSize.min,
                                            children: [
                                              Container(
                                                width:
                                                    getProportionateScreenWidth(
                                                        160),
                                                height:
                                                    getProportionateScreenWidth(
                                                        90),
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(8),
                                                  image: DecorationImage(
                                                    fit: BoxFit.fill,
                                                    image: NetworkImage(widget
                                                            .dataMyCourseModel
                                                            .thumbnail ??
                                                        'http://api.vocasia.pasia.id/uploads/courses_thumbnail/course_thumbnail_default_57.jpg'),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                height:
                                                    getProportionateScreenWidth(
                                                        16),
                                              ),
                                              Text(
                                                  widget.dataMyCourseModel
                                                          .title ??
                                                      '',
                                                  style: secondaryTextStyle
                                                      .copyWith(
                                                          letterSpacing: 1,
                                                          fontWeight: semiBold),
                                                  textAlign: TextAlign.center),
                                              SizedBox(
                                                height:
                                                    getProportionateScreenWidth(
                                                        23),
                                              ),
                                              RatingBar.builder(
                                                  itemPadding:
                                                      EdgeInsets.only(left: 9),
                                                  itemSize: 25,
                                                  maxRating: 5,
                                                  itemBuilder: (context, _) {
                                                    return FaIcon(
                                                        FontAwesomeIcons
                                                            .solidStar,
                                                        color: primaryColor);
                                                  },
                                                  onRatingUpdate: (val) {
                                                    value = val;
                                                  }),
                                              SizedBox(
                                                height:
                                                    getProportionateScreenWidth(
                                                        16),
                                              ),
                                              TextField(
                                                controller: _controller,
                                                textInputAction:
                                                    TextInputAction.done,
                                                cursorColor: secondaryColor,
                                                scrollPadding: EdgeInsets.zero,
                                                minLines: 2,
                                                maxLines: null,
                                                decoration: InputDecoration(
                                                  hintStyle: secondaryTextStyle
                                                      .copyWith(
                                                    color: secondaryColor,
                                                    letterSpacing: 0.5,
                                                    fontSize:
                                                        getProportionateScreenWidth(
                                                            12),
                                                  ),
                                                  hintText: 'Tulis Ulasan',
                                                  border: OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                      10,
                                                    ),
                                                    borderSide: BorderSide(
                                                      color: thirdColor,
                                                    ),
                                                  ),
                                                  focusedBorder:
                                                      OutlineInputBorder(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                      10,
                                                    ),
                                                    borderSide: BorderSide(
                                                      color: secondaryColor,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              SizedBox(
                                                height:
                                                    getProportionateScreenWidth(
                                                        10),
                                              ),
                                              ElevatedButton(
                                                onPressed: () async {
                                                  await state.postingReview(
                                                      _controller.text,
                                                      int.parse(widget
                                                          .dataMyCourseModel
                                                          .courseId
                                                          .toString()),
                                                      value.toInt());
                                                  await Provider.of<
                                                              MyCourseProvider>(
                                                          context,
                                                          listen: false)
                                                      .getMyCourse();
                                                },
                                                child: Text(
                                                  'Kirim Ulasan',
                                                  style:
                                                      thirdTextStyle.copyWith(
                                                          color: Colors.black),
                                                ),
                                                style: ElevatedButton.styleFrom(
                                                  minimumSize: Size(
                                                      getProportionateScreenWidth(
                                                          90),
                                                      getProportionateScreenHeight(
                                                          33)),
                                                  primary: primaryColor,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      );
                                    } else if (state.state ==
                                        postReviewProvider
                                            .ResultState.loading) {
                                      return AlertDialog(
                                        content: Container(
                                          height:
                                              getProportionateScreenHeight(40),
                                          child: Center(
                                            child: CircularProgressIndicator(
                                              strokeWidth: 2,
                                              color: primaryColor,
                                            ),
                                          ),
                                        ),
                                      );
                                    } else if (state.state ==
                                        postReviewProvider
                                            .ResultState.success) {
                                      _controller.clear();
                                      return AlertDialog(
                                          content: Container(
                                        height:
                                            getProportionateScreenHeight(30),
                                        child: Center(
                                          child: Text(
                                            'Berhasil memberikan ulasan',
                                            style: primaryTextStyle.copyWith(
                                                fontSize:
                                                    getProportionateScreenWidth(
                                                        12),
                                                letterSpacing: 0.5),
                                          ),
                                        ),
                                      ));
                                    } else if (state.state ==
                                        postReviewProvider.ResultState.failed) {
                                      return AlertDialog(
                                        title: Text(
                                          'Terjadi Kesalahan',
                                          style: primaryTextStyle,
                                        ),
                                      );
                                    }
                                    return AlertDialog(
                                      title: Text(
                                        'Terjadi Kesalahan',
                                        style: primaryTextStyle,
                                      ),
                                    );
                                  }),
                                ),
                              ),
                              child: Text(
                                'Edit Penilaian',
                                style: primaryTextStyle.copyWith(
                                  color: primaryColor,
                                  fontSize: 10,
                                ),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Stack(
                children: [
                  Container(
                    width: double.infinity,
                    height: 4,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white),
                  ),
                  Container(
                    width: (SizeConfig.screenWidth -
                            getProportionateScreenWidth(32)) *
                        int.parse(
                            widget.dataMyCourseModel.totalProgress.toString()) /
                        100,
                    height: 4,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: primaryColor),
                  ),
                ],
              ),
              SizedBox(height: getProportionateScreenWidth(6)),
              Align(
                alignment: Alignment.topLeft,
                child: Text(
                  '${widget.dataMyCourseModel.totalProgress}% Lengkap',
                  style: primaryTextStyle.copyWith(
                    fontSize: SizeConfig.blockHorizontal! * 2.7,
                    letterSpacing: 0.2,
                  ),
                ),
              ),
              Row(children: [
                ElevatedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MultiProvider(
                          providers: [
                            ChangeNotifierProvider(
                              create: (context) => LessonCourseProvider(
                                lessonCourseService: LessonCourseService(),
                                id: int.parse(
                                    widget.dataMyCourseModel.courseId ?? '0'),
                              ),
                            ),
                            ChangeNotifierProvider(
                                create: (context) => DetailCourseProvider(
                                    courseService: CourseService(),
                                    id: widget.dataMyCourseModel.courseId ??
                                        '1')),
                            ChangeNotifierProvider(
                              create: (context) => InstructorProvider(
                                  instructorService: InstructorService(),
                                  id: int.parse(
                                      widget.dataMyCourseModel.instructorId!)),
                            ),
                          ],
                          child: PlayCourse(
                            judul: widget.dataMyCourseModel.title ?? '',
                            instruktur:
                                widget.dataMyCourseModel.instructor ?? '',
                            thumbnail: widget.dataMyCourseModel.thumbnail ??
                                'http://api.vocasia.pasia.id/uploads/courses_thumbnail/course_thumbnail_default_57.jpg',
                          ),
                        ),
                      ),
                    );
                  },
                  child: Text(
                    'Mulai Kursus',
                    style: thirdTextStyle.copyWith(color: Colors.black),
                  ),
                  style: ElevatedButton.styleFrom(
                    minimumSize: Size(getProportionateScreenWidth(90),
                        getProportionateScreenHeight(33)),
                    primary: primaryColor,
                  ),
                ),
                SizedBox(
                  width: getProportionateScreenWidth(10),
                ),
                OutlinedButton(
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => DetailCourseScreen(
                          idcourse: widget.dataMyCourseModel.courseId ?? '1',
                        ),
                      ),
                    );
                  },
                  child: Text('Detail Kursus'),
                  style: OutlinedButton.styleFrom(
                    minimumSize: Size(getProportionateScreenWidth(90),
                        getProportionateScreenHeight(33)),
                    primary: primaryColor,
                    textStyle: thirdTextStyle,
                    side: BorderSide(color: primaryColor),
                  ),
                ),
              ]),
              SizedBox(
                height: getProportionateScreenHeight(10),
              ),
            ],
          ),
        ),
        Divider(
          color: fourthColor,
        ),
      ],
    );
  }
}
