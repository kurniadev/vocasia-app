import 'package:flutter/material.dart';
import 'package:initial_folder/models/announcement_model.dart';
import 'package:initial_folder/screens/course/component/detail_quest_and_answer.dart';
import 'package:initial_folder/theme.dart';

import '../size_config.dart';

class QandA extends StatelessWidget {
  const QandA(
      {Key? key,
      this.divider,
      this.pointInstruktur = const SizedBox(),
      required this.announcementDataModel})
      : super(key: key);
  final Widget? divider;
  final Widget pointInstruktur;
  final AnnouncementDataModel announcementDataModel;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: getProportionateScreenWidth(16),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          InkWell(
            onTap: () {
              print(announcementDataModel.idAnnouncement);
            },
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    CircleAvatar(
                      backgroundColor: primaryColor,
                    ),
                    SizedBox(
                      width: getProportionateScreenWidth(8),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text(
                              announcementDataModel.instructorName ?? ' ',
                              style: primaryTextStyle.copyWith(
                                  fontSize: getProportionateScreenWidth(12),
                                  color: tenthColor),
                            ),
                            pointInstruktur,
                          ],
                        ),
                        Text(
                          announcementDataModel.date ?? '',
                          style: primaryTextStyle.copyWith(
                              fontSize: getProportionateScreenWidth(12),
                              color: secondaryColor),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(height: getProportionateScreenHeight(3)),
                Text(
                  announcementDataModel.bodyContent ?? '',
                  style: secondaryTextStyle.copyWith(
                      color: Color(0xffFFFFFF),
                      letterSpacing: 1,
                      fontSize: SizeConfig.blockHorizontal! * 3.4),
                ),
                SizedBox(height: getProportionateScreenHeight(16)),
              ],
            ),
          ),
          Row(
            children: [
              Icon(
                Icons.favorite_border,
                color: secondaryColor,
                size: 12,
              ),
              SizedBox(
                width: getProportionateScreenWidth(3),
              ),
              Text(
                announcementDataModel.likes ?? '',
                style: secondaryTextStyle.copyWith(
                    fontSize: getProportionateScreenWidth(10),
                    letterSpacing: 0.3),
              ),
              SizedBox(
                width: getProportionateScreenWidth(13),
              ),
              Icon(
                Icons.question_answer_rounded,
                color: secondaryColor,
                size: 12,
              ),
              SizedBox(
                width: getProportionateScreenWidth(3),
              ),
              Text(
                '50',
                style: secondaryTextStyle.copyWith(
                    fontSize: getProportionateScreenWidth(10),
                    letterSpacing: 0.3),
              )
            ],
          ),
          SizedBox(
            height: getProportionateScreenWidth(13),
          ),
          SizedBox(
            child: divider,
          )
        ],
      ),
    );
  }
}
