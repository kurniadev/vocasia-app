import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:initial_folder/models/history_transaction_model.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:shimmer/shimmer.dart';

class RiwayatList extends StatelessWidget {
  const RiwayatList({Key? key, required this.dataHistoryTransactionModel})
      : super(key: key);
  final DataHistoryTransactionModel dataHistoryTransactionModel;
  final bool berhasil = true;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
          bottom: getProportionateScreenWidth(16),
          left: getProportionateScreenWidth(16),
          right: getProportionateScreenWidth(16)),
      decoration: BoxDecoration(
          color: Color(0xFF212121), borderRadius: BorderRadius.circular(4)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(
              top: getProportionateScreenWidth(16),
              left: getProportionateScreenWidth(8),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "AB1234567890",
                  style: primaryTextStyle.copyWith(
                    fontWeight: reguler,
                    fontSize: getProportionateScreenWidth(12),
                    letterSpacing: 0.5,
                  ),
                ),
                SizedBox(height: 9),
                Row(
                  children: [
                    Text(
                      dataHistoryTransactionModel.date ?? '',
                      style: primaryTextStyle.copyWith(
                        color: secondaryColor,
                        fontWeight: reguler,
                        fontSize: getProportionateScreenWidth(12),
                        letterSpacing: 0.5,
                      ),
                    ),
                    SizedBox(width: getProportionateScreenWidth(8)),
                    (berhasil == true)
                        ? Container(
                            alignment: Alignment.center,
                            width: getProportionateScreenWidth(57),
                            height: getProportionateScreenWidth(20),
                            child: Text(
                              'Berhasil',
                              style: primaryTextStyle.copyWith(
                                  letterSpacing: 0.5,
                                  color: backgroundColor,
                                  fontSize: SizeConfig.blockHorizontal! * 2.5,
                                  fontWeight: semiBold),
                            ),
                            decoration: BoxDecoration(
                              color: eightColor,
                              borderRadius: BorderRadius.circular(5),
                              //border: Border.all(color: kPrimaryColor),
                            ),
                          )
                        : Container(
                            alignment: Alignment.center,
                            width: getProportionateScreenWidth(125),
                            height: getProportionateScreenWidth(18),
                            child: Text(
                              'Melebihi batas waktu',
                              style: primaryTextStyle.copyWith(
                                  letterSpacing: 0.5,
                                  color: Colors.black,
                                  fontSize: SizeConfig.blockHorizontal! * 2.2,
                                  fontWeight: semiBold),
                            ),
                            decoration: BoxDecoration(
                              color: Color(0xffFF502A),
                              borderRadius: BorderRadius.circular(5),
                              //border: Border.all(color: kPrimaryColor),
                            ),
                          )
                  ],
                ),
              ],
            ),
          ),

          SizedBox(height: 17),
          Container(
            margin: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(8)),
            child: Divider(
              color: Color(0xff2D2D2D),
              thickness: 0.5,
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              top: getProportionateScreenWidth(12),
              left: getProportionateScreenWidth(8),
              right: getProportionateScreenWidth(12),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Kursus",
                  style: primaryTextStyle.copyWith(
                    fontWeight: reguler,
                    fontSize: getProportionateScreenWidth(12),
                    letterSpacing: 0.5,
                  ),
                ),
                SizedBox(height: 8),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                        flex: 34,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CachedNetworkImage(
                              imageUrl: dataHistoryTransactionModel.thumbnail ??
                                  'http://api.vocasia.pasia.id/uploads/courses_thumbnail/course_thumbnail_default_57.jpg',
                              imageBuilder: (context, imageProvider) =>
                                  Container(
                                width: getProportionateScreenWidth(108),
                                height: getProportionateScreenWidth(56),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: imageProvider,
                                  ),
                                ),
                              ),
                              placeholder: (context, url) => Shimmer(
                                  child: Container(
                                    color: thirdColor,
                                  ),
                                  gradient: LinearGradient(stops: [
                                    0.4,
                                    0.5,
                                    0.6
                                  ], colors: [
                                    secondaryColor,
                                    thirdColor,
                                    secondaryColor
                                  ])),
                              errorWidget: (context, url, error) =>
                                  Icon(Icons.error),
                            ),
                          ],
                        )),
                    SizedBox(width: getProportionateScreenWidth(10)),
                    Flexible(
                        flex: 80,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              dataHistoryTransactionModel.title ?? ' ',
                              style: primaryTextStyle.copyWith(
                                letterSpacing: 0.5,
                                fontWeight: reguler,
                                fontSize: getProportionateScreenWidth(11),
                              ),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ],
                        )),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(height: 16),
          Container(
            margin: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(8)),
            child: Divider(
              color: Color(0xff2D2D2D),
              thickness: 0.5,
            ),
          ),
          SizedBox(height: 16),
          Container(
            margin: EdgeInsets.only(
              left: getProportionateScreenWidth(12),
              right: getProportionateScreenWidth(20),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      "Jenis Pembayaran",
                      style: primaryTextStyle.copyWith(
                        fontWeight: reguler,
                        fontSize: getProportionateScreenWidth(12),
                        letterSpacing: 0.5,
                      ),
                    ),
                    Spacer(),
                    Text(
                      dataHistoryTransactionModel.paymentType!.toUpperCase(),
                      style: primaryTextStyle.copyWith(
                        fontWeight: reguler,
                        color: secondaryColor,
                        fontSize: getProportionateScreenWidth(12),
                        letterSpacing: 0.5,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 8),
                Row(
                  children: [
                    Text(
                      "Total Pembelian",
                      style: primaryTextStyle.copyWith(
                        fontWeight: reguler,
                        fontSize: getProportionateScreenWidth(12),
                        letterSpacing: 0.5,
                      ),
                    ),
                    Spacer(),
                    Text(
                      "Rp. ${dataHistoryTransactionModel.totalPrice}",
                      style: primaryTextStyle.copyWith(
                        fontWeight: reguler,
                        color: primaryColor,
                        fontSize: getProportionateScreenWidth(12),
                        letterSpacing: 0.5,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 15),
              ],
            ),
          ),
          // Container(
          //   margin: EdgeInsets.only(
          //     left: getProportionateScreenWidth(16),
          //     right: getProportionateScreenWidth(16),
          //     top: getProportionateScreenWidth(10),
          //   ),
          //   child: Column(
          //     children: [
          //       Row(
          //         crossAxisAlignment: CrossAxisAlignment.start,
          //         children: [
          //           Flexible(
          //               flex: 11,
          //               child: Column(
          //                 mainAxisAlignment: MainAxisAlignment.start,
          //                 crossAxisAlignment: CrossAxisAlignment.start,
          //                 children: [
          //                   Container(
          //                     width: getProportionateScreenWidth(156),
          //                     height: getProportionateScreenWidth(88),
          //                     decoration: BoxDecoration(
          //                       borderRadius: BorderRadius.circular(5),
          //                       image: DecorationImage(
          //                         fit: BoxFit.fill,
          //                         image: AssetImage(
          //                             'assets/images/course_thumbnail_default_13 1.png'),
          //                       ),
          //                     ),
          //                   ),
          //                   SizedBox(
          //                     height: getProportionateScreenHeight(8),
          //                   ),
          //                 ],
          //               )),
          //           SizedBox(
          //             width: getProportionateScreenWidth(9),
          //           ),
          //           Flexible(
          //             flex: 10,
          //             child: Column(
          //               crossAxisAlignment: CrossAxisAlignment.start,
          //               children: [
          //                 Text(
          //                   '428 Menit Menjadi Pengusaha Sukses',
          //                   style: secondaryTextStyle.copyWith(
          //                     letterSpacing: 1,
          //                     fontWeight: semiBold,
          //                     fontSize: SizeConfig.blockHorizontal! * 3.5,
          //                   ),
          //                   maxLines: 2,
          //                   overflow: TextOverflow.ellipsis,
          //                 ),
          //                 SizedBox(
          //                   height: getProportionateScreenWidth(8),
          //                 ),
          //                 Text(
          //                   'Oleh Farid Subkhan',
          //                   style: primaryTextStyle.copyWith(
          //                     color: secondaryColor,
          //                     fontSize: SizeConfig.blockHorizontal! * 3,
          //                   ),
          //                 ),
          //                 SizedBox(
          //                   height: getProportionateScreenWidth(6),
          //                 ),
          //                 Row(
          //                   children: [
          //                     RatingBarIndicator(
          //                       itemSize: SizeConfig.blockHorizontal! * 3.5,
          //                       rating: 0,
          //                       direction: Axis.horizontal,
          //                       itemCount: 5,
          //                       //itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
          //                       itemBuilder: (context, _) => Icon(
          //                         Icons.star_outline_rounded,
          //                         color: Colors.amber,
          //                       ),
          //                     ),
          //                     SizedBox(
          //                       width: getProportionateScreenWidth(5),
          //                     ),
          //                     Text(
          //                       'Edit Penilaian',
          //                       style: primaryTextStyle.copyWith(
          //                         color: primaryColor,
          //                         fontSize: SizeConfig.blockHorizontal! * 2.5,
          //                       ),
          //                     )
          //                   ],
          //                 ),
          //               ],
          //             ),
          //           ),
          //         ],
          //       ),
          //       Stack(
          //         children: [
          //           Container(
          //             width: double.infinity,
          //             height: 4,
          //             decoration: BoxDecoration(
          //                 borderRadius: BorderRadius.circular(10),
          //                 color: Colors.white),
          //           ),
          //           Container(
          //             width: (SizeConfig.screenWidth -
          //                     getProportionateScreenWidth(32)) *
          //                 0.45,
          //             height: getProportionateScreenWidth(4),
          //             decoration: BoxDecoration(
          //                 borderRadius: BorderRadius.circular(10),
          //                 color: primaryColor),
          //           ),
          //         ],
          //       ),
          //       SizedBox(height: getProportionateScreenWidth(6)),
          //       Align(
          //         alignment: Alignment.topLeft,
          //         child: Text(
          //           '45% Lengkap',
          //           style: primaryTextStyle.copyWith(
          //             fontSize: SizeConfig.blockHorizontal! * 2.7,
          //             letterSpacing: 0.2,
          //           ),
          //         ),
          //       ),
          //       Row(children: [
          //         ElevatedButton(
          //           onPressed: () {
          //             Navigator.push(
          //               context,
          //               MaterialPageRoute(
          //                 builder: (context) => PlayCourse(),
          //               ),
          //             );
          //           },
          //           child: Text(
          //             'Mulai Kursus',
          //             style: thirdTextStyle.copyWith(color: Colors.black),
          //           ),
          //           style: ElevatedButton.styleFrom(
          //             minimumSize: Size(getProportionateScreenWidth(90),
          //                 getProportionateScreenHeight(33)),
          //             primary: primaryColor,
          //           ),
          //         ),
          //         SizedBox(
          //           width: getProportionateScreenWidth(10),
          //         ),
          //         OutlinedButton(
          //           onPressed: () {
          //             Navigator.push(
          //               context,
          //               MaterialPageRoute(
          //                 builder: (context) => DataDiri(),
          //               ),
          //             );
          //           },
          //           child: Text('Detail Kursus'),
          //           style: OutlinedButton.styleFrom(
          //             minimumSize: Size(getProportionateScreenWidth(90),
          //                 getProportionateScreenHeight(33)),
          //             primary: primaryColor,
          //             textStyle: thirdTextStyle,
          //             side: BorderSide(color: primaryColor),
          //           ),
          //         ),
          //       ]),
          //       SizedBox(
          //         height: getProportionateScreenHeight(10),
          //       ),
          //     ],
          //   ),
          // ),
          // Divider(
          //   //thickness: 8,
          //   color: fourthColor,
          // ),
        ],
      ),
    );
  }
}
