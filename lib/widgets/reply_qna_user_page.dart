import 'package:flutter/material.dart';
import 'package:initial_folder/providers/reply_qna_provider.dart';
import 'package:initial_folder/widgets/reply_qna_user.dart';
import 'package:provider/provider.dart';

import '../theme.dart';

class ReplyQnaUserPage extends StatefulWidget {
  const ReplyQnaUserPage(
      {Key? key, required this.idCourse, required this.index})
      : super(key: key);
  final idCourse;
  final int index;

  @override
  State<ReplyQnaUserPage> createState() => _ReplyQnaUserPageState();
}

class _ReplyQnaUserPageState extends State<ReplyQnaUserPage> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) =>
          ReplyQnaProvider(idCourse: widget.idCourse, index: widget.index),
      child: Consumer<ReplyQnaProvider>(builder: (context, state, _) {
        if (state.state == ResultState.loading) {
          return Center(
            child: CircularProgressIndicator(
              color: primaryColor,
              strokeWidth: 2,
            ),
          );
        } else if (state.state == ResultState.noData) {
          return Center(
            child: Text(
              'belum ada balasan',
              style: thirdTextStyle,
            ),
          );
        } else if (state.state == ResultState.hasData) {
          return ListView.builder(
              itemCount: state.result!.data[0][widget.index].comment.length,
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                var comment =
                    state.result!.data[0][widget.index].comment[index];
                var qnauser = state.result!.data[0][index];
                return ReplyQnaUser(
                  divider: Divider(),
                  comment: comment,
                  qnaDataModel: qnauser,
                );
              });
        } else if (state.state == ResultState.error) {
          return Center(
              child: Column(
            children: [
              Text(
                'Terjadi Kesalahan Coba Lagi',
                style: thirdTextStyle,
              ),
            ],
          ));
        }
        return Center(
          child: Text(
            'Terjadi Kesalahan',
            style: thirdTextStyle,
          ),
        );
      }),
    );
  }
}
