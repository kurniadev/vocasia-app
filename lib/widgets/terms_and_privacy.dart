import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:initial_folder/theme.dart';
import 'package:webview_flutter/webview_flutter.dart';

class TermsAndCondition extends StatefulWidget {
  static const routeName = '/article_web';

  final String url;
  final String id;

  const TermsAndCondition({Key? key, required this.url, required this.id})
      : super(key: key);

  @override
  State<TermsAndCondition> createState() => _TermsAndConditionState();
}

class _TermsAndConditionState extends State<TermsAndCondition> {
  double progres = 0;
  @override
  Widget build(BuildContext context) {
    final juduls = {
      'sk': Text('Syarat dan Ketentuan'),
      'prv': Text('Kebijakan Privasi'),
      'about': Text('Tentang Vocasia'),
      'ctc': Text('Kontak Kami'),
      'help': Text('Bantuan'),
    };
    final judul = juduls[widget.id];
    return Scaffold(
      appBar: AppBar(title: judul),
      body: Column(
        children: [
          LinearProgressIndicator(
            value: progres,
            color: sevenColor,
            backgroundColor: secondaryColor,
          ),
          Expanded(
            child: WebView(
              javascriptMode: JavascriptMode.unrestricted,
              onProgress: (progres) =>
                  setState(() => this.progres = progres / 100),
              initialUrl: widget.url,
            ),
          ),
        ],
      ),
    );
  }
}
