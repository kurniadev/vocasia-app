import 'package:flutter/material.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';

class LoadingMyCourse extends StatelessWidget {
  const LoadingMyCourse({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(
            left: getProportionateScreenWidth(16),
            right: getProportionateScreenWidth(16),
            top: getProportionateScreenWidth(10),
          ),
          child: Column(
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                      flex: 11,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: getProportionateScreenWidth(156),
                            height: getProportionateScreenWidth(88),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: secondaryColor),
                          ),
                          SizedBox(
                            height: getProportionateScreenHeight(8),
                          ),
                        ],
                      )),
                  SizedBox(
                    width: getProportionateScreenWidth(9),
                  ),
                  Flexible(
                    flex: 10,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          height: 30,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: secondaryColor),
                        ),
                        SizedBox(
                          height: getProportionateScreenWidth(8),
                        ),
                        Container(
                          height: 8,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: secondaryColor),
                        ),
                        SizedBox(
                          height: getProportionateScreenWidth(6),
                        ),
                        Row(
                          children: [
                            Container(
                              height: 10,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: secondaryColor),
                            ),
                            SizedBox(
                              width: getProportionateScreenWidth(5),
                            ),
                            Container(
                              height: 10,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5),
                                  color: secondaryColor),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Container(
                height: getProportionateScreenWidth(4),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: secondaryColor),
              ),
              SizedBox(height: getProportionateScreenWidth(6)),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  width: 30,
                  height: 10,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: secondaryColor),
                ),
              ),
              Row(children: [
                Container(
                  height: 20,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: secondaryColor),
                ),
                SizedBox(
                  width: getProportionateScreenWidth(10),
                ),
                Container(
                  height: 20,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: secondaryColor),
                ),
              ]),
            ],
          ),
        ),
        Divider(
          color: fourthColor,
        ),
      ],
    );
  }
}
