import 'package:flutter/material.dart';
import 'package:initial_folder/models/counter_qna_comment_model.dart';

import '../size_config.dart';
import '../theme.dart';

class CounterQnaComment extends StatelessWidget {
  const CounterQnaComment({Key? key, required this.counterComment})
      : super(key: key);

  final String counterComment;
  @override
  Widget build(BuildContext context) {
    return Text(
      '${counterComment}',
      style: secondaryTextStyle.copyWith(
          fontSize: getProportionateScreenWidth(10), letterSpacing: 0.3),
    );
  }
}
