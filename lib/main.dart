import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:initial_folder/providers/auth_provider.dart';
import 'package:initial_folder/providers/banners_provider.dart';
import 'package:initial_folder/providers/cart_provider.dart';
import 'package:initial_folder/providers/carts_provider.dart';
import 'package:initial_folder/providers/categories_provider.dart';
import 'package:initial_folder/providers/checkbox_provider.dart';
import 'package:initial_folder/providers/data_diri_provider.dart';
import 'package:initial_folder/providers/filters_course_provider.dart';
import 'package:initial_folder/providers/latest_course_provider.dart';
import 'package:initial_folder/providers/my_course_provider.dart';
import 'package:initial_folder/providers/others_course_provider.dart';
import 'package:initial_folder/providers/payments_provider.dart';
import 'package:initial_folder/providers/play_video_course_provider.dart';
import 'package:initial_folder/providers/posting_qna_provider.dart';
import 'package:initial_folder/providers/posting_qna_reply_provider.dart';
import 'package:initial_folder/providers/profile_image_provider.dart';
import 'package:initial_folder/providers/search_provider.dart';
import 'package:initial_folder/providers/section_lesson_provider.dart';
import 'package:initial_folder/providers/tab_play_course_provider.dart';
import 'package:initial_folder/providers/description_provider.dart';
import 'package:initial_folder/providers/google_sign_in.dart';
import 'package:initial_folder/providers/metode_provider.dart';
import 'package:initial_folder/providers/page_provider.dart';
import 'package:initial_folder/providers/profile_provider.dart';
import 'package:initial_folder/providers/tab_provider.dart';
import 'package:initial_folder/providers/top_course_provider.dart';
import 'package:initial_folder/providers/update_data_diri_provider.dart';
import 'package:initial_folder/providers/update_password_provider.dart';
import 'package:initial_folder/providers/user_info_provider.dart';
import 'package:initial_folder/providers/whislist_provider.dart';
import 'package:initial_folder/providers/wishlist_post_provider.dart';
import 'package:initial_folder/screens/splash/splash_screen_login.dart';
import 'package:initial_folder/services/banners_service.dart';
import 'package:initial_folder/services/cart_service.dart';
import 'package:initial_folder/services/categories_service.dart';
import 'package:initial_folder/services/course_service.dart';
import 'package:initial_folder/services/payment_service.dart';
import 'package:initial_folder/services/search_service.dart';
import 'package:initial_folder/services/user_info_service.dart';
import 'package:initial_folder/theme.dart';
import 'package:provider/provider.dart';
import 'package:initial_folder/routes.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:responsive_framework/responsive_framework.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => TabProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => PostingQnaReplyProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => PostingQnaProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => UpdateDataDiriProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => UpdatePasswordProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => TabPlayCourseProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => SectionLessonProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => PageProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => ProfileImageProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => WishlistPostProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => ProfileProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => CheckboxProvider(),
        ),
        ChangeNotifierProvider(
            create: (context) => UserInfoProvider(
                  userInfoService: UserInfoService(),
                )),
        ChangeNotifierProvider(
          create: (context) => DescriptionProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => MetodeProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => GoogleSignInProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => AuthProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) =>
              CategoriesProvider(categoriesService: CategoriesService()),
        ),
        ChangeNotifierProvider(
          create: (context) =>
              BannersProvider(bannersService: BannersService()),
        ),
        ChangeNotifierProvider(
          create: (context) =>
              OthersCourseProvider(otherCourseService: CourseService()),
        ),
        ChangeNotifierProvider(
          create: (context) => CartsProvider(cartService: CartService()),
        ),
        ChangeNotifierProvider(
          create: (context) => CartProvider(cartService: CartService()),
        ),
        ChangeNotifierProvider<WishlistProvider>(
          create: (context) => WishlistProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => PaymentsProvider(
            paymentServices: PaymentServices(),
          ),
        ),
        ChangeNotifierProvider(
          create: (context) => PlayVideoCourseProvider(),
        ),
        ChangeNotifierProvider(
          create: (context) => MyCourseProvider(courseService: CourseService()),
        ),
        ChangeNotifierProvider(
          create: (context) =>
              FilterCourseProvider(searchService: SearchService()),
        ),
        ChangeNotifierProvider(
          create: (context) => SearchProvider(searchService: SearchService()),
        ),
        ChangeNotifierProvider(
          create: (context) =>
              TopCourseProvider(courseService: CourseService()),
        ),
        ChangeNotifierProvider(
          create: (context) =>
              LatestCourseProvider(courseService: CourseService()),
        )
      ],
      child: MaterialApp(
        builder: (context, widget) => ResponsiveWrapper.builder(
          ClampingScrollWrapper.builder(context, widget!),
          maxWidth: 1200,
          minWidth: 240,
          defaultScale: true,
          breakpoints: [
            ResponsiveBreakpoint.resize(240, name: MOBILE),
            ResponsiveBreakpoint.autoScale(800, name: TABLET),
            ResponsiveBreakpoint.resize(1000, name: DESKTOP),
          ],
        ),
        debugShowCheckedModeBanner: false,
        title: 'Vocasia',
        theme: ThemeData.dark().copyWith(
          bottomNavigationBarTheme:
              BottomNavigationBarThemeData(backgroundColor: backgroundColor),
          scaffoldBackgroundColor: backgroundColor,
          appBarTheme:
              AppBarTheme(backgroundColor: backgroundColor, elevation: 0),
          textButtonTheme: TextButtonThemeData(
              style: TextButton.styleFrom(primary: primaryColor)),
        ),
        home: SplashScreenLogin(),
        routes: routes,
      ),
    );
  }
}
