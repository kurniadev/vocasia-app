import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

class UsersInfo {
  Future setToken(String? value) async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.setString("token", value!);
  }

  Future setEmail(String? value) async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.setString("email", value!);
  }

  Future setIdUser(int? value) async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.setInt('idUser', value!);
  }

  setListData(String key, List<String> value) async {
    SharedPreferences myPrefs = await SharedPreferences.getInstance();
    myPrefs.setStringList(key, value);
  }

  Future<List<String>?> getListData(String key) async {
    SharedPreferences myPrefs = await SharedPreferences.getInstance();
    return myPrefs.getStringList(key);
  }

  Future<String?> getToken() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString("token");
  }

  Future<String?> getEmail() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString("email");
  }

  Future<int?> getIdUser() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getInt("idUser");
  }

  Future logout() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    pref.clear();
  }
}
