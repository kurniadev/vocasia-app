class ProductProgram {
  final int id;
  final List<String> images;

  ProductProgram({
    required this.id,
    required this.images,
  });
}

// Our demo Products

List<ProductProgram> demoProducts = [
  ProductProgram(
    id: 1,
    images: [
      "assets/images/workshop.png",
    ],
  ),
  ProductProgram(
    id: 2,
    images: [
      "assets/images/prakerja.png",
    ],
  ),
  ProductProgram(
    id: 3,
    images: [
      "assets/images/workshop.png",
    ],
  ),
  ProductProgram(
    id: 4,
    images: [
      "assets/images/prakerja.png",
    ],
  ),
];
