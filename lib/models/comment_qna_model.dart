class Comment {
  Comment({
    this.id_rep,
    this.sender,
    this.username,
    this.text_rep,
    this.foto_profile,
    this.create_at,
  });

  String? id_rep;
  String? sender;
  String? username;
  String? text_rep;
  String? foto_profile;
  String? create_at;

  factory Comment.fromJson(Map<String, dynamic> json) => Comment(
        id_rep: json["id_rep"],
        sender: json["sender"],
        username: json["username"],
        text_rep: json["text_rep"],
        foto_profile: json["foto_profile"],
        create_at: json["create_at"],
      );

  Map<String, dynamic> toJson() => {
        "id_rep": id_rep,
        "sender": sender,
        "username": username,
        "text_rep": text_rep,
        "foto_profile": foto_profile,
        "create_at": create_at,
      };
}
