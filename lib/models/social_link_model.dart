class SocialLink {
  SocialLink({
    this.facebook,
  });

  String? facebook;

  factory SocialLink.fromJson(Map<String, dynamic> json) => SocialLink(
        facebook: json["facebook"],
      );

  Map<String, dynamic> toJson() => {
        "facebook": facebook,
      };
}
