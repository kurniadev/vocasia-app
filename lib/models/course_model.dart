import 'package:initial_folder/models/rating_course_model.dart';

class CourseModel {
  CourseModel({
    this.idCourse = '',
    this.instructorId = '',
    this.title = '',
    this.price = '',
    this.instructorName = '',
    this.discountFlag,
    this.discountPrice = '',
    this.thumbnail,
    this.students,
    required this.rating,
    this.totalDiscount,
    this.fotoProfile,
    this.topCourse,
  });

  String idCourse;
  String instructorId;
  String title;
  String price;
  String instructorName;
  String? discountFlag;
  String discountPrice;
  String? thumbnail;
  String? students;
  List<Rating?> rating;
  int? totalDiscount;
  String? fotoProfile;
  String? topCourse;

  factory CourseModel.fromJson(Map<String, dynamic> json) => CourseModel(
        idCourse: json["id_course"],
        instructorId: json["instructor_id"],
        title: json["title"],
        price: json["price"],
        instructorName: json["instructor_name"],
        discountFlag: json["discount_flag"],
        discountPrice: json["discount_price"],
        thumbnail: json["thumbnail"],
        students: json["students"],
        rating: List<Rating>.from(json["rating"].map((x) => Rating.fromJson(x)))
            .toList(),
        totalDiscount: json["total_discount"],
        fotoProfile: json["foto_profile"],
        topCourse: json["top_course"],
      );

  Map<String, dynamic> toJson() => {
        "id_course": idCourse,
        "instructor_id": instructorId,
        "title": title,
        "price": price,
        "instructor_name": instructorName,
        "discount_flag": discountFlag,
        "discount_price": discountPrice,
        "thumbnail": thumbnail,
        "students": students,
        "rating": List<dynamic>.from(rating.map((x) => x!.toJson())).toList(),
        "total_discount": totalDiscount,
        "foto_profile": fotoProfile,
        "top_course": topCourse,
      };
}
