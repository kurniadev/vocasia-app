import 'package:initial_folder/models/rating_course_model.dart';
// To parse this JSON data, do
//
//     final detailCourseModel = detailCourseModelFromJson(jsonString);

import 'dart:convert';

DetailCourseModel detailCourseModelFromJson(String str) =>
    DetailCourseModel.fromJson(json.decode(str));

String detailCourseModelToJson(DetailCourseModel data) =>
    json.encode(data.toJson());

class DetailCourseModel {
  DetailCourseModel({
    this.status,
    this.error,
    required this.data,
  });

  final int? status;
  final bool? error;
  final List<List<DataDetailCourseModel>> data;

  factory DetailCourseModel.fromJson(Map<String, dynamic> json) =>
      DetailCourseModel(
        status: json["status"],
        error: json["error"],
        data: List<List<DataDetailCourseModel>>.from(json["data"].map((x) =>
            List<DataDetailCourseModel>.from(
                x.map((x) => DataDetailCourseModel.fromJson(x))))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "data": List<dynamic>.from(
            data.map((x) => List<dynamic>.from(x.map((x) => x.toJson())))),
      };
}

class DataDetailCourseModel {
  DataDetailCourseModel({
    required this.id,
    this.title,
    this.instructorId,
    this.instructor,
    this.shortDescription,
    this.levelCourse,
    this.totalLesson,
    this.totalStudents,
    this.description,
    this.outcome,
    this.requirement,
    this.price,
    this.discountPrice,
    this.discountFlag,
    this.videoUrl,
    this.totalDuration,
    this.bio,
    required this.rating,
    this.totalDiscount,
    this.lastModified,
    this.fotoProfile,
    this.thumbnail,
    this.isFreeCourse,
    this.isMine,
    required this.breadcrumbs,
  });

  final String id;
  final String? title;
  final String? instructorId;
  final String? instructor;
  final String? shortDescription;
  final String? levelCourse;
  final String? totalLesson;
  final String? totalStudents;
  final String? description;
  final String? outcome;
  final String? requirement;
  final String? price;
  final String? discountPrice;
  final String? discountFlag;
  final String? videoUrl;
  final String? totalDuration;
  final String? bio;
  final List<Rating> rating;
  final int? totalDiscount;
  final String? lastModified;
  final dynamic fotoProfile;
  final String? thumbnail;
  final String? isFreeCourse;
  final dynamic isMine;
  final Breadcrumbs breadcrumbs;

  factory DataDetailCourseModel.fromJson(Map<String, dynamic> json) =>
      DataDetailCourseModel(
        id: json["id"],
        title: json["title"],
        instructorId: json["instructor_id"],
        instructor: json["instructor"],
        shortDescription: json["short_description"],
        levelCourse: json["level_course"],
        totalLesson: json["total_lesson"],
        totalStudents: json["total_students"],
        description: json["description"],
        outcome: json["outcome"],
        requirement: json["requirement"],
        price: json["price"],
        discountPrice: json["discount_price"],
        discountFlag: json["discount_flag"],
        videoUrl: json["video_url"],
        totalDuration: json["total_duration"],
        bio: json["bio"],
        rating:
            List<Rating>.from(json["rating"].map((x) => Rating.fromJson(x))),
        totalDiscount: json["total_discount"],
        lastModified: json["last_modified"],
        fotoProfile: json["foto_profile"],
        thumbnail: json["thumbnail"],
        isFreeCourse: json["is_free_course"],
        isMine: json["is_mine"],
        breadcrumbs: Breadcrumbs.fromJson(json["breadcrumbs"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "instructor_id": instructorId,
        "instructor": instructor,
        "short_description": shortDescription,
        "level_course": levelCourse,
        "total_lesson": totalLesson,
        "total_students": totalStudents,
        "description": description,
        "outcome": outcome,
        "requirement": requirement,
        "price": price,
        "discount_price": discountPrice,
        "discount_flag": discountFlag,
        "video_url": videoUrl,
        "total_duration": totalDuration,
        "bio": bio,
        "rating": List<dynamic>.from(rating.map((x) => x.toJson())),
        "total_discount": totalDiscount,
        "last_modified": lastModified,
        "foto_profile": fotoProfile,
        "thumbnail": thumbnail,
        "is_free_course": isFreeCourse,
        "is_mine": isMine,
        "breadcrumbs": breadcrumbs.toJson(),
      };
}

class Breadcrumbs {
  Breadcrumbs({
    this.idCategory,
    this.parentName,
    this.subCategoryId,
    this.subParentCategory,
  });

  final String? idCategory;
  final String? parentName;
  final String? subCategoryId;
  final String? subParentCategory;

  factory Breadcrumbs.fromJson(Map<String, dynamic> json) => Breadcrumbs(
        idCategory: json["id_category"],
        parentName: json["parent_name"],
        subCategoryId: json["sub_category_id"],
        subParentCategory: json["sub_parent_category"],
      );

  Map<String, dynamic> toJson() => {
        "id_category": idCategory,
        "parent_name": parentName,
        "sub_category_id": subCategoryId,
        "sub_parent_category": subParentCategory,
      };
}
