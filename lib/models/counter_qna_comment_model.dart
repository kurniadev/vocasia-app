class CounterCommentModel {
  CounterCommentModel({
    this.status,
    this.error,
    required this.data,
  });

  final int? status;
  final bool? error;
  String? data;

  factory CounterCommentModel.fromJson(Map<String, dynamic> json) =>
      CounterCommentModel(
        status: json["status"],
        error: json["error"],
        data: (json["data"]["count comment "]).toString(),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "data": data,
      };
}

class CounterComment {
  CounterComment({
    this.countCommentQna,
  });

  String? countCommentQna;

  factory CounterComment.fromJson(Map<String, dynamic> json) => CounterComment(
        countCommentQna: json["count comment"],
      );

  Map<String, dynamic> toJson() => {
        "count comment": countCommentQna,
      };
}
