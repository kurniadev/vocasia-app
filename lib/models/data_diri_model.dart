class DataDiriModel {
  DataDiriModel({
    this.status,
    this.error,
    required this.data,
  });

  int? status;
  bool? error;
  List<DataOfDataDiriModel> data;

  factory DataDiriModel.fromJson(Map<String, dynamic> json) => DataDiriModel(
        status: json["status"],
        error: json["error"],
        data: List<DataOfDataDiriModel>.from(
            json["data"].map((x) => DataOfDataDiriModel.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataOfDataDiriModel {
  DataOfDataDiriModel({
    this.idUser,
    this.fullname,
    this.biography,
    this.datebirth,
    this.phone,
    this.gender,
    this.heading,
    this.socialLink,
  });

  String? idUser;
  String? fullname;
  String? biography;
  String? datebirth;
  String? phone;
  String? gender;
  String? heading;
  SocialLink? socialLink;

  factory DataOfDataDiriModel.fromJson(Map<String, dynamic> json) =>
      DataOfDataDiriModel(
        idUser: json["id_user"],
        fullname: json["fullname"],
        biography: json["biography"],
        datebirth: json["datebirth"],
        phone: json["phone"],
        gender: json["jenis_kelamin"],
        heading: json["heading"],
        socialLink: json["social_link"] == null
            ? null
            : SocialLink.fromJson(json["social_link"]),
      );

  Map<String, dynamic> toJson() => {
        "id_user": idUser,
        "fullname": fullname,
        "biography": biography,
        "datebirth": datebirth,
        "phone": phone,
        "jenis_kelamin": gender,
        "heading": heading,
        "social_link": socialLink == null ? null : socialLink!.toJson(),
      };
}

class SocialLink {
  SocialLink({
    this.facebook,
    this.twitter,
    this.instagram,
    this.linkedin,
  });

  String? facebook;
  String? twitter;
  String? instagram;
  String? linkedin;

  factory SocialLink.fromJson(Map<String, dynamic> json) => SocialLink(
        facebook: json["facebook"],
        twitter: json["twitter"],
        instagram: json["instagram"],
        linkedin: json["linkedin"],
      );

  Map<String, dynamic> toJson() => {
        "facebook": facebook,
        "twitter": twitter,
        "instagram": instagram,
        "linkedin": linkedin,
      };
}
