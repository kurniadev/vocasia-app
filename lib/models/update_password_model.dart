class UpdatePasswordModel {
  UpdatePasswordModel({
    this.status,
    this.error = false,
    required this.data,
  });

  final int? status;
  final bool error;
  final List<Data> data;

  factory UpdatePasswordModel.fromJson(Map<String, dynamic> json) =>
      UpdatePasswordModel(
        status: json["status"],
        error: json["error"],
        data: List<Data>.from(json["data"].map((x) => Data.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Data {
  Data({
    this.message,

    //required this.social_link,
  });

  final String? message;
  //final List<SocialLink?> social_link;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        message: json["message"],
        //social_link: List<SocialLink>.from(
        //json["social_link"].map((x) => SocialLink.fromJson(x))).toList(),
      );

  Map<String, dynamic> toJson() => {
        "message": message,
        //"social_link":
        //    List<dynamic>.from(social_link.map((x) => x!.toJson())).toList(),
      };
}
