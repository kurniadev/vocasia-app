class CategoriesModel {
  CategoriesModel({
    this.id,
    this.nameCategory,
    this.slugCategory,
    this.parentCategory,
    this.fontAwesomeClass,
  });

  final String? id;
  final String? nameCategory;
  final String? slugCategory;
  final String? parentCategory;
  final String? fontAwesomeClass;

  factory CategoriesModel.fromJson(Map<String, dynamic> json) =>
      CategoriesModel(
        id: json["id"],
        nameCategory: json["name_category"],
        slugCategory: json["slug_category"],
        parentCategory: json["parent_category"],
        fontAwesomeClass: json["font_awesome_class"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name_category": nameCategory,
        "slug_category": slugCategory,
        "parent_category": parentCategory,
        "font_awesome_class": fontAwesomeClass,
      };
}
