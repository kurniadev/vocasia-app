class BannersModel {
  BannersModel({
    this.id,
    this.status,
    this.img = '',
  });
  String? id;
  String? status;
  late String img;

  BannersModel.fromJson(Map<String, dynamic> json) {
    id = json["id"];
    status = json["status"];
    img = json["img"];
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'status': status,
        'img': img,
      };
}
