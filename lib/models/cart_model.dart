class CartModel {
  CartModel({
    this.status,
    this.error,
    required this.data,
  });

  final int? status;
  final bool? error;
  final Data data;

  factory CartModel.fromJson(Map<String, dynamic> json) => CartModel(
        status: json["status"],
        error: json["error"],
        data: Data.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "data": data.toJson(),
      };
}

class Data {
  Data({
    this.messages,
  });

  final String? messages;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        messages: json["messages"],
      );

  Map<String, dynamic> toJson() => {
        "messages": messages,
      };
}
