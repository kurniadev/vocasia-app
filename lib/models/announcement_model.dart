class AnnouncementModel {
  AnnouncementModel({
    this.status,
    this.error,
    required this.data,
  });

  final int? status;
  final bool? error;
  final List<List<AnnouncementDataModel>> data;

  factory AnnouncementModel.fromJson(Map<String, dynamic> json) =>
      AnnouncementModel(
        status: json["status"],
        error: json["error"],
        data: List<List<AnnouncementDataModel>>.from(json["data"].map((x) =>
            List<AnnouncementDataModel>.from(
                x.map((x) => AnnouncementDataModel.fromJson(x))))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "data": List<dynamic>.from(
            data.map((x) => List<dynamic>.from(x.map((x) => x.toJson())))),
      };
}

class AnnouncementDataModel {
  AnnouncementDataModel({
    this.idAnnouncement,
    this.tokenAnnouncement,
    this.instructorName,
    this.bodyContent,
    this.likes,
    this.comment,
    this.date,
    required this.replies,
  });

  final String? idAnnouncement;
  final String? tokenAnnouncement;
  final String? instructorName;
  final String? bodyContent;
  final dynamic likes;
  final int? comment;
  final String? date;
  final List<dynamic> replies;

  factory AnnouncementDataModel.fromJson(Map<String, dynamic> json) =>
      AnnouncementDataModel(
        idAnnouncement: json["id_announcement"],
        tokenAnnouncement: json["token_announcement"],
        instructorName: json["instructor_name"],
        bodyContent: json["body_content"],
        likes: json["likes"],
        comment: json["comment"],
        date: json["date"],
        replies: List<dynamic>.from(json["replies"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id_announcement": idAnnouncement,
        "token_announcement": tokenAnnouncement,
        "instructor_name": instructorName,
        "body_content": bodyContent,
        "likes": likes,
        "comment": comment,
        "date": date,
        "replies": List<dynamic>.from(replies.map((x) => x)),
      };
}

class AnnouncementLikeModel {
  AnnouncementLikeModel({
    this.userLikes,
  });

  final String? userLikes;

  factory AnnouncementLikeModel.fromJson(Map<String, dynamic> json) =>
      AnnouncementLikeModel(
        userLikes: json["user_likes"],
      );

  Map<String, dynamic> toJson() => {
        "user_likes": userLikes,
      };
}
