class RatingCourseDetailModel {
  RatingCourseDetailModel({
    this.status,
    this.error,
    required this.data,
    required this.dataReview,
  });

  final int? status;
  final bool? error;
  final Data data;
  final List<DataReview> dataReview;

  factory RatingCourseDetailModel.fromJson(Map<String, dynamic> json) =>
      RatingCourseDetailModel(
        status: json["status"],
        error: json["error"],
        data: Data.fromJson(json["data"]),
        dataReview: List<DataReview>.from(
            json["data_review"].map((x) => DataReview.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "data": data.toJson(),
        "data_review": List<dynamic>.from(dataReview.map((x) => x.toJson())),
      };
}

class Data {
  Data({
    this.avgRating,
    required this.precentageRating,
  });

  final dynamic avgRating;
  final PrecentageRating precentageRating;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        avgRating: json["avg_rating"],
        precentageRating: PrecentageRating.fromJson(json["precentage_rating"]),
      );

  Map<String, dynamic> toJson() => {
        "avg_rating": avgRating,
        "precentage_rating": precentageRating.toJson(),
      };
}

class PrecentageRating {
  PrecentageRating({
    this.rating1,
    this.rating2,
    this.rating3,
    this.rating4,
    this.rating5,
  });

  final dynamic rating1;
  final dynamic rating2;
  final dynamic rating3;
  final dynamic rating4;
  final dynamic rating5;

  factory PrecentageRating.fromJson(Map<String, dynamic> json) =>
      PrecentageRating(
        rating1: json["rating_1"],
        rating2: json["rating_2"],
        rating3: json["rating_3"],
        rating4: json["rating_4"],
        rating5: json["rating_5"],
      );

  Map<String, dynamic> toJson() => {
        "rating_1": rating1,
        "rating_2": rating2,
        "rating_3": rating3,
        "rating_4": rating4,
        "rating_5": rating5,
      };
}

class DataReview {
  DataReview({
    this.name,
    this.review,
    this.rating,
    this.date,
  });

  final String? name;
  final String? review;
  final String? rating;
  final String? date;

  factory DataReview.fromJson(Map<String, dynamic> json) => DataReview(
        name: json["name"],
        review: json["review"],
        rating: json["rating"],
        date: json["date"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
        "review": review,
        "rating": rating,
        "date": date,
      };
}
