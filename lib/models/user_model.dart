class UserModel {
  UserModel({
    this.messages,
    this.token,
    this.expireAt,
  });

  final String? messages;
  String? token;
  final int? expireAt;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        messages: json["messages"],
        token: json["token"],
        expireAt: json["expire_at"],
      );

  Map<String, dynamic> toJson() => {
        "messages": messages,
        "token": token,
        "expire_at": expireAt,
      };
}
