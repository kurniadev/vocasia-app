class SectionLessonModel {
  SectionLessonModel({
    this.status,
    this.error,
    this.data,
  });

  final int? status;
  final bool? error;
  final List<List<SectionLessonList>>? data;

  factory SectionLessonModel.fromJson(Map<String, dynamic> json) =>
      SectionLessonModel(
        status: json["status"],
        error: json["error"],
        data: List<List<SectionLessonList>>.from(json["data"].map((x) =>
            List<SectionLessonList>.from(
                x.map((x) => SectionLessonList.fromJson(x))))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "data": List<dynamic>.from(
            data!.map((x) => List<dynamic>.from(x.map((x) => x.toJson())))),
      };
}

class SectionLessonList {
  SectionLessonList({
    this.title,
    this.duration,
    this.dataLesson,
  });

  final String? title;
  final String? duration;
  final List<List<DataLesson>>? dataLesson;

  factory SectionLessonList.fromJson(Map<String, dynamic> json) =>
      SectionLessonList(
        title: json["title"],
        duration: json["duration"] == null ? null : json["duration"],
        dataLesson: List<List<DataLesson>>.from(json["data_lesson"].map((x) =>
            List<DataLesson>.from(x.map((x) => DataLesson.fromJson(x))))),
      );

  Map<String, dynamic> toJson() => {
        "title": title,
        "duration": duration == null ? null : duration,
        "data_lesson": List<dynamic>.from(dataLesson!
            .map((x) => List<dynamic>.from(x.map((x) => x.toJson())))),
      };
}

class DataLesson {
  DataLesson({
    this.titleLesson,
    this.duration,
  });

  final String? titleLesson;
  final String? duration;

  factory DataLesson.fromJson(Map<String, dynamic> json) => DataLesson(
        titleLesson: json["title_lesson"],
        duration: json["duration"],
      );

  Map<String, dynamic> toJson() => {
        "title_lesson": titleLesson,
        "duration": duration,
      };
}
