import 'package:initial_folder/models/comment_qna_model.dart';

class QnaModel {
  QnaModel({
    this.status,
    this.error,
    required this.data,
  });

  final int? status;
  final bool? error;
  final List<List<QnaDataModel>> data;

  factory QnaModel.fromJson(Map<String, dynamic> json) => QnaModel(
        status: json["status"],
        error: json["error"],
        data: List<List<QnaDataModel>>.from(json["data"].map((x) =>
            List<QnaDataModel>.from(x.map((x) => QnaDataModel.fromJson(x))))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "data": List<dynamic>.from(
            data.map((x) => List<dynamic>.from(x.map((x) => x.toJson())))),
      };
}

class QnaDataModel {
  QnaDataModel({
    this.id_qna,
    this.sender,
    this.username,
    this.quest,
    this.foto_profile,
    this.like,
    this.date,
    required this.comment,
  });

  final String? id_qna;
  final String? sender;
  final String? username;
  final String? quest;
  final String? foto_profile;
  final String? like;
  final String? date;
  final List<Comment> comment;

  factory QnaDataModel.fromJson(Map<String, dynamic> json) => QnaDataModel(
        id_qna: json["id_qna"],
        sender: json["sender"],
        username: json["username"],
        quest: json["quest"],
        foto_profile: json["foto_profile"],
        like: json["like"],
        date: json["date"],
        comment:
            List<Comment>.from(json["comment"].map((x) => Comment.fromJson(x)))
                .toList(),
      );

  Map<String, dynamic> toJson() => {
        "id_qna": id_qna,
        "sender": sender,
        "username": username,
        "quest": quest,
        "foto_profile": foto_profile,
        "like": like,
        "date": date,
        "comment": List<dynamic>.from(comment.map((x) => x)),
      };
}

/// Class untuk response tambah
class QnaPostModel {
  QnaPostModel({
    this.status,
    this.error,
    required this.data,
  });

  final int? status;
  final bool? error;
  final DataPostQna data;

  factory QnaPostModel.fromJson(Map<String, dynamic> json) => QnaPostModel(
        status: json["status"],
        error: json["error"],
        data: DataPostQna.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "data": data.toJson(),
      };
}

class DataPostQna {
  DataPostQna({this.sender, this.quest, this.id_course});

  final String? sender;
  final String? quest;
  final String? id_course;

  factory DataPostQna.fromJson(Map<String, dynamic> json) => DataPostQna(
        sender: json["sender"],
        quest: json["quest"],
        id_course: json["id_course"],
      );

  Map<String, dynamic> toJson() => {
        "messages": sender,
        "quest": quest,
        "id_course": id_course,
      };
}
