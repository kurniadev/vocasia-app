// import 'package:flutter/material.dart';
// import '../size_config.dart';
// import '../theme.dart';

// // class Product {
// //   final int id;
// //   final String title;
// //   final RichText description;
// //   final List<String> images;
// //   final List<Color> colors;
// //   final double rating, price, disc;
// //   final bool isFavourite, isPopular;

// //   Product({
// //     required this.id,
// //     required this.images,
// //     required this.colors,
// //     this.rating = 0.0,
// //     this.isFavourite = false,
// //     this.isPopular = false,
// //     required this.title,
// //     required this.price,
// //     required this.disc,
// //     required this.description,
// //   });
// // }

// // Our demo Products
// final TextStyle light = primaryTextStyle.copyWith(
//     letterSpacing: 0.5,
//     color: secondaryColor,
//     fontSize: getProportionateScreenWidth(10),
//     fontWeight: reguler);
// final TextStyle thick = primaryTextStyle.copyWith(
//     letterSpacing: 0.5,
//     color: secondaryColor,
//     fontSize: getProportionateScreenWidth(10),
//     fontWeight: semiBold);
// List<Product> demoProducts = [
//   Product(
//     id: 1,
//     images: [
//       "assets/images/Kur1.png",
//     ],
//     colors: [
//       Color(0xFFF6625E),
//       Color(0xFF836DB8),
//       Color(0xFFDECB9C),
//       Colors.white,
//     ],
//     title: "Menjadi Pengusaha Sukses dan Bertalenta",
//     price: 250000,
//     disc: 1250000,
//     description: RichText(
//       text: new TextSpan(
//         // Note: Styles for TextSpans must be explicitly defined.
//         // Child text spans will inherit styles from parent
//         style: TextStyle(
//             fontSize: 10, color: Colors.white, fontWeight: FontWeight.w300),
//         //maxLines: 1,
//         children: <TextSpan>[
//           new TextSpan(text: 'Oleh ', style: light),
//           new TextSpan(text: 'Farid Subkhan', style: thick),
//         ],
//       ),
//     ),
//     rating: 4.8,
//     isFavourite: true,
//     isPopular: true,
//   ),
//   Product(
//     id: 2,
//     images: [
//       "assets/images/Kur2.png",
//     ],
//     colors: [
//       Color(0xFFF6625E),
//       Color(0xFF836DB8),
//       Color(0xFFDECB9C),
//       Colors.white,
//     ],
//     title: "Kiat Menjadi Youtuber",
//     price: 500500,
//     disc: 1500000,
//     description: RichText(
//       text: new TextSpan(
//         // Note: Styles for TextSpans must be explicitly defined.
//         // Child text spans will inherit styles from parent
//         style: TextStyle(fontSize: 10, fontWeight: FontWeight.w300),
//         children: <TextSpan>[
//           new TextSpan(text: 'Oleh ', style: light),
//           new TextSpan(text: 'Farid Subkhan', style: thick),
//         ],
//       ),
//     ),
//     rating: 4.1,
//     isPopular: true,
//   ),
//   Product(
//     id: 3,
//     images: [
//       "assets/images/Kur3.png",
//     ],
//     colors: [
//       Color(0xFFF6625E),
//       Color(0xFF836DB8),
//       Color(0xFFDECB9C),
//       Colors.white,
//     ],
//     title: "Menjadi Pengusaha Sukses dan Berjiwa Entrepreneur",
//     price: 365500,
//     disc: 1500000,
//     description: RichText(
//       text: new TextSpan(
//         // Note: Styles for TextSpans must be explicitly defined.
//         // Child text spans will inherit styles from parent
//         style: TextStyle(fontSize: 10, fontWeight: FontWeight.w300),
//         children: <TextSpan>[
//           new TextSpan(text: 'Oleh ', style: light),
//           new TextSpan(text: 'Ali Sanjani', style: thick),
//         ],
//       ),
//     ),
//     rating: 4.1,
//     isFavourite: true,
//     isPopular: true,
//   ),
//   Product(
//     id: 4,
//     images: [
//       "assets/images/Kur4.png",
//     ],
//     colors: [
//       Color(0xFFF6625E),
//       Color(0xFF836DB8),
//       Color(0xFFDECB9C),
//       Colors.white,
//     ],
//     title: "Menguasai Excel dengan Cepat dan Handal Banget",
//     price: 200500,
//     disc: 1000000,
//     description: RichText(
//       text: new TextSpan(
//         // Note: Styles for TextSpans must be explicitly defined.
//         // Child text spans will inherit styles from parent
//         style: TextStyle(fontSize: 10, fontWeight: FontWeight.w300),
//         children: <TextSpan>[
//           new TextSpan(text: 'Oleh ', style: light),
//           new TextSpan(text: 'Ali Sanjani', style: thick),
//         ],
//       ),
//     ),
//     rating: 4.1,
//     isFavourite: true,
//     isPopular: true,
//   ),
// ];
