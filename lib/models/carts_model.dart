class CartsModel {
  CartsModel({
    this.status,
    this.error,
    required this.data,
    this.totalPayment,
  });

  final int? status;
  final bool? error;
  final List<DataCartsModel> data;
  final String? totalPayment;

  factory CartsModel.fromJson(Map<String, dynamic> json) => CartsModel(
        status: json["status"],
        error: json["error"],
        data: List<DataCartsModel>.from(
            json["data"].map((x) => DataCartsModel.fromJson(x))),
        totalPayment: json["total_payment"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "total_payment": totalPayment,
      };
}

class DataCartsModel {
  DataCartsModel({
    this.cartId,
    this.courseId,
    this.title,
    this.price,
    this.instructor,
    this.thumbnail,
    this.discountPrice,
    this.discountFlag,
    this.totalDiscount,
    this.student,
    required this.review,
    this.fotoProfile,
  });

  final String? cartId;
  final String? courseId;
  final String? title;
  final String? price;
  final String? instructor;
  final String? thumbnail;
  final String? discountPrice;
  final String? discountFlag;
  final int? totalDiscount;
  final String? student;
  final List<Review> review;
  final dynamic fotoProfile;

  factory DataCartsModel.fromJson(Map<String, dynamic> json) => DataCartsModel(
        cartId: json["cart_id"],
        courseId: json["course_id"],
        title: json["title"],
        price: json["price"],
        instructor: json["instructor"],
        thumbnail: json["thumbnail"],
        discountPrice: json["discount_price"],
        discountFlag: json["discount_flag"],
        totalDiscount: json["total_discount"],
        student: json["student"],
        review:
            List<Review>.from(json["review"].map((x) => Review.fromJson(x))),
        fotoProfile: json["foto_profile"],
      );

  Map<String, dynamic> toJson() => {
        "cart_id": cartId,
        "course_id": courseId,
        "title": title,
        "price": price,
        "instructor": instructor,
        "thumbnail": thumbnail,
        "discount_price": discountPrice,
        "discount_flag": discountFlag,
        "total_discount": totalDiscount,
        "student": student,
        "review": List<dynamic>.from(review.map((x) => x.toJson())),
        "foto_profile": fotoProfile,
      };
}

class Review {
  Review({
    this.totalReview,
    this.avgRating,
  });

  final String? totalReview;
  final int? avgRating;

  factory Review.fromJson(Map<String, dynamic> json) => Review(
        totalReview: json["total_review"],
        avgRating: json["avg_rating"] == null ? null : json["avg_rating"],
      );

  Map<String, dynamic> toJson() => {
        "total_review": totalReview,
        "avg_rating": avgRating == null ? null : avgRating,
      };
}
