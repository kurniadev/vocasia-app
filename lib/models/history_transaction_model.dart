class HistoryTransactionsModel {
  HistoryTransactionsModel({
    this.status,
    this.error,
    required this.data,
  });

  final int? status;
  final bool? error;
  final List<List<DataHistoryTransactionModel>> data;

  factory HistoryTransactionsModel.fromJson(Map<String, dynamic> json) =>
      HistoryTransactionsModel(
        status: json["status"],
        error: json["error"],
        data: List<List<DataHistoryTransactionModel>>.from(json["data"].map(
            (x) => List<DataHistoryTransactionModel>.from(
                x.map((x) => DataHistoryTransactionModel.fromJson(x))))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "data": List<dynamic>.from(
            data.map((x) => List<dynamic>.from(x.map((x) => x.toJson())))),
      };
}

class DataHistoryTransactionModel {
  DataHistoryTransactionModel({
    this.title,
    this.thumbnail,
    this.instructor,
    this.fotoProfile,
    this.totalPrice,
    this.date,
    this.paymentType,
    this.statusPayment,
  });

  final String? title;
  final String? thumbnail;
  final String? instructor;
  final String? fotoProfile;
  final String? totalPrice;
  final String? date;
  final String? paymentType;
  final String? statusPayment;

  factory DataHistoryTransactionModel.fromJson(Map<String, dynamic> json) =>
      DataHistoryTransactionModel(
        title: json["title"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
        instructor: json["instructor"],
        fotoProfile: json["foto_profile"],
        totalPrice: json["total_price"],
        date: json["date"],
        paymentType: json["payment_type"],
        statusPayment: json["status_payment"],
      );

  Map<String, dynamic> toJson() => {
        "title": title,
        "thumbnail": thumbnail == null ? null : thumbnail,
        "instructor": instructor,
        "foto_profile": fotoProfile,
        "total_price": totalPrice,
        "date": date,
        "payment_type": paymentType,
        "status_payment": statusPayment,
      };
}
