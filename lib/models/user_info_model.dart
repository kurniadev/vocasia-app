class UserInfoModel {
  UserInfoModel({
    this.status,
    this.error = false,
    required this.data,
  });

  final int? status;
  final bool error;
  final List<Data> data;

  factory UserInfoModel.fromJson(Map<String, dynamic> json) => UserInfoModel(
        status: json["status"],
        error: json["error"],
        data: List<Data>.from(json["data"].map((x) => Data.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class Data {
  Data({
    this.idUser,
    this.fullname,
    this.email,
    this.fotoProfile,
    this.isInstructor,
  });

  final String? idUser;
  final String? fullname;
  final String? email;
  final String? fotoProfile;
  final String? isInstructor;

  factory Data.fromJson(Map<String, dynamic> json) => Data(
        idUser: json["id_user"],
        fullname: json["fullname"],
        email: json["email"],
        fotoProfile: json["foto_profile"],
        isInstructor: json["is_instructor"],
      );

  Map<String, dynamic> toJson() => {
        "id_user": idUser,
        "fullname": fullname,
        "email": email,
        "foto_profile": fotoProfile,
        "is_instructor": isInstructor,
      };
}
