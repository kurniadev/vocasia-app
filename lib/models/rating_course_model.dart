class Rating {
  Rating({
    this.totalReview,
    this.avgRating,
  });

  String? totalReview;
  int? avgRating;

  factory Rating.fromJson(Map<String, dynamic> json) => Rating(
        totalReview: json["total_review"],
        avgRating: json["avg_rating"],
      );

  Map<String, dynamic> toJson() => {
        "total_review": totalReview,
        "avg_rating": avgRating,
      };
}
