class ProfileImagePostModel {
  ProfileImagePostModel({
    this.status,
    this.error,
    required this.data,
  });

  final int? status;
  final bool? error;
  final DataPostImage data;

  factory ProfileImagePostModel.fromJson(Map<String, dynamic> json) =>
      ProfileImagePostModel(
        status: json["status"],
        error: json["error"],
        data: DataPostImage.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "data": data.toJson(),
      };
}

class DataPostImage {
  DataPostImage({
    this.messages,
  });

  final String? messages;

  factory DataPostImage.fromJson(Map<String, dynamic> json) => DataPostImage(
        messages: json["messages"],
      );

  Map<String, dynamic> toJson() => {
        "messages": messages,
      };
}
