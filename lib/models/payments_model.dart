class PaymentModel {
  PaymentModel({
    this.status,
    this.error,
    this.messages,
  });

  final int? status;
  final bool? error;
  final String? messages;

  factory PaymentModel.fromJson(Map<String, dynamic> json) => PaymentModel(
        status: json["status"],
        error: json["error"],
        messages: json["messages"],
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "messages": messages,
      };
}
