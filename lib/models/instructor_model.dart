class InstructorModel {
  InstructorModel({
    this.status,
    this.error,
    required this.data,
  });

  final int? status;
  final bool? error;
  final List<DataInstructor> data;

  factory InstructorModel.fromJson(Map<String, dynamic> json) =>
      InstructorModel(
        status: json["status"],
        error: json["error"],
        data: List<DataInstructor>.from(
            json["data"].map((x) => DataInstructor.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
      };
}

class DataInstructor {
  DataInstructor({
    this.instructorName,
    this.totalReview,
    this.totalCourse,
    this.totalStudents,
  });

  final String? instructorName;
  final String? totalReview;
  final String? totalCourse;
  final String? totalStudents;

  factory DataInstructor.fromJson(Map<String, dynamic> json) => DataInstructor(
        instructorName: json["instructor_name"],
        totalReview: json["total_review"],
        totalCourse: json["total_course"],
        totalStudents: json["total_students"],
      );

  Map<String, dynamic> toJson() => {
        "instructor_name": instructorName,
        "total_review": totalReview,
        "total_course": totalCourse,
        "total_students": totalStudents,
      };
}
