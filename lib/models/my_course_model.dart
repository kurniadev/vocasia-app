class MyCourseModel {
  MyCourseModel({
    this.status,
    this.error,
    required this.data,
  });

  final int? status;
  final bool? error;
  final List<List<DataMyCourseModel>> data;

  factory MyCourseModel.fromJson(Map<String, dynamic> json) => MyCourseModel(
        status: json["status"],
        error: json["error"],
        data: List<List<DataMyCourseModel>>.from(json["data"].map((x) =>
            List<DataMyCourseModel>.from(
                x.map((x) => DataMyCourseModel.fromJson(x))))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "data": List<dynamic>.from(
            data.map((x) => List<dynamic>.from(x.map((x) => x.toJson())))),
      };
}

class DataMyCourseModel {
  DataMyCourseModel({
    this.courseId,
    this.instructorId,
    this.instructor,
    this.title,
    this.thumbnail,
    required this.rating,
    this.fotoProfile,
    this.totalProgress,
  });

  final String? courseId;
  final String? instructorId;
  final String? instructor;
  final String? title;
  final String? thumbnail;
  final List<Rating> rating;
  final dynamic fotoProfile;
  final int? totalProgress;

  factory DataMyCourseModel.fromJson(Map<String, dynamic> json) =>
      DataMyCourseModel(
        courseId: json["course_id"],
        instructorId: json["instructor_id"],
        instructor: json["instructor"],
        title: json["title"],
        thumbnail: json["thumbnail"],
        rating: json['rating'] == []
            ? []
            : List<Rating>.from(json["rating"].map((x) => Rating.fromJson(x))),
        fotoProfile: json["foto_profile"],
        totalProgress: json["total_progress"],
      );

  Map<String, dynamic> toJson() => {
        "course_id": courseId,
        "instructor_id": instructorId,
        "instructor": instructor,
        "title": title,
        "thumbnail": thumbnail,
        "rating": List<dynamic>.from(rating.map((x) => x.toJson())),
        "foto_profile": fotoProfile,
        "total_progress": totalProgress,
      };
}

class Rating {
  Rating({
    this.ratableId,
    this.rating,
    this.review,
  });

  final String? ratableId;
  final String? rating;
  final String? review;

  factory Rating.fromJson(Map<String, dynamic> json) => Rating(
        ratableId: json["ratable_id"],
        rating: json["rating"],
        review: json["review"],
      );

  Map<String, dynamic> toJson() => {
        "ratable_id": ratableId,
        "rating": rating,
        "review": review,
      };
}
