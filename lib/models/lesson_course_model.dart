class LessonCourseModel {
  LessonCourseModel({
    this.status,
    this.error,
    required this.data,
  });

  final int? status;
  final bool? error;
  final List<List<DataLessonCourseModel>> data;

  factory LessonCourseModel.fromJson(Map<String, dynamic> json) =>
      LessonCourseModel(
        status: json["status"],
        error: json["error"],
        data: List<List<DataLessonCourseModel>>.from(json["data"].map((x) =>
            List<DataLessonCourseModel>.from(
                x.map((x) => DataLessonCourseModel.fromJson(x))))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "data": List<dynamic>.from(
            data.map((x) => List<dynamic>.from(x.map((x) => x.toJson())))),
      };
}

class DataLessonCourseModel {
  DataLessonCourseModel({
    this.courseId,
    this.lessonId,
    this.sectionTitle,
    this.lessonTitle,
    this.duration,
    this.attachmentType,
    this.videoType,
    this.videoUrl,
    this.lessonType,
    this.attachment,
    this.isSkip,
    this.isFinished,
  });

  final String? courseId;
  final String? lessonId;
  final String? sectionTitle;
  final String? lessonTitle;
  final String? duration;
  final String? attachmentType;
  final String? videoType;
  final String? videoUrl;
  final String? lessonType;
  final dynamic attachment;
  final String? isSkip;
  late int? isFinished;

  factory DataLessonCourseModel.fromJson(Map<String, dynamic> json) =>
      DataLessonCourseModel(
        courseId: json["course_id"],
        lessonId: json["lesson_id"],
        sectionTitle: json["section_title"],
        lessonTitle: json["lesson_title"],
        duration: json["duration"],
        attachmentType: json["attachment_type"],
        videoType: json["video_type"] == null ? null : json["video_type"],
        videoUrl: json["video_url"] == null ? null : json["video_url"],
        lessonType: json["lesson_type"],
        attachment: json["attachment"],
        isSkip: json["is_skip"],
        isFinished: json["is_finished"],
      );

  Map<String, dynamic> toJson() => {
        "course_id": courseId,
        "lesson_id": lessonId,
        "section_title": sectionTitle,
        "lesson_title": lessonTitle,
        "duration": duration,
        "attachment_type": attachmentType,
        "video_type": videoType == null ? null : videoType,
        "video_url": videoUrl == null ? null : videoUrl,
        "lesson_type": lessonType,
        "attachment": attachment,
        "is_skip": isSkip,
        "is_finished": isFinished,
      };
}

class NewMap {
  NewMap({
    required this.title,
  });

  final List<Title> title;

  factory NewMap.fromMap(Map<String, dynamic> json) => NewMap(
        title: List<Title>.from(json["Title"].map((x) => Title.fromMap(x))),
      );

  Map<String, dynamic> toMap() => {
        "Title": List<dynamic>.from(title.map((x) => x.toMap())),
      };
}

class Title {
  Title({
    this.courseId,
    this.lessonId,
    this.sectionTitle,
    this.lessonTitle,
    this.duration,
    this.attachmentType,
    this.videoType,
    this.videoUrl,
    this.lessonType,
    this.attachment,
    this.isSkip,
    this.isFinished,
  });

  final String? courseId;
  final String? lessonId;
  final String? sectionTitle;
  final String? lessonTitle;
  final String? duration;
  final String? attachmentType;
  final String? videoType;
  final String? videoUrl;
  final String? lessonType;
  final dynamic attachment;
  final String? isSkip;
  final int? isFinished;

  factory Title.fromMap(Map<String, dynamic> json) => Title(
        courseId: json["course_id"],
        lessonId: json["lesson_id"],
        sectionTitle: json["section_title"],
        lessonTitle: json["lesson_title"],
        duration: json["duration"],
        attachmentType: json["attachment_type"],
        videoType: json["video_type"],
        videoUrl: json["video_url"] == '' ? 'a' : json["video_url"],
        lessonType: json["lesson_type"],
        attachment: json["attachment"],
        isSkip: json["is_skip"],
        isFinished: json["is_finished"],
      );

  Map<String, dynamic> toMap() => {
        "course_id": courseId,
        "lesson_id": lessonId,
        "section_title": sectionTitle,
        "lesson_title": lessonTitle,
        "duration": duration,
        "attachment_type": attachmentType,
        "video_type": videoType,
        "video_url": videoUrl,
        "lesson_type": lessonType,
        "attachment": attachment,
        "is_skip": isSkip,
        "is_finished": isFinished,
      };
}
