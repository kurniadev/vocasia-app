/// Class untuk mengambil data wishlist

class WishlistModel {
  WishlistModel({
    this.status,
    this.error,
    required this.data,
  });

  final int? status;
  final bool? error;
  final List<List<DataWihslistModel>> data;

  factory WishlistModel.fromJson(Map<String, dynamic> json) => WishlistModel(
        status: json["status"],
        error: json["error"],
        data: List<List<DataWihslistModel>>.from(json["data"].map((x) =>
            List<DataWihslistModel>.from(
                x.map((x) => DataWihslistModel.fromJson(x))))),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "data": List<dynamic>.from(
            data.map((x) => List<dynamic>.from(x.map((x) => x.toJson())))),
      };
}

class DataWihslistModel {
  DataWihslistModel({
    this.wishlistId,
    this.courseId,
    this.title,
    this.price,
    this.instructor,
    this.thumbnail,
    this.discountPrice,
    this.discountFlag,
    this.totalDiscount,
    this.student,
    required this.review,
    this.fotoProfile,
  });

  final String? wishlistId;
  final String? courseId;
  final String? title;
  final String? price;
  final String? instructor;
  final String? thumbnail;
  final String? discountPrice;
  final String? discountFlag;
  final int? totalDiscount;
  final String? student;
  final List<Review> review;
  final dynamic fotoProfile;

  factory DataWihslistModel.fromJson(Map<String, dynamic> json) =>
      DataWihslistModel(
        wishlistId: json["wishlist_id"],
        courseId: json["course_id"],
        title: json["title"],
        price: json["price"],
        instructor: json["instructor"],
        thumbnail: json["thumbnail"] == null ? null : json["thumbnail"],
        discountPrice: json["discount_price"],
        discountFlag: json["discount_flag"],
        totalDiscount: json["total_discount"],
        student: json["student"],
        review:
            List<Review>.from(json["review"].map((x) => Review.fromJson(x))),
        fotoProfile: json["foto_profile"],
      );

  Map<String, dynamic> toJson() => {
        "wishlist_id": wishlistId,
        "course_id": courseId,
        "title": title,
        "price": price,
        "instructor": instructor,
        "thumbnail": thumbnail == null ? null : thumbnail,
        "discount_price": discountPrice,
        "discount_flag": discountFlag,
        "total_discount": totalDiscount,
        "student": student,
        "review": List<dynamic>.from(review.map((x) => x.toJson())),
        "foto_profile": fotoProfile,
      };
}

class Review {
  Review({
    this.totalReview,
    this.avgRating,
  });

  final String? totalReview;
  final int? avgRating;

  factory Review.fromJson(Map<String, dynamic> json) => Review(
        totalReview: json["total_review"],
        avgRating: json["avg_rating"] == null ? null : json["avg_rating"],
      );

  Map<String, dynamic> toJson() => {
        "total_review": totalReview,
        "avg_rating": avgRating == null ? null : avgRating,
      };
}

/// Class untuk response tambah atau menghapus wishlist
class WishlistPostModel {
  WishlistPostModel({
    this.status,
    this.error,
    required this.data,
  });

  final int? status;
  final bool? error;
  final DataPostWishlist data;

  factory WishlistPostModel.fromJson(Map<String, dynamic> json) =>
      WishlistPostModel(
        status: json["status"],
        error: json["error"],
        data: DataPostWishlist.fromJson(json["data"]),
      );

  Map<String, dynamic> toJson() => {
        "status": status,
        "error": error,
        "data": data.toJson(),
      };
}

class DataPostWishlist {
  DataPostWishlist({
    this.messages,
  });

  final String? messages;

  factory DataPostWishlist.fromJson(Map<String, dynamic> json) =>
      DataPostWishlist(
        messages: json["messages"],
      );

  Map<String, dynamic> toJson() => {
        "messages": messages,
      };
}
