import 'dart:convert';

import 'package:initial_folder/helper/user_info.dart';
import 'package:initial_folder/models/update_password_model.dart';
import 'package:initial_folder/models/user_info_model.dart';
import 'package:initial_folder/models/data_diri_model.dart';
import 'package:http/http.dart' as http;
import 'package:initial_folder/providers/data_diri_provider.dart';

class UserInfoService {
  Future<UserInfoModel> getUserInfo(email) async {
    String? token = await UsersInfo().getToken();
    Uri url = Uri.parse('https://apivocasia.nouky.xyz/auth/me?email=$email');

    var header = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    var response = await http.get(url, headers: header);
    print(response.body);
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      UserInfoModel userInfoModel = UserInfoModel.fromJson(data);
      await UsersInfo()
          .setIdUser(int.tryParse(userInfoModel.data[0].idUser ?? ''));

      return userInfoModel;
      // return UserInfoModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Gagal ambil data');
    }
  }

  Future<DataDiriModel> getDataDiri() async {
    int? id = await UsersInfo().getIdUser();
    String? token = await UsersInfo().getToken();
    Uri url = Uri.parse('https://apivocasia.nouky.xyz/users/users-info/$id');

    var header = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    var response = await http.get(url, headers: header);
    print("Data diri" + response.body);
    if (response.statusCode == 200) {
      return DataDiriModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Gagal ambil data');
    }
  }

  Future<bool> updateDataDiri({
    String? fullname,
    String? biograph,
    String? twitter,
    String? facebook,
    String? linkedin,
    String? instagram,
    String? datebirth,
    String? phone,
    String? gender,
    String? heading,
  }) async {
    int? id = await UsersInfo().getIdUser();
    String? token = await UsersInfo().getToken();
    Uri url = Uri.parse(
        'https://apivocasia.nouky.xyz/users/profile/user-profile/$id');

    var header = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    var body = jsonEncode({
      "first_name": fullname,
      "biography": biograph,
      "phone": phone,
      "datebirth": datebirth,
      "jenis_kel": gender,
      "heading": heading,
      "social_link": {
        "twitter": twitter,
        "facebook": facebook,
        "linkedin": linkedin,
        "instagram": instagram
      }
    });
    var response = await http.put(url, headers: header, body: body);
    print("Data diri" + response.body);
    if (response.statusCode == 200) {
      return true;
      // return UserInfoModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Gagal update data');
    }
  }

  Future<UpdatePasswordModel> updatePassword(
      {required idUser,
      required String? email,
      required String? old_password,
      required String? password,
      required String? new_password_confirm}) async {
    String? token = await UsersInfo().getToken();
    Uri url = Uri.parse(
        'https://apivocasia.nouky.xyz/users/profile/user-credentials/' +
            idUser);

    var header = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    var body = jsonEncode({
      'email': email,
      'old_password': old_password,
      "password": password,
      "new_password_confirm": new_password_confirm,
    });
    var response = await http.put(url, headers: header, body: body);
    print("Update Password" + response.body);
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body);
      UpdatePasswordModel updatePasswordModel =
          UpdatePasswordModel.fromJson(data);

      return updatePasswordModel;
      // return UserInfoModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Gagal update password');
    }
  }
}
