import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:initial_folder/helper/user_info.dart';
import 'package:initial_folder/models/counter_qna_comment_model.dart';
import 'package:initial_folder/models/qna_model.dart';

class QnaService {
  //get QNA user
  Future<QnaModel> getMyQna(String idCourse) async {
    String? token = await UsersInfo().getToken();
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    Uri url = Uri.parse('https://apivocasia.nouky.xyz/users/qna/$idCourse');
    http.Response response = await http.get(url, headers: headers);
    print(response.body);
    if (response.statusCode == 200) {
      return QnaModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Data QNA, Gagal Diambil');
    }
  }

  // Post QNA User
  Future<bool> postingQna(String quest, String idCourse) async {
    int? idUser = await UsersInfo().getIdUser();
    String? token = await UsersInfo().getToken();
    Uri url = Uri.parse('https://apivocasia.nouky.xyz/users/qna');
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    var body = jsonEncode(
        {"sender": idUser.toString(), "quest": quest, "id_course": idCourse});
    http.Response response = await http.post(url, headers: headers, body: body);
    print(response.statusCode);

    if (response.statusCode == 201) {
      return true;
    } else {
      return false;
    }
  }

  // Update QNA User
  Future<bool> updateQna(int id_rep, String quest, String id_qna) async {
    int? idUser = await UsersInfo().getIdUser();
    String? token = await UsersInfo().getToken();
    Uri url = Uri.parse('https://apivocasia.nouky.xyz/users/qna/$id_rep');
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    var body = jsonEncode(
        {"sender": idUser.toString(), "quest": quest, "id_qna": id_qna});
    http.Response response = await http.put(url, headers: headers, body: body);
    print(response.body);

    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  //Post Reply QNA User
  Future<bool> postQnaReply(String text_rep, String id_qna) async {
    int? idUser = await UsersInfo().getIdUser();
    String? token = await UsersInfo().getToken();
    Uri url = Uri.parse('https://apivocasia.nouky.xyz/users/qna/reply');
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    var body = jsonEncode(
        {"sender": idUser.toString(), "text_rep": text_rep, "id_qna": id_qna});
    http.Response response = await http.post(url, headers: headers, body: body);
    print(response.statusCode);

    if (response.statusCode == 201) {
      return true;
    } else {
      return false;
    }
  }

  //Edit Reply QNA User
  Future<bool> editQnaReply(int id_rep, String text_rep, String id_qna) async {
    int? idUser = await UsersInfo().getIdUser();
    String? token = await UsersInfo().getToken();
    Uri url = Uri.parse('https://apivocasia.nouky.xyz/users/qna/reply/$id_rep');
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    var body = jsonEncode(
        {"sender": idUser.toString(), "text_rep": text_rep, "id_qna": id_qna});
    http.Response response = await http.put(url, headers: headers, body: body);
    print(response.body);

    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  // Delete Qna (Pertanyaan User)
  Future<bool> deleteQna(
    int id_qna,
  ) async {
    Uri url = Uri.parse('https://apivocasia.nouky.xyz/users/qna/$id_qna');
    String? token = await UsersInfo().getToken();
    print(token);
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };

    http.Response response = await http.delete(
      url,
      headers: headers,
    );
    print(response.body);
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  // Delete Reply Qna User
  Future<bool> deleteReplyQna(
    int id_rep,
  ) async {
    Uri url = Uri.parse('https://apivocasia.nouky.xyz/users/qna/reply/$id_rep');
    String? token = await UsersInfo().getToken();
    print(token);
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };

    http.Response response = await http.delete(
      url,
      headers: headers,
    );
    print(response.body);
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  //get counter comment qna
  Future<CounterCommentModel> getCounterComment(String idQna) async {
    String? token = await UsersInfo().getToken();
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    Uri url = Uri.parse('https://apivocasia.nouky.xyz/users/qna/count/$idQna');
    http.Response response = await http.get(url, headers: headers);
    print(response.body);
    print(response.statusCode);
    if (response.statusCode == 200) {
      print('SUKESSSS');
      return CounterCommentModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Data Counter Comment QNA, Gagal Diambil');
    }
  }
}
