import 'dart:convert';

import 'package:initial_folder/helper/user_info.dart';
import 'package:initial_folder/models/history_transaction_model.dart';
import 'package:http/http.dart' as http;

class HistoryTransactionService {
  Future<HistoryTransactionsModel> historyTransactions() async {
    int? idUser = await UsersInfo().getIdUser();
    String? token = await UsersInfo().getToken();
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    Uri url = Uri.parse('https://apivocasia.nouky.xyz/users/history/$idUser');

    http.Response response = await http.get(url, headers: headers);
    print(response.body);
    if (response.statusCode == 200) {
      return HistoryTransactionsModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Data Transaksi Gagal Diambil');
    }
  }
}
