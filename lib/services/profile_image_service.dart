import 'dart:convert';
import 'dart:io';
import 'package:initial_folder/helper/user_info.dart';
import 'package:initial_folder/models/profile_image_post_model.dart';
import 'package:http/http.dart' as http;

class ProfileImageService {
  var _baseUrl = 'https://apivocasia.nouky.xyz/users/profile/user-photo';
  Future<ProfileImagePostModel> addProfileImage({required File pckFile}) async {
    int? idUser = await UsersInfo().getIdUser();
    Uri url = Uri.parse('$_baseUrl/$idUser');
    String? token = await UsersInfo().getToken();

    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };

    var req = http.MultipartRequest('POST', url);
    req.headers.addAll(headers);
    req.files.add(http.MultipartFile(
        'foto_profile', pckFile.readAsBytes().asStream(), pckFile.lengthSync(),
        filename: pckFile.path.split("/").last));

    var streamed = await req.send();
    var response = await http.Response.fromStream(streamed);
    // http.Response response = await http.post(url,
    //     headers: headers, body: pckFile.path.split("/").last);
    print("HHayuu  " + pckFile.path.split("/").last + response.body);
    if (response.statusCode == 201 || response.statusCode == 200) {
      return ProfileImagePostModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Gagal Mengganti Foto Profil');
    }
    //return response;
  }
}
