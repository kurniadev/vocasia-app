import 'dart:convert';

import 'package:initial_folder/helper/user_info.dart';
import 'package:initial_folder/models/cart_model.dart';
import 'package:http/http.dart' as http;
import 'package:initial_folder/models/carts_model.dart';

class CartService {
  Future<bool> addCart(idCourse) async {
    var token = await UsersInfo().getToken();
    var idUser = await UsersInfo().getIdUser();

    Uri url = Uri.parse('https://apivocasia.nouky.xyz/users/add-to-cart');
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    var body = jsonEncode({
      'id_user': idUser,
      'cart_item': idCourse,
    });
    http.Response response = await http.post(url, headers: headers, body: body);
    if (response.statusCode == 201) {
      return true;
    } else {
      throw Exception('Gagal Menambahkan ke keranjang');
    }
  }

  Future<bool> deleteCart(idCart) async {
    var token = await UsersInfo().getToken();

    Uri url =
        Uri.parse('https://apivocasia.nouky.xyz/users/cart/delete/$idCart');
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };

    http.Response response = await http.delete(
      url,
      headers: headers,
    );
    if (response.statusCode == 200) {
      return true;
    } else {
      throw Exception('Gagal Menghapus keranjang');
    }
  }

  Future<CartsModel> getCarts() async {
    var token = await UsersInfo().getToken();
    var idUser = await UsersInfo().getIdUser();

    Uri url = Uri.parse('https://apivocasia.nouky.xyz/users/carts/$idUser');
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };

    http.Response response = await http.get(
      url,
      headers: headers,
    );
    if (response.statusCode == 200) {
      return CartsModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Gagal Megambil data keranjang');
    }
  }
}
