import 'dart:convert';

import 'package:initial_folder/helper/user_info.dart';
import 'package:initial_folder/models/user_model.dart';
import 'package:http/http.dart' as http;

class AuthService {
  String baseUrl = 'https://apivocasia.nouky.xyz/auth/';

  // buat function
  Future<UserModel> register(
      {required String name,
      required String email,
      required String password}) async {
    var url = Uri.parse('$baseUrl/register');
    var headers = {'content-type': 'application/json'};
    var body = jsonEncode({
      'name': name,
      'email': email,
      'password': password,
    });

    var response = await http.post(url, headers: headers, body: body);
    print(response.body);
    if (response.statusCode == 201) {
      return UserModel.fromJson(jsonDecode(response.body)['data']);
    } else {
      throw Exception('Gagal register');
    }
  }

  Future<UserModel> login(
      {required String email, required String password}) async {
    var url = Uri.parse('$baseUrl/mobile/login');

    var headers = {'content-type': 'application/json'};
    var body = jsonEncode({
      'email': email,
      'password': password,
    });
    var response = await http.post(url, headers: headers, body: body);
    print(response.body);
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['data'];
      UserModel user = UserModel.fromJson(data);
      user.token = data['token'];

      var token = await UsersInfo().setToken(user.token!);

      print(token);

      return user;
    } else {
      throw Exception('Gagal login');
    }
  }
}
