import 'dart:convert';

import 'package:initial_folder/helper/user_info.dart';
import 'package:http/http.dart' as http;

class PaymentServices {
  var _baseUrl = 'https://apivocasia.nouky.xyz/users/payment/free-course';
  Future<bool> freeCoure(int idCourse) async {
    Uri url = Uri.parse(_baseUrl);
    String? token = await UsersInfo().getToken();
    int? idUser = await UsersInfo().getIdUser();

    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    var body = jsonEncode({
      "user_id": idUser,
      "course_id": idCourse,
    });
    http.Response response = await http.post(url, headers: headers, body: body);
    print(response.body);
    if (response.statusCode == 201) {
      return true;
    } else if (response.statusCode == 400) {
      return false;
    } else {
      throw Exception('Gagal Mmebeli kursus');
    }
  }
}
