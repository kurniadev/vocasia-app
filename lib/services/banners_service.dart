import 'dart:convert';

import 'package:initial_folder/models/banners_model.dart';
import 'package:http/http.dart' as http;

class BannersService {
  Future<List<BannersModel>> getAllBanners() async {
    Uri url = Uri.parse('https://apivocasia.nouky.xyz/mobile/banners');
    var header = {
      'Content-Type': 'application/json; charset=UTF-8',
      'Connection': 'Keep-Alive'
    };
    var response = await http.get(url, headers: header);
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['data'][0];

      List<BannersModel> banners = [];
      for (var item in data) {
        if (item['status'] == '1') {
          banners.add(BannersModel.fromJson(item));
        }
      }
      return banners;
    } else {
      throw Exception('Gagal ambil data');
    }
  }
}
