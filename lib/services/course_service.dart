import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:initial_folder/helper/user_info.dart';
import 'package:initial_folder/models/detail_course_model.dart';
import 'package:initial_folder/models/detail_rating_course_model.dart';
import 'package:initial_folder/models/course_model.dart';
import 'package:initial_folder/models/my_course_model.dart';

class CourseService {
  var _baseUrl = 'https://apivocasia.nouky.xyz/homepage/courses';
  var headers = {'Content-Type': 'application/json'};

  Future<List<CourseModel>> getOthersCourse(page) async {
    Uri url = Uri.parse(
        'https://apivocasia.nouky.xyz/homepage/courses?page=$page&limit=10');

    http.Response response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['data'][0];
      List<CourseModel> course = [];
      for (var item in data) {
        course.add(CourseModel.fromJson(item));
      }

      return course;
    } else {
      throw Exception('Data Kursus lainnya Gagal Diambil');
    }
  }

  Future<DetailCourseModel> getDetailCourse(String id) async {
    Uri url = Uri.parse('https://apivocasia.nouky.xyz/homepage/course/$id');
    http.Response response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      return DetailCourseModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Data Detail Kursus Gagal Diambil');
    }
  }

  Future<DetailCourseModel> getDetailCourseLogin(String id) async {
    int? idUser = await UsersInfo().getIdUser();
    String? token = await UsersInfo().getToken();
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    Uri url = Uri.parse(
        'https://apivocasia.nouky.xyz/homepage/course/$id?id_user=$idUser');
    http.Response response = await http.get(url, headers: headers);

    if (response.statusCode == 200) {
      return DetailCourseModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Data Detail Kursus Gagal Diambil');
    }
  }

  Future<RatingCourseDetailModel> getRatingDetailCourse(String id) async {
    Uri url =
        Uri.parse('https://apivocasia.nouky.xyz/homepage/course/rating/' + id);
    http.Response response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      return RatingCourseDetailModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Data Rating Detail Kursus Gagal Diambil');
    }
  }

  Future<MyCourseModel> getMyCourse() async {
    int? id = await UsersInfo().getIdUser();
    String? token = await UsersInfo().getToken();

    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    Uri url = Uri.parse('https://apivocasia.nouky.xyz/users/course/my/$id');
    http.Response response = await http.get(url, headers: headers);
    // print(response.body);
    if (response.statusCode == 200) {
      return MyCourseModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Data Kursus Saya, Gagal Diambil');
    }
  }

  Future<bool> postingReviewCourse(
      String review, int courseId, int valueRating) async {
    int? idUser = await UsersInfo().getIdUser();
    String? token = await UsersInfo().getToken();
    Uri url = Uri.parse('https://apivocasia.nouky.xyz/users/review/$idUser');
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    var body = jsonEncode(
        {"review": review, "course_id": courseId, "rating": valueRating});
    http.Response response = await http.post(url, headers: headers, body: body);
    // print(response.statusCode);

    if (response.statusCode == 201) {
      return true;
    } else {
      return false;
    }
  }

  Future<List<CourseModel>> getTopCourse() async {
    Uri url =
        Uri.parse('https://apivocasia.nouky.xyz/homepage/courses?top_course=1');

    http.Response response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['data'][0];
      List<CourseModel> course = [];
      for (var item in data) {
        course.add(CourseModel.fromJson(item));
      }

      return course;
    } else {
      throw Exception('Data Kursus Teratas Gagal Diambil');
    }
  }

  Future<List<CourseModel>> getLatestCourse() async {
    Uri url = Uri.parse('https://apivocasia.nouky.xyz/mobile/newcourse');

    http.Response response = await http.get(url, headers: headers);
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['data'][0];
      List<CourseModel> course = [];
      for (var item in data) {
        course.add(CourseModel.fromJson(item));
      }

      return course.where((item) => item.thumbnail != null).toList();
    } else {
      throw Exception('Data Kursus Teratas Gagal Diambil');
    }
  }
}
