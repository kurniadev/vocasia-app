import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:initial_folder/helper/user_info.dart';
import 'package:initial_folder/models/announcement_model.dart';

class AnnouncementService {
  Future<AnnouncementModel> getAnnouncement(String idCourse) async {
    String? token = await UsersInfo().getToken();
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Connection': 'Keep-Alive',
      'Authorization': 'Bearer $token',
    };
    Uri url =
        Uri.parse('https://apivocasia.nouky.xyz/users/announcement/$idCourse');

    http.Response response = await http.get(url, headers: headers);
    print(response.statusCode);
    if (response.statusCode == 200) {
      return AnnouncementModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Gagal ambil data');
    }
  }

  Future<AnnouncementLikeModel> likeAnnouncement(String tokenId) async {
    int? userId = await UsersInfo().getIdUser();
    String? token = await UsersInfo().getToken();
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    var body = jsonEncode({"id_user": userId, "token": tokenId});

    Uri url = Uri.parse(
        'https://apivocasia.nouky.xyz/instructor/users/announcement/reply');

    http.Response response = await http.post(url, headers: headers, body: body);
    print(response.statusCode);
    if (response.statusCode == 200) {
      return AnnouncementLikeModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Gagal ambil data');
    }
  }
}
