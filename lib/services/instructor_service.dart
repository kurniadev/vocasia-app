import 'dart:convert';

import 'package:initial_folder/models/instructor_model.dart';
import 'package:http/http.dart' as http;

class InstructorService {
  Future<InstructorModel> getInstructorProfile(id) async {
    Uri url = Uri.parse(
        'https://apivocasia.nouky.xyz/homepage/course/detail/instructor/$id');
    var headers = {'Content-Type': 'application/json'};
    http.Response response = await http.get(url, headers: headers);
    print('instructor' + response.body);
    if (response.statusCode == 200) {
      return InstructorModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Data Profile Instruktur Gagal Diambil');
    }
  }
}
