import 'dart:convert';

import 'package:initial_folder/models/course_model.dart';
import 'package:http/http.dart' as http;
import 'package:initial_folder/models/my_course_model.dart';

class SearchService {
  Future<List<CourseModel>> search(String? judul) async {
    Uri url = Uri.parse(
        'https://apivocasia.nouky.xyz/homepage/filter?keyword=$judul');
    var headers = {'content-type': 'application/json'};
    http.Response response = await http.get(url, headers: headers);
    print(response.body);
    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['data'][0];
      List<CourseModel> course = [];
      for (var item in data) {
        course.add(CourseModel.fromJson(item));
      }

      return course;
    } else {
      throw Exception('Gagal');
    }
  }

  Future<dynamic> filter({
    String namePrice = '',
    String nameLevel = '',
    String nameRating = '',
    String nameSubcategory = '',
    String price = '',
    String level = '',
    String rating = '',
    String subCategory = '',
  }) async {
    Map<String, dynamic> queryParams = {
      namePrice: price,
      nameLevel: level,
      nameRating: rating,
      nameSubcategory: subCategory,
    };

    Uri url = Uri.parse('https://apivocasia.nouky.xyz/homepage/filter')
        .replace(queryParameters: queryParams);

    http.Response response = await http.get(url);
    print(url);
    print(response.statusCode);
    if (response.statusCode == 500) {
      return url.toString();
    } else {
      throw Exception('Gagal');
    }
  }
}
