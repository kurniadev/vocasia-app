import 'dart:convert';

import 'package:initial_folder/helper/user_info.dart';
import 'package:initial_folder/models/lesson_course_model.dart';
import 'package:http/http.dart' as http;

class LessonCourseService {
  Future<LessonCourseModel> getLessonCourse(int id) async {
    String? token = await UsersInfo().getToken();
    int? idUser = await UsersInfo().getIdUser();

    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    Uri url = Uri.parse(
        'https://apivocasia.nouky.xyz/users/course/my/lesson?course=$id&user=$idUser');

    http.Response response = await http.get(url, headers: headers);
    // print(response.body);
    if (response.statusCode == 200) {
      return LessonCourseModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Data Lesson Kursus Saya, Gagal Diambil');
    }
  }

  Future<bool> updateLessonCourse(
    courseId,
    lessonId,
  ) async {
    int? idUser = await UsersInfo().getIdUser();
    String? token = await UsersInfo().getToken();
    var body = jsonEncode({
      "id_user": idUser,
      "course_id": courseId,
      "lesson_id": lessonId,
      "progress": "1"
    });
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    Uri url =
        Uri.parse('https://apivocasia.nouky.xyz/users/lesson/update/progress');
    http.Response response = await http.post(url, headers: headers, body: body);
    print(body);
    if (response.statusCode == 201) {
      return true;
    } else {
      throw Exception('Gagal Update');
    }
  }
}
