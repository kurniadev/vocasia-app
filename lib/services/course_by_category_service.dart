import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:initial_folder/models/course_model.dart';

class CourseByCategoryService {
  Future<List<CourseModel>> getCourseByCategory(categoriId) async {
    Uri url = Uri.parse(
        'https://apivocasia.nouky.xyz/homepage/courses?category=' + categoriId);
    var header = {'Content-Type': 'application/json; charset=UTF-8'};
    var response = await http.get(url, headers: header);

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['data'][0];
      List<CourseModel> courseByCategory = [];
      for (var item in data) {
        courseByCategory.add(CourseModel.fromJson(item));
      }
      return courseByCategory;
    } else {
      throw Exception('Gagal ambil data');
    }
  }
}
