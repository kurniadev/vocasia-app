import 'dart:convert';

import 'package:initial_folder/models/section_lesson_model.dart';
import 'package:http/http.dart' as http;

class SectionLessonService {
  Future<SectionLessonModel> getSectionLessonCourse(String id) async {
    Uri url = Uri.parse('https://apivocasia.nouky.xyz/homepage/section/' + id);
    var headers = {'Content-Type': 'application/json'};
    http.Response response = await http.get(url, headers: headers);

    if (response.statusCode == 200) {
      return SectionLessonModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Data Detail Kursus Gagal Diambil');
    }
  }
}
