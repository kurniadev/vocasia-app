import 'dart:convert';

import 'package:initial_folder/helper/user_info.dart';
import 'package:initial_folder/models/wishlist_model.dart';
import 'package:http/http.dart' as http;

class WishlistService {
  var _baseUrl = 'https://apivocasia.nouky.xyz/users/wishlist';
  Future<WishlistPostModel> addWishlist(
    int wishlistItem,
  ) async {
    Uri url = Uri.parse(_baseUrl);
    String? token = await UsersInfo().getToken();
    int? idUser = await UsersInfo().getIdUser();

    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };
    var body = jsonEncode({
      'id_user': idUser,
      'wishlist_item': wishlistItem,
    });
    http.Response response = await http.post(url, headers: headers, body: body);
    print(response.body);
    if (response.statusCode == 201 || response.statusCode == 200) {
      return WishlistPostModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Gagal Menambahkan Wishlist');
    }
  }

  Future<WishlistPostModel> deleteWishlist(
    int wishlistItem,
  ) async {
    Uri url = Uri.parse('$_baseUrl/delete/$wishlistItem');
    String? token = await UsersInfo().getToken();
    print(token);
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };

    http.Response response = await http.delete(
      url,
      headers: headers,
    );
    print(response.body);
    if (response.statusCode == 200) {
      return WishlistPostModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Gagal Menghapus Wishlist');
    }
  }

  Future<WishlistModel> getWishlist() async {
    int? id = await UsersInfo().getIdUser();
    String? token = await UsersInfo().getToken();
    Uri url = Uri.parse('$_baseUrl?users=$id');
    var headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    };

    http.Response response = await http.get(
      url,
      headers: headers,
    );
    print(response.body);
    if (response.statusCode == 200) {
      return WishlistModel.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Gagal Mendapatkan Wishlist');
    }
  }
}
