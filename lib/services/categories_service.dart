import 'dart:convert';

import 'package:initial_folder/models/catagories_model.dart';
import 'package:http/http.dart' as http;

class CategoriesService {
  Future<List<CategoriesModel>> getAllCategories() async {
    Uri url = Uri.parse('https://apivocasia.nouky.xyz/homepage/categories');
    var header = {'Content-Type': 'application/json; charset=UTF-8'};
    var response = await http.get(url, headers: header);

    if (response.statusCode == 200) {
      var data = jsonDecode(response.body)['data'][0];
      List<CategoriesModel> categories = [];
      for (var item in data) {
        if (item['parent_category'] == '0') {
          categories.add(CategoriesModel.fromJson(item));
        }
      }

      return categories;
    } else {
      throw Exception('Gagal ambil data');
    }
  }
}
