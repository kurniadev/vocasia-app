import 'package:flutter/widgets.dart';
import 'package:initial_folder/screens/course/sertif.dart';
import 'package:initial_folder/screens/home/components/home_page.dart';
import 'package:initial_folder/screens/home/home_screen.dart';
import 'package:initial_folder/screens/login/reset/reset_screen.dart';
import 'package:initial_folder/screens/login/reset/success_screen.dart';
import 'package:initial_folder/screens/login/login_email/login_email_screen.dart';
import 'package:initial_folder/screens/login/login_screen.dart';
import 'package:initial_folder/screens/registrasi/registrasi_screen.dart';
import 'package:initial_folder/screens/registrasi/registrasi_with_email/registrasi_email.dart';
import 'package:initial_folder/screens/registrasi/registrasi_with_email/success_regis.dart';

// We use name route
// All our routes will be available here
final Map<String, WidgetBuilder> routes = {
  HomeScreen.routeName: (context) => HomeScreen(),
  HomePage.routeName: (context) => HomePage(),
  // Splash.routeName: (context) => Splash(),
  RegistrationScreen.routeName: (context) => RegistrationScreen(),
  RegistrationEmail.routeName: (context) => RegistrationEmail(),
  LoginScreen.routeName: (context) => LoginScreen(),
  LoginEmail.routeName: (context) => LoginEmail(),
  ResetScreen.routeName: (context) => ResetScreen(),
  RegisSuccess.routeName: (context) => RegisSuccess(),
  ResetSuccess.routeName: (context) => ResetSuccess(),
  Sertif.routeName: (context) => Sertif()
  // '/detail_course': (context) => DetailCourseScreen(
  //     otherCourseModel:
  //         ModalRoute.of(context)?.settings.arguments as OthersCourseModel,id: ,),
  // '/course_by_category': (context) => CourseByCategory(
  //     courseByCategory: ModalRoute.of(context)?.settings.arguments as NewCourseModel, id: '',),
};
