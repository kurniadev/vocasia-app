import 'package:flutter/material.dart';
import 'package:initial_folder/helper/validator.dart';
import 'package:initial_folder/providers/carts_provider.dart';
import 'package:initial_folder/screens/cart/components/cart_list.dart';
import 'package:initial_folder/screens/checkout/checkout_page.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/widgets/login_regist/default_button.dart';
import 'package:provider/provider.dart';

class CartPage extends StatelessWidget {
  const CartPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget custombottomCart(String price, String total) {
      return Container(
        height: getProportionateScreenHeight(121),
        width: double.infinity,
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.all(16),
              child: Row(
                children: [
                  Text(
                    'Total :',
                    style: secondaryTextStyle.copyWith(
                        fontWeight: semiBold,
                        letterSpacing: 1,
                        fontSize: SizeConfig.blockHorizontal! * 3),
                  ),
                  Spacer(),
                  Text(
                    numberFormat(total),
                    style: primaryTextStyle.copyWith(
                      letterSpacing: 0.5,
                      decoration: TextDecoration.lineThrough,
                      color: fourthColor,
                      fontWeight: reguler,
                      fontSize: SizeConfig.blockHorizontal! * 2.7,
                    ),
                  ),
                  SizedBox(
                    width: getProportionateScreenWidth(8),
                  ),
                  Text(
                    numberFormat(price),
                    style: secondaryTextStyle.copyWith(
                      letterSpacing: 0.23,
                      fontWeight: reguler,
                      fontSize: SizeConfig.blockHorizontal! * 4.5,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(horizontal: 16),
              child: DefaultButton(
                text: 'Checkout',
                press: () => Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => CheckoutPage())),
              ),
            )
          ],
        ),
      );
    }

    return Scaffold(
        //backgroundColor: Color(0xff181818),
        appBar: AppBar(
          //backgroundColor: Colors.black,
          title: Text(
            'Keranjang',
            style: secondaryTextStyle.copyWith(
                letterSpacing: 1,
                fontWeight: semiBold,
                fontSize: getProportionateScreenWidth(14)),
          ),
        ),
        body: Consumer<CartsProvider>(builder: (context, state, _) {
          if (state.state == ResultState.Loading) {
            return Center(
              child: CircularProgressIndicator(
                color: primaryColor,
                strokeWidth: 2,
              ),
            );
          } else if (state.state == ResultState.HasData) {
            return SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.only(
                    left: getProportionateScreenWidth(16),
                    right: getProportionateScreenWidth(16)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${state.result?.data.length} Kursus di Keranjang ',
                      style: secondaryTextStyle.copyWith(
                        letterSpacing: 1,
                        fontWeight: semiBold,
                        fontSize: getProportionateScreenWidth(14),
                      ),
                    ),
                    SizedBox(
                      height: getProportionateScreenHeight(13),
                    ),
                    ListView.builder(
                      physics: ScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: state.result!.data.length,
                      itemBuilder: (context, index) {
                        var carts = state.result!.data[index];
                        return CartList(
                          idCourse: carts.courseId ?? '0',
                          id: carts.cartId ?? '',
                          image: carts.thumbnail ??
                              'https://vocasia.id/uploads/thumbnails/course_thumbnails/course_thumbnail_default_63.jpg',
                          title: carts.title ?? '',
                          instruktur: carts.instructor ?? '',
                          price: carts.price ?? '',
                          discountPrice: carts.discountPrice ?? '',
                        );
                      },
                    )
                  ],
                ),
              ),
            );
          } else if (state.state == ResultState.NoData) {
            return SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(height: 48),
                  Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: getProportionateScreenWidth(16)),
                    child: Center(
                      child: Column(
                        children: [
                          Container(
                            width: getProportionateScreenWidth(100),
                            height: getProportionateScreenWidth(100),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(2),
                              image: DecorationImage(
                                fit: BoxFit.fill,
                                image: AssetImage("assets/images/search.png"),
                              ),
                            ),
                          ),
                          SizedBox(height: 16),
                          Text(
                            "Keranjang Kosong",
                            style: secondaryTextStyle.copyWith(
                              letterSpacing: 1,
                              fontWeight: semiBold,
                              fontSize: getProportionateScreenWidth(14),
                              color: tenthColor,
                            ),
                          ),
                          SizedBox(height: 4),
                          Text(
                            "Keranjang kamu kosong, tetap berbelanja dan cari kursus",
                            textAlign: TextAlign.center,
                            style: primaryTextStyle.copyWith(
                              letterSpacing: 0.5,
                              fontWeight: reguler,
                              fontSize: getProportionateScreenWidth(12),
                              color: secondaryColor,
                            ),
                          ),
                          SizedBox(height: 16),
                          Container(
                            margin: EdgeInsets.symmetric(
                                horizontal: getProportionateScreenWidth(78),
                                vertical: getProportionateScreenHeight(10)),
                            child: DefaultButton(
                              weight: reguler,
                              text: 'Belanja Kursus',
                              press: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            );
          } else if (state.state == ResultState.Error) {
            return AlertDialog(
              title: const Text('Koneksi Internet'),
              content: const Text('Terjadi Kesalahan'),
              actions: <Widget>[
                TextButton(
                  onPressed: () => Navigator.pop(context, 'Cancel'),
                  child: const Text('Cancel'),
                ),
              ],
            );
          } else {
            return Center(child: Text(''));
          }
        }),
        bottomNavigationBar:
            Consumer<CartsProvider>(builder: (context, state, _) {
          if (state.state == ResultState.Loading) {
            return Container(
              width: double.infinity,
              height: getProportionateScreenHeight(121),
              child: Center(
                child: CircularProgressIndicator(
                  color: primaryColor,
                  strokeWidth: 2,
                ),
              ),
            );
          } else if (state.state == ResultState.HasData) {
            var total = state.result!.data
                .map((e) => int.parse(e.price ?? '0'))
                .toList();
            var result = 0;
            for (var i = 0; i < total.length; i++) {
              result += total[i];
            }
            print(total.whereType());
            return custombottomCart(
                state.result?.totalPayment ?? '0', result.toString());
          } else if (state.state == ResultState.NoData) {
            return Text(
              "",
              style: secondaryTextStyle.copyWith(
                letterSpacing: 1,
                fontWeight: semiBold,
                fontSize: getProportionateScreenWidth(14),
                color: tenthColor,
              ),
            );
          } else if (state.state == ResultState.Error) {
            return Text('');
          } else {
            return Center(child: Text(''));
          }
        }));
  }
}
