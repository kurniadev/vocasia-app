import 'package:flutter/material.dart';
import 'package:initial_folder/helper/validator.dart';
import 'package:initial_folder/providers/cart_provider.dart';
import 'package:initial_folder/providers/carts_provider.dart';
import 'package:initial_folder/providers/whislist_provider.dart';
import 'package:initial_folder/providers/wishlist_post_provider.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:provider/provider.dart';

class CartList extends StatelessWidget {
  const CartList(
      {Key? key,
      required this.image,
      required this.title,
      required this.instruktur,
      required this.price,
      required this.id,
      required this.idCourse,
      required this.discountPrice})
      : super(key: key);
  final String image, instruktur, title, price, discountPrice, id, idCourse;
  @override
  Widget build(BuildContext context) {
    // CartsDatabaseProvider cartsDatabaseProvider =
    //     Provider.of<CartsDatabaseProvider>(context);
    // WishlistPostProvider wishlistPostProvider =
    //     Provider.of<WishlistPostProvider>(context);
    // Future _showMessage() {
    //   return showDialog(
    //     context: context,
    //     builder: (context) => AlertDialog(
    //       contentPadding: EdgeInsets.fromLTRB(22, 30, 22, 30),
    //       content: Text(
    //         'Berhasil menambahkan kursus ke wishlist',
    //         textAlign: TextAlign.center,
    //         style: primaryTextStyle.copyWith(
    //             fontSize: getProportionateScreenWidth(12), letterSpacing: 1),
    //       ),
    //     ),
    //   );
    // }

    // addWishlist() async {
    //   var connectivityResult = await (Connectivity().checkConnectivity());
    //   if (connectivityResult == ConnectivityResult.none) {
    //     ScaffoldMessenger.of(context).showSnackBar(
    //       SnackBar(
    //         duration: Duration(seconds: 1),
    //         backgroundColor: Colors.red[600],
    //         content: Text(
    //           'No Internet Connections',
    //           textAlign: TextAlign.center,
    //           style: primaryTextStyle.copyWith(color: Colors.white),
    //         ),
    //         behavior: SnackBarBehavior.floating,
    //         shape: RoundedRectangleBorder(
    //           borderRadius: BorderRadius.circular(5),
    //         ),
    //       ),
    //     );
    //   } else {
    //     await wishlistPostProvider.addWishlist(int.parse(idCourse));
    //     // wishlistPostProvider.setWhishlist(idCourse);
    //     _showMessage();
    //   }
    // }

    // Future _courseExist() {
    //   return showDialog(
    //     context: context,
    //     builder: (context) => AlertDialog(
    //       contentPadding: EdgeInsets.all(15),
    //       content: Text(
    //         'Kursus sudah ada dalam wishlist',
    //         style: primaryTextStyle.copyWith(
    //             fontSize: getProportionateScreenWidth(12), letterSpacing: 1),
    //       ),
    //     ),
    //   );
    // }

    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Flexible(
                flex: 11,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: getProportionateScreenWidth(156),
                      height: getProportionateScreenWidth(88),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(2),
                        image: DecorationImage(
                          fit: BoxFit.fill,
                          image: NetworkImage(image),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: getProportionateScreenHeight(8),
                    ),
                    Row(
                      children: [
                        GestureDetector(
                          onTap: () async {
                            await Provider.of<CartProvider>(context,
                                    listen: false)
                                .deleteCart(id);
                            await Provider.of<CartsProvider>(context,
                                    listen: false)
                                .getCarts();
                          },
                          child: Text(
                            'Hapus',
                            style: primaryTextStyle.copyWith(
                                fontWeight: reguler,
                                color: primaryColor,
                                fontSize: SizeConfig.blockHorizontal! * 2.5,
                                letterSpacing: 0.5),
                          ),
                        ),
                        SizedBox(
                          width: getProportionateScreenWidth(8.5),
                        ),
                        GestureDetector(
                          onTap: () async {
                            await Provider.of<CartProvider>(context,
                                    listen: false)
                                .deleteCart(id);
                            await Provider.of<WishlistPostProvider>(context,
                                    listen: false)
                                .addWishlist(int.parse(idCourse));
                            await Provider.of<WishlistProvider>(context,
                                    listen: false)
                                .getWishlist();
                            await Provider.of<CartsProvider>(context,
                                    listen: false)
                                .getCarts();
                          },
                          child: Text(
                            'Pindahkan ke wishlist',
                            style: primaryTextStyle.copyWith(
                                fontWeight: reguler,
                                color: primaryColor,
                                fontSize: SizeConfig.blockHorizontal! * 2.5,
                                letterSpacing: 0.5),
                          ),
                        ),
                      ],
                    )
                  ],
                )),
            Flexible(
              flex: 10,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: secondaryTextStyle.copyWith(
                      letterSpacing: 1,
                      fontWeight: semiBold,
                      fontSize: getProportionateScreenWidth(14),
                    ),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(
                    height: getProportionateScreenHeight(4),
                  ),
                  Text(
                    'oleh $instruktur',
                    style: primaryTextStyle.copyWith(
                      color: secondaryColor,
                      fontSize: getProportionateScreenWidth(12),
                    ),
                  ),
                  SizedBox(
                    height: getProportionateScreenHeight(2),
                  ),
                  Text(
                    numberFormat(discountPrice),
                    style: primaryTextStyle.copyWith(
                      fontSize: getProportionateScreenWidth(12),
                    ),
                  ),
                  SizedBox(
                    height: getProportionateScreenHeight(2),
                  ),
                  Text(
                    numberFormat(price),
                    style: primaryTextStyle.copyWith(
                      decoration: TextDecoration.lineThrough,
                      color: secondaryColor,
                      fontSize: getProportionateScreenWidth(10),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
        SizedBox(
          height: getProportionateScreenHeight(16),
        ),
        Divider(
          color: fourthColor,
        ),
        SizedBox(
          height: getProportionateScreenHeight(5),
        ),
      ],
    );
  }
}
