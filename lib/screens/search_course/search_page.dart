import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:initial_folder/helper/validator.dart';
import 'package:initial_folder/providers/categories_provider.dart';
import 'package:initial_folder/providers/page_provider.dart';
import 'package:initial_folder/screens/home/components/body_comp/course_by_category.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/widgets/login_regist/custom_font_awesome.dart';
import 'package:initial_folder/widgets/search_and_filter_course.dart';
import 'package:provider/provider.dart';
import 'package:recase/recase.dart';
import 'component/list_category_icon.dart';

class SearchPage extends StatelessWidget {
  const SearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    PageProvider pageProvider = Provider.of<PageProvider>(context);
    final listIc = <Ic>[
      Ic(
        name: '0xee2a',
      ),
      Ic(
        name: '0xe03c',
      ),
    ];

    return SafeArea(
      child: Scaffold(
          body: SingleChildScrollView(
        child: Container(
          // color: primaryColor,
          margin: EdgeInsets.symmetric(vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  IconButton(
                      splashRadius: 15,
                      onPressed: () {
                        pageProvider.currentIndex = 0;
                      },
                      icon: Icon(
                        Icons.arrow_back,
                        size: 20,
                      )),
                  Expanded(
                    child: Padding(
                        padding: EdgeInsets.only(
                            right: getProportionateScreenWidth(16)),
                        child: GestureDetector(
                          onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      SearchAndFilterCourse())),
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                border: Border.all(color: secondaryColor)),
                            height: 40,
                            child: Row(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(left: 13),
                                  child: Icon(
                                    FeatherIcons.search,
                                    size: 18,
                                    color: secondaryColor,
                                  ),
                                ),
                                SizedBox(
                                  width: getProportionateScreenWidth(10),
                                ),
                                Text(
                                  'Cari kursus',
                                  style: secondaryTextStyle.copyWith(
                                      color: secondaryColor, fontSize: 12),
                                )
                              ],
                            ),
                          ),
                        )),
                  ),
                ],
              ),
              SizedBox(
                height: getProportionateScreenWidth(16),
              ),
              Container(
                margin: EdgeInsets.only(left: getProportionateScreenWidth(16)),
                child: Text(
                  'Cari Berdasarkan Kategori',
                  style: primaryTextStyle.copyWith(
                    letterSpacing: 0.5,
                  ),
                ),
              ),
              // Column(
              //   children: listIc
              //       .map((e) =>
              //           Icon(IconData(int.parse(e.name), fontFamily: e.tile)))
              //       .toList(),
              // ),
              SizedBox(
                height: getProportionateScreenWidth(12),
              ),
              Container(
                child:
                    Consumer<CategoriesProvider>(builder: (context, state, _) {
                  if (state.state == ResultState.Loading) {
                    return Center(
                      child: CircularProgressIndicator(
                        color: secondaryColor,
                        strokeWidth: 2,
                      ),
                    );
                  } else if (state.state == ResultState.HasData) {
                    var categori = state.result;

                    return Column(
                      children: categori
                          .map((e) => ListCategoryIcon(
                              title: e.nameCategory ?? '',
                              // iconFa: Icon(fontAwesomeIconsFromString(iconCategory(e.fontAwesomeClass?? '').ca)),
                              iconFa: Icon(
                                fontAwesomeIconsFromString(
                                    'FontAwesomeIcons.${iconCategory(e.fontAwesomeClass ?? '')}'),
                                size: 18,
                                color: secondaryColor,
                              ),
                              onTap: () {
                                Navigator.of(context).push(
                                  MaterialPageRoute(
                                    builder: (context) => CourseByCategory(
                                      id: e.id!,
                                      name: e.nameCategory ?? '',
                                    ),
                                  ),
                                );
                              }))
                          .toList(),
                    );
                  } else if (state.state == ResultState.NoData) {
                    return Center(child: Text(state.message));
                  } else if (state.state == ResultState.Error) {
                    return Center(child: Text("No internet connections."));
                  } else {
                    return Center(
                        child: Text(
                      'Terjadi Kesalahan ',
                      style: primaryTextStyle,
                    ));
                  }
                }),
              )
            ],
          ),
        ),
      )),
    );
  }
}

class Ic {
  String name;
  String tile;
  Ic({required this.name, this.tile = 'MaterialIcons'});
}
