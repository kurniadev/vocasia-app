import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:initial_folder/providers/categories_provider.dart';
import 'package:initial_folder/providers/filters_course_provider.dart'
    as filterCourseProv;
import 'package:initial_folder/providers/search_provider.dart'
    as searchProvider;
import 'package:initial_folder/screens/detail_course/components/ulasan.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:provider/provider.dart';

class Filter extends StatelessWidget {
  const Filter({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    filterCourseProv.FilterCourseProvider filterCourseProvider =
        Provider.of<filterCourseProv.FilterCourseProvider>(context);

    final listChoices = <ItemChoiceFilter>[
      ItemChoiceFilter('', '0', 'Semua '),
      ItemChoiceFilter('1', '1', '1'),
      ItemChoiceFilter('2', '1', '2'),
      ItemChoiceFilter('3', '1', '3'),
      ItemChoiceFilter('4', '1', '4'),
      ItemChoiceFilter('5', '1', '5'),
    ];

    Widget categoriCheckBox(String id, String title) {
      return Container(
        margin: EdgeInsets.only(bottom: getProportionateScreenWidth(8)),
        child: Row(
          children: [
            SizedBox(
              width: 24,
              height: 24,
              child: Checkbox(
                value: id == filterCourseProvider.currentIndex ? true : false,
                onChanged: (c) {
                  filterCourseProvider.currentIndex = id;
                },
                activeColor: primaryColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
                side: BorderSide(color: fourthColor),
              ),
            ),
            SizedBox(
              width: getProportionateScreenWidth(12),
            ),
            Expanded(
              child: GestureDetector(
                onTap: () {
                  filterCourseProvider.currentIndex = id;
                },
                child: Text(
                  title,
                  style: primaryTextStyle.copyWith(
                      color: secondaryColor,
                      fontSize: getProportionateScreenWidth(12),
                      letterSpacing: 0.5),
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget levelCheckBox(String name, String title) {
      return Container(
        margin: EdgeInsets.only(bottom: getProportionateScreenWidth(8)),
        child: Row(
          children: [
            SizedBox(
              width: 24,
              height: 24,
              child: Checkbox(
                value: name == filterCourseProvider.currentIndexLevelCheckBox
                    ? true
                    : false,
                onChanged: (c) {
                  filterCourseProvider.currentIndexLevelCheckBox = name;
                },
                activeColor: primaryColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
                side: BorderSide(color: fourthColor),
              ),
            ),
            SizedBox(
              width: getProportionateScreenWidth(12),
            ),
            Expanded(
              child: GestureDetector(
                onTap: () {
                  filterCourseProvider.currentIndexLevelCheckBox = name;
                },
                child: Text(
                  title,
                  style: primaryTextStyle.copyWith(
                      color: secondaryColor,
                      fontSize: getProportionateScreenWidth(12),
                      letterSpacing: 0.5),
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget radioLevel(
      String val,
      String title,
    ) {
      return Container(
        margin: EdgeInsets.only(bottom: getProportionateScreenWidth(8)),
        child: Row(
          children: [
            SizedBox(
              width: 24,
              height: 24,
              child: Theme(
                data: ThemeData(
                  unselectedWidgetColor: fourthColor,
                ),
                child: Radio(
                    activeColor: primaryColor,
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    value: val,
                    groupValue: filterCourseProvider.currentIndexRadio,
                    onChanged: (c) {
                      filterCourseProvider.currentIndexRadio = val;
                      // filterCourseProvider.nameCurrentIndexRadio = nameVal;
                    }),
              ),
            ),
            SizedBox(
              width: getProportionateScreenWidth(12),
            ),
            Expanded(
              child: GestureDetector(
                onTap: () {
                  filterCourseProvider.currentIndexRadio = val;
                  // filterCourseProvider.nameCurrentIndexRadio = nameVal;
                },
                child: Text(
                  title,
                  style: primaryTextStyle.copyWith(
                      color: secondaryColor,
                      fontSize: getProportionateScreenWidth(12),
                      letterSpacing: 0.5),
                ),
              ),
            ),
          ],
        ),
      );
    }

    Widget bottomNav() {
      return Container(
          width: double.infinity,
          height: getProportionateScreenHeight(60),
          decoration: BoxDecoration(
              border:
                  Border.symmetric(horizontal: BorderSide(color: fourthColor)),
              boxShadow: [
                BoxShadow(
                    offset: Offset(0, -2),
                    blurRadius: 50,
                    color: fourthColor.withOpacity(0.15))
              ]),
          child: Center(
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(getProportionateScreenWidth(90),
                      getProportionateScreenHeight(33)),
                  primary: primaryColor,
                ),
                onPressed: () async {
                  Provider.of<searchProvider.SearchProvider>(context,
                          listen: false)
                      .clearSearchBox();
                  // if (filterCourseProvider.currentIndex.isEmpty &&
                  //     filterCourseProvider.currentIndexLevelCheckBox.isEmpty) {
                  //   print('gg');
                  // } else {
                  await filterCourseProvider.filterCourse(
                      price: filterCourseProvider.currentIndexRadio,
                      level: filterCourseProvider.currentIndexLevelCheckBox,
                      rating: filterCourseProvider.currentIndexRating,
                      subCategory: filterCourseProvider.currentIndex);
                  // }

                  // Navigator.pop(context);
                },
                child: Text(
                  'Terapkan Filter',
                  style: thirdTextStyle.copyWith(color: Colors.black),
                )),
          ));
    }

    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        actions: [
          TextButton(
              onPressed: () {
                filterCourseProvider.reset();
              },
              child: Text(
                'Reset',
                style: primaryTextStyle.copyWith(letterSpacing: 0.5),
              ))
        ],
      ),
      body: Container(
        margin:
            EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(16)),
        child: ListView(
          children: [
            // ExpansionTile(
            //   tilePadding: EdgeInsets.zero,
            //   title: Text(
            //     'Urutkan',
            //     style: secondaryTextStyle.copyWith(letterSpacing: 1),
            //   ),
            //   children: [Text('1'), Text('2'), Text('3')],
            // ),
            Text(
              'Tingkat',
              style: secondaryTextStyle.copyWith(
                  color: tenthColor, letterSpacing: 1),
            ),
            SizedBox(
              height: getProportionateScreenWidth(8),
            ),
            categoriCheckBox('', 'Semua'),
            Consumer<CategoriesProvider>(builder: (context, state, _) {
              if (state.state == ResultState.Loading) {
                return Center(
                  child: CircularProgressIndicator(
                    color: secondaryColor,
                    strokeWidth: 2,
                  ),
                );
              } else if (state.state == ResultState.HasData) {
                var categori = state.result;
                return Column(
                  children: categori
                      .map((e) =>
                          categoriCheckBox(e.id ?? '0', e.nameCategory ?? ''))
                      .toList(),
                );
              } else if (state.state == ResultState.NoData) {
                return Center(child: Text(state.message));
              } else if (state.state == ResultState.Error) {
                return Center(child: Text("No internet connections."));
              } else {
                print(state.result.length);
                return Center(child: Text(''));
              }
            }),
            Text(
              'Harga',
              style: secondaryTextStyle.copyWith(
                  color: tenthColor, letterSpacing: 1),
            ),
            SizedBox(
              height: getProportionateScreenWidth(8),
            ),
            radioLevel(
              '',
              'Semua',
            ),
            radioLevel(
              'price=1',
              'Gratis',
            ),
            radioLevel(
              'price=2',
              'Berbayar',
            ),
            Text(
              'Tingkat',
              style: secondaryTextStyle.copyWith(
                  color: tenthColor, letterSpacing: 1),
            ),
            SizedBox(
              height: getProportionateScreenWidth(8),
            ),
            levelCheckBox('', 'Semua'),
            levelCheckBox('beginner', 'Pemula'),
            levelCheckBox('intermediate', 'Menengah'),
            levelCheckBox('expert', 'Ahli'),
            Text(
              'Rating',
              style: secondaryTextStyle.copyWith(
                  color: tenthColor, letterSpacing: 1),
            ),
            SizedBox(
              height: getProportionateScreenWidth(8),
            ),
            Wrap(
                spacing: 5,
                runSpacing: 5,
                children: listChoices
                    .map((e) => ChoiceChip(
                          materialTapTargetSize:
                              MaterialTapTargetSize.shrinkWrap,
                          backgroundColor: Color(0xff181818),
                          side: BorderSide(color: Colors.white),
                          labelPadding: EdgeInsets.symmetric(horizontal: 6),
                          label: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              (e.icon == '0')
                                  ? Text('')
                                  : FaIcon(
                                      FontAwesomeIcons.solidStar,
                                      color: primaryColor,
                                      size: getProportionateScreenWidth(11),
                                    ),
                              SizedBox(
                                width: 4,
                              ),
                              Text(e.label,
                                  style: primaryTextStyle.copyWith(
                                      color: filterCourseProvider
                                                  .currentIndexRating ==
                                              e.id.toString()
                                          ? Colors.black
                                          : tenthColor,
                                      letterSpacing: 0.5,
                                      fontSize: getProportionateScreenWidth(12),
                                      fontWeight: reguler)),
                            ],
                          ),
                          selected: filterCourseProvider.currentIndexRating ==
                              e.id.toString(),
                          selectedColor: tenthColor,
                          onSelected: (_) {
                            filterCourseProvider.currentIndexRating =
                                e.id.toString();
                          },
                          // backgroundColor: color,
                          elevation: 1,
                        ))
                    .toList()),
            SizedBox(
              height: getProportionateScreenWidth(16),
            ),
          ],
        ),
      ),
      bottomNavigationBar: bottomNav(),
    );
  }
}

class ItemChoiceFilter {
  final String id;
  final String icon;
  final String label;

  ItemChoiceFilter(this.id, this.icon, this.label);
}
