import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../../../size_config.dart';
import '../../../theme.dart';

class ListCategoryIcon extends StatelessWidget {
  const ListCategoryIcon({
    Key? key,
    required this.title,
    required this.iconFa,
    required this.onTap,
  }) : super(key: key);
  final String title;
  final Icon iconFa;
  final Function()? onTap;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
          left: getProportionateScreenWidth(21),
          right: getProportionateScreenWidth(17),
          bottom: getProportionateScreenWidth(11)),
      child: InkWell(
        onTap: onTap,
        child: Row(
          children: [
            iconFa,
            SizedBox(
              width: getProportionateScreenWidth(10),
            ),
            Expanded(
              child: Html(
                data: title,
                style: {
                  "body": Style(
                      margin: EdgeInsets.zero,
                      padding: EdgeInsets.zero,
                      fontSize: FontSize(12),
                      fontWeight: reguler,
                      letterSpacing: 0.5,
                      fontFamily: 'Noto Sans',
                      color: tenthColor),
                },
              ),
            ),
            Icon(
              Icons.keyboard_arrow_right,
              size: 22,
            ),
          ],
        ),
      ),
    );
  }
}
