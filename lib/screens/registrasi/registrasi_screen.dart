import 'package:flutter/material.dart';
import 'package:initial_folder/screens/splash/splash_screen_login.dart';
import 'package:initial_folder/screens/home/home_screen.dart';
import 'package:initial_folder/screens/login/login_screen.dart';
import 'package:initial_folder/screens/registrasi/registrasi_with_email/registrasi_email.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/widgets/login_regist/default_button.dart';
import 'package:initial_folder/widgets/login_regist/default_icon_button.dart';
import 'package:initial_folder/widgets/login_regist/footer.dart';
import 'package:initial_folder/widgets/login_regist/header.dart';
import 'package:initial_folder/providers/google_sign_in.dart';
import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
// GoogleSignIn _googleSignIn = GoogleSignIn(
//   // Optional clientId
//   // clientId: '479882132969-9i9aqik3jfjd7qhci1nqf0bm2g71rm1u.apps.googleusercontent.com',
//   scopes: <String>[
//     'email',
//     'https://www.googleapis.com/auth/contacts.readonly',
//   ],
// );

// Future<void> _handleSignIn() async {
//   try {
//     await _googleSignIn.signIn();
//   } catch (error) {
//     print(error);
//   }
// }

class RegistrationScreen extends StatelessWidget {
  const RegistrationScreen({Key? key}) : super(key: key);
  static String routeName = "/registration";
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: StreamBuilder(
            stream: FirebaseAuth.instance.authStateChanges(),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return Center(child: CircularProgressIndicator());
              } else if (snapshot.hasData) {
                return HomeScreen();
              } else if (snapshot.hasError) {
                return Center(child: Text('Something Went Wrong!'));
              } else {
                return SingleChildScrollView(
                  child: Column(children: [
                    Container(
                        padding: EdgeInsets.only(
                            left: getProportionateScreenWidth(12),
                            top: getProportionateScreenHeight(8)),
                        alignment: Alignment.topLeft,
                        child: TextButton(
                            child: Text('Daftar nanti',
                                style: primaryTextStyle.copyWith(
                                    color: primaryColor,
                                    fontWeight: reguler,
                                    fontSize: getProportionateScreenWidth(14))),
                            onPressed: () {
                              Navigator.of(context).pushNamedAndRemoveUntil(
                                  HomeScreen.routeName,
                                  (Route<dynamic> route) => false);
                              print('nanti');
                            })),
                    SizedBox(height: getProportionateScreenHeight(35)),
                    Container(
                      alignment: Alignment.center,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Header(
                            jarak: 16,
                            text:
                                'Yuk Siapkan Dirimu, Mulai Upgrade Skill Bersama Vocasia',
                            style: secondaryTextStyle.copyWith(
                              color: tenthColor,
                              fontWeight: semiBold,
                              fontSize: getProportionateScreenWidth(14),
                            ),
                          ),
                          SizedBox(height: getProportionateScreenHeight(32)),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: getProportionateScreenWidth(15.0)),
                            child: DefaultIconButton(
                              text: "Lanjutkan dengan Google",
                              press: () async {
                                // Navigator.pop(
                                //     context, RegistrationScreen.routeName);
                                final provider =
                                    Provider.of<GoogleSignInProvider>(context,
                                        listen: false);
                                print('Login Gmail');
                                await provider.googleLogin().then((value) {
                                  if (snapshot.hasData) {
                                    Condition.loginFirebase = true;
                                    Navigator.of(context)
                                        .pushNamedAndRemoveUntil(
                                            HomeScreen.routeName,
                                            (Route<dynamic> route) => false);
                                  }
                                });
                              },
                              icon: 'assets/icons/google.png',
                              iconWidth: 25,
                              iconHeight: 25,
                            ),
                          ),
                          SizedBox(height: getProportionateScreenWidth(16)),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: getProportionateScreenWidth(15.0)),
                            child: DefaultIconButton(
                              text: "Lanjutkan dengan Facebook",
                              press: () {
                                //Navigator.pushNamed(context, SignInScreen.routeName);
                                print('Login FB');
                              },
                              icon: 'assets/icons/facebook.png',
                              iconWidth: 25,
                              iconHeight: 25,
                            ),
                          ),
                          SizedBox(height: getProportionateScreenWidth(16)),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: getProportionateScreenWidth(15.0)),
                            child: DefaultButton(
                              text: "Daftar dengan Email",
                              press: () {
                                Navigator.pushNamed(
                                    context, RegistrationEmail.routeName);
                              },
                            ),
                          ),
                          SizedBox(height: getProportionateScreenHeight(32)),
                          Footer(
                            textOne: 'Sudah punya akun? ',
                            textTwo: 'Masuk',
                            route: LoginScreen.routeName,
                          )
                        ],
                      ),
                    ),
                    // Padding(
                    //   padding: EdgeInsets.symmetric(horizontal: 50),
                    //   child: Text(
                    //       'Dengan membuat akun, anda menyetujui Kebijakan dan Persyaratan Privasi',
                    //       maxLines: 2,
                    //       textAlign: TextAlign.center,
                    //       style: TextStyle(
                    //           height: 2,
                    //           fontSize: getProportionateScreenWidth(14),
                    //           fontWeight: FontWeight.w400,
                    //           color: Colors.white)),
                    // ),
                  ]),
                );
              }
            }),
      ),
    );
  }
}
