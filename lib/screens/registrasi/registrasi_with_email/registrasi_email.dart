import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:initial_folder/screens/registrasi/registrasi_with_email/success_regis.dart';
import 'package:initial_folder/screens/splash/splash_screen_login.dart';
import 'package:initial_folder/helper/validator.dart';
import 'package:initial_folder/providers/auth_provider.dart';
import 'package:initial_folder/screens/login/login_screen.dart';
import 'package:initial_folder/screens/registrasi/registrasi_screen.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/widgets/login_regist/custom_text_form_field.dart';
import 'package:initial_folder/widgets/login_regist/default_button.dart';
import 'package:initial_folder/widgets/login_regist/footer.dart';
import 'package:initial_folder/widgets/login_regist/header.dart';
import 'package:initial_folder/widgets/login_regist/loading_button.dart';
import 'package:provider/provider.dart';
import 'package:initial_folder/services/auth_service.dart';
import 'package:initial_folder/helper/user_info.dart';

class RegistrationEmail extends StatefulWidget {
  static String routeName = "/registration_email";
  RegistrationEmail({Key? key}) : super(key: key);

  @override
  _RegistrationEmailState createState() => _RegistrationEmailState();
}

class _RegistrationEmailState extends State<RegistrationEmail> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController nameController = TextEditingController(text: '');

  final TextEditingController emailController = TextEditingController(text: '');

  final TextEditingController passwordController =
      TextEditingController(text: '');
  final TextEditingController rePasswordController =
      TextEditingController(text: '');
  bool isLoading = false;
  bool invalidEmail = false;
  bool _isObscure = true;
  bool _isObscure1 = true;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    //ShowHidePassword showHidePassword = Provider.of<ShowHidePassword>(context);
    handleSignUp() async {
      setState(() {
        isLoading = true;
      });

      if (await authProvider.register(
        name: nameController.text,
        email: emailController.text,
        password: passwordController.text,
      )) {
        var postEmail = await UsersInfo().setEmail(emailController.text);
        await AuthService()
            .login(
          email: emailController.text,
          password: passwordController.text,
        )
            .then((value) async {
          Condition.loginEmail = true;
          Navigator.of(context).pushNamedAndRemoveUntil(
              RegisSuccess.routeName, (Route<dynamic> route) => false);
        });
      } else {
        setState(() {
          invalidEmail = true;
        });
      }

      setState(() {
        isLoading = false;
      });
    }

    void _validateInputs() {
      if (this._formKey.currentState!.validate()) {
        handleSignUp();
      }
    }

    Widget form() {
      Widget nameInput() {
        return CustomTextField(
          pad: getProportionateScreenWidth(16),
          controler: nameController,
          hinttext: 'Masukan nama lengkap',
          title: 'Nama Lengkap',
          validate: validateName,
        );
      }

      Widget emailInput() {
        return CustomTextField(
          pad: getProportionateScreenWidth(16),
          controler: emailController,
          hinttext: 'Masukan email',
          title: 'Email',
          validate: validateEmail,
        );
      }

      Widget inValidEmail() {
        return GestureDetector(
          onTap: () {
            setState(() {
              invalidEmail = false;
            });
          },
          child: Container(
            margin: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(16)),
            width: double.infinity,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Email',
                  style: secondaryTextStyle.copyWith(
                      fontWeight: semiBold,
                      fontSize: getProportionateScreenWidth(12),
                      color: secondaryColor,
                      letterSpacing: 0.5),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(4),
                ),
                Container(
                  padding: EdgeInsets.symmetric(
                      horizontal: getProportionateScreenWidth(10),
                      vertical: getProportionateScreenHeight(14.2)),
                  width: double.infinity,
                  child: Text(
                    emailController.text,
                    style: primaryTextStyle.copyWith(
                        fontSize: getProportionateScreenWidth(14),
                        color: secondaryColor,
                        letterSpacing: 0.5),
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: Colors.red[900]!),
                  ),
                ),
                Text(
                  'Email Anda sudah terdaftar',
                  style: primaryTextStyle.copyWith(
                      fontSize: getProportionateScreenWidth(12),
                      color: Colors.red[600]!,
                      letterSpacing: 0.5),
                )
              ],
            ),
          ),
        );
      }

      Widget passwordInput() {
        return CustomTextField(
          pad: getProportionateScreenWidth(16),
          controler: passwordController,
          hinttext: 'Masukan password',
          title: 'Password',
          obscuretext: _isObscure,
          validate: validatePassword,
          suffix: GestureDetector(
              onTap: () => setState(() {
                    _isObscure = !_isObscure;
                  }),
              child: _isObscure
                  ? Icon(
                      FeatherIcons.eyeOff,
                      color: secondaryColor,
                      size: 18,
                    )
                  : Icon(
                      FeatherIcons.eye,
                      color: secondaryColor,
                      size: 18,
                    )),
        );
      }

      Widget rePasswordInput() {
        return CustomTextField(
          pad: getProportionateScreenWidth(16),
          controler: rePasswordController,
          hinttext: 'Masukan password',
          title: 'Konfirmasi Password',
          obscuretext: _isObscure1,
          suffix: GestureDetector(
              onTap: () => setState(() {
                    _isObscure1 = !_isObscure1;
                  }),
              child: _isObscure1
                  ? Icon(
                      FeatherIcons.eyeOff,
                      color: secondaryColor,
                      size: 18,
                    )
                  : Icon(
                      FeatherIcons.eye,
                      color: secondaryColor,
                      size: 18,
                    )),
          validate: (String? value) {
            if (value!.length == 0)
              return 'Password tidak boleh kosong';
            else if (value.length < 8)
              return 'Password minimal harus berjumlah 8 karakter';
            else if (value != passwordController.text) {
              return 'Password tidak sama';
            }
            return null;
          },
        );
      }

      Widget button() {
        return Padding(
          padding: EdgeInsets.only(
              left: getProportionateScreenWidth(15),
              right: getProportionateScreenWidth(15),
              top: getProportionateScreenHeight(3)),
          child: isLoading
              ? LoadingButton()
              : DefaultButton(
                  text: 'Daftar',
                  press: _validateInputs,
                ),
        );
      }

      return Form(
        key: _formKey,
        child: Column(
          children: [
            nameInput(),
            invalidEmail ? inValidEmail() : emailInput(),
            passwordInput(),
            rePasswordInput(),
            SizedBox(
              height: getProportionateScreenHeight(8),
            ),
            button(),
          ],
        ),
      );
    }

    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              IconButton(
                onPressed: () => Navigator.of(context).pushNamedAndRemoveUntil(
                    RegistrationScreen.routeName,
                    (Route<dynamic> route) => false),
                icon: Icon(Icons.arrow_back),
                iconSize: getProportionateScreenWidth(18),
                padding: EdgeInsets.all(getProportionateScreenWidth(8)),
              ),
              Container(
                child: Align(
                  alignment: Alignment.center,
                  child: Header(
                      text: 'Daftar dan mulai belajar banyak hal',
                      style: primaryTextStyle.copyWith(
                          fontSize: getProportionateScreenWidth(14),
                          letterSpacing: 0.5)),
                ),
              ),
              SizedBox(
                height: getProportionateScreenHeight(32),
              ),
              form(),
              SizedBox(height: getProportionateScreenHeight(32)),
              Footer(
                textOne: 'Sudah punya akun? ',
                textTwo: 'Masuk',
                route: LoginScreen.routeName,
                height: 14,
              )
            ],
          ),
        ),
      ),
    );
  }
}
