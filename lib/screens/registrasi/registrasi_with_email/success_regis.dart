import 'package:flutter/material.dart';
import 'package:initial_folder/helper/user_info.dart';
import 'package:initial_folder/providers/carts_provider.dart';
import 'package:initial_folder/providers/my_course_provider.dart';
import 'package:initial_folder/providers/user_info_provider.dart';
import 'package:initial_folder/providers/whislist_provider.dart';
import 'package:initial_folder/screens/home/home_screen.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/widgets/login_regist/header.dart';
import 'package:flutter/gestures.dart';
import 'package:provider/provider.dart';

class RegisSuccess extends StatelessWidget {
  static String routeName = "/regis_success";
  RegisSuccess({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            Icon(
              Icons.check_rounded,
              color: Colors.greenAccent[700],
              size: getProportionateScreenWidth(60),
            ),
            SizedBox(height: getProportionateScreenHeight(24)),
            Header(
                title: false,
                text2: "Registrasi Akun Sukses",
                text:
                    'Kami telah mengirimkan notifikasi registrasi akun ke email anda',
                style: primaryTextStyle.copyWith(
                    color: tenthColor,
                    fontWeight: reguler,
                    fontSize: getProportionateScreenWidth(14),
                    letterSpacing: 0.5)),
            SizedBox(height: getProportionateScreenHeight(44)),
            Container(
              // padding: EdgeInsets.only(
              //     left: getProportionateScreenWidth(5),
              //     top: getProportionateScreenWidth(5)),
              alignment: Alignment.center,
              child: RichText(
                text: TextSpan(
                  children: <TextSpan>[
                    TextSpan(
                        text: 'Lanjut ke Halaman Beranda',
                        style: primaryTextStyle.copyWith(
                          color: primaryColor,
                          fontWeight: reguler,
                          fontSize: getProportionateScreenWidth(18),
                        ),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () async {
                            var email = await UsersInfo().getEmail();
                            await Provider.of<UserInfoProvider>(context,
                                    listen: false)
                                .getUserInfo(email);

                            await Provider.of<CartsProvider>(context,
                                    listen: false)
                                .getCarts();
                            await Provider.of<WishlistProvider>(context,
                                    listen: false)
                                .getWishlist();
                            await Provider.of<MyCourseProvider>(context,
                                    listen: false)
                                .getMyCourse();

                            Navigator.of(context).pushNamedAndRemoveUntil(
                                HomeScreen.routeName,
                                (Route<dynamic> route) => false);
                            // Navigator.of(context).pushNamedAndRemoveUntil(
                            //     LoginEmail.routeName,
                            //     (Route<dynamic> route) => false);
                            //Navigator.pushNamed(context, LoginEmail.routeName);
                            print('masuk');
                          }),
                  ],
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
