import 'package:flutter/material.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';

class AboutAccountList extends StatelessWidget {
  const AboutAccountList({Key? key, required this.onPress, required this.title})
      : super(key: key);
  final GestureTapCallback onPress;
  final String title;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPress,
      child: Center(
        child: Container(
          // color: primaryColor,
          margin: EdgeInsets.only(bottom: getProportionateScreenWidth(26)),
          child: Row(
            children: [
              Expanded(
                child: Text(
                  title,
                  style: primaryTextStyle.copyWith(
                      fontSize: getProportionateScreenWidth(12),
                      letterSpacing: 0.5),
                ),
              ),
              Icon(
                Icons.keyboard_arrow_right,
                size: getProportionateScreenWidth(24),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
