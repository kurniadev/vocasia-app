import 'package:flutter/material.dart';
import 'package:initial_folder/providers/page_provider.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/screens/profile/components/about_profile_list.dart';
import 'package:initial_folder/screens/login/login_screen.dart';
import 'package:initial_folder/widgets/terms_and_privacy.dart';
import 'package:provider/provider.dart';

class NotSignInScreen extends StatelessWidget {
  const NotSignInScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    PageProvider pageProvider =
        Provider.of<PageProvider>(context, listen: false);
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: Container(
          margin: EdgeInsets.all(getProportionateScreenWidth(16)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Akun',
                style: secondaryTextStyle.copyWith(
                    fontWeight: semiBold,
                    letterSpacing: 2.3,
                    fontSize: getProportionateScreenWidth(16)),
              ),
              SizedBox(
                height: getProportionateScreenHeight(29),
              ),
              AboutAccountList(
                  onPress: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => TermsAndCondition(
                            url: 'https://vocasia.id/home/about_us',
                            id: 'about'),
                      ),
                    );
                  },
                  title: 'Tentang Vocasia'),
              AboutAccountList(
                  onPress: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => TermsAndCondition(
                            url: 'https://vocasia.id/home/terms_and_condition',
                            id: 'sk'),
                      ),
                    );
                  },
                  title: 'Syarat dan Ketentuan'),
              AboutAccountList(
                  onPress: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => TermsAndCondition(
                            url: 'https://vocasia.id/home/privacy_policy',
                            id: 'prv'),
                      ),
                    );
                  },
                  title: 'Kebijakan Privasi'),
              AboutAccountList(
                  onPress: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => TermsAndCondition(
                            url: 'https://vocasia.id/home/contact', id: 'ctc'),
                      ),
                    );
                  },
                  title: 'Kontak Kami'),
              AboutAccountList(
                  onPress: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => TermsAndCondition(
                            url: 'https://vocasia.id/home/help', id: 'help'),
                      ),
                    );
                  },
                  title: 'Bantuan'),
              Center(
                child: TextButton(
                  onPressed: () {
                    Navigator.of(context).pushNamedAndRemoveUntil(
                        LoginScreen.routeName, (Route<dynamic> route) => false);
                    pageProvider.remove();
                  },
                  child: Text(
                    'Masuk',
                    style: secondaryTextStyle.copyWith(
                        fontWeight: semiBold,
                        letterSpacing: 1,
                        color: primaryColor,
                        fontSize: getProportionateScreenWidth(14)),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
