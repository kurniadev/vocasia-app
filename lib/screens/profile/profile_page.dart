import 'package:flutter/material.dart';
import 'package:initial_folder/screens/profile/account_sign_in/sign_in_screen_google.dart';
import 'package:initial_folder/screens/profile/account_sign_in/sign_in_screen_email.dart';
import 'package:initial_folder/screens/profile/account_not_sign_in/not_sign_in_screen.dart';
import 'package:initial_folder/screens/splash/splash_screen_login.dart';

// class ProfilePage extends StatelessWidget {
//   ProfilePage({Key? key}) : super(key: key);

//   final _pageList = [
//     SignInScreenEmail(),
//     SignInScreenGoogle(),
//     NotSignInScreen(),
//   ];
//   @override
//   Widget build(BuildContext context) {
//     ProfileProvider profileProvider = Provider.of<ProfileProvider>(context);
//     void profpage() async {
//       var token = await UsersInfo().getToken();
//       FirebaseAuth.instance.authStateChanges().listen((User? user) {
//         if (token != null || user != null) {
//           if (token != null) {
//             profileProvider.currentIndex = 0;
//           } else if (user != null) {
//             profileProvider.currentIndex = 1;
//           }
//         } else {
//           profileProvider.currentIndex = 2;
//         }
//       });
//     }

//     Widget body() {
//       switch (profileProvider.currentIndex) {
//         case 0:
//           return SignInScreenEmail();
//         case 1:
//           return SignInScreenGoogle();
//         case 2:
//           return NotSignInScreen();
//         default:
//           return SignInScreenEmail();
//       }
//     }

//     profpage();
//     // return Scaffold(body: body());
//     return IndexedStack(
//       index: profileProvider.currentIndex,
//       children: _pageList,
//     );
//   }
// }

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: (Condition.loginEmail || Condition.loginFirebase)
            ? (Condition.loginEmail)
                ? SignInScreenEmail()
                : (Condition.loginFirebase)
                    ? SignInScreenGoogle()
                    : NotSignInScreen()
            : NotSignInScreen());
  }
}
