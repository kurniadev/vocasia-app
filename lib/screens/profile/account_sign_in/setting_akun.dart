import 'package:flutter/material.dart';
import 'package:initial_folder/providers/update_password_provider.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/widgets/login_regist/custom_text_form_field.dart';
import 'package:initial_folder/helper/validator.dart';
import 'package:initial_folder/widgets/login_regist/default_button.dart';
import 'package:initial_folder/providers/user_info_provider.dart';
import 'package:provider/provider.dart';

class SettingAkun extends StatefulWidget {
  @override
  State<SettingAkun> createState() => _SettingAkunState();
}

class _SettingAkunState extends State<SettingAkun> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController _currentPasswordController =
      TextEditingController(text: '');

  final TextEditingController _newPasswordController =
      TextEditingController(text: '');

  final TextEditingController _confirmPasswordController =
      TextEditingController(text: '');
  bool _isObscure = true;
  bool _isObscure1 = true;
  bool _isObscure2 = true;
  @override
  Widget build(BuildContext context) {
    UserInfoProvider userInfoProvider = Provider.of<UserInfoProvider>(context);
    UpdatePasswordProvider updatePasswordProvider =
        Provider.of<UpdatePasswordProvider>(context);
    // EmailProvider emailProvider = Provider.of<EmailProvider>(context);
    final TextEditingController _emailController =
        TextEditingController(text: userInfoProvider.result!.data[0].email);
    // ShowHidePassword showHidePassword = Provider.of<ShowHidePassword>(context);
    // ShowHidePassword1 showHidePassword1 =
    //     Provider.of<ShowHidePassword1>(context);
    // ShowHidePassword2 showHidePassword2 =
    //     Provider.of<ShowHidePassword2>(context);
    Future _showMessage() {
      return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          contentPadding: EdgeInsets.fromLTRB(22, 30, 22, 30),
          content: Text(
            (updatePasswordProvider.updatePasswordModel?.data[0].message ==
                    "Wrong current password")
                ? 'Password sekarang salah, silahkan ganti ke password yang benar'
                : 'Password berhasil diubah',
            textAlign: TextAlign.center,
            style: primaryTextStyle.copyWith(
                fontSize: getProportionateScreenWidth(12), letterSpacing: 1),
          ),
        ),
      );
    }

    updatePassword(
        {required id_user,
        String? email,
        String? old_password,
        String? password,
        String? new_password_confirm}) async {
      await Provider.of<UpdatePasswordProvider>(context, listen: false)
          .passwordUpdate(
              idUser: id_user,
              email: email,
              old_password: old_password,
              password: password,
              new_password_confirm: new_password_confirm);
    }

    void _validateInputs() async {
      await updatePassword(
          id_user: userInfoProvider.result!.data[0].idUser,
          email: userInfoProvider.result!.data[0].email,
          old_password: _currentPasswordController.text,
          password: _newPasswordController.text,
          new_password_confirm: _confirmPasswordController.text);
      print(
          "ayammm ${updatePasswordProvider.updatePasswordModel?.data[0].message}");
      if (this._formKey.currentState!.validate()) {
        _showMessage();
      }
    }

    Widget form() {
      Widget emailInput() {
        return CustomTextField(
          enable: false,
          pad: getProportionateScreenWidth(2),
          controler: _emailController,
          hinttext: 'Masukkan email',
          title: 'Email',
          validate: validateEmail,
        );
      }

      Widget currentPasswordInput() {
        return CustomTextField(
          pad: getProportionateScreenWidth(2),
          controler: _currentPasswordController,
          hinttext: 'Masukkan password saat ini',
          title: 'Password',
          obscuretext: _isObscure,
          validate: (String? value) {
            if (value!.length == 0)
              return 'Password tidak boleh kosong';
            else if (value.length < 8)
              return 'Password minimal harus berjumlah 8 karakter';

            return null;
          },
          suffix: GestureDetector(
              onTap: () => setState(() {
                    _isObscure = !_isObscure;
                  }),
              child: _isObscure
                  ? Icon(
                      Icons.visibility_off,
                      color: secondaryColor,
                      size: 18,
                    )
                  : Icon(
                      Icons.visibility,
                      color: secondaryColor,
                      size: 18,
                    )),
        );
      }

      Widget newPasswordInput() {
        return CustomTextField(
          pad: getProportionateScreenWidth(2),
          controler: _newPasswordController,
          hinttext: 'Masukkan password baru',
          noTitle: true,
          obscuretext: _isObscure1,
          validate: validatePassword,
          suffix: GestureDetector(
              onTap: () {
                setState(() {
                  _isObscure1 = !_isObscure1;
                });
              },
              child: _isObscure1
                  ? Icon(
                      Icons.visibility_off,
                      color: secondaryColor,
                      size: 18,
                    )
                  : Icon(
                      Icons.visibility,
                      color: secondaryColor,
                      size: 18,
                    )),
        );
      }

      Widget confirmPasswordInput() {
        return CustomTextField(
          pad: getProportionateScreenWidth(2),
          controler: _confirmPasswordController,
          hinttext: 'Konfirmasi password baru',
          title: 'Konfirmasi Password',
          obscuretext: _isObscure2,
          suffix: GestureDetector(
              onTap: () => setState(() {
                    _isObscure2 = !_isObscure2;
                  }),
              child: _isObscure2
                  ? Icon(
                      Icons.visibility_off,
                      color: secondaryColor,
                      size: 18,
                    )
                  : Icon(
                      Icons.visibility,
                      color: secondaryColor,
                      size: 18,
                    )),
          validate: (String? value) {
            if (value!.length == 0)
              return 'Password tidak boleh kosong';
            else if (value.length < 8)
              return 'Password minimal harus berjumlah 8 karakter';
            else if (value != _newPasswordController.text) {
              return 'Password baru dengan konfirmasi tidak sama';
            }
            return null;
          },
        );
      }

      Widget bottomNav() {
        return Container(
          margin: EdgeInsets.symmetric(
              horizontal: getProportionateScreenWidth(45),
              vertical: getProportionateScreenHeight(10)),
          child: DefaultButton(
            text: 'Simpan perubahan',
            press: _validateInputs,
          ),
        );
      }

      return Form(
        key: _formKey,
        child: Column(
          children: [
            emailInput(),
            SizedBox(height: getProportionateScreenHeight(15)),
            currentPasswordInput(),
            SizedBox(height: getProportionateScreenHeight(15)),
            newPasswordInput(),
            SizedBox(height: getProportionateScreenHeight(15)),
            confirmPasswordInput(),
            SizedBox(height: getProportionateScreenHeight(70)),
            bottomNav(),
          ],
        ),
      );
    }

    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
            centerTitle: true,
            title: Text(
              'Pengaturan Akun',
              style: secondaryTextStyle.copyWith(
                  letterSpacing: 0.23,
                  fontWeight: semiBold,
                  fontSize: getProportionateScreenWidth(14)),
            )),
        body: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.all(16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Column(
                      children: [
                        Text(
                          'Ubah pengaturan akunmu di sini',
                          style: primaryTextStyle.copyWith(
                              letterSpacing: 1,
                              color: secondaryColor,
                              fontWeight: reguler,
                              fontSize: getProportionateScreenWidth(12)),
                        ),
                        SizedBox(
                          height: getProportionateScreenHeight(16),
                        ),
                      ],
                    ),
                  ),
                  form(),
                  SizedBox(
                    height: getProportionateScreenHeight(13),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
