import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:initial_folder/helper/user_info.dart';
import 'package:initial_folder/providers/carts_provider.dart' as cartsProvider;
import 'package:initial_folder/providers/data_diri_provider.dart';
import 'package:initial_folder/providers/profile_image_provider.dart';
import 'package:image_picker/image_picker.dart';
import 'package:initial_folder/providers/page_provider.dart';
import 'package:initial_folder/providers/user_info_provider.dart';
import 'package:initial_folder/screens/login/login_screen.dart';
import 'package:initial_folder/screens/profile/account_sign_in/data_diri.dart';
import 'package:initial_folder/screens/profile/account_sign_in/riwayat_transaksi.dart';
import 'package:initial_folder/screens/profile/account_sign_in/setting_akun.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/screens/profile/components/about_profile_list.dart';
import 'package:initial_folder/providers/google_sign_in.dart';
import 'package:initial_folder/widgets/terms_and_privacy.dart';
import 'package:provider/provider.dart';
import 'package:initial_folder/screens/splash/splash_screen_login.dart';

class SignInScreenEmail extends StatelessWidget {
  const SignInScreenEmail({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget imageProfile(String? urlImage) {
      return CircleAvatar(
        radius: getProportionateScreenWidth(40),
        backgroundImage: urlImage == null
            ? AssetImage("assets/images/Profile Image.png")
            : NetworkImage(urlImage) as ImageProvider,
      );
    }

    getProfile() async {
      var email = await UsersInfo().getEmail();
      print(email);
      Provider.of<UserInfoProvider>(context, listen: false).getUserInfo(email);
    }

    PageProvider pageProvider =
        Provider.of<PageProvider>(context, listen: false);
    Future _showDialogLogout() {
      return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          contentPadding: EdgeInsets.fromLTRB(12, 20, 12, 1),
          content: Text(
            'Apakah anda yakin ingin logout?',
            style: primaryTextStyle.copyWith(
                fontSize: getProportionateScreenWidth(12), letterSpacing: 1),
          ),
          actions: [
            GestureDetector(
              onTap: () async {
                if (Condition.loginFirebase == true) {
                  final provider =
                      Provider.of<GoogleSignInProvider>(context, listen: false);

                  await provider.logout().then((value) {
                    Condition.loginFirebase = false;
                  });
                  pageProvider.remove();
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      LoginScreen.routeName, (Route<dynamic> route) => false);
                } else {
                  await UsersInfo().logout().then(
                    (value) {
                      Condition.loginEmail = false;
                      pageProvider.remove();

                      Navigator.of(context).pushNamedAndRemoveUntil(
                          LoginScreen.routeName,
                          (Route<dynamic> route) => false);
                    },
                  );
                }
              },
              child: Text('Ya',
                  style: primaryTextStyle.copyWith(
                      fontSize: getProportionateScreenWidth(12),
                      letterSpacing: 1,
                      color: primaryColor)),
            ),
            SizedBox(
              width: getProportionateScreenWidth(5),
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Text('Tidak',
                  style: primaryTextStyle.copyWith(
                      fontSize: getProportionateScreenWidth(12),
                      letterSpacing: 1,
                      color: primaryColor)),
            ),
          ],
        ),
      );
    }

    // WidgetsBinding.instance?.addPostFrameCallback(
    //     (_) => getData(userInfoProvider.result?.data[0].idUser));
    // print(userInfoProvider.result?.data[0].idUser);
    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Akun',
                  style: secondaryTextStyle.copyWith(
                      fontSize: getProportionateScreenHeight(16),
                      fontWeight: semiBold,
                      letterSpacing: 2.3),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(20),
                ),
                Center(
                  child: Consumer<UserInfoProvider>(
                    builder: (context, state, _) {
                      if (state.state == ResultState.Loading) {
                        return Center(
                          child: CircularProgressIndicator(
                            color: secondaryColor,
                            strokeWidth: 2,
                          ),
                        );
                      } else if (state.state == ResultState.HasData) {
                        //var idus = state.result!.data[0].idUser;

                        return Column(
                          children: [
                            imageProfile(state.result?.data[0].fotoProfile),
                            SizedBox(
                              height: getProportionateScreenHeight(16),
                            ),
                            Text(
                              state.result!.data[0].fullname ?? ' ',
                              style: secondaryTextStyle.copyWith(
                                  fontWeight: semiBold,
                                  letterSpacing: 1,
                                  fontSize: getProportionateScreenWidth(14)),
                              textAlign: TextAlign.center,
                            ),
                            SizedBox(
                              height: getProportionateScreenHeight(8),
                            ),
                            Text(
                              state.result!.data[0].email ?? '',
                              style: primaryTextStyle.copyWith(
                                color: secondaryColor,
                                letterSpacing: 0.5,
                                fontSize: getProportionateScreenWidth(12),
                              ),
                              textAlign: TextAlign.center,
                            ),
                          ],
                        );
                      } else if (state.state == ResultState.NoData) {
                        return Center(child: Text(state.message));
                      } else if (state.state == ResultState.Error) {
                        return Center(
                          child: TextButton(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [Icon(Icons.refresh)],
                              ),
                              onPressed: () => getProfile()),
                        );
                      } else {
                        print(state.result);
                        return Center(child: Text(''));
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(40),
                ),
                Text(
                  'Preferensi Akun',
                  style: primaryTextStyle.copyWith(
                      fontSize: getProportionateScreenWidth(10),
                      color: secondaryColor),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(13),
                ),
                AboutAccountList(
                    onPress: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => DataDiri(),
                        ),
                      );
                    },
                    title: 'Data Diri'),
                AboutAccountList(
                    onPress: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SettingAkun(),
                        ),
                      );
                    },
                    title: 'Pengaturan Akun'),
                AboutAccountList(
                    onPress: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => RiwayatTransaksi(),
                        ),
                      );
                    },
                    title: 'Riwayat Transaksi'),
                SizedBox(
                  height: getProportionateScreenWidth(10),
                ),
                Text(
                  'Bantuan Dan Dukungan',
                  style: primaryTextStyle.copyWith(
                      fontSize: getProportionateScreenWidth(10),
                      color: secondaryColor),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(13),
                ),
                AboutAccountList(
                    onPress: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => TermsAndCondition(
                              url: 'https://vocasia.id/home/about_us',
                              id: 'about'),
                        ),
                      );
                    },
                    title: 'Tentang Vocasia'),
                AboutAccountList(
                    onPress: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => TermsAndCondition(
                              url:
                                  'https://vocasia.id/home/terms_and_condition',
                              id: 'sk'),
                        ),
                      );
                    },
                    title: 'Syarat dan Ketentuan'),
                AboutAccountList(
                    onPress: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => TermsAndCondition(
                              url: 'https://vocasia.id/home/privacy_policy',
                              id: 'prv'),
                        ),
                      );
                    },
                    title: 'Kebijakan Privasi'),
                AboutAccountList(
                    onPress: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => TermsAndCondition(
                              url: 'https://vocasia.id/home/contact',
                              id: 'ctc'),
                        ),
                      );
                    },
                    title: 'Kontak Kami'),
                AboutAccountList(
                    onPress: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => TermsAndCondition(
                              url: 'https://vocasia.id/home/help', id: 'help'),
                        ),
                      );
                    },
                    title: 'Bantuan'),
                Center(
                  child: TextButton(
                    child: Text(
                      'Logout',
                      style: secondaryTextStyle.copyWith(
                        fontWeight: semiBold,
                        letterSpacing: 1,
                        color: primaryColor,
                        fontSize: getProportionateScreenWidth(14),
                      ),
                    ),
                    onPressed: () {
                      _showDialogLogout();
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
