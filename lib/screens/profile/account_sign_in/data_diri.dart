import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:initial_folder/helper/user_info.dart';
import 'package:initial_folder/providers/profile_image_provider.dart';
import 'package:initial_folder/providers/update_data_diri_provider.dart';
import 'package:initial_folder/providers/data_diri_provider.dart';
import 'package:initial_folder/services/user_info_service.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/helper/validator.dart';
import 'package:initial_folder/widgets/login_regist/custom_profile_text_field.dart';
import 'package:initial_folder/widgets/login_regist/custom_text_form_field.dart';
import 'package:initial_folder/providers/user_info_provider.dart' as userInfo;
import 'package:provider/provider.dart';

class DataDiri extends StatefulWidget {
  @override
  State<DataDiri> createState() => _DataDiriState();
}

class _DataDiriState extends State<DataDiri> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final List<String> gender = ['pria', 'wanita'];
  String? _currentGender = 'pria';
  String? _newGender;
  late String newName;
  late String newBiograpy;
  late String newPhone;
  late String newInstagram = '';
  late String newTwitter = '';
  late String newFacebook = '';
  late String newLinkedin = '';

  @override
  Widget build(BuildContext context) {
    UpdateDataDiriProvider updateDataDiriProvider =
        Provider.of<UpdateDataDiriProvider>(context);

    ProfileImageProvider profileImageProvider =
        Provider.of<ProfileImageProvider>(context, listen: false);
    final ImagePicker _picker = ImagePicker();

    Future _showMessage(String text) {
      return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          contentPadding: EdgeInsets.fromLTRB(22, 30, 22, 30),
          content: Text(
            text,
            textAlign: TextAlign.center,
            style: primaryTextStyle.copyWith(
                fontSize: getProportionateScreenWidth(12), letterSpacing: 1),
          ),
        ),
      );
    }

    void takePhoto(ImageSource source) async {
      try {
        final XFile? pickedFile = await _picker.pickImage(source: source);
        var imageFile = (pickedFile != null) ? File(pickedFile.path) : File('');
        var email = await UsersInfo().getEmail();
        if (await profileImageProvider.addProfileImage(pckFile: imageFile)) {
          await Provider.of<userInfo.UserInfoProvider>(context, listen: false)
              .getUserInfo(email);
          _showMessage('Berhasil Upload Image');
        }
      } on PlatformException catch (e) {
        print('Failed to pick image : $e');
      }
    }

    Widget bottomSheet() {
      return Container(
        height: 100.0,
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.symmetric(
          horizontal: 20,
          vertical: 20,
        ),
        child: Column(
          children: <Widget>[
            Text("Choose Profile photo",
                style: secondaryTextStyle.copyWith(fontSize: 20)),
            SizedBox(
              height: 20,
            ),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              TextButton.icon(
                icon: Icon(Icons.camera),
                onPressed: () {
                  Navigator.pop(context);
                  takePhoto(ImageSource.camera);
                },
                label: Text(
                  "Camera",
                  style: primaryTextStyle,
                ),
              ),
              TextButton.icon(
                icon: Icon(Icons.image),
                onPressed: () {
                  Navigator.pop(context);
                  takePhoto(ImageSource.gallery);
                },
                label: Text("Gallery", style: primaryTextStyle),
              ),
            ])
          ],
        ),
      );
    }

    Widget imageProfile(String? urlImage) {
      return Stack(
        alignment: AlignmentDirectional.center,
        children: [
          CircleAvatar(
            radius: getProportionateScreenWidth(40),
            backgroundColor: Colors.amber,
            backgroundImage: urlImage == null
                ? AssetImage("assets/images/Profile Image.png")
                : NetworkImage(urlImage) as ImageProvider,
          ),
          CircleAvatar(
            radius: getProportionateScreenWidth(40),
            backgroundColor: Color(0xff000000).withOpacity(0.5),
          ),
          IconButton(
            onPressed: () {
              showModalBottomSheet(
                context: context,
                builder: (context) => GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: bottomSheet()),
              );
            },
            icon: Icon(
              Icons.camera_alt_outlined,
              color: Colors.white,
            ),
          )
        ],
      );
    }

    return SafeArea(
      child: ChangeNotifierProvider(
        create: (context) =>
            DataDiriProvider(userInfoService: UserInfoService()),
        child: Consumer<DataDiriProvider>(
          builder: (context, state, _) {
            if (state.state == ResultStateData.Loading) {
              return Center(
                child: CircularProgressIndicator(
                  color: secondaryColor,
                  strokeWidth: 2,
                ),
              );
            } else if (state.state == ResultStateData.HasData) {
              var result = state.result!.data[0];
              _currentGender = result.gender == null ? 'pria' : result.gender;
              DateTime birthDate = DateTime.now();
              return Scaffold(
                appBar: AppBar(
                  centerTitle: true,
                  title: Text(
                    'Data Diri',
                    style: secondaryTextStyle.copyWith(
                        letterSpacing: 0.23,
                        fontWeight: semiBold,
                        fontSize: getProportionateScreenWidth(14)),
                  ),
                  actions: <Widget>[
                    TextButton(
                        child: Text('Simpan',
                            style: primaryTextStyle.copyWith(
                                color: primaryColor,
                                fontWeight: reguler,
                                letterSpacing: 0.5,
                                fontSize: getProportionateScreenWidth(12))),
                        onPressed: () async {
                          final heading = '';
                          print(_currentGender);
                          if (this._formKey.currentState!.validate()) {
                            if (await updateDataDiriProvider.dataDiriUpdate(
                                datebirth: state.isSelected
                                    ? state.dateBirth
                                    : result.datebirth,
                                fullname:
                                    state.newName ? newName : result.fullname,
                                biograph: state.newBiograpy
                                    ? newBiograpy
                                    : result.biography,
                                twitter: state.newTwitter
                                    ? newTwitter
                                    : result.socialLink?.twitter,
                                facebook: state.newFacebook
                                    ? newFacebook
                                    : result.socialLink?.facebook,
                                linkedin: state.newLinkedin
                                    ? newLinkedin
                                    : result.socialLink?.linkedin,
                                instagram: state.newInstagram
                                    ? newInstagram
                                    : result.socialLink?.instagram,
                                phone: state.newPhone ? newPhone : result.phone,
                                gender: _newGender == null
                                    ? _currentGender
                                    : _newGender,
                                heading: heading)) {
                              print(newLinkedin);
                              await Provider.of<DataDiriProvider>(context,
                                      listen: false)
                                  .getDataDiri();
                              _showMessage('Data diri berhasil diubah');
                            } else {
                              _showMessage('Silahkan Coba lagi');
                            }
                          }
                        })
                  ],
                ),
                body: ListView(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Center(
                            child: Consumer<userInfo.UserInfoProvider>(
                                builder: (context, state, _) {
                              if (state.state == userInfo.ResultState.Loading) {
                                return Center(
                                  child: CircularProgressIndicator(
                                    color: secondaryColor,
                                    strokeWidth: 2,
                                  ),
                                );
                              } else if (state.state ==
                                  userInfo.ResultState.HasData) {
                                return imageProfile(
                                    state.result?.data[0].fotoProfile);
                              } else if (state.state ==
                                  userInfo.ResultState.NoData) {
                                return Center(child: Text(state.message));
                              } else {
                                print(state.result);
                                return Center(child: Text(''));
                              }
                            }),
                          ),
                          SizedBox(
                            height: getProportionateScreenHeight(16),
                          ),
                          Form(
                              key: _formKey,
                              child: Column(
                                children: [
                                  CustomProfileTextField(
                                    pad: getProportionateScreenWidth(2),
                                    text: state.newName
                                        ? newName
                                        : result.fullname ?? '',
                                    hinttext: 'Masukkan nama lengkap',
                                    title: 'Nama Lengkap',
                                    validate: validateName,
                                    onChanged: (value) {
                                      state.newName = true;
                                      newName = value;
                                    },
                                  ),
                                  CustomProfileTextField(
                                    pad: getProportionateScreenWidth(2),
                                    text: state.newBiograpy
                                        ? newBiograpy
                                        : result.biography ?? '',
                                    hinttext: '',
                                    title: 'Biografi',
                                    minLines: 3,
                                    maxLines: 5,
                                    validate: validateBiography,
                                    onChanged: (value) {
                                      state.newBiograpy = true;
                                      newBiograpy = value;
                                    },
                                  ),
                                  GestureDetector(
                                      child: Container(
                                        margin: EdgeInsets.only(
                                          bottom:
                                              getProportionateScreenWidth(20),
                                        ),
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text('Tanggal Lahir',
                                                style: secondaryTextStyle.copyWith(
                                                    fontWeight: semiBold,
                                                    fontSize:
                                                        getProportionateScreenWidth(
                                                            12),
                                                    color: secondaryColor,
                                                    letterSpacing: 0.5)),
                                            SizedBox(
                                              height:
                                                  getProportionateScreenWidth(
                                                      5),
                                            ),
                                            Container(
                                              width: double.infinity,
                                              height:
                                                  getProportionateScreenWidth(
                                                      50),
                                              child: Row(
                                                children: [
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        left: 13),
                                                    child: Text(
                                                      state.isSelected
                                                          ? state.dateBirth
                                                          : result.datebirth ??
                                                              'YYYY-MM-DD',
                                                      style: primaryTextStyle.copyWith(
                                                          fontSize:
                                                              getProportionateScreenWidth(
                                                                  12),
                                                          color: state
                                                                  .isSelected
                                                              ? secondaryColor
                                                              : Colors.white,
                                                          letterSpacing: 0.5),
                                                    ),
                                                  )
                                                ],
                                              ),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                    10,
                                                  ),
                                                  border: Border.all(
                                                      color: fourthColor)),
                                            ),
                                          ],
                                        ),
                                      ),
                                      onTap: () async {
                                        final datePick = await showDatePicker(
                                            context: context,
                                            initialDate: DateTime.now(),
                                            firstDate: DateTime(1900),
                                            lastDate: DateTime(2100));
                                        if (datePick != null &&
                                            datePick != birthDate) {
                                          birthDate = datePick;
                                          state.isSelected = true;

                                          state.datebirth =
                                              "${birthDate.year}-${birthDate.month}-${birthDate.day}";
                                        }
                                      }),
                                  CustomProfileTextField(
                                    keyboardType: TextInputType.number,
                                    pad: getProportionateScreenWidth(2),
                                    text: state.newPhone
                                        ? newPhone
                                        : result.phone ?? '',
                                    hinttext: '',
                                    title: 'Nomor Telepon / WA',
                                    validate: validatePhone,
                                    onChanged: (value) {
                                      state.newPhone = true;
                                      newPhone = value;
                                    },
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        'Jenis Kelamin',
                                        style: secondaryTextStyle.copyWith(
                                            fontWeight: semiBold,
                                            fontSize:
                                                getProportionateScreenWidth(12),
                                            color: secondaryColor,
                                            letterSpacing: 0.5),
                                      ),
                                      Container(
                                          alignment: Alignment.centerLeft,
                                          decoration: BoxDecoration(
                                            border: Border(
                                              bottom: BorderSide(
                                                  color: Colors.black,
                                                  width: 4),
                                            ),
                                          ),
                                          height: 60.0,
                                          child: DropdownButtonFormField(
                                            decoration: const InputDecoration(
                                              enabledBorder:
                                                  UnderlineInputBorder(
                                                borderSide: BorderSide(
                                                    color: Colors.white),
                                              ),
                                            ),
                                            value: _currentGender,
                                            items: gender.map((gender) {
                                              return DropdownMenuItem(
                                                value: gender,
                                                child: Text(
                                                  gender,
                                                  style: secondaryTextStyle
                                                      .copyWith(
                                                          fontWeight: semiBold,
                                                          fontSize:
                                                              getProportionateScreenWidth(
                                                                  12),
                                                          color: secondaryColor,
                                                          letterSpacing: 0.5),
                                                ),
                                              );
                                            }).toList(),
                                            onChanged: (val) => setState(() =>
                                                _newGender = val as String),
                                          )),
                                    ],
                                  ),
                                  SizedBox(
                                    height: getProportionateScreenHeight(20),
                                  ),
                                  CustomProfileTextField(
                                    prefix: Padding(
                                      padding: EdgeInsets.only(
                                        top: getProportionateScreenHeight(10),
                                        left: getProportionateScreenWidth(20),
                                        right: getProportionateScreenHeight(10),
                                      ),
                                      child: FaIcon(FontAwesomeIcons.instagram,
                                          color: tenthColor),
                                    ),
                                    pad: getProportionateScreenWidth(2),
                                    text: state.newInstagram
                                        ? newInstagram
                                        : result.socialLink?.instagram ?? '',
                                    hinttext: 'http://instagram.com/',
                                    noTitle: true,
                                    onChanged: (value) {
                                      state.newInstagram = true;
                                      newInstagram = value;
                                    },
                                  ),
                                  CustomProfileTextField(
                                    prefix: Padding(
                                      padding: EdgeInsets.only(
                                        top: getProportionateScreenHeight(10),
                                        left: getProportionateScreenWidth(20),
                                        right: getProportionateScreenHeight(10),
                                      ),
                                      child: FaIcon(FontAwesomeIcons.twitter,
                                          color: tenthColor),
                                    ),
                                    pad: getProportionateScreenWidth(2),
                                    text: state.newTwitter
                                        ? newTwitter
                                        : result.socialLink?.twitter ?? '',
                                    hinttext: 'http://twitter.com/',
                                    noTitle: true,
                                    onChanged: (value) {
                                      state.newTwitter = true;
                                      newTwitter = value;
                                    },
                                  ),
                                  CustomProfileTextField(
                                    prefix: Padding(
                                      padding: EdgeInsets.only(
                                        top: getProportionateScreenHeight(10),
                                        left: getProportionateScreenWidth(20),
                                        right: getProportionateScreenHeight(10),
                                      ),
                                      child: FaIcon(FontAwesomeIcons.facebookF,
                                          color: tenthColor),
                                    ),
                                    pad: getProportionateScreenWidth(2),
                                    text: state.newFacebook
                                        ? newFacebook
                                        : result.socialLink?.facebook ?? '',
                                    hinttext: 'http://facebook.com/',
                                    noTitle: true,
                                    onChanged: (value) {
                                      state.newFacebook = true;
                                      newFacebook = value;
                                    },
                                  ),
                                  CustomProfileTextField(
                                    prefix: Padding(
                                      padding: EdgeInsets.only(
                                        top: getProportionateScreenHeight(10),
                                        left: getProportionateScreenWidth(20),
                                        right: getProportionateScreenHeight(10),
                                      ),
                                      child: FaIcon(FontAwesomeIcons.linkedinIn,
                                          color: tenthColor),
                                    ),
                                    pad: getProportionateScreenWidth(2),
                                    text: state.newLinkedin
                                        ? newLinkedin
                                        : result.socialLink?.linkedin ?? '',
                                    hinttext: 'http://linkedin.com/',
                                    noTitle: true,
                                    onChanged: (value) {
                                      state.newLinkedin = true;
                                      newLinkedin = value;
                                    },
                                  ),
                                ],
                              )),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            } else if (state.state == ResultStateData.NoData) {
              return Center(child: Text(state.message));
            } else if (state.state == ResultStateData.Error) {
              return Center(
                child: TextButton(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text('Server internal Error ${state.message}'),
                        Icon(Icons.refresh)
                      ],
                    ),
                    onPressed: () {}),
              );
            } else {
              return Center(child: Text(''));
            }
          },
        ),
      ),
    );
  }
}
