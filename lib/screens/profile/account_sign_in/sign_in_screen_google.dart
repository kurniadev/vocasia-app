import 'package:flutter/material.dart';
import 'package:initial_folder/helper/user_info.dart';
import 'package:initial_folder/screens/login/login_screen.dart';
import 'package:initial_folder/screens/profile/account_sign_in/data_diri.dart';
import 'package:initial_folder/screens/profile/account_sign_in/riwayat_transaksi.dart';
import 'package:initial_folder/screens/profile/account_sign_in/setting_akun.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/screens/profile/components/about_profile_list.dart';
import 'package:initial_folder/providers/google_sign_in.dart';
import 'package:provider/provider.dart';
import 'package:initial_folder/screens/splash/splash_screen_login.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:initial_folder/providers/page_provider.dart';

class SignInScreenGoogle extends StatelessWidget {
  const SignInScreenGoogle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    PageProvider pageProvider = Provider.of<PageProvider>(context);
    Future _showDialogLogout() {
      return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          contentPadding: EdgeInsets.fromLTRB(12, 20, 12, 1),
          content: Text(
            'Apakah anda yakin ingin logout?',
            style: primaryTextStyle.copyWith(
                fontSize: getProportionateScreenWidth(12), letterSpacing: 1),
          ),
          actions: [
            GestureDetector(
              onTap: () async {
                if (Condition.loginFirebase == true) {
                  final provider =
                      Provider.of<GoogleSignInProvider>(context, listen: false);

                  await provider.logout().then((value) {
                    Condition.loginFirebase = false;
                  });
                  pageProvider.remove();
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      LoginScreen.routeName, (Route<dynamic> route) => false);
                } else {
                  await UsersInfo().logout().then(
                    (value) {
                      Condition.loginEmail = false;
                      pageProvider.remove();
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          LoginScreen.routeName,
                          (Route<dynamic> route) => false);
                    },
                  );
                }
              },
              child: Text('Ya',
                  style: primaryTextStyle.copyWith(
                      fontSize: getProportionateScreenWidth(12),
                      letterSpacing: 1,
                      color: primaryColor)),
            ),
            SizedBox(
              width: getProportionateScreenWidth(5),
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Text('Tidak',
                  style: primaryTextStyle.copyWith(
                      fontSize: getProportionateScreenWidth(12),
                      letterSpacing: 1,
                      color: primaryColor)),
            ),
          ],
        ),
      );
    }

    final user = FirebaseAuth.instance.currentUser;

    return Scaffold(
      backgroundColor: Colors.transparent,
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Akun',
                  style: secondaryTextStyle.copyWith(
                      fontSize: getProportionateScreenHeight(16),
                      fontWeight: semiBold,
                      letterSpacing: 1),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(20),
                ),
                Center(
                  child: Column(
                    children: [
                      CircleAvatar(
                        radius: getProportionateScreenWidth(40),
                        backgroundColor: Colors.amber,
                        backgroundImage: user == null
                            ? NetworkImage(
                                'http://api.vocasia.pasia.id/uploads/courses_thumbnail/course_thumbnail_default_17.jpg')
                            : NetworkImage(user.photoURL!),
                      ),
                      SizedBox(
                        height: getProportionateScreenHeight(16),
                      ),
                      Text(user == null ? 'no name' : user.displayName!,
                          style: secondaryTextStyle.copyWith(
                              fontWeight: semiBold,
                              letterSpacing: 1,
                              fontSize: getProportionateScreenWidth(14)),
                          textAlign: TextAlign.center),
                      SizedBox(
                        height: getProportionateScreenHeight(8),
                      ),
                      Text(user == null ? 'email' : user.email!,
                          style: primaryTextStyle.copyWith(
                            color: secondaryColor,
                            letterSpacing: 0.5,
                            fontSize: getProportionateScreenWidth(12),
                          ),
                          textAlign: TextAlign.center),
                    ],
                  ),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(40),
                ),
                Text(
                  'Preferensi Akun',
                  style: primaryTextStyle.copyWith(
                      fontSize: getProportionateScreenWidth(10),
                      color: secondaryColor),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(13),
                ),
                AboutAccountList(
                    onPress: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => DataDiri(),
                        ),
                      );
                    },
                    title: 'Data Diri'),
                AboutAccountList(
                    onPress: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => SettingAkun(),
                        ),
                      );
                    },
                    title: 'Pengaturan Akun'),
                AboutAccountList(
                    onPress: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => RiwayatTransaksi(),
                        ),
                      );
                    },
                    title: 'Riwayat Transaksi'),
                SizedBox(
                  height: getProportionateScreenWidth(10),
                ),
                Text(
                  'Bantuan Dan Dukungan',
                  style: primaryTextStyle.copyWith(
                      fontSize: getProportionateScreenWidth(10),
                      color: secondaryColor),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(13),
                ),
                AboutAccountList(onPress: () {}, title: 'Tentang Vocasia'),
                AboutAccountList(onPress: () {}, title: 'Ketentuan'),
                AboutAccountList(onPress: () {}, title: 'Kebijakan Privasi'),
                AboutAccountList(onPress: () {}, title: 'Kontak Kami'),
                AboutAccountList(onPress: () {}, title: 'Bantuan'),
                Center(
                  child: TextButton(
                    child: Text(
                      'Logout',
                      style: secondaryTextStyle.copyWith(
                        fontWeight: semiBold,
                        letterSpacing: 1,
                        color: primaryColor,
                        fontSize: getProportionateScreenWidth(14),
                      ),
                    ),
                    onPressed: () {
                      _showDialogLogout();
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
