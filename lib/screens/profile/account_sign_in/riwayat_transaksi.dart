import 'package:flutter/material.dart';
import 'package:initial_folder/providers/history_transactions_provider.dart';
import 'package:initial_folder/services/history_transactions_service.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/widgets/riwayat_list.dart';
import 'package:provider/provider.dart';

class RiwayatTransaksi extends StatelessWidget {
  const RiwayatTransaksi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
            centerTitle: true,
            title: Text(
              'Riwayat Transaksi',
              style: secondaryTextStyle.copyWith(
                  letterSpacing: 0.23,
                  fontWeight: semiBold,
                  fontSize: getProportionateScreenWidth(14)),
            )),
        body: ChangeNotifierProvider(
          create: (context) => HistoryTranscationsProvider(
              historyTransactionService: HistoryTransactionService()),
          child: Consumer<HistoryTranscationsProvider>(
              builder: (context, state, _) {
            if (state.state == ResultState.loading) {
              return Center(
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  color: primaryColor,
                ),
              );
            } else if (state.state == ResultState.noData) {
              return Center(
                child: Text(state.message),
              );
            } else if (state.state == ResultState.hasData) {
              List historyTransaction = state.result!.data[0];
              return ListView(
                  children: historyTransaction
                      .map((e) => RiwayatList(
                            dataHistoryTransactionModel: e,
                          ))
                      .toList());
            } else if (state.state == ResultState.error) {
              return Center(
                child: Text(state.message),
              );
            } else {
              return Center(
                child: CircularProgressIndicator(
                  color: primaryColor,
                  strokeWidth: 2,
                ),
              );
            }
          }),
        ),
      ),
    );
  }
}
