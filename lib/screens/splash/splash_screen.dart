// import 'package:flutter/material.dart';
// import 'package:initial_folder/theme.dart';
// import '../login/login_screen.dart';
// import 'package:splash_screen_view/SplashScreenView.dart';
// import '../../size_config.dart';

// class Splash extends StatelessWidget {
//   const Splash({Key? key}) : super(key: key);
//   static String routeName = "/splash";
//   @override
//   Widget build(BuildContext context) {
//     SizeConfig().init(context);
//     print(SizeConfig.screenWidth);
//     print(SizeConfig.screenHeight);
//     return Center(
//       child: Transform.scale(
//         scale: getProportionateScreenWidth(1),
//         child: SplashScreenView(
//           duration: 3000,
//           navigateRoute: LoginScreen(),
//           imageSize: 30,
//           imageSrc: "assets/images/VOCASIA logo.png",
//           text: "Make You Competent",
//           textType: TextType.NormalText,
//           textStyle: primaryTextStyle.copyWith(
//               fontWeight: reguler, fontSize: 12.0, color: secondaryColor),
//           backgroundColor: Colors.black,
//         ),
//       ),
//     );
//   }
// }
