import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:initial_folder/helper/user_info.dart';
import 'package:initial_folder/providers/user_info_provider.dart';
import 'package:initial_folder/screens/home/home_screen.dart';
import 'package:initial_folder/screens/login/login_screen.dart';
import 'package:initial_folder/theme.dart';
import 'package:provider/provider.dart';
import '../../size_config.dart';

class SplashScreenLogin extends StatefulWidget {
  const SplashScreenLogin({Key? key}) : super(key: key);

  static String routeName = "/splash";

  @override
  _SplashScreenLoginState createState() => _SplashScreenLoginState();
}

class Condition {
  static bool loginFirebase = false;
  static bool loginEmail = false;
}

class _SplashScreenLoginState extends State<SplashScreenLogin>
    with TickerProviderStateMixin {
  late final AnimationController _controller = AnimationController(
    duration: const Duration(seconds: 1),
    vsync: this,
  )..forward();
  late final Animation<double> _animation = CurvedAnimation(
    parent: _controller,
    curve: Curves.easeIn,
  );

  @override
  void initState() {
    // TODO: implement initState

    Timer(Duration(seconds: 3), () {
      isLogin();
    });

    super.initState();
  }

  void isLogin() async {
    // await Hive.openBox('carts');

    var token = await UsersInfo().getToken();
    var email = await UsersInfo().getEmail();

    FirebaseAuth.instance.authStateChanges().listen((User? user) async {
      if (token != null || user != null) {
        await Provider.of<UserInfoProvider>(context, listen: false)
            .getUserInfo(email);
        setState(() {
          if (token == null) {
            // loader = true;
            Condition.loginFirebase = true;
          } else {
            // loader = true;
            Condition.loginEmail = true;
          }

          Navigator.of(context).pushNamedAndRemoveUntil(
              HomeScreen.routeName, (Route<dynamic> route) => false);
        });
      } else {
        setState(() {
          // loader = false;
          Navigator.of(context).pushNamedAndRemoveUntil(
              LoginScreen.routeName, (Route<dynamic> route) => false);
        });
      }
      print(token);
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  // getInit() async {
  //   await Provider.of<CourseProvider>(context, listen: false).getAllCourse();
  //   Navigator.of(context).pushNamedAndRemoveUntil(
  //       HomeScreen.routeName, (Route<dynamic> route) => false);
  // }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: Center(
        child: Transform.scale(
          scale: getProportionateScreenWidth(1),
          child: Column(
            children: [
              Flexible(
                flex: 10,
                child: FadeTransition(
                  opacity: _animation,
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Image.asset(
                          "assets/images/VOCASIA logo.png",
                          width: getProportionateScreenWidth(140),
                        ),
                        SizedBox(
                          height: getProportionateScreenHeight(10),
                        ),
                        Text(
                          "Make You Competent",
                          style: primaryTextStyle.copyWith(
                              fontWeight: reguler,
                              fontSize: getProportionateScreenWidth(12.0),
                              color: secondaryColor),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              Flexible(
                flex: 1,
                child: CircularProgressIndicator(
                  color: Colors.white,
                  strokeWidth: 3,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
