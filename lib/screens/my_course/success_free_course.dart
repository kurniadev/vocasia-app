import 'package:flutter/material.dart';
import 'package:initial_folder/providers/detail_course_provider.dart';
import 'package:initial_folder/providers/lesson_course_provider.dart';
import 'package:initial_folder/screens/course/play_course_page.dart';
import 'package:initial_folder/screens/home/components/body_comp/latest_course.dart';
import 'package:initial_folder/services/course_service.dart';
import 'package:initial_folder/services/lesson_course_service.dart';
// import 'package:initial_folder/models/course_model.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/widgets/login_regist/default_button.dart';
import 'package:provider/provider.dart';

class SuccessFreeCourse extends StatelessWidget {
  const SuccessFreeCourse({
    Key? key,
    this.title,
    this.id,
    this.thumbnail,
    this.instructor,
  }) : super(key: key);

  final String? instructor, id, thumbnail, title;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leadingWidth: 14,
      ),
      body: ListView(
        children: [
          SizedBox(height: getProportionateScreenHeight(8)),
          Text(
            'PENDAFTARAN BERHASIL',
            style: secondaryTextStyle.copyWith(
                fontWeight: bold,
                color: primaryColor,
                fontSize: getProportionateScreenWidth(20),
                letterSpacing: 1),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            height: getProportionateScreenHeight(16),
          ),
          Container(
            margin: EdgeInsets.only(left: 16, right: 16),
            width: SizeConfig.screenWidth,
            height: getProportionateScreenHeight(180),
            child: Column(
              children: [
                Container(
                  width: getProportionateScreenWidth(278),
                  height: getProportionateScreenHeight(156),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      image: DecorationImage(
                          image: NetworkImage(thumbnail ??
                              'http://api.vocasia.pasia.id/uploads/courses_thumbnail/course_thumbnail_default_57.jpg'),
                          fit: BoxFit.fill)),
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left: getProportionateScreenWidth(16),
              right: getProportionateScreenWidth(16),
              bottom: getProportionateScreenHeight(8),
            ),
            child: Center(
              child: Text(
                title ?? '',
                style: secondaryTextStyle.copyWith(
                    fontSize: 20, letterSpacing: 0.2),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          Container(
            child: Center(
              child: Text(
                'Oleh $instructor ',
                style:
                    primaryTextStyle.copyWith(fontSize: 14, letterSpacing: 0.5),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          SizedBox(
            height: getProportionateScreenHeight(20),
          ),
          Container(
            margin: EdgeInsets.only(left: 16, right: 16),
            child: DefaultButton(
              text: 'Mulai Kursus',
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MultiProvider(
                      providers: [
                        ChangeNotifierProvider(
                          create: (context) => LessonCourseProvider(
                            lessonCourseService: LessonCourseService(),
                            id: int.parse(id ?? '0'),
                          ),
                        ),
                        ChangeNotifierProvider(
                            create: (context) => DetailCourseProvider(
                                courseService: CourseService(), id: id ?? '1'))
                      ],
                      child: PlayCourse(
                        judul: title ?? '',
                        instruktur: instructor ?? '',
                        thumbnail: thumbnail ??
                            'http://api.vocasia.pasia.id/uploads/courses_thumbnail/course_thumbnail_default_57.jpg',
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
          SizedBox(
            height: getProportionateScreenHeight(20),
          ),
          LatestCourse()
        ],
      ),
    );
  }
}
