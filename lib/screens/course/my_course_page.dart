import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:initial_folder/providers/my_course_provider.dart';
import 'package:initial_folder/screens/home/components/body_comp/latest_course.dart';
import 'package:initial_folder/screens/home/components/body_comp/populer_course.dart';
import 'package:initial_folder/screens/splash/splash_screen_login.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/widgets/loading/loading_my_course.dart';
import 'package:initial_folder/widgets/my_course_list.dart';
import 'package:provider/provider.dart';
import '../../theme.dart';

class MyCoursePage extends StatelessWidget {
  const MyCoursePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget notLogin() {
      return Center(
        child: Container(
          margin: EdgeInsets.symmetric(
            horizontal: getProportionateScreenWidth(40),
          ),
          child: Text(
            'Kamu belum login, silahkan login untuk melihat kursus mu',
            textAlign: TextAlign.center,
            style: primaryTextStyle,
          ),
        ),
      );
    }

    Widget myCourse() {
      return RefreshIndicator(
        displacement: 40,
        color: primaryColor,
        onRefresh: () async {
          await Provider.of<MyCourseProvider>(context, listen: false)
              .getMyCourse();
        },
        child: Consumer<MyCourseProvider>(
          builder: (BuildContext context, state, _) {
            if (state.state == ResultState.Loading) {
              return Column(
                children: [
                  LoadingMyCourse(),
                  LoadingMyCourse(),
                  LoadingMyCourse(),
                ],
              );
            } else if (state.state == ResultState.HasData) {
              return ListView.builder(
                  shrinkWrap: true,
                  itemCount: state.result!.data[0].length,
                  itemBuilder: (context, index) {
                    var myCourse = state.result!.data[0][index];
                    // var prg = state.result!.data
                    //     .map((e) => e[0].courseId == '602')
                    //     .toList();
                    // print(prg);
                    return MyCourseList(
                      dataMyCourseModel: myCourse,
                    );
                  });
            } else if (state.state == ResultState.NoData) {
              return Column(
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: getProportionateScreenWidth(16)),
                    child: Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(height: 48),
                          Container(
                            width: getProportionateScreenWidth(100),
                            height: getProportionateScreenWidth(100),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(2),
                              image: DecorationImage(
                                fit: BoxFit.fill,
                                image: AssetImage(
                                    "assets/images/kursuskosong.png"),
                              ),
                            ),
                          ),
                          SizedBox(height: 16),
                          Text(
                            "Kursus tidak tersedia",
                            style: secondaryTextStyle.copyWith(
                              letterSpacing: 1,
                              fontWeight: semiBold,
                              fontSize: getProportionateScreenWidth(14),
                              color: tenthColor,
                            ),
                          ),
                          SizedBox(height: 4),
                          Text(
                            "Kamu belum memiliki kursus, daftar kursus sekarang agar kamu dapat mengikuti kursus",
                            textAlign: TextAlign.center,
                            style: primaryTextStyle.copyWith(
                              letterSpacing: 0.5,
                              fontWeight: reguler,
                              fontSize: getProportionateScreenWidth(12),
                              color: secondaryColor,
                            ),
                          ),
                          SizedBox(height: 16),
                          PopulerCourse(),
                          LatestCourse(),
                        ],
                      ),
                    ),
                  ),
                ],
              );
            } else if (state.state == ResultState.Error) {
              return Center(
                  child: Text(
                'Terjadi Kesalahan ',
                style: thirdTextStyle,
              ));
            } else {
              return Center(child: Text(''));
            }
          },
        ),
      );
    }

    return Scaffold(
        appBar: (Condition.loginEmail || Condition.loginFirebase)
            ? AppBar(
                title: Text('Kursusku',
                    style: secondaryTextStyle.copyWith(
                        fontWeight: semiBold,
                        letterSpacing: 2.3,
                        fontSize: getProportionateScreenWidth(16))),
                actions: [
                  IconButton(
                    onPressed: () {},
                    icon: Icon(FeatherIcons.search),
                  ),
                  IconButton(
                    onPressed: () {},
                    icon: Icon(Icons.tune_outlined),
                  ),
                  SizedBox(
                    width: getProportionateScreenWidth(10),
                  ),
                ],
              )
            : AppBar(
                title: Text('Kursusku',
                    style: secondaryTextStyle.copyWith(
                        fontWeight: semiBold,
                        letterSpacing: 2.3,
                        fontSize: getProportionateScreenWidth(16))),
              ),
        body: (Condition.loginEmail || Condition.loginFirebase)
            ? myCourse()
            : notLogin());
  }
}
