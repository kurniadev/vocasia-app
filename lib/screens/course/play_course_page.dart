import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:initial_folder/helper/user_info.dart';
import 'package:initial_folder/providers/lesson_course_provider.dart';
import 'package:initial_folder/providers/my_course_provider.dart'
    as myCourseProvider;
import 'package:initial_folder/providers/play_video_course_provider.dart';
import 'package:initial_folder/screens/course/component/detail_play_course.dart';
import 'package:initial_folder/screens/course/component/pengumuman.dart';
import 'package:initial_folder/screens/course/component/quest_and_answer.dart';
import 'package:initial_folder/screens/course/sertif.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:provider/provider.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class PlayCourse extends StatefulWidget {
  const PlayCourse(
      {Key? key,
      required this.instruktur,
      required this.judul,
      required this.thumbnail})
      : super(key: key);
  final String judul, instruktur, thumbnail;

  @override
  State<PlayCourse> createState() => _PlayCoursePageState();
}

class _PlayCoursePageState extends State<PlayCourse>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  late YoutubePlayerController _controller;

  late PlayerState _playerState;
  late YoutubeMetaData _videoMetaData;
  bool _isPlayerReady = false;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 4);
    _controller = YoutubePlayerController(
      initialVideoId:
          YoutubePlayer.convertUrlToId('https://youtu.be/y1VlqvQ4IQg') ?? '',
      flags: const YoutubePlayerFlags(
        mute: false,
        disableDragSeek: false,
        loop: false,
        isLive: false,
        forceHD: false,
        enableCaption: false,
        autoPlay: false,
      ),
    )..addListener(listener);
    _videoMetaData = const YoutubeMetaData();
    // _playerState = PlayerState.values
  }

  @override
  void deactivate() {
    // Pauses video while navigating to next page.
    _controller.pause();
    super.deactivate();
  }

  void listener() {
    if (_isPlayerReady && mounted && !_controller.value.isFullScreen) {
      setState(() {
        _playerState = _controller.value.playerState;
        _videoMetaData = _controller.metadata;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    PlayVideoCourseProvider playVideoCourseProvider =
        Provider.of<PlayVideoCourseProvider>(context);

    _getTab(index, child) {
      return Tab(
        height: 30,
        iconMargin: EdgeInsets.zero,
        child: Container(
          child: Center(
            child: Text(
              child,
              style: primaryTextStyle.copyWith(
                fontSize: SizeConfig.blockHorizontal! * 3.2,
                letterSpacing: 0.4,
              ),
            ),
          ),
        ),
      );
    }

    Widget cekbox(int e) {
      return Checkbox(
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
          activeColor: primaryColor,
          value: (e == 1) ? true : false,
          onChanged: (a) {});
    }

    return SafeArea(
      child: Scaffold(
        body: Consumer<LessonCourseProvider>(
          builder: (context, state, _) {
            if (state.state == ResultState.loading) {
              return Center(
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                  color: primaryColor,
                ),
              );
            } else if (state.state == ResultState.hasData) {
              var leson = state.result!.data[0];
              List<String> _listVideoUrl = leson
                  .map((e) =>
                      YoutubePlayer.convertUrlToId(e.videoUrl ?? '') ?? '')
                  .where((element) => element.isNotEmpty)
                  .toList();

              Map<String?, dynamic> lessonMapId = Map.fromIterable(leson,
                  key: (e) => YoutubePlayer.convertUrlToId(e.videoUrl ?? ''),
                  value: (e) => e.lessonId)
                ..removeWhere((key, value) => key == null || value == null);
              return YoutubePlayerBuilder(
                  onExitFullScreen: () {
                    SystemChrome.setPreferredOrientations(
                        DeviceOrientation.values);
                  },
                  player: YoutubePlayer(
                      thumbnail: Container(
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                fit: BoxFit.fill,
                                image: NetworkImage(widget.thumbnail))),
                      ),
                      showVideoProgressIndicator: true,
                      progressIndicatorColor: primaryColor,
                      topActions: [
                        GestureDetector(
                            onTap: () {}, child: Icon(Icons.expand_more)),
                        Spacer(),
                        Switch(
                            inactiveThumbImage:
                                AssetImage('assets/images/switch.png'),
                            activeColor: tenthColor,
                            value: state.switchbutton,
                            onChanged: (s) {
                              state.autoplay();
                            }),
                        // Icon(Icons.settings),
                      ],
                      onEnded: (state.switchbutton)
                          ? (data) async {
                              await state.updateLessonCourse(
                                  leson
                                      .map((e) => e.courseId ?? '')
                                      .toList()
                                      .first,
                                  lessonMapId[
                                      '${playVideoCourseProvider.url}']);

                              leson
                                  .map((e) => e)
                                  .where((element) =>
                                      element.lessonId ==
                                      lessonMapId[
                                          '${playVideoCourseProvider.url}'])
                                  .first
                                  .isFinished = 1;
                              await Provider.of<
                                          myCourseProvider.MyCourseProvider>(
                                      context,
                                      listen: false)
                                  .getMyCourse()
                                  .then((value) => _controller.load(
                                      _listVideoUrl[
                                          (_listVideoUrl.indexOf(data.videoId) +
                                                  1) %
                                              _listVideoUrl.length]));

                              playVideoCourseProvider.indexUri(_listVideoUrl[
                                  (_listVideoUrl.indexOf(data.videoId) + 1) %
                                      _listVideoUrl.length]);
                            }
                          : (d) async {
                              await state.updateLessonCourse(
                                  leson
                                      .map((e) => e.courseId ?? '')
                                      .toList()
                                      .first,
                                  lessonMapId[
                                      '${playVideoCourseProvider.url}']);
                              await Provider.of<
                                          myCourseProvider.MyCourseProvider>(
                                      context,
                                      listen: false)
                                  .getMyCourse();
                              setState(() {
                                leson
                                    .map((e) => e)
                                    .where((element) =>
                                        element.lessonId ==
                                        lessonMapId[
                                            '${playVideoCourseProvider.url}'])
                                    .first
                                    .isFinished = 1;
                              });
                              // print('g masuk');
                            },
                      onReady: () {
                        _controller.load(
                            YoutubePlayer.convertUrlToId(_listVideoUrl.first) ??
                                '');

                        _isPlayerReady = true;
                        playVideoCourseProvider.uri = _listVideoUrl.first;
                      },
                      controller: _controller),
                  builder: (contex, player) => DefaultTabController(
                      length: 4,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          player,
                          Padding(
                            padding:
                                EdgeInsets.all(getProportionateScreenWidth(10)),
                            child: Text(
                              '${widget.judul}',
                              textAlign: TextAlign.start,
                              style: secondaryTextStyle.copyWith(
                                  letterSpacing: 1,
                                  fontSize: getProportionateScreenWidth(14),
                                  color: tenthColor,
                                  fontWeight: semiBold),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: getProportionateScreenWidth(10)),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  '${widget.instruktur}',
                                  style: primaryTextStyle.copyWith(
                                    fontSize: getProportionateScreenWidth(12),
                                  ),
                                ),
                                TextButton(
                                    onPressed: () {
                                      Navigator.pushNamed(
                                          context, Sertif.routeName);
                                    },
                                    child: SizedBox(
                                      width: getProportionateScreenWidth(73),
                                      height: getProportionateScreenHeight(14),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceEvenly,
                                        children: [
                                          Image.asset(
                                              'assets/images/certificate_icon.png'),
                                          Text('Sertifikat',
                                              style: primaryTextStyle.copyWith(
                                                fontSize:
                                                    getProportionateScreenWidth(
                                                        12),
                                              ))
                                        ],
                                      ),
                                    ))
                              ],
                            ),
                          ),
                          SizedBox(
                            height: getProportionateScreenWidth(15),
                          ),
                          Consumer<myCourseProvider.MyCourseProvider>(
                            builder: (context, state, _) {
                              var progres = state.result!.data[0]
                                  .map((e) => e)
                                  .where(
                                    (element) =>
                                        element.courseId ==
                                        leson
                                            .map((e) => e.courseId ?? '')
                                            .toList()
                                            .first,
                                  )
                                  .toList();
                              return Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal:
                                        getProportionateScreenWidth(10)),
                                child: Stack(
                                  children: [
                                    Container(
                                      width: double.infinity,
                                      height: getProportionateScreenWidth(20),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(10),
                                        color: Colors.white,
                                      ),
                                    ),
                                    Container(
                                        width: (SizeConfig.screenWidth -
                                                getProportionateScreenWidth(
                                                    20)) *
                                            int.parse(progres[0]
                                                .totalProgress
                                                .toString()) /
                                            100,
                                        height: getProportionateScreenWidth(20),
                                        decoration: BoxDecoration(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          color: primaryColor,
                                        ),
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(top: 2),
                                          child: Text(
                                            '${progres[0].totalProgress}%',
                                            textAlign: TextAlign.center,
                                            style: primaryTextStyle.copyWith(
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      12),
                                              fontWeight: reguler,
                                              color: Color(0xff000000),
                                              letterSpacing: 0.5,
                                            ),
                                          ),
                                        )),
                                  ],
                                ),
                              );
                            },
                          ),
                          SizedBox(
                            height: getProportionateScreenHeight(10),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: getProportionateScreenWidth(10)),
                            child: TabBar(
                              isScrollable: true,
                              padding: EdgeInsets.zero,
                              unselectedLabelColor: secondaryColor,
                              labelColor: primaryColor,
                              indicatorColor: primaryColor,
                              controller: _tabController,
                              labelPadding:
                                  EdgeInsets.symmetric(horizontal: 10.0),
                              tabs: [
                                _getTab(0, 'Konten Kursus'),
                                _getTab(1, 'Detail'),
                                _getTab(2, 'Q & A'),
                                _getTab(3, 'Pengumuman'),
                              ],
                            ),
                          ),
                          Expanded(
                            child: TabBarView(
                              // physics: NeverScrollableScrollPhysics(),
                              controller: _tabController,
                              children: [
                                Container(
                                  margin: EdgeInsets.only(
                                      top: getProportionateScreenHeight(10)),
                                  child: ListView(
                                      children: leson
                                          .map(
                                            (e) => Container(
                                              margin: EdgeInsets.only(
                                                  bottom: 16,
                                                  left:
                                                      getProportionateScreenWidth(
                                                          10),
                                                  right:
                                                      getProportionateScreenWidth(
                                                          10)),
                                              decoration: BoxDecoration(
                                                  color: (YoutubePlayer
                                                              .convertUrlToId(
                                                                  e.videoUrl ??
                                                                      '') ==
                                                          playVideoCourseProvider
                                                              .url)
                                                      ? primaryColor
                                                          .withOpacity(0.2)
                                                      : Color(0xFF212121),
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          10)),
                                              child: Column(
                                                children: [
                                                  GestureDetector(
                                                    onTap: _isPlayerReady
                                                        ? (e.attachmentType ==
                                                                'url')
                                                            ? () {
                                                                playVideoCourseProvider
                                                                    .indexUri(
                                                                        YoutubePlayer.convertUrlToId(e.videoUrl ??
                                                                                '') ??
                                                                            '');

                                                                _controller.load(
                                                                    YoutubePlayer.convertUrlToId(
                                                                            playVideoCourseProvider.url) ??
                                                                        '');
                                                              }
                                                            : () {
                                                                ScaffoldMessenger.of(
                                                                        context)
                                                                    .showSnackBar(
                                                                        SnackBar(
                                                                  duration:
                                                                      Duration(
                                                                          seconds:
                                                                              2),
                                                                  backgroundColor:
                                                                      primaryColor,
                                                                  content: Text(
                                                                    'Bukan video url ',
                                                                    textAlign:
                                                                        TextAlign
                                                                            .center,
                                                                    style: primaryTextStyle
                                                                        .copyWith(
                                                                      color:
                                                                          backgroundColor,
                                                                    ),
                                                                  ),
                                                                  behavior:
                                                                      SnackBarBehavior
                                                                          .floating,
                                                                  shape:
                                                                      RoundedRectangleBorder(
                                                                    borderRadius:
                                                                        BorderRadius
                                                                            .circular(5),
                                                                  ),
                                                                ));
                                                              }
                                                        : () {
                                                            ScaffoldMessenger
                                                                    .of(context)
                                                                .showSnackBar(
                                                                    SnackBar(
                                                              duration:
                                                                  Duration(
                                                                      seconds:
                                                                          2),
                                                              backgroundColor:
                                                                  primaryColor,
                                                              content: Text(
                                                                'Mohon tunggu player belum siap ',
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                                style:
                                                                    primaryTextStyle
                                                                        .copyWith(
                                                                  color:
                                                                      backgroundColor,
                                                                ),
                                                              ),
                                                              behavior:
                                                                  SnackBarBehavior
                                                                      .floating,
                                                              shape:
                                                                  RoundedRectangleBorder(
                                                                borderRadius:
                                                                    BorderRadius
                                                                        .circular(
                                                                            5),
                                                              ),
                                                            ));
                                                          },
                                                    child: ListTile(
                                                      trailing: Text(
                                                        e.duration ?? '',
                                                        style: primaryTextStyle
                                                            .copyWith(
                                                                color:
                                                                    elveColor,
                                                                fontSize: 12),
                                                      ),
                                                      leading: cekbox(
                                                          e.isFinished ?? 0),
                                                      title: Html(
                                                        shrinkWrap: true,
                                                        data: e.lessonTitle ??
                                                            ' ',
                                                        style: {
                                                          "body": Style(
                                                            margin:
                                                                EdgeInsets.zero,
                                                            padding:
                                                                EdgeInsets.zero,
                                                            fontSize: FontSize(
                                                                (YoutubePlayer.convertUrlToId(e.videoUrl ??
                                                                            '') ==
                                                                        playVideoCourseProvider
                                                                            .url)
                                                                    ? 14
                                                                    : 12),
                                                            color: Colors.white,
                                                            letterSpacing: 0.5,
                                                            fontFamily:
                                                                'Noto Sans',
                                                          ),
                                                        },
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          )
                                          .toList()),
                                ),
                                DetailPlayCourse(),
                                QuestAndAnswer(
                                  id: leson
                                      .map((e) => e.courseId ?? '')
                                      .toList()
                                      .first,
                                ),
                                PengumumanPlayCourse(
                                  id: leson
                                      .map((e) => e.courseId ?? '')
                                      .toList()
                                      .first,
                                ),
                              ],
                            ),
                          )
                        ],
                      )));
            } else if (state.state == ResultState.error) {
              return Center(
                  child: Column(
                children: [
                  Text(
                    'Terjadi Kesalahan Coba Lagi',
                    style: thirdTextStyle,
                  ),
                ],
              ));
            } else {
              return Center(
                child: Text(
                  'Terjadi kesalahan',
                  style: thirdTextStyle,
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
