import 'dart:convert';

import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:initial_folder/providers/detail_course_provider.dart';
import 'package:initial_folder/providers/section_lesson_provider.dart';
import 'package:initial_folder/screens/detail_course/components/instruktur.dart';
import 'package:provider/provider.dart';

import '../../../size_config.dart';
import '../../../theme.dart';

class DetailPlayCourse extends StatelessWidget {
  const DetailPlayCourse({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SectionLessonProvider sectionLessonProvider =
        Provider.of<SectionLessonProvider>(context);
    Widget kemampuanDiraih(String title) {
      return Container(
        margin: EdgeInsets.only(bottom: 6),
        child: Row(
          children: [
            Icon(Icons.check,
                size: getProportionateScreenHeight(13), color: thirdColor),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  title,
                  style: primaryTextStyle.copyWith(
                      color: secondaryColor,
                      fontSize: getProportionateScreenWidth(12),
                      letterSpacing: 0.5),
                ),
              ),
            ),
          ],
        ),
      );
    }

    return Consumer<DetailCourseProvider>(builder: (context, state, _) {
      if (state.state == ResultState.Loading) {
        return Center(
          child: CircularProgressIndicator(
            color: primaryColor,
            strokeWidth: 2,
          ),
        );
      } else if (state.state == ResultState.HasData) {
        var detailCourse = state.result!.data[0][0];
        List outcomes = jsonDecode(detailCourse.outcome ?? 'gagal');

        return SingleChildScrollView(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ExpandableNotifier(
                  // <-- Provides ExpandableController to its children
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        height: 12,
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            left: getProportionateScreenWidth(10)),
                        child: Text('Kemampuan Yang Akan Diraih',
                            style: primaryTextStyle.copyWith(
                                fontWeight: semiBold,
                                letterSpacing: 1,
                                fontSize: getProportionateScreenWidth(14))),
                      ),
                      SizedBox(
                        height: 12,
                      ),
                      Expandable(
                        collapsed: ExpandableButton(
                          child: Container(
                            margin: EdgeInsets.only(
                                left: getProportionateScreenWidth(10)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                outcomes.isEmpty
                                    ? Row(
                                        children: [
                                          Expanded(
                                            child: Align(
                                              alignment: Alignment.topLeft,
                                              child: Text(
                                                '',
                                              ),
                                            ),
                                          ),
                                        ],
                                      )
                                    : Column(children: [
                                        ...outcomes
                                            .map((e) => kemampuanDiraih(e))
                                            .take(3)
                                      ]),
                                SizedBox(
                                  height: 8,
                                ),
                                Container(
                                  child: Text(
                                    "Tampilkan Lebih Banyak",
                                    style: primaryTextStyle.copyWith(
                                      fontWeight: semiBold,
                                      color: primaryColor,
                                      fontSize: getProportionateScreenWidth(12),
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ),
                        expanded: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              ExpandableButton(
                                // <-- Collapses when tapped on

                                child: Container(
                                  margin: EdgeInsets.only(
                                      left: getProportionateScreenWidth(10)),
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      outcomes.isEmpty
                                          ? Row(
                                              children: [
                                                Expanded(
                                                  child: Align(
                                                    alignment:
                                                        Alignment.topLeft,
                                                    child: Text(
                                                      '',
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            )
                                          : Column(
                                              children: outcomes
                                                  .map(
                                                      (e) => kemampuanDiraih(e))
                                                  .toList(),
                                            ),
                                      SizedBox(
                                        height: 16,
                                      ),
                                      Container(
                                        child: Text(
                                          "Tampilkan Lebih Sedikit",
                                          style: primaryTextStyle.copyWith(
                                            fontWeight: semiBold,
                                            color: primaryColor,
                                            fontSize:
                                                getProportionateScreenWidth(12),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              )
                            ]),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(10),
                ),
                Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                    margin:
                        EdgeInsets.only(left: getProportionateScreenWidth(10)),
                    child: Text(
                      'Deskripsi',
                      style: primaryTextStyle.copyWith(
                        fontWeight: semiBold,
                        letterSpacing: 1,
                        fontSize: getProportionateScreenWidth(14),
                      ),
                    ),
                  ),
                ),
                AnimatedSize(
                    curve: Curves.fastOutSlowIn,
                    duration: const Duration(milliseconds: 300),
                    child: Container(
                      child: sectionLessonProvider.isDescription
                          ? Html(
                              data: detailCourse.shortDescription,
                              style: {
                                "body": Style(
                                    fontSize: FontSize(12),
                                    fontWeight: reguler,
                                    color: secondaryColor),
                              },
                            )
                          : Wrap(
                              children: [
                                Html(
                                  data: detailCourse.description,
                                  style: {
                                    "body": Style(
                                        fontSize: FontSize(12),
                                        fontWeight: reguler,
                                        color: secondaryColor),
                                  },
                                ),
                              ],
                            ),
                    )),
                Padding(
                  padding: EdgeInsets.only(
                    left: getProportionateScreenWidth(3),
                  ),
                  child: TextButton(
                    style: TextButton.styleFrom(primary: primaryColor),
                    onPressed: () =>
                        sectionLessonProvider.descriptionPlayCourse(),
                    child: sectionLessonProvider.isDescription
                        ? Text(
                            'Tampilkan Lebih Banyak',
                            style: primaryTextStyle.copyWith(
                              fontWeight: semiBold,
                              color: primaryColor,
                              fontSize: getProportionateScreenWidth(12),
                            ),
                            textAlign: TextAlign.left,
                          )
                        : Text(
                            'Tampilkan Lebih Sedikit',
                            style: primaryTextStyle.copyWith(
                              fontWeight: semiBold,
                              color: primaryColor,
                              fontSize: getProportionateScreenWidth(12),
                            ),
                          ),
                  ),
                ),
                Instruktur(
                  id: detailCourse.instructorId ?? '1',
                  bio: detailCourse.bio,
                  instructor: detailCourse.instructor,
                  rating: detailCourse.rating[0].avgRating.toString(),
                  review: detailCourse.rating[0].totalReview ?? '',
                  fotoProfile: detailCourse.fotoProfile,
                  totalLesson: detailCourse.totalLesson,
                  totalStudent: detailCourse.totalStudents,
                ),
                SizedBox(
                  height: getProportionateScreenHeight(20),
                )
              ],
            ),
          ),
        );
      } else if (state.state == ResultState.Error) {
        return Center(
          child: Text('Terjadi Kesalahan'),
        );
      } else {
        return Center(child: Text('error'));
      }
    });
  }
}
