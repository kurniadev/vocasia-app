// import 'package:flutter/material.dart';
// import 'package:initial_folder/providers/lesson_course_provider.dart';
// import 'package:initial_folder/providers/play_video_course_provider.dart';
// import 'package:initial_folder/providers/section_lesson_provider.dart';
// import 'package:provider/provider.dart';
// import 'package:youtube_player_flutter/youtube_player_flutter.dart';

// import '../../../size_config.dart';
// import '../../../theme.dart';

// class ContentCourse extends StatelessWidget {
//   const ContentCourse({
//     Key? key,
//   }) : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     PlayVideoCourseProvider playVideoCourseProvider =
//         Provider.of<PlayVideoCourseProvider>(context);

//     return SingleChildScrollView(
//       child: Container(
//         margin: EdgeInsets.only(
//             left: getProportionateScreenWidth(16),
//             right: getProportionateScreenWidth(16),
//             top: getProportionateScreenWidth(10)),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             // Indikator angka sudah di solve
//             Stack(
//               children: [
//                 Container(
//                   width: double.infinity,
//                   height: getProportionateScreenWidth(20),
//                   decoration: BoxDecoration(
//                     borderRadius: BorderRadius.circular(10),
//                     color: Colors.white,
//                   ),
//                 ),
//                 Container(
//                     width: (SizeConfig.screenWidth -
//                             getProportionateScreenWidth(32)) *
//                         0.45,
//                     height: getProportionateScreenWidth(20),
//                     decoration: BoxDecoration(
//                       borderRadius: BorderRadius.circular(10),
//                       color: primaryColor,
//                     ),
//                     child: Text(
//                       '45%',
//                       textAlign: TextAlign.center,
//                       style: primaryTextStyle.copyWith(
//                         fontSize: getProportionateScreenWidth(12),
//                         fontWeight: reguler,
//                         color: Color(0xff000000),
//                         letterSpacing: 0.5,
//                       ),
//                     )),
//               ],
//             ),
//             SizedBox(
//               height: getProportionateScreenHeight(10),
//             ),

//             Consumer<LessonCourseProvider>(builder: (context, state, _) {
//               if (state.state == ResultState.loading) {
//                 return Center(
//                   child: CircularProgressIndicator(
//                     strokeWidth: 2,
//                     color: primaryColor,
//                   ),
//                 );
//               } else if (state.state == ResultState.hasData) {
//                 var leson = state.result!.data[0];
//                 return Column(
//                     children: leson
//                         .map(
//                           (e) => Theme(
//                             data: ThemeData.dark().copyWith(
//                               dividerColor: Colors.transparent,
//                             ),
//                             child: Container(
//                               margin: EdgeInsets.only(bottom: 16),
//                               decoration: BoxDecoration(
//                                   color: Color(0xFF212121),
//                                   border: Border.all(color: secondaryColor),
//                                   borderRadius: BorderRadius.circular(10)),
//                               child: Column(
//                                 children: [
//                                   GestureDetector(
//                                     onTap: () {
//                                       playVideoCourseProvider
//                                           .indexUri(e.videoUrl!);

//                                       YoutubePlayerController(
//                                               initialVideoId:
//                                                   YoutubePlayer.convertUrlToId(
//                                                           playVideoCourseProvider
//                                                               .url) ??
//                                                       '')
//                                           .load(playVideoCourseProvider.url);
//                                     },
//                                     child: ListTile(
//                                       leading: Checkbox(
//                                           value: true, onChanged: (a) {}),
//                                       title: Text(
//                                         'Bab',
//                                         style: primaryTextStyle.copyWith(
//                                             fontWeight: reguler,
//                                             letterSpacing: 0.5,
//                                             color: secondaryColor,
//                                             fontSize:
//                                                 getProportionateScreenWidth(
//                                                     10)),
//                                       ),
//                                       subtitle: Text(
//                                         e.title ?? 'kososng',
//                                         style: primaryTextStyle.copyWith(
//                                             color: Colors.white),
//                                       ),
//                                     ),
//                                   ),
//                                 ],
//                               ),
//                             ),
//                           ),
//                         )
//                         .toList());
//               } else {
//                 return Text('Terjadi Kesalahan , Coba Lagi');
//               }
//             })
//           ],
//         ),
//       ),
//     );
//   }
// }

// class LabeledCheckbox extends StatelessWidget {
//   const LabeledCheckbox({
//     Key? key,
//     required this.label,
//     required this.onChanged,
//     required this.time,
//   }) : super(key: key);

//   final String label;
//   final String time;
//   final ValueChanged<bool?>? onChanged;

//   @override
//   Widget build(BuildContext context) {
//     SectionLessonProvider sectionLessonProvider =
//         Provider.of<SectionLessonProvider>(context);
//     return Row(
//       children: <Widget>[
//         Checkbox(
//           value: sectionLessonProvider.isCheckBoxLesson,
//           onChanged: onChanged,
//           activeColor: primaryColor,
//           shape: RoundedRectangleBorder(
//             borderRadius: BorderRadius.circular(5),
//           ),
//           side: BorderSide(color: fourthColor),
//         ),
//         Expanded(
//             child: Text(
//           label,
//           style: primaryTextStyle.copyWith(
//               color: Colors.white,
//               fontSize: getProportionateScreenWidth(12),
//               letterSpacing: 0.5),
//         )),
//         Padding(
//           padding: const EdgeInsets.only(right: 3),
//           child: Text(time,
//               style: primaryTextStyle.copyWith(
//                   color: Colors.white,
//                   fontSize: getProportionateScreenHeight(11),
//                   letterSpacing: 0.5)),
//         ),
//       ],
//     );
//   }
// }
