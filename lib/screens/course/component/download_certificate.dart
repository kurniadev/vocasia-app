import 'package:flutter/material.dart';

class DownloadCertificate extends StatelessWidget {
  const DownloadCertificate({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          Row(
            children: [
              ElevatedButton(onPressed: () {}, child: Text('PNG')),
              ElevatedButton(onPressed: () {}, child: Text('PDF'))
            ],
          )
        ],
      ),
    );
  }
}
