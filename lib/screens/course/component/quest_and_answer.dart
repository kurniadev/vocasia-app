import 'package:flutter/material.dart';
import 'package:initial_folder/providers/qna_provider.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/providers/posting_qna_provider.dart';
import 'package:initial_folder/widgets/qna_user_page.dart';
import 'package:provider/provider.dart';

class QuestAndAnswer extends StatefulWidget {
  const QuestAndAnswer({
    Key? key,
    required this.id,
  }) : super(key: key);

  final id;

  @override
  State<QuestAndAnswer> createState() => _QuestAndAnswerState();
}

class _QuestAndAnswerState extends State<QuestAndAnswer> {
  double value = 0;

  final _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    PostingQnaProvider postingQnaProvider =
        Provider.of<PostingQnaProvider>(context);

    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: getProportionateScreenWidth(16),
            ),
            child: Column(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Ajukan Pertanyaan',
                      style: primaryTextStyle.copyWith(
                        fontWeight: semiBold,
                        letterSpacing: 0.1,
                        fontSize: getProportionateScreenWidth(14),
                      ),
                    ),
                    SizedBox(
                      height: getProportionateScreenWidth(8),
                    ),
                    TextField(
                      controller: _controller,
                      cursorColor: secondaryColor,
                      scrollPadding: EdgeInsets.zero,
                      minLines: 2,
                      keyboardType: TextInputType.multiline,
                      maxLines: null,
                      decoration: InputDecoration(
                        hintStyle: secondaryTextStyle.copyWith(
                          color: secondaryColor,
                          letterSpacing: 0.5,
                          fontSize: getProportionateScreenWidth(12),
                        ),
                        hintText: 'Ketikkan pertanyaanmu di sini...',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(
                            10,
                          ),
                          borderSide: BorderSide(
                            color: thirdColor,
                          ),
                        ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(
                            10,
                          ),
                          borderSide: BorderSide(
                            color: secondaryColor,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: getProportionateScreenHeight(4),
                    ),
                    Align(
                      alignment: Alignment.topRight,
                      child: ElevatedButton(
                        onPressed: () async {
                          if (await postingQnaProvider.postingQna(
                              _controller.text, widget.id)) {
                            _controller.clear();
                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                duration: Duration(seconds: 2),
                                backgroundColor: primaryColor,
                                content: Text(
                                  'Pertanyaan berhasil dikirimkan',
                                  style: primaryTextStyle.copyWith(
                                    color: backgroundColor,
                                  ),
                                ),
                                behavior: SnackBarBehavior.floating,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5),
                                ),
                                action: SnackBarAction(
                                  label: 'Lihat',
                                  onPressed: () {
                                    ScaffoldMessenger.of(context)
                                        .hideCurrentSnackBar();
                                  },
                                ),
                              ),
                            );
                          } else {
                            ScaffoldMessenger.of(context).showSnackBar(
                              SnackBar(
                                duration: Duration(seconds: 2),
                                backgroundColor: primaryColor,
                                content: Text(
                                  'Terjadi kesalahan',
                                  style: primaryTextStyle.copyWith(
                                    color: backgroundColor,
                                  ),
                                ),
                                behavior: SnackBarBehavior.floating,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(5),
                                ),
                              ),
                            );
                          }
                        },
                        child: Text(
                          'Kirim Pertanyaan',
                          style: thirdTextStyle.copyWith(
                            color: Colors.black,
                            fontSize: SizeConfig.blockHorizontal! * 4,
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                          primary: primaryColor,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Divider(),
          Container(
            margin: EdgeInsets.symmetric(
              horizontal: getProportionateScreenWidth(16),
            ),
            child: Text(
              'Pertanyaan Dan Jawaban',
              style: primaryTextStyle.copyWith(
                fontWeight: semiBold,
                letterSpacing: 1,
                fontSize: getProportionateScreenWidth(14),
              ),
            ),
          ),
          SizedBox(
            height: getProportionateScreenHeight(14),
          ),
          QnaUserPage(
            idCourse: widget.id,
          ),
          // QandA(
          //   divider: Divider(),
          // ),
          // QandA(
          //   divider: Divider(),
          // ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();

    super.dispose();
  }
}
