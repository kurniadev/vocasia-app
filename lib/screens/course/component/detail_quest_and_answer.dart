import 'package:flutter/material.dart';
import 'package:initial_folder/models/qna_model.dart';
import 'package:initial_folder/providers/posting_qna_reply_provider.dart';
import 'package:initial_folder/providers/reply_qna_provider.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/widgets/point_istruktur.dart';
import 'package:initial_folder/widgets/q_and_a.dart';
import 'package:initial_folder/widgets/qna_user.dart';
import 'package:initial_folder/widgets/reply_qna_user_page.dart';
import 'package:provider/provider.dart';

class DetailQuestAndAnswer extends StatefulWidget {
  const DetailQuestAndAnswer(
      {Key? key,
      required this.id,
      required this.qnaDataModel,
      required this.index,
      required this.userId})
      : super(key: key);
  final QnaDataModel qnaDataModel;
  final id;
  final int index;
  final int userId;
  @override
  State<DetailQuestAndAnswer> createState() => _DetailQuestAndAnswerState();
}

class _DetailQuestAndAnswerState extends State<DetailQuestAndAnswer> {
  final _controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    PostingQnaReplyProvider postingQnaReplyProvider =
        Provider.of<PostingQnaReplyProvider>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Pertanyaan',
          style: primaryTextStyle.copyWith(
            fontWeight: semiBold,
            fontSize: getProportionateScreenWidth(16),
            letterSpacing: 0.2,
          ),
        ),
      ),
      body: Stack(
        children: [
          ListView(
            children: [
              QnaUser(
                qnaDataModel: widget.qnaDataModel,
                id: widget.id,
                index: widget.index,
                userId: widget.userId,
              ),
              Divider(
                height: 1,
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(16),
                    vertical: getProportionateScreenWidth(8)),
                child: Text(
                  'Balasan',
                  style: primaryTextStyle.copyWith(
                      fontWeight: semiBold,
                      fontSize: getProportionateScreenWidth(16),
                      letterSpacing: 1),
                ),
              ),
              Divider(
                height: 1,
              ),
              SizedBox(
                height: getProportionateScreenHeight(13),
              ),
              // QandA(
              //   divider: Divider(),
              //   pointInstruktur: PointInstruktur(),
              // ),
              ReplyQnaUserPage(
                idCourse: widget.id,
                index: widget.index,
              ),
              // //TODO : perlu dirapihkan divider nya
              // QandA(
              //   divider: Divider(),
              // ),
            ],
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              color: backgroundColor,
              height: getProportionateScreenWidth(72),
              width: double.infinity,
              child: Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: getProportionateScreenWidth(16),
                  vertical: getProportionateScreenWidth(16),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: SizeConfig.screenWidth * 0.65,
                      child: TextFormField(
                        controller: _controller,
                        scrollPadding: EdgeInsets.zero,
                        cursorColor: secondaryColor,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(left: 10),
                          hintStyle: secondaryTextStyle.copyWith(
                            color: secondaryColor,
                            letterSpacing: 0.5,
                            fontSize: getProportionateScreenWidth(12),
                          ),
                          hintText: 'Balas Pertanyaan',
                          border: OutlineInputBorder(
                            gapPadding: 0,
                            borderRadius: BorderRadius.circular(
                              10,
                            ),
                            borderSide: BorderSide(
                              color: thirdColor,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(
                              10,
                            ),
                            borderSide: BorderSide(
                              color: secondaryColor,
                            ),
                          ),
                        ),
                      ),
                    ),
                    ElevatedButton(
                      onPressed: () async {
                        if (await postingQnaReplyProvider.postQnaReply(
                            _controller.text,
                            widget.qnaDataModel.id_qna.toString())) {
                          _controller.clear();
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              duration: Duration(seconds: 2),
                              backgroundColor: primaryColor,
                              content: Text(
                                'Balasan Terkirim',
                                style: primaryTextStyle.copyWith(
                                    color: backgroundColor),
                              ),
                              behavior: SnackBarBehavior.floating,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5),
                              ),
                              action: SnackBarAction(
                                label: 'Lihat',
                                onPressed: () {
                                  ScaffoldMessenger.of(context)
                                      .hideCurrentSnackBar();
                                },
                              ),
                            ),
                          );
                        } else {
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(
                              duration: Duration(seconds: 2),
                              backgroundColor: primaryColor,
                              content: Text(
                                'Terjadi kesalahan',
                                style: primaryTextStyle.copyWith(
                                  color: backgroundColor,
                                ),
                              ),
                              behavior: SnackBarBehavior.floating,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(5),
                              ),
                            ),
                          );
                        }
                      },
                      child: Text(
                        'Kirim',
                        style: thirdTextStyle.copyWith(
                          color: Color(0xff050505),
                          letterSpacing: 0.3,
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                        primary: primaryColor,
                        minimumSize: Size(
                          getProportionateScreenWidth(35),
                          getProportionateScreenWidth(35),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

// class DetailQuestAndAnswer extends StatelessWidget {
//   const DetailQuestAndAnswer(
//       {Key? key, required this.id, required this.qnaDataModel})
//       : super(key: key);

//   final QnaDataModel qnaDataModel;
//   final id;

//   @override
//   Widget build(BuildContext context) {

//   }
// }
