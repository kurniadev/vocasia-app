import 'package:flutter/material.dart';
import 'package:initial_folder/models/announcement_model.dart';
import 'package:initial_folder/providers/announcement_provider.dart';
import 'package:initial_folder/services/announcement_service.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/widgets/q_and_a.dart';
import 'package:provider/provider.dart';

class PengumumanPlayCourse extends StatelessWidget {
  const PengumumanPlayCourse({Key? key, required this.id}) : super(key: key);
  final String id;
  @override
  Widget build(BuildContext context) {
    print(id);
    return ChangeNotifierProvider(
      create: (context) => AnnouncementProvider(
          announcementService: AnnouncementService(), id: id),
      child: Consumer<AnnouncementProvider>(builder: (context, state, _) {
        if (state.state == ResultState.loading) {
          return Center(
            child: CircularProgressIndicator(
              color: primaryColor,
              strokeWidth: 2,
            ),
          );
        } else if (state.state == ResultState.noData) {
          return Center(
            child: Text(
              'Belum ada pengumuman saat ini',
              style: thirdTextStyle,
            ),
          );
        } else if (state.state == ResultState.hasData) {
          // var announcement = state.result!.data[0];
          return ListView.builder(
              itemCount: 3,
              itemBuilder: (context, index) {
                var announcement = state.result!.data[0][index];
                return QandA(
                  divider: Divider(),
                  announcementDataModel: announcement,
                );
              });
        } else if (state.state == ResultState.error) {
          return Center(
              child: Column(
            children: [
              Text(
                'Terjadi Kesalahan Coba Lagi',
                style: thirdTextStyle,
              ),
            ],
          ));
        }
        return Center(
          child: Text(
            'Terjadi Kesalahan',
            style: thirdTextStyle,
          ),
        );
      }),
    );
  }
}
