// import 'package:flutter/material.dart';
// import 'package:initial_folder/providers/tab_play_course_provider.dart';
// import 'package:initial_folder/screens/course/component/content_course.dart';
// import 'package:initial_folder/screens/course/component/detail_play_course.dart';
// import 'package:initial_folder/screens/course/component/pengumuman.dart';
// import 'package:initial_folder/screens/course/component/quest_and_answer.dart';
// import 'package:initial_folder/size_config.dart';
// import 'package:provider/provider.dart';
// import 'package:youtube_player_flutter/youtube_player_flutter.dart';

// import '../../../theme.dart';

// class TabBarPlayCourse extends StatelessWidget {
//   const TabBarPlayCourse({
//     Key? key,
//   }) : super(key: key);
//   @override
//   Widget build(BuildContext context) {
//     TabPlayCourseProvider tab = Provider.of<TabPlayCourseProvider>(context);
//     Widget buildContent(int currentIndex) {
//       switch (currentIndex) {
//         case 0:
//           return ContentCourse();
//         case 1:
//           return DetailPlayCourse();
//         case 2:
//           return QuestAndAnswer();
//         case 3:
//           return PengumumanPlayCourse();
//         default:
//           return ContentCourse();
//       }
//     }

//     Widget tabPlayCourse(int index, String title) {
//       return GestureDetector(
//         onTap: () {
//           tab.currentIndex = index;
//         },
//         child: Container(
//           child: Container(
//             margin: EdgeInsets.only(bottom: 5),
//             child: Text(
//               title,
//               style: primaryTextStyle.copyWith(
//                 color: tab.currentIndex == index ? primaryColor : Colors.white,
//                 fontSize: SizeConfig.blockHorizontal! * 3.2,
//               ),
//             ),
//           ),
//           decoration: BoxDecoration(
//             border: Border(
//               bottom: BorderSide(
//                 color: tab.currentIndex == index
//                     ? primaryColor
//                     : Colors.transparent,
//                 width: 2,
//               ),
//             ),
//           ),
//         ),
//       );
//     }

//     return Column(
//       children: [
//         Container(
//           width: SizeConfig.screenWidth,
//           height: getProportionateScreenHeight(40),
//           child: Center(
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.spaceAround,
//               children: [
//                 tabPlayCourse(0, 'Konten Kursus'),
//                 tabPlayCourse(1, 'Detail'),
//                 tabPlayCourse(2, 'Q & A'),
//                 tabPlayCourse(3, 'Pengumuman'),
//               ],
//             ),
//           ),
//         ),
//         Container(
//           child: buildContent(tab.currentIndex),
//         )
//       ],
//     );
//   }
// }
