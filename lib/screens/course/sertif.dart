import 'package:flutter/material.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';

class Sertif extends StatelessWidget {
  static String routeName = "/sertif";
  const Sertif({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
            title: Text(
          'Sertifikat',
          style: secondaryTextStyle.copyWith(
              letterSpacing: 0.23,
              fontWeight: semiBold,
              fontSize: getProportionateScreenWidth(14)),
        )),
        body: Center(
          child: Container(
            margin: EdgeInsets.only(
              left: getProportionateScreenWidth(16),
              right: getProportionateScreenWidth(16),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  'Sertifikat',
                  textAlign: TextAlign.center,
                  style: secondaryTextStyle.copyWith(
                      letterSpacing: -0.5,
                      color: secondaryColor,
                      fontWeight: reguler,
                      fontSize: getProportionateScreenWidth(28)),
                ),
                Text(
                  'Belum Tersedia',
                  textAlign: TextAlign.center,
                  style: secondaryTextStyle.copyWith(
                      letterSpacing: -0.5,
                      color: secondaryColor,
                      fontWeight: reguler,
                      fontSize: getProportionateScreenWidth(28)),
                ),
                SizedBox(height: getProportionateScreenHeight(24)),
                Stack(
                  children: [
                    Container(
                      width: double.infinity,
                      height: getProportionateScreenWidth(20),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.white,
                      ),
                    ),
                    Container(
                        width: (SizeConfig.screenWidth -
                                getProportionateScreenWidth(32)) *
                            0.45,
                        height: getProportionateScreenWidth(20),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: primaryColor,
                        ),
                        child: Text(
                          '45%',
                          textAlign: TextAlign.center,
                          style: primaryTextStyle.copyWith(
                            fontSize: getProportionateScreenWidth(12),
                            fontWeight: reguler,
                            color: Color(0xff000000),
                            letterSpacing: 0.5,
                          ),
                        )),
                  ],
                ),
                SizedBox(height: getProportionateScreenHeight(16)),
                Text(
                  'Anda baru menyelesaikan 45% kursus',
                  textAlign: TextAlign.center,
                  style: primaryTextStyle.copyWith(
                      letterSpacing: 0.5,
                      color: tenthColor,
                      fontWeight: reguler,
                      fontSize: getProportionateScreenWidth(13)),
                ),
                SizedBox(height: getProportionateScreenHeight(16)),
                Text(
                  'Anda belum memenuhi persyaratan untuk mendapatkan sertfikat. Selesaikan kursus anda untuk mendapatkan sertifikat penyelesaian kursus.',
                  textAlign: TextAlign.center,
                  style: primaryTextStyle.copyWith(
                      letterSpacing: 0.5,
                      color: secondaryColor,
                      fontWeight: reguler,
                      fontSize: getProportionateScreenWidth(13)),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
