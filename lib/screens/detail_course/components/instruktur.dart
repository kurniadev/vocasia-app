import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:initial_folder/providers/instructor_provider.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:provider/provider.dart';

class Instruktur extends StatelessWidget {
  const Instruktur(
      {Key? key,
      required this.id,
      this.instructor,
      this.bio,
      this.rating,
      this.review,
      this.totalStudent,
      this.totalLesson,
      this.video = false,
      this.fotoProfile})
      : super(key: key);
  final String id;
  final String? instructor;
  final String? bio;
  final String? rating;
  final String? review;
  final String? fotoProfile;
  final String? totalLesson;
  final String? totalStudent;
  final bool video;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
          horizontal: getProportionateScreenWidth(video ? 16 : 10),
          vertical: getProportionateScreenWidth(10)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Istruktur Kursus',
            style: primaryTextStyle.copyWith(
                fontWeight: semiBold,
                letterSpacing: 1,
                fontSize: getProportionateScreenWidth(14)),
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            decoration: BoxDecoration(
              color: Color(0xFF212121),
              borderRadius: BorderRadius.circular(8),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 16,
                    ),
                    // AssetImage("assets/images/Profile Image.png")
                    Container(
                      margin: EdgeInsets.only(
                          left: getProportionateScreenWidth(10)),
                      child: CircleAvatar(
                        radius: getProportionateScreenWidth(40),
                        backgroundColor: Colors.amber,
                        backgroundImage: fotoProfile == null
                            ? AssetImage("assets/images/Profile Image.png")
                            : NetworkImage(fotoProfile ?? '') as ImageProvider,
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          left: getProportionateScreenWidth(10)),
                      child: Text(
                        instructor ?? '',
                        style: primaryTextStyle.copyWith(
                            letterSpacing: 0.5,
                            fontSize: getProportionateScreenWidth(14)),
                      ),
                    ),
                    SizedBox(height: getProportionateScreenWidth(10)),
                    Container(
                      margin: EdgeInsets.only(
                          left: getProportionateScreenWidth(10)),
                      child: Row(
                        children: [
                          RatingBarIndicator(
                              itemSize: getProportionateScreenWidth(11),
                              rating: double.parse(rating ?? '0'),
                              direction: Axis.horizontal,
                              itemCount: 5,
                              //itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                              itemBuilder: (context, _) => FaIcon(
                                  FontAwesomeIcons.solidStar,
                                  color: primaryColor)),
                          SizedBox(
                            width: getProportionateScreenWidth(4),
                          ),
                          Text(
                            double.parse(rating ?? '0').toString(),
                            style: primaryTextStyle.copyWith(
                                fontSize: getProportionateScreenWidth(10),
                                color: secondaryColor,
                                fontWeight: reguler),
                          ),
                          SizedBox(
                            width: getProportionateScreenWidth(4),
                          ),
                          Text(
                            '(${review ?? '0'})',
                            style: primaryTextStyle.copyWith(
                                fontSize: getProportionateScreenWidth(10),
                                color: secondaryColor,
                                fontWeight: reguler),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          left: getProportionateScreenWidth(10), top: 1),
                      child: Row(
                        children: [
                          Text(
                            '${totalStudent ?? ''} Murid',
                            style: primaryTextStyle.copyWith(
                                color: secondaryColor,
                                fontSize: getProportionateScreenWidth(10),
                                letterSpacing: 0.5),
                          ),
                          SizedBox(width: getProportionateScreenWidth(10)),
                          Text(
                            '${totalLesson ?? ''} Kursus',
                            style: primaryTextStyle.copyWith(
                                color: secondaryColor,
                                fontSize: getProportionateScreenWidth(10),
                                letterSpacing: 0.5),
                          )
                        ],
                      ),
                    ),
                    ExpandableNotifier(
                      // <-- Provides ExpandableController to its children
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expandable(
                            // <-- Driven by ExpandableController from ExpandableNotifier
                            collapsed: ExpandableButton(
                              // <-- Expands when tapped on the cover photo
                              child: Wrap(
                                children: [
                                  Html(
                                    data: bio ?? '',
                                    style: {
                                      "body": Style(
                                          fontSize: FontSize(12),
                                          fontWeight: reguler,
                                          maxLines: 2,
                                          fontFamily: 'Noto Sans',
                                          color: secondaryColor),
                                    },
                                  ),
                                  SizedBox(
                                    height: 16,
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: getProportionateScreenWidth(10)),
                                    child: Text(
                                      'Tampilkan Lebih Banyak',
                                      style: primaryTextStyle.copyWith(
                                        fontWeight: semiBold,
                                        color: primaryColor,
                                        fontSize:
                                            getProportionateScreenWidth(12),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            expanded: Column(
                              children: [
                                ExpandableButton(
                                  // <-- Collapses when tapped on

                                  child: Container(
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Html(
                                          data: bio ?? '',
                                          style: {
                                            "body": Style(
                                                fontSize: FontSize(12),
                                                fontWeight: reguler,
                                                fontFamily: 'Noto Sans',
                                                color: secondaryColor),
                                          },
                                        ),
                                        SizedBox(
                                          height: 16,
                                        ),
                                        Container(
                                          margin: EdgeInsets.only(
                                              left: getProportionateScreenWidth(
                                                  10)),
                                          child: Text(
                                            "Tampilkan Lebih Sedikit",
                                            style: primaryTextStyle.copyWith(
                                              fontWeight: semiBold,
                                              color: primaryColor,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      12),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                        ],
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
          SizedBox(
            height: 30,
          )
        ],
      ),
    );
  }
}
