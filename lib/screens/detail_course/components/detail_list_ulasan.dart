import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:initial_folder/helper/validator.dart';

import '../../../size_config.dart';
import '../../../theme.dart';

class DetailListUlasan extends StatelessWidget {
  const DetailListUlasan({
    Key? key,
    required this.name,
    required this.starRating,
    required this.review,
    required this.date,
  }) : super(key: key);
  final String name;
  final double starRating;
  final String review;
  final String date;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(name,
              style: primaryTextStyle.copyWith(
                  fontWeight: semiBold,
                  letterSpacing: 1,
                  fontSize: getProportionateScreenWidth(14))),
          SizedBox(
            height: 6,
          ),
          Row(
            children: [
              RatingBarIndicator(
                  itemSize: getProportionateScreenWidth(10),
                  rating: starRating,
                  direction: Axis.horizontal,
                  itemCount: 5,
                  //itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                  itemBuilder: (context, _) =>
                      FaIcon(FontAwesomeIcons.solidStar, color: primaryColor)),
              SizedBox(
                width: 3,
              ),
              Text(
                dateFormatUlasan(date),
                style: primaryTextStyle.copyWith(
                    fontSize: getProportionateScreenWidth(10),
                    color: secondaryColor),
              )
            ],
          ),
          Container(
            child: Text(
              review,
              style: primaryTextStyle.copyWith(
                letterSpacing: 1,
                color: secondaryColor,
                fontSize: getProportionateScreenWidth(12),
              ),
            ),
          ),
          SizedBox(
            height: getProportionateScreenWidth(16),
          ),
          Divider(
            color: Color(0xff2D2D2D),
            thickness: 0.5,
          ),
        ],
      ),
    );
  }
}
