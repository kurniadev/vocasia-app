import 'package:flutter/material.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';

class KemampuainDiraihList extends StatelessWidget {
  const KemampuainDiraihList({
    Key? key,
    required this.title,
  }) : super(key: key);
  final String title;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 6),
      child: Row(
        children: [
          Icon(Icons.check,
              size: getProportionateScreenHeight(13), color: thirdColor),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Align(
              alignment: Alignment.topLeft,
              // TODO : saat teks menjadi 2 baris menjadi tidak sejajar perlu dirapihkan
              child: Text(
                // TODO : MAX 75 Chareacters
                title,
                style: primaryTextStyle.copyWith(
                    color: secondaryColor,
                    fontSize: getProportionateScreenWidth(12),
                    letterSpacing: 0.5),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
