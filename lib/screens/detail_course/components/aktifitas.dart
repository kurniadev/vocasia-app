import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:initial_folder/helper/validator.dart';
import 'package:initial_folder/providers/section_lesson_course_provider.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:provider/provider.dart';

// Rubah Ke Statles nanti
class Aktifitas extends StatefulWidget {
  const Aktifitas({Key? key, required this.id, required this.totalDuration})
      : super(key: key);
  final String id;
  final String totalDuration;
  @override
  _AktifitasState createState() => _AktifitasState();
}

class _AktifitasState extends State<Aktifitas> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 16, right: 16, top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Aktivitas',
            style: primaryTextStyle.copyWith(
                letterSpacing: 1,
                fontWeight: semiBold,
                fontSize: getProportionateScreenWidth(14)),
          ),
          SizedBox(
            height: 8,
          ),
          Consumer<SectionLessonCourseProvider>(
            builder: (context, state, _) {
              if (state.state == ResultState.Loading) {
                return Center(
                  child: CircularProgressIndicator(
                    color: Colors.amber,
                    strokeWidth: 2,
                  ),
                );
              } else if (state.state == ResultState.HasData) {
                // state.result!.data![0]
                //     .asMap()
                //     .forEach((index, value) => Text('Bab $index'));
                return Column(
                  children: [
                    Row(
                      children: [
                        Text('${state.result!.data![0].length} Pelajaran',
                            style: primaryTextStyle.copyWith(
                                color: secondaryColor)),
                        SizedBox(
                          width: 8,
                        ),
                        Text(widget.totalDuration,
                            style: primaryTextStyle.copyWith(
                                color: secondaryColor)),
                      ],
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    // state.result!.data![0].asMap().forEach((index, value) => Text(index.toString())),
                    Column(
                        children: state.result!.data![0]
                            .asMap()
                            .entries
                            .map(
                              (e) => Theme(
                                data: ThemeData.dark().copyWith(
                                  colorScheme:
                                      ColorScheme.dark(primary: secondaryColor),
                                  dividerColor: Colors.transparent,
                                ),
                                child: Container(
                                  margin: EdgeInsets.only(bottom: 16),
                                  decoration: BoxDecoration(
                                      color: Color(0xFF212121),
                                      border: Border.all(color: secondaryColor),
                                      borderRadius: BorderRadius.circular(10)),
                                  child: Column(
                                    children: [
                                      ListTileTheme(
                                        dense: true,
                                        child: ExpansionTile(
                                          // tilePadding: EdgeInsets.zero,
                                          title: Text(
                                            'Bab ${e.key + 1}',
                                            style: primaryTextStyle.copyWith(
                                                fontWeight: reguler,
                                                letterSpacing: 0.5,
                                                color: secondaryColor,
                                                fontSize:
                                                    getProportionateScreenWidth(
                                                        10)),
                                          ),
                                          subtitle: Html(
                                            shrinkWrap: true,
                                            data: e.value.title ?? '',
                                            style: {
                                              "body": Style(
                                                  margin: EdgeInsets.zero,
                                                  padding: EdgeInsets.zero,
                                                  fontSize: FontSize(
                                                      getProportionateScreenWidth(
                                                          12)),
                                                  fontWeight: reguler,
                                                  letterSpacing: 0.5,
                                                  fontFamily: 'Noto Sans',
                                                  color: Colors.white),
                                            },
                                          ),
                                          children: e.value.dataLesson![0]
                                              .map(
                                                (e) => ListTile(
                                                  title: Html(
                                                    data: e.titleLesson ?? '',
                                                    style: {
                                                      "body": Style(
                                                          margin:
                                                              EdgeInsets.zero,
                                                          padding:
                                                              EdgeInsets.zero,
                                                          fontSize: FontSize(
                                                              getProportionateScreenWidth(
                                                                  12)),
                                                          fontWeight: reguler,
                                                          letterSpacing: 0.5,
                                                          fontFamily:
                                                              'Noto Sans',
                                                          color: Colors.white),
                                                    },
                                                  ),
                                                  //  Text(
                                                  //   filterAnd(
                                                  //       e.titleLesson ?? ''),
                                                  //   style: primaryTextStyle
                                                  //       .copyWith(
                                                  //           color: Colors.white,
                                                  //           fontSize:
                                                  //               getProportionateScreenWidth(
                                                  //                   12.1),
                                                  //           letterSpacing: 0.5),
                                                  // ),
                                                  trailing: Container(
                                                    child: Text(
                                                        e.duration ?? '00:00:0',
                                                        style: primaryTextStyle
                                                            .copyWith(
                                                                color: Colors
                                                                    .white,
                                                                fontSize:
                                                                    getProportionateScreenHeight(
                                                                        11),
                                                                letterSpacing:
                                                                    0.5)),
                                                  ),
                                                ),
                                              )
                                              .toList(),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            )
                            .toList()),
                  ],
                );
              } else if (state.state == ResultState.NoData) {
                return Center(child: Text(state.message));
              } else if (state.state == ResultState.Error) {
                return Center(
                  child: Text('Server Internal Error'),
                );
              } else {
                return Center(child: Text(''));
              }
            },
          ),
        ],
      ),
    );
  }
}

// ExpansionTile(
//                 title: Text(
//                   'Bab 1',
//                   style: primaryTextStyle.copyWith(
//                       fontWeight: reguler,
//                       letterSpacing: 0.5,
//                       color: secondaryColor,
//                       fontSize: getProportionateScreenWidth(10)),
//                 ),
//                 subtitle: Text(
//                   'Belajar dari pengusaha succes',
//                   style: TextStyle(
//                       color: Colors.white,
//                       fontSize: getProportionateScreenWidth(12)),
//                 ),
//                 trailing: Icon(
//                   _customTileExpanded
//                       ? Icons.keyboard_arrow_up
//                       : Icons.keyboard_arrow_down,
//                   color: Colors.white,
//                 ),
//                 children: <Widget>[
//                   ListTile(
//                     title: Text(
//                       'Belajar Dari Pengusaha ',
//                       style: primaryTextStyle.copyWith(
//                           color: Colors.white,
//                           fontSize: getProportionateScreenWidth(12.1),
//                           letterSpacing: 0.5),
//                     ),
//                     trailing: Container(
//                       child: Text('00:02:00',
//                           style: primaryTextStyle.copyWith(
//                               color: Colors.white,
//                               fontSize: getProportionateScreenHeight(11),
//                               letterSpacing: 0.5)),
//                     ),
//                   ),
//                   ListTile(
//                     title: Text(
//                       'Belajar Dari Pengusaha Sukses',
//                       style: primaryTextStyle.copyWith(
//                           color: Colors.white,
//                           fontSize: getProportionateScreenWidth(12),
//                           letterSpacing: 0.5),
//                     ),
//                     trailing: Text('00:02:00',
//                         style: primaryTextStyle.copyWith(
//                             color: Colors.white,
//                             fontSize: getProportionateScreenHeight(11),
//                             letterSpacing: 0.5)),
//                   ),
//                 ],
//                 onExpansionChanged: (bool expanded) {
//                   setState(() => _customTileExpanded = expanded);
//                 },
//               ),
