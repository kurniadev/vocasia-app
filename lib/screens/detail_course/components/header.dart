import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:initial_folder/models/course_model.dart';
import 'package:initial_folder/models/detail_course_model.dart';
import 'package:initial_folder/screens/detail_course/components/murid_and_whislist.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:shimmer/shimmer.dart';

class Header extends StatelessWidget {
  const Header({
    Key? key,
    required this.dataDetailCourseModel,
  }) : super(key: key);

  final DataDetailCourseModel dataDetailCourseModel;

  @override
  Widget build(BuildContext context) {
    // var finalRating =
    //     double.parse((course.specificRating![0] / 20).toStringAsFixed(2));
    Widget imageCourse() {
      return Container(
        margin: EdgeInsets.only(
            left: getProportionateScreenWidth(2),
            right: getProportionateScreenWidth(2)),
        width: double.infinity,
        height: getProportionateScreenWidth(178),
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: getProportionateScreenWidth(178),
              child: CachedNetworkImage(
                imageUrl: dataDetailCourseModel.thumbnail ??
                    'http://api.vocasia.pasia.id/uploads/courses_thumbnail/course_thumbnail_default_57.jpg',
                imageBuilder: (context, imageProvider) => Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.vertical(
                        top: Radius.circular(5), bottom: Radius.circular(5)),
                    image: DecorationImage(
                      image: imageProvider,
                      fit: BoxFit.fill,
                    ),
                  ),
                ),
                placeholder: (context, url) => Shimmer(
                    child: Container(
                      color: thirdColor,
                    ),
                    gradient: LinearGradient(
                        stops: [0.4, 0.5, 0.6],
                        colors: [secondaryColor, thirdColor, secondaryColor])),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            ),
          ],
        ),
      );
    }

    return Container(
      margin: EdgeInsets.only(
          left: getProportionateScreenWidth(15),
          right: getProportionateScreenWidth(15)),
      width: SizeConfig.screenWidth,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: getProportionateScreenWidth(232),
            child: Text(dataDetailCourseModel.title ?? ' ',
                style: primaryTextStyle.copyWith(
                    letterSpacing: 0.1,
                    fontSize: getProportionateScreenHeight(14)),
                maxLines: 3,
                overflow: TextOverflow.ellipsis),
          ),
          SizedBox(
            height: getProportionateScreenHeight(13),
          ),
          Row(
            // crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              RatingBarIndicator(
                  itemSize: getProportionateScreenWidth(10),
                  rating: double.parse(
                      dataDetailCourseModel.rating[0].avgRating != null
                          ? '${dataDetailCourseModel.rating[0].avgRating}'
                          : '5.0'),
                  direction: Axis.horizontal,
                  itemCount: 5,
                  //itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                  itemBuilder: (context, _) =>
                      FaIcon(FontAwesomeIcons.solidStar, color: primaryColor)),
              SizedBox(
                width: getProportionateScreenWidth(4),
              ),
              Text(
                double.parse(dataDetailCourseModel.rating[0].avgRating != null
                        ? '${dataDetailCourseModel.rating[0].avgRating}'
                        : '5.0')
                    .toString(),
                style: primaryTextStyle.copyWith(
                    fontSize: getProportionateScreenWidth(10),
                    color: secondaryColor,
                    fontWeight: reguler),
              ),
              SizedBox(
                width: getProportionateScreenWidth(4),
              ),
              Text(
                // '(${course.numberOfRatings.toString()})',
                '(${dataDetailCourseModel.rating[0].totalReview})',
                style: primaryTextStyle.copyWith(
                    fontSize: getProportionateScreenWidth(10),
                    color: secondaryColor,
                    fontWeight: reguler),
              ),
            ],
          ),
          SizedBox(
            height: getProportionateScreenHeight(9),
          ),
          MuridAndWhislist(
            dataDetailCourseModel: dataDetailCourseModel,
          ),
          SizedBox(
            height: getProportionateScreenHeight(16),
          ),
          imageCourse(),
          SizedBox(
            height: getProportionateScreenHeight(11),
          ),
        ],
      ),
    );
  }
}
