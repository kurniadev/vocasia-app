import 'package:flutter/material.dart';
import 'package:initial_folder/models/detail_course_model.dart';
import 'package:initial_folder/providers/tab_provider.dart';
import 'package:initial_folder/screens/detail_course/components/aktifitas.dart';
import 'package:initial_folder/screens/detail_course/components/deskripsi.dart';
import 'package:initial_folder/screens/detail_course/components/instruktur.dart';
import 'package:initial_folder/screens/detail_course/components/tab_bar_items.dart';
import 'package:initial_folder/screens/detail_course/components/ulasan.dart';
import 'package:initial_folder/size_config.dart';
import 'package:provider/provider.dart';

class CustomTabBar extends StatelessWidget {
  const CustomTabBar(
      {Key? key,
      required this.dataDetailCourseModel,
      this.totalDuration,
      this.bio,
      this.instructor,
      this.rating,
      this.review,
      this.totalLesson,
      this.totalStudent,
      this.fotoProfile})
      : super(key: key);
  final DataDetailCourseModel dataDetailCourseModel;
  final String? totalDuration,
      bio,
      instructor,
      review,
      totalLesson,
      totalStudent,
      fotoProfile,
      rating;
  @override
  Widget build(BuildContext context) {
    TabProvider tab = Provider.of<TabProvider>(context);
    Widget buildContent(int currentIndex) {
      switch (currentIndex) {
        case 0:
          return Desksripsi(
            dataDetailCourseModel: dataDetailCourseModel,
          );
        case 1:
          return Aktifitas(
            id: dataDetailCourseModel.id,
            totalDuration: totalDuration ?? '',
          );
        case 2:
          return Instruktur(
            video: true,
            id: dataDetailCourseModel.id,
            instructor: instructor,
            bio: bio,
            rating: rating,
            fotoProfile: fotoProfile,
            review: review,
            totalLesson: totalLesson,
            totalStudent: totalStudent,
          );

        case 3:
          return Ulasan(
            id: dataDetailCourseModel.id,
          );
        default:
          return Desksripsi(
            dataDetailCourseModel: dataDetailCourseModel,
          );
      }
    }

    return Column(
      children: [
        Container(
          // color: Colors.blue,
          width: SizeConfig.screenWidth,
          height: getProportionateScreenHeight(40),
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                TabBarItems(
                  index: 0,
                  title: 'Deskripsi',
                ),
                TabBarItems(
                  index: 1,
                  title: 'Aktivitas',
                ),
                TabBarItems(
                  index: 2,
                  title: 'Instruktur',
                ),
                TabBarItems(
                  index: 3,
                  title: 'Ulasan',
                ),
              ],
            ),
          ),
        ),
        Container(
          child: buildContent(tab.currentIndex),
        )
      ],
    );
  }
}
