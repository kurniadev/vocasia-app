import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:initial_folder/helper/validator.dart';
import 'package:initial_folder/providers/detail_rating_course_provider.dart';
import 'package:initial_folder/screens/detail_course/components/detail_list_ulasan.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:provider/provider.dart';

class Ulasan extends StatelessWidget {
  const Ulasan({Key? key, required this.id}) : super(key: key);
  final String id;
  @override
  Widget build(BuildContext context) {
    final listChoices = <ItemChoice>[
      ItemChoice(0, 0, 'Semua '),
      ItemChoice(1, 1, '1'),
      ItemChoice(2, 1, '2'),
      ItemChoice(3, 1, '3'),
      ItemChoice(4, 1, '4'),
      ItemChoice(5, 1, '5'),
    ];
    return Column(
      children: [
        Container(
          decoration: BoxDecoration(
            color: Color(0xFF212121),
            borderRadius: BorderRadius.circular(4),
          ),
          margin: EdgeInsets.only(
              left: getProportionateScreenWidth(16),
              right: getProportionateScreenWidth(16)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.only(
                  left: getProportionateScreenWidth(10),
                  right: getProportionateScreenWidth(10),
                ),
                child: Consumer<DetailRatingCourseProvider>(
                    builder: (context, state, _) {
                  if (state.state == ResultState.Loading) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: CircularProgressIndicator(
                          color: primaryColor,
                          strokeWidth: 2,
                        ),
                      ),
                    );
                  } else if (state.state == ResultState.HasData) {
                    var ulasan = state.result;

                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: 12),
                        Text(
                          'Ulasan Kursus',
                          style: primaryTextStyle.copyWith(
                              fontWeight: semiBold, letterSpacing: 1),
                        ),
                        Row(
                          children: [
                            Text(
                              ulasan!.data.avgRating is List<dynamic>
                                  ? '5,0'
                                  : double.parse(ulasan.data.avgRating)
                                      .toString(),
                              style: primaryTextStyle.copyWith(
                                  fontSize: getProportionateScreenWidth(35),
                                  letterSpacing: -0.5),
                            ),
                            SizedBox(
                              width: 14,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                // Five(),
                                SizedBox(
                                  height: 2,
                                ),
                                RatingBarIndicator(
                                    itemSize: getProportionateScreenWidth(10),
                                    rating: ulasan.data.avgRating
                                            is List<dynamic>
                                        ? 5.0
                                        : double.parse(ulasan.data.avgRating),
                                    direction: Axis.horizontal,
                                    itemCount: 5,
                                    //itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                                    itemBuilder: (context, _) => FaIcon(
                                        FontAwesomeIcons.solidStar,
                                        color: primaryColor)),
                                SizedBox(height: 3),
                                Text(
                                  '(${ulasan.dataReview.length} Ulasan)',
                                  style: primaryTextStyle.copyWith(
                                      fontSize: getProportionateScreenWidth(12),
                                      letterSpacing: 0.5),
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 12,
                        ),
                        VerticalRatingBar(
                            lebar:
                                ulasan.data.precentageRating.rating5.toString(),
                            text: '5.0',
                            total: (ulasan.data.precentageRating.rating5
                                        .runtimeType ==
                                    String)
                                ? '${persentaseUlasan(ulasan.data.precentageRating.rating5)}%'
                                : '${ulasan.data.precentageRating.rating5}%'),
                        VerticalRatingBar(
                            lebar:
                                ulasan.data.precentageRating.rating4.toString(),
                            text: '4.0',
                            total: (ulasan.data.precentageRating.rating4
                                        .runtimeType ==
                                    String)
                                ? '${persentaseUlasan(ulasan.data.precentageRating.rating4)}%'
                                : '${ulasan.data.precentageRating.rating4}%'),
                        VerticalRatingBar(
                            lebar:
                                ulasan.data.precentageRating.rating3.toString(),
                            text: '3.0',
                            total: (ulasan.data.precentageRating.rating3
                                        .runtimeType ==
                                    String)
                                ? '${persentaseUlasan(ulasan.data.precentageRating.rating3)}%'
                                : '${ulasan.data.precentageRating.rating3}%'),
                        VerticalRatingBar(
                            lebar:
                                ulasan.data.precentageRating.rating2.toString(),
                            text: '2.0',
                            total: (ulasan.data.precentageRating.rating2
                                        .runtimeType ==
                                    String)
                                ? '${persentaseUlasan(ulasan.data.precentageRating.rating2)}%'
                                : '${ulasan.data.precentageRating.rating2}%'),
                        VerticalRatingBar(
                            lebar:
                                ulasan.data.precentageRating.rating1.toString(),
                            text: '1.0',
                            total: (ulasan.data.precentageRating.rating1
                                        .runtimeType ==
                                    String)
                                ? '${persentaseUlasan(ulasan.data.precentageRating.rating1)}%'
                                : '${ulasan.data.precentageRating.rating1}%'),
                      ],
                    );
                  }
                  return Text(
                    'Terjadi Kesalan ',
                    style: primaryTextStyle,
                  );
                }),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 12,
        ),
        Consumer<DetailRatingCourseProvider>(
          builder: (context, state, _) {
            if (state.state == ResultState.HasData) {
              var ulasan = state.result;
              return Container(
                margin: EdgeInsets.only(
                    left: getProportionateScreenWidth(16),
                    right: getProportionateScreenWidth(16)),
                decoration: BoxDecoration(
                  color: Color(0xFF212121),
                  borderRadius: BorderRadius.circular(4),
                ),
                child: Column(
                  children: [
                    SizedBox(
                      height: getProportionateScreenHeight(8),
                    ),
                    Wrap(
                        spacing: 3,
                        runSpacing: 3,
                        children: listChoices
                            .map((e) => ChoiceChip(
                                  materialTapTargetSize:
                                      MaterialTapTargetSize.shrinkWrap,
                                  backgroundColor: Color(0xff181818),
                                  side: BorderSide(color: Colors.white),
                                  labelPadding:
                                      EdgeInsets.symmetric(horizontal: 5),
                                  label: Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: [
                                      (e.icon == 0)
                                          ? Text('')
                                          : FaIcon(
                                              FontAwesomeIcons.solidStar,
                                              color: primaryColor,
                                              size: getProportionateScreenWidth(
                                                  11),
                                            ),
                                      SizedBox(
                                        width: 4,
                                      ),
                                      Text(e.label,
                                          style: primaryTextStyle.copyWith(
                                              color: state.currentIndex == e.id
                                                  ? Colors.black
                                                  : tenthColor,
                                              letterSpacing: 0.5,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      12),
                                              fontWeight: reguler)),
                                    ],
                                  ),
                                  selected: state.currentIndex == e.id,
                                  selectedColor: tenthColor,
                                  onSelected: (_) {
                                    state.currentIndex = e.id;
                                  },
                                  // backgroundColor: color,
                                  elevation: 1,
                                ))
                            .toList()),
                    SizedBox(
                      height: 8,
                    ),
                    Divider(
                      color: Color(0xff2D2D2D),
                      thickness: 0.5,
                    ),
                    Container(
                      margin: EdgeInsets.only(
                        left: getProportionateScreenWidth(10),
                        right: getProportionateScreenWidth(10),
                      ),
                      child: (ulasan!.dataReview.isEmpty)
                          ? SizedBox(
                              height: 16,
                            )
                          : Column(
                              children: ulasan.dataReview
                                  .map(
                                    (e) => DetailListUlasan(
                                      review: e.review ?? '',
                                      date: e.date ?? '-',
                                      name: e.name ?? '',
                                      starRating: double.parse(e.rating ?? '5'),
                                    ),
                                  )
                                  .toList()),
                    )
                  ],
                ),
              );
            }
            return Text('');
          },
        ),
        SizedBox(
          height: 30,
        )
      ],
    );
  }
}

//  SizedBox(
//                       height: getProportionateScreenHeight(8),
//                     ),
//                     Wrap(
//                       spacing: 5.0,
//                       children: [
// Chip(
// materialTapTargetSize:
//     MaterialTapTargetSize.shrinkWrap,
//   padding: EdgeInsets.zero,
//   label: Container(
//     //alignment: Alignment.center,
//     child: Row(
//       mainAxisSize: MainAxisSize.min,
//       children: [
//         Text('Semua',
//             style: primaryTextStyle.copyWith(
//                 color: tenthColor,
//                 fontSize:
//                     getProportionateScreenWidth(12),
//                 fontWeight: reguler)),
//       ],
//     ),
//   ),
//   backgroundColor: Color(0xff181818),
//   side: BorderSide(color: Colors.white),
// ),
//                         Chip(
//                           materialTapTargetSize:
//                               MaterialTapTargetSize.shrinkWrap,
//                           padding: EdgeInsets.zero,
//                           label: Container(
//                             //alignment: Alignment.center,
//                             //width: 100,
// child: Row(
//   mainAxisSize: MainAxisSize.min,
//   children: [
//     FaIcon(
//       FontAwesomeIcons.solidStar,
//       color: primaryColor,
//       size: getProportionateScreenWidth(11),
//     ),
//     SizedBox(
//       width: 4,
//     ),
//     Text('5 ',
//         style: primaryTextStyle.copyWith(
//             color: tenthColor,
//             fontSize:
//                 getProportionateScreenWidth(12),
//             fontWeight: reguler)),
//   ],
// ),
//                           ),
// backgroundColor: Color(0xff181818),
// side: BorderSide(color: Colors.white),
//                         ),
//                         Chip(
//                           materialTapTargetSize:
//                               MaterialTapTargetSize.shrinkWrap,
//                           padding: EdgeInsets.zero,
//                           labelPadding: EdgeInsets.symmetric(
//                             horizontal: 8,
//                           ),
//                           label: Container(
//                             //alignment: Alignment.center,
//                             //width: 100,
//                             child: Row(
//                               mainAxisSize: MainAxisSize.min,
//                               children: [
//                                 FaIcon(
//                                   FontAwesomeIcons.solidStar,
//                                   color: primaryColor,
//                                   size: getProportionateScreenWidth(11),
//                                 ),
//                                 SizedBox(
//                                   width: 4,
//                                 ),
//                                 Text('4',
//                                     style: primaryTextStyle.copyWith(
//                                         color: tenthColor,
//                                         fontSize:
//                                             getProportionateScreenWidth(12),
//                                         fontWeight: reguler)),
//                               ],
//                             ),
//                           ),
//                           backgroundColor: Color(0xff181818),
//                           side: BorderSide(color: Colors.white),
//                         ),
//                         Chip(
//                           materialTapTargetSize:
//                               MaterialTapTargetSize.shrinkWrap,
//                           padding: EdgeInsets.zero,
//                           labelPadding: EdgeInsets.symmetric(
//                             horizontal: 8,
//                           ),
//                           label: Container(
//                             //alignment: Alignment.center,
//                             //width: 100,
//                             child: Row(
//                               mainAxisSize: MainAxisSize.min,
//                               children: [
//                                 FaIcon(
//                                   FontAwesomeIcons.solidStar,
//                                   color: primaryColor,
//                                   size: getProportionateScreenWidth(11),
//                                 ),
//                                 SizedBox(
//                                   width: 4,
//                                 ),
//                                 Text('3',
//                                     style: primaryTextStyle.copyWith(
//                                         color: tenthColor,
//                                         fontSize:
//                                             getProportionateScreenWidth(12),
//                                         fontWeight: reguler)),
//                               ],
//                             ),
//                           ),
//                           backgroundColor: Color(0xff181818),
//                           side: BorderSide(color: Colors.white),
//                         ),
//                         Chip(
//                           materialTapTargetSize:
//                               MaterialTapTargetSize.shrinkWrap,
//                           padding: EdgeInsets.zero,
//                           labelPadding: EdgeInsets.symmetric(
//                             horizontal: 8,
//                           ),
//                           label: Container(
//                             //alignment: Alignment.center,
//                             //width: 100,
//                             child: Row(
//                               mainAxisSize: MainAxisSize.min,
//                               children: [
//                                 FaIcon(
//                                   FontAwesomeIcons.solidStar,
//                                   color: primaryColor,
//                                   size: getProportionateScreenWidth(11),
//                                 ),
//                                 SizedBox(
//                                   width: 4,
//                                 ),
//                                 Text('2',
//                                     style: primaryTextStyle.copyWith(
//                                         color: tenthColor,
//                                         fontSize:
//                                             getProportionateScreenWidth(12),
//                                         fontWeight: reguler)),
//                               ],
//                             ),
//                           ),
//                           backgroundColor: Color(0xff181818),
//                           side: BorderSide(color: Colors.white),
//                         ),
//                         Chip(
//                           materialTapTargetSize:
//                               MaterialTapTargetSize.shrinkWrap,
//                           padding: EdgeInsets.zero,
//                           labelPadding: EdgeInsets.symmetric(
//                             horizontal: 8,
//                           ),
//                           label: Container(
//                             //alignment: Alignment.center,
//                             //width: 100,
//                             child: Row(
//                               mainAxisSize: MainAxisSize.min,
//                               children: [
//                                 FaIcon(
//                                   FontAwesomeIcons.solidStar,
//                                   color: primaryColor,
//                                   size: getProportionateScreenWidth(11),
//                                 ),
//                                 SizedBox(
//                                   width: 4,
//                                 ),
//                                 Text('1',
//                                     style: primaryTextStyle.copyWith(
//                                         color: tenthColor,
//                                         fontSize:
//                                             getProportionateScreenWidth(12),
//                                         fontWeight: reguler)),
//                               ],
//                             ),
//                           ),
//                           backgroundColor: Color(0xff181818),
//                           side: BorderSide(color: Colors.white),
//                         ),
//                       ],
//                     ),

// ================================================double
//  else if (state.state == ResultState.NoData) {
//                     return Container(
//                       margin: EdgeInsets.only(
//                         left: getProportionateScreenWidth(10),
//                         right: getProportionateScreenWidth(10),
//                       ),
//                       child: Column(
//                         crossAxisAlignment: CrossAxisAlignment.start,
//                         children: [
//                           SizedBox(height: 12),
//                           Text(
//                             'Ulasan Kursus',
//                             style: primaryTextStyle.copyWith(
//                                 fontWeight: semiBold, letterSpacing: 1),
//                           ),
//                           Row(
//                             children: [
//                               Text(
//                                 (ulasa.data.avgRating is List<dynamic>)
//                                     ? '4,0'
//                                     : double.parse(state.result!.data.avgRating)
//                                         .toString(),
//                                 style: primaryTextStyle.copyWith(
//                                     fontSize: getProportionateScreenWidth(35),
//                                     letterSpacing: -0.5),
//                               ),
//                               SizedBox(
//                                 width: 14,
//                               ),
//                               Column(
//                                 crossAxisAlignment: CrossAxisAlignment.start,
//                                 children: [
//                                   // Five(),
//                                   SizedBox(
//                                     height: 2,
//                                   ),
//                                   RatingBarIndicator(
//                                       itemSize: getProportionateScreenWidth(10),
//                                       rating: (state.result!.data.avgRating
//                                               is List<dynamic>)
//                                           ? double.parse('3')
//                                           : double.parse(
//                                               state.result!.data.avgRating),
//                                       direction: Axis.horizontal,
//                                       itemCount: 5,
//                                       //itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
//                                       itemBuilder: (context, _) => FaIcon(
//                                           FontAwesomeIcons.solidStar,
//                                           color: primaryColor)),
//                                   SizedBox(height: 3),
//                                   Text(
//                                     '(${state.result!.dataReview.length} Ulasan)',
//                                     style: primaryTextStyle.copyWith(
//                                         fontSize:
//                                             getProportionateScreenWidth(12),
//                                         letterSpacing: 0.5),
//                                   )
//                                 ],
//                               )
//                             ],
//                           ),
//                           SizedBox(
//                             height: 12,
//                           ),
//                         ],
//                       ),
//                     );
//                   }

class VerticalRatingBar extends StatelessWidget {
  const VerticalRatingBar({
    Key? key,
    required this.text,
    this.lebar = '0',
    required this.total,
  }) : super(key: key);
  final String text;
  final String total;
  final String lebar;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: getProportionateScreenWidth(8)),
      // color: eightColor,
      // height: getProportionateScreenHeight(25),
      child: Column(
        children: [
          Row(
            children: [
              FaIcon(
                FontAwesomeIcons.solidStar,
                color: primaryColor,
                size: getProportionateScreenWidth(11),
              ),
              SizedBox(
                width: getProportionateScreenWidth(4),
              ),
              Text(
                text,
                style: primaryTextStyle.copyWith(
                    color: secondaryColor,
                    fontSize: getProportionateScreenWidth(10)),
              ),
              SizedBox(
                width: getProportionateScreenWidth(6),
              ),
              Expanded(
                child: Column(
                  children: [
                    Stack(
                      children: [
                        Container(
                          height: getProportionateScreenWidth(4),
                          decoration: BoxDecoration(
                              color: fourthColor,
                              borderRadius: BorderRadius.circular(10)),
                        ),
                        Container(
                          width: (SizeConfig.screenWidth -
                                  getProportionateScreenWidth(110)) *
                              int.parse(persentaseUlasan(lebar)) /
                              100,
                          height: getProportionateScreenWidth(4),
                          decoration: BoxDecoration(
                              color: primaryColor,
                              borderRadius: BorderRadius.circular(10)),
                        )
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: getProportionateScreenWidth(8),
              ),
              Container(
                width: getProportionateScreenWidth(30),
                child: Text(
                  total,
                  style: primaryTextStyle.copyWith(
                    color: secondaryColor,
                    fontSize: getProportionateScreenWidth(10),
                  ),
                  textAlign: TextAlign.start,
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}

class ItemChoice {
  final int id;
  final int icon;
  final String label;

  ItemChoice(this.id, this.icon, this.label);
}
