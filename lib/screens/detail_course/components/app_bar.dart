import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:initial_folder/providers/carts_provider.dart';
import 'package:initial_folder/screens/home/components/appBar/icon_btn_with_counter.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:provider/provider.dart';
import '../../../../size_config.dart';
import 'package:initial_folder/helper/user_info.dart';
import 'package:initial_folder/screens/login/login_screen.dart';
import 'package:initial_folder/screens/cart/cart_page.dart';
import 'package:initial_folder/screens/home/components/notifikasi.dart';
import 'package:initial_folder/screens/splash/splash_screen_login.dart';

class AppBarHeader extends StatelessWidget {
  const AppBarHeader({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Future _showDialogNotLogin(String teks) {
      return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          contentPadding: EdgeInsets.fromLTRB(12, 20, 12, 1),
          content: Text(
            'Mohon login terlebih dahulu sebelum $teks',
            style: primaryTextStyle.copyWith(
                fontSize: getProportionateScreenWidth(12), letterSpacing: 1),
          ),
          actions: [
            GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Text('Batal',
                  style: primaryTextStyle.copyWith(
                      fontSize: getProportionateScreenWidth(12),
                      letterSpacing: 1,
                      color: primaryColor)),
            ),
            SizedBox(
              width: getProportionateScreenWidth(5),
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamedAndRemoveUntil(
                    LoginScreen.routeName, (Route<dynamic> route) => false);
              },
              child: Text('Login',
                  style: primaryTextStyle.copyWith(
                      fontSize: getProportionateScreenWidth(12),
                      letterSpacing: 1,
                      color: primaryColor)),
            ),
          ],
        ),
      );
    }

    handleNotLoginCart() async {
      var token = await UsersInfo().getToken();
      if (token != null || Condition.loginFirebase == true) {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => CartPage(),
          ),
        );
      } else {
        String teks = 'dapat mengakses keranjang';
        return _showDialogNotLogin(teks);
      }
    }

    handleNotLoginNotif() async {
      var token = await UsersInfo().getToken();
      if (token != null || Condition.loginFirebase == true) {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => Notifikasi(),
          ),
        );
      } else {
        String teks = 'dapat mengakses notifikasi';
        return _showDialogNotLogin(teks);
      }
    }

    return Container(
      margin: EdgeInsets.only(
          right: getProportionateScreenWidth(10),
          left: getProportionateScreenWidth(7)),
      width: SizeConfig.screenWidth,
      height: getProportionateScreenHeight(40),
      child: Row(
        children: [
          IconButton(
            alignment: Alignment.centerLeft,
            icon: Icon(
              Icons.arrow_back,
              size: getProportionateScreenHeight(18),
            ),
            onPressed: () {
              // await Provider.of<CartsProvider>(context, listen: false)
              //     .getCarts();
              Navigator.pop(context);
            },
            color: Colors.white,
          ),
          Spacer(),
          !Condition.loginEmail
              ? Transform.scale(
                  origin: Offset(0, 0),
                  scale: getProportionateScreenHeight(1),
                  child: Container(
                    padding: EdgeInsets.fromLTRB(
                        getProportionateScreenHeight(3),
                        0,
                        getProportionateScreenHeight(3),
                        0),
                    child: IconBtnWithCounter(
                      numOfitem: 0,
                      icon: FeatherIcons.shoppingCart,
                      press: () => handleNotLoginCart(),
                    ),
                  ),
                )
              : Transform.scale(
                  origin: Offset(0, 0),
                  scale: getProportionateScreenHeight(1),
                  child: Container(
                    padding: EdgeInsets.fromLTRB(
                        getProportionateScreenHeight(3),
                        0,
                        getProportionateScreenHeight(3),
                        0),
                    child:
                        Consumer<CartsProvider>(builder: (context, state, _) {
                      return IconBtnWithCounter(
                        numOfitem: state.result == null ? 0 : state.lenght,
                        icon: FeatherIcons.shoppingCart,
                        press: () => handleNotLoginCart(),
                      );
                    }),
                  ),
                ),
          SizedBox(width: SizeConfig.blockHorizontal! * 0.5),
          Transform.scale(
            origin: Offset(-11, 0),
            scale: getProportionateScreenHeight(1),
            child: Container(
              padding: EdgeInsets.fromLTRB(getProportionateScreenHeight(3), 0,
                  getProportionateScreenHeight(4), 0),
              child: IconBtnWithCounter(
                icon: FeatherIcons.bell,
                press: () {
                  handleNotLoginNotif();
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
