import 'package:flutter/material.dart';

import '../../../size_config.dart';
import '../../../theme.dart';

class InstructorStudentsCourses extends StatelessWidget {
  const InstructorStudentsCourses(
      {Key? key, required this.course, required this.murid})
      : super(key: key);
  final String murid;
  final String course;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          ' $murid Murid',
          style: primaryTextStyle.copyWith(
              fontSize: getProportionateScreenWidth(10), letterSpacing: 0.5),
        ),
        SizedBox(width: getProportionateScreenWidth(10)),
        Text(
          ' $course Kursus',
          style: primaryTextStyle.copyWith(
              fontSize: getProportionateScreenWidth(10), letterSpacing: 0.5),
        )
      ],
    );
  }
}
