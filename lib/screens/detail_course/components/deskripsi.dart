import 'dart:convert';

import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:initial_folder/helper/validator.dart';
import 'package:initial_folder/models/detail_course_model.dart';
import 'package:initial_folder/providers/description_provider.dart';
import 'package:initial_folder/screens/detail_course/components/kursus_include_item.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:provider/provider.dart';

class Desksripsi extends StatelessWidget {
  const Desksripsi({Key? key, required this.dataDetailCourseModel})
      : super(key: key);
  final DataDetailCourseModel dataDetailCourseModel;
  @override
  Widget build(BuildContext context) {
    DescriptionProvider descriptionProvider =
        Provider.of<DescriptionProvider>(context);
    List outcomes = jsonDecode(dataDetailCourseModel.outcome ?? 'gagal');

    Widget kemampuanDiraih(String title) {
      return Container(
        margin: EdgeInsets.only(bottom: 6),
        child: Row(
          children: [
            Icon(Icons.check,
                size: getProportionateScreenHeight(13), color: thirdColor),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  title,
                  style: primaryTextStyle.copyWith(
                      color: secondaryColor,
                      fontSize: getProportionateScreenWidth(12),
                      letterSpacing: 0.5),
                ),
              ),
            ),
          ],
        ),
      );
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          decoration: BoxDecoration(
            color: Color(0xFF212121),
            borderRadius: BorderRadius.circular(8),
          ),
          margin: EdgeInsets.only(
              left: getProportionateScreenWidth(10),
              right: getProportionateScreenWidth(10)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 10),
              Container(
                margin: EdgeInsets.only(left: getProportionateScreenWidth(10)),
                child: Text(
                  'Kursus Ini Sudah Termasuk',
                  style: primaryTextStyle.copyWith(
                      fontWeight: semiBold,
                      letterSpacing: 1,
                      fontSize: getProportionateScreenWidth(14)),
                ),
              ),
              SizedBox(height: 10),
              KursusIncludeItems(
                  icon: Icons.watch_later_outlined,
                  text: filterDuration(dataDetailCourseModel.totalDuration!)),
              KursusIncludeItems(
                  icon: Icons.smart_screen,
                  text: '${dataDetailCourseModel.totalLesson} pelajaran'),
              KursusIncludeItems(
                  icon: Icons.calendar_today_outlined,
                  text: 'Akses full seumur hidup'),
              KursusIncludeItems(
                  icon: Icons.smartphone_outlined,
                  text: 'Akses di ponsel dan TV '),
              SizedBox(height: 10),
            ],
          ),
        ),
        SizedBox(height: 18),
        Container(
          decoration: BoxDecoration(
            color: Color(0xFF212121),
            borderRadius: BorderRadius.circular(8),
          ),
          margin: EdgeInsets.only(
              left: getProportionateScreenWidth(10),
              right: getProportionateScreenWidth(10)),
          child: Column(
            children: [
              ExpandableNotifier(
                // <-- Provides ExpandableController to its children
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 12,
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          left: getProportionateScreenWidth(10)),
                      child: Text('Kemampuan Yang Akan Diraih',
                          style: primaryTextStyle.copyWith(
                              fontWeight: semiBold,
                              letterSpacing: 1,
                              fontSize: getProportionateScreenWidth(14))),
                    ),
                    SizedBox(
                      height: 12,
                    ),
                    Expandable(
                      collapsed: ExpandableButton(
                        child: Container(
                          margin: EdgeInsets.only(
                              left: getProportionateScreenWidth(10)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              outcomes.isEmpty
                                  ? Row(
                                      children: [
                                        Expanded(
                                          child: Align(
                                            alignment: Alignment.topLeft,
                                            child: Text(
                                              '',
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                  : Column(children: [
                                      ...outcomes
                                          .map((e) => kemampuanDiraih(e))
                                          .take(3)
                                    ]),
                              SizedBox(
                                height: 8,
                              ),
                              Container(
                                child: Text(
                                  "Tampilkan Lebih Banyak",
                                  style: primaryTextStyle.copyWith(
                                    fontWeight: semiBold,
                                    color: primaryColor,
                                    fontSize: getProportionateScreenWidth(12),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      expanded: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            ExpandableButton(
                              // <-- Collapses when tapped on

                              child: Container(
                                margin: EdgeInsets.only(
                                    left: getProportionateScreenWidth(10)),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    outcomes.isEmpty
                                        ? Row(
                                            children: [
                                              Expanded(
                                                child: Align(
                                                  alignment: Alignment.topLeft,
                                                  child: Text(
                                                    '',
                                                  ),
                                                ),
                                              ),
                                            ],
                                          )
                                        : Column(
                                            children: outcomes
                                                .map((e) => kemampuanDiraih(e))
                                                .toList(),
                                          ),
                                    SizedBox(
                                      height: 16,
                                    ),
                                    Container(
                                      child: Text(
                                        "Tampilkan Lebih Sedikit",
                                        style: primaryTextStyle.copyWith(
                                          fontWeight: semiBold,
                                          color: primaryColor,
                                          fontSize:
                                              getProportionateScreenWidth(12),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            )
                          ]),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: getProportionateScreenWidth(12),
              ),
            ],
          ),
        ),
        SizedBox(
          height: getProportionateScreenWidth(18),
        ),
        Container(
          decoration: BoxDecoration(
            color: Color(0xFF212121),
            borderRadius: BorderRadius.circular(8),
          ),
          margin: EdgeInsets.only(
              left: getProportionateScreenWidth(10),
              right: getProportionateScreenWidth(10)),
          child: Column(
            children: [
              SizedBox(
                height: getProportionateScreenWidth(12),
              ),
              Align(
                alignment: Alignment.topLeft,
                child: Container(
                  margin:
                      EdgeInsets.only(left: getProportionateScreenWidth(10)),
                  child: Text(
                    'Deskripsi',
                    style: primaryTextStyle.copyWith(
                      fontWeight: semiBold,
                      letterSpacing: 1,
                      fontSize: getProportionateScreenWidth(14),
                    ),
                  ),
                ),
              ),
              AnimatedSize(
                  curve: Curves.fastOutSlowIn,
                  duration: const Duration(milliseconds: 300),
                  child: Container(
                    child: descriptionProvider.isExpanded
                        ? Html(
                            data: dataDetailCourseModel.shortDescription,
                            style: {
                              "body": Style(
                                  fontSize:
                                      FontSize(getProportionateScreenWidth(12)),
                                  fontWeight: reguler,
                                  fontFamily: 'Noto Sans',
                                  color: secondaryColor),
                            },
                          )
                        : Wrap(
                            children: [
                              Html(
                                data: dataDetailCourseModel.description,
                                style: {
                                  "body": Style(
                                      fontSize: FontSize(
                                          getProportionateScreenWidth(12)),
                                      fontWeight: reguler,
                                      fontFamily: 'Noto Sans',
                                      color: secondaryColor),
                                },
                              ),
                            ],
                          ),
                  )),
              Align(
                alignment: Alignment.topLeft,
                child: TextButton(
                  onPressed: () => descriptionProvider.expanded(),
                  child: descriptionProvider.isExpanded
                      ? Text(
                          'Tampilkan Lebih Banyak',
                          style: primaryTextStyle.copyWith(
                            fontWeight: semiBold,
                            color: primaryColor,
                            fontSize: getProportionateScreenWidth(12),
                          ),
                          textAlign: TextAlign.left,
                        )
                      : Text(
                          'Tampilkan Lebih Sedikit',
                          style: primaryTextStyle.copyWith(
                            fontWeight: semiBold,
                            color: primaryColor,
                            fontSize: getProportionateScreenWidth(12),
                          ),
                        ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 15,
        )
      ],
    );
  }
}
