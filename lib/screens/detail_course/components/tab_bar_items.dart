import 'package:flutter/material.dart';
import 'package:initial_folder/providers/tab_provider.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:provider/provider.dart';

class TabBarItems extends StatelessWidget {
  final int index;
  final String title;

  const TabBarItems({
    Key? key,
    required this.index,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TabProvider tab = Provider.of<TabProvider>(context, listen: false);

    return GestureDetector(
      onTap: () {
        tab.currentIndex = index;
      },
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(title,
                style: primaryTextStyle.copyWith(
                    letterSpacing: 0.5,
                    color:
                        tab.currentIndex == index ? primaryColor : tenthColor,
                    fontSize: getProportionateScreenHeight(12))),
            Container(
              width: getProportionateScreenHeight(45),
              height: getProportionateScreenHeight(2),
              decoration: BoxDecoration(
                color: tab.currentIndex == index
                    ? primaryColor
                    : Colors.transparent,
                borderRadius:
                    BorderRadius.circular(getProportionateScreenHeight(19)),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
