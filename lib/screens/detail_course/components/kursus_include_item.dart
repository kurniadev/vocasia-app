import 'package:flutter/material.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';

class KursusIncludeItems extends StatelessWidget {
  const KursusIncludeItems({Key? key, required this.icon, required this.text})
      : super(key: key);

  final IconData icon;
  final String text;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 10),
      child: Column(
        children: [
          Row(
            children: [
              Icon(
                icon,
                color: thirdColor,
                size: getProportionateScreenWidth(14),
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                text,
                style: primaryTextStyle.copyWith(
                    fontSize: SizeConfig.blockHorizontal! * 3,
                    color: secondaryColor,
                    letterSpacing: 0.5),
              ),
            ],
          ),
          SizedBox(height: getProportionateScreenHeight(6)),
        ],
      ),
    );
  }
}
