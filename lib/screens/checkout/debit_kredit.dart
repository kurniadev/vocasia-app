import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:initial_folder/screens/checkout/components/cardmonth.dart';
import 'package:initial_folder/screens/checkout/components/cardnumber.dart';
import 'package:initial_folder/screens/checkout/detail_bayar.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/providers/checkbox_provider.dart';
import 'package:initial_folder/widgets/login_regist/default_button.dart';
import 'package:initial_folder/helper/validator.dart';
import 'package:provider/provider.dart';

class DebitCreditPage extends StatefulWidget {
  static String? _cardNumber;
  static int? month;
  static int? year;
  static List<int> getExpiryDate(String? value) {
    var split = value!.split(new RegExp(r'(\/)'));
    return [int.parse(split[0]), int.parse(split[1])];
  }

  static String getCleanedNumber(String? value) {
    var split = value!.split(new RegExp(r'(  )'));
    String nomorKartu = split.join();
    return nomorKartu;
  }

  @override
  State<DebitCreditPage> createState() => _DebitCreditPageState();
}

class _DebitCreditPageState extends State<DebitCreditPage> {
  final bool isChecked = false;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController nameController = TextEditingController(text: '');

  final TextEditingController kartuController = TextEditingController(text: '');

  final TextEditingController masaController = TextEditingController(text: '');

  final TextEditingController kodeController = TextEditingController(text: '');

  @override
  void dispose() {
    nameController.dispose();
    kartuController.dispose();
    masaController.dispose();
    kodeController.dispose();
    super.dispose();
  }

  bool _isObscure = true;
  @override
  Widget build(BuildContext context) {
    CheckboxProvider checkboxProvider = Provider.of<CheckboxProvider>(context);

    void _validateInputs() async {
      if (this._formKey.currentState!.validate()) {
        this._formKey.currentState!.save();
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => DetailBayar()));
      }
    }

    Widget kartuInput() {
      return Container(
        margin:
            EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(2)),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Nomor Kartu",
              style: secondaryTextStyle.copyWith(
                letterSpacing: 1,
                fontWeight: semiBold,
                fontSize: getProportionateScreenWidth(12),
                color: tenthColor,
              ),
            ),
            // SizedBox(
            //   height: getProportionateScreenHeight(4),
            // ),
            TextFormField(
              inputFormatters: [
                FilteringTextInputFormatter.digitsOnly, // Ini jangan dihapus
                LengthLimitingTextInputFormatter(16),
                CardNumberInputFormatter(),
              ],
              keyboardType: TextInputType.number,
              autofocus: false,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: validateNomorKartu,
              obscureText: false,
              controller: kartuController,
              onSaved: (value) {
                DebitCreditPage._cardNumber =
                    DebitCreditPage.getCleanedNumber(value);
                print(DebitCreditPage._cardNumber);
              },
              style: primaryTextStyle.copyWith(
                  fontSize: getProportionateScreenWidth(11),
                  letterSpacing: 0.5,
                  color: tenthColor),
              cursorColor: tenthColor,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.only(
                  left: getProportionateScreenWidth(0),
                ),
                hintText: 'Contoh : 1234 5678 8765 4321',
                hintStyle: primaryTextStyle.copyWith(
                    fontSize: getProportionateScreenWidth(11),
                    color: fourthColor,
                    letterSpacing: 0.5),
                border: UnderlineInputBorder(
                  borderSide: BorderSide(color: fourthColor),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: fourthColor),
                ),
              ),
            ),
            // SizedBox(
            //   height: getProportionateScreenHeight(5),
            // ),
          ],
        ),
      );
    }

    Widget masaInput() {
      return Container(
        margin:
            EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(2)),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: getProportionateScreenHeight(4),
            ),
            TextFormField(
              inputFormatters: [
                LengthLimitingTextInputFormatter(5),
                FilteringTextInputFormatter.digitsOnly, // Ini jangan dihapus
                CardMonthInputFormatter(),
              ],
              keyboardType: TextInputType.number,
              autofocus: false,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: validateDate,
              obscureText: false,
              controller: masaController,
              onSaved: (value) {
                List<int> expiryDate = DebitCreditPage.getExpiryDate(value);
                DebitCreditPage.month = expiryDate[0];
                DebitCreditPage.year = convertYearTo4Digits(expiryDate[1]);
                print(DebitCreditPage.month);
                print(DebitCreditPage.year);
              },
              style: primaryTextStyle.copyWith(
                  fontSize: getProportionateScreenWidth(11),
                  letterSpacing: 0.5,
                  color: tenthColor),
              cursorColor: tenthColor,
              decoration: InputDecoration(
                errorMaxLines: 3,
                contentPadding: EdgeInsets.only(
                  left: getProportionateScreenWidth(0),
                ),
                hintText: 'mm / yy',
                hintStyle: primaryTextStyle.copyWith(
                    fontSize: getProportionateScreenWidth(11),
                    color: fourthColor,
                    letterSpacing: 0.5),
                border: UnderlineInputBorder(
                  borderSide: BorderSide(color: fourthColor),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: fourthColor),
                ),
              ),
            ),
            SizedBox(
              height: getProportionateScreenHeight(5),
            ),
          ],
        ),
      );
    }

    Widget ingat() {
      Color getColor(Set<MaterialState> states) {
        const Set<MaterialState> interactiveStates = <MaterialState>{
          MaterialState.pressed,
          MaterialState.hovered,
          MaterialState.focused,
        };
        if (states.any(interactiveStates.contains)) {
          return Colors.blue;
        }
        return Colors.white;
      }

      return Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Checkbox(
            splashRadius: 20,
            checkColor: Colors.white,
            activeColor: Color(0xff25D366),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            side: BorderSide(color: tenthColor),
            value: checkboxProvider.isChecked,
            onChanged: (bool? value) {
              //setState(() {
              checkboxProvider.isChecked = value ?? true;
              //});
            },
          ),
          SizedBox(width: getProportionateScreenWidth(2)),
          Text(
            'Simpan nomor dan masa berlaku kartu',
            style: primaryTextStyle.copyWith(
              letterSpacing: 0.5,
              fontWeight: reguler,
              fontSize: getProportionateScreenWidth(10),
              color: tenthColor,
            ),
          ),
        ],
      );
    }

    Widget kodeInput() {
      return Container(
        margin:
            EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(2)),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "CVV",
              style: secondaryTextStyle.copyWith(
                letterSpacing: 1,
                fontWeight: semiBold,
                fontSize: getProportionateScreenWidth(12),
                color: tenthColor,
              ),
            ),
            // SizedBox(
            //   height: getProportionateScreenHeight(4),
            // ),
            SizedBox(
              height: getProportionateScreenHeight(4),
            ),
            TextFormField(
              inputFormatters: [
                FilteringTextInputFormatter.digitsOnly, // Ini jangan dihapus
                LengthLimitingTextInputFormatter(3),
              ],
              keyboardType: TextInputType.number,
              autofocus: false,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              validator: validateKodeKeamanan,
              controller: kodeController,
              onSaved: (value) {
                DebitCreditPage._cardNumber =
                    DebitCreditPage.getCleanedNumber(value);
                print(DebitCreditPage._cardNumber);
              },
              style: primaryTextStyle.copyWith(
                  fontSize: getProportionateScreenWidth(11),
                  letterSpacing: 0.5,
                  color: tenthColor),
              cursorColor: tenthColor,
              obscureText: _isObscure,
              decoration: InputDecoration(
                errorMaxLines: 3,
                suffixIcon: GestureDetector(
                    onTap: () => setState(() {
                          _isObscure = !_isObscure;
                        }),
                    child: _isObscure
                        ? Icon(
                            Icons.visibility_off,
                            color: secondaryColor,
                            size: 18,
                          )
                        : Icon(
                            Icons.visibility,
                            color: secondaryColor,
                            size: 18,
                          )),
                contentPadding: EdgeInsets.only(
                  top: 15,
                ),
                hintText: 'Contoh : 123',
                hintStyle: primaryTextStyle.copyWith(
                    fontSize: getProportionateScreenWidth(11),
                    color: fourthColor,
                    letterSpacing: 0.5),
                border: UnderlineInputBorder(
                  borderSide: BorderSide(color: fourthColor),
                ),
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: fourthColor),
                ),
              ),
            ),
            // SizedBox(
            //   height: getProportionateScreenHeight(5),
            // ),
          ],
        ),
      );
    }
    // Widget form() {
    //   Widget nameInput() {
    //     return CustomTextField(
    //       color: fourthColor,
    //       borderColor: fourthColor,
    //       pad: getProportionateScreenWidth(2),
    //       controler: nameController,
    //       hinttext: 'Masukkan nama lengkap pada kartu',
    //       title: 'Nama Lengkap',
    //       validate: validateName,
    //     );
    //   }

    //   Widget kartuInput() {
    //     return Container(
    //       margin:
    //           EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(2)),
    //       width: double.infinity,
    //       child: Column(
    //         crossAxisAlignment: CrossAxisAlignment.start,
    //         children: [
    //           Text(
    //             "Nomor Kartu",
    //             style: secondaryTextStyle.copyWith(
    //               letterSpacing: 1,
    //               fontWeight: semiBold,
    //               fontSize: getProportionateScreenWidth(12),
    //               color: tenthColor,
    //             ),
    //           ),
    //           SizedBox(
    //             height: getProportionateScreenHeight(4),
    //           ),
    //           TextFormField(
    //             inputFormatters: [
    //               FilteringTextInputFormatter.digitsOnly, // Ini jangan dihapus
    //               LengthLimitingTextInputFormatter(16),
    //               CardNumberInputFormatter(),
    //             ],
    //             keyboardType: TextInputType.number,
    //             autofocus: false,
    //             autovalidateMode: AutovalidateMode.onUserInteraction,
    //             validator: validateNomorKartu,
    //             obscureText: false,
    //             controller: kartuController,
    //             onSaved: (value) {
    //               _cardNumber = getCleanedNumber(value);
    //               print(_cardNumber);
    //             },
    //             style: primaryTextStyle.copyWith(
    //                 fontSize: getProportionateScreenWidth(14),
    //                 letterSpacing: 0.5,
    //                 color: secondaryColor),
    //             cursorColor: secondaryColor,
    //             decoration: InputDecoration(
    //               contentPadding: EdgeInsets.only(
    //                 left: getProportionateScreenWidth(15),
    //               ),
    //               hintText: 'Masukkan Nomor Kartu',
    //               hintStyle: primaryTextStyle.copyWith(
    //                   fontSize: getProportionateScreenWidth(12),
    //                   color: fourthColor,
    //                   letterSpacing: 0.5),
    //               border: OutlineInputBorder(
    //                 borderRadius: BorderRadius.circular(
    //                   10,
    //                 ),
    //               ),
    //               focusedBorder: OutlineInputBorder(
    //                 borderRadius: BorderRadius.circular(
    //                   10,
    //                 ),
    //                 borderSide: BorderSide(
    //                   color: fourthColor,
    //                 ),
    //               ),
    //             ),
    //           ),
    //           SizedBox(
    //             height: getProportionateScreenHeight(5),
    //           ),
    //         ],
    //       ),
    //     );
    //   }

    //   Widget masaInput() {
    //     return Container(
    //       margin:
    //           EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(2)),
    //       width: double.infinity,
    //       child: Column(
    //         crossAxisAlignment: CrossAxisAlignment.start,
    //         children: [
    //           Text(
    //             "Masa Berlaku",
    //             style: secondaryTextStyle.copyWith(
    //                 fontWeight: semiBold,
    //                 fontSize: getProportionateScreenWidth(12),
    //                 color: secondaryColor,
    //                 letterSpacing: 0.5),
    //           ),
    //           SizedBox(
    //             height: getProportionateScreenHeight(4),
    //           ),
    //           TextFormField(
    //             inputFormatters: [
    //               LengthLimitingTextInputFormatter(5),
    //               FilteringTextInputFormatter.digitsOnly, // Ini jangan dihapus
    //               CardMonthInputFormatter(),
    //             ],
    //             keyboardType: TextInputType.number,
    //             autofocus: false,
    //             autovalidateMode: AutovalidateMode.onUserInteraction,
    //             validator: validateDate,
    //             obscureText: false,
    //             controller: masaController,
    //             onSaved: (value) {
    //               List<int> expiryDate = getExpiryDate(value);
    //               month = expiryDate[0];
    //               year = convertYearTo4Digits(expiryDate[1]);
    //               print(month);
    //               print(year);
    //             },
    //             style: primaryTextStyle.copyWith(
    //                 fontSize: getProportionateScreenWidth(14),
    //                 letterSpacing: 0.5,
    //                 color: secondaryColor),
    //             cursorColor: secondaryColor,
    //             decoration: InputDecoration(
    //               contentPadding: EdgeInsets.only(
    //                 left: getProportionateScreenWidth(15),
    //               ),
    //               hintText: 'MM / YY',
    //               hintStyle: primaryTextStyle.copyWith(
    //                   fontSize: getProportionateScreenWidth(12),
    //                   color: fourthColor,
    //                   letterSpacing: 0.5),
    //               border: OutlineInputBorder(
    //                 borderRadius: BorderRadius.circular(
    //                   10,
    //                 ),
    //               ),
    //               focusedBorder: OutlineInputBorder(
    //                 borderRadius: BorderRadius.circular(
    //                   10,
    //                 ),
    //                 borderSide: BorderSide(
    //                   color: fourthColor,
    //                 ),
    //               ),
    //             ),
    //           ),
    //           SizedBox(
    //             height: getProportionateScreenHeight(5),
    //           ),
    //         ],
    //       ),
    //     );
    //   }

    //   Widget kodeInput() {
    //     return CustomTextField(
    //       length: 3,
    //       borderColor: fourthColor,
    //       color: fourthColor,
    //       keyboardType: TextInputType.number,
    //       pad: getProportionateScreenWidth(2),
    //       controler: kodeController,
    //       hinttext: 'Masukkan 3 digit kode keamanan',
    //       title: 'Kode Keamanan',
    //       validate: validateKodeKeamanan,
    //     );
    //   }

    //   Widget ingat() {
    //     Color getColor(Set<MaterialState> states) {
    //       const Set<MaterialState> interactiveStates = <MaterialState>{
    //         MaterialState.pressed,
    //         MaterialState.hovered,
    //         MaterialState.focused,
    //       };
    //       if (states.any(interactiveStates.contains)) {
    //         return Colors.blue;
    //       }
    //       return Colors.white;
    //     }

    //     return Row(
    //       mainAxisAlignment: MainAxisAlignment.start,
    //       children: [
    //         Checkbox(
    //           splashRadius: 20,
    //           checkColor: Colors.white,
    //           activeColor: Color(0xff25D366),
    //           shape: RoundedRectangleBorder(
    //             borderRadius: BorderRadius.circular(5),
    //           ),
    //           side: BorderSide(color: tenthColor),
    //           value: checkboxProvider.isChecked,
    //           onChanged: (bool? value) {
    //             //setState(() {
    //             checkboxProvider.isChecked = value ?? true;
    //             //});
    //           },
    //         ),
    //         SizedBox(width: getProportionateScreenWidth(2)),
    //         Text(
    //           'Simpan nomor dan masa berlaku kartu',
    //           style: primaryTextStyle.copyWith(
    //             letterSpacing: 0.5,
    //             fontWeight: reguler,
    //             fontSize: getProportionateScreenWidth(12),
    //             color: tenthColor,
    //           ),
    //         ),
    //       ],
    //     );
    //   }

    //   Widget bottomNav() {
    //     return Container(
    //       margin: EdgeInsets.symmetric(
    //           horizontal: getProportionateScreenWidth(2),
    //           vertical: getProportionateScreenHeight(10)),
    //       child: DefaultButton(
    //         text: 'Lanjutkan',
    //         press: _validateInputs,
    //       ),
    //     );
    //   }

    //   return Form(
    //     key: _formKey,
    //     child: Column(
    //       children: [
    //         nameInput(),
    //         kartuInput(),
    //         masaInput(),
    //         kodeInput(),
    //         ingat(),
    //         SizedBox(height: getProportionateScreenHeight(15)),
    //         bottomNav(),
    //       ],
    //     ),
    //   );
    // }
    Widget bottomNav() {
      return Container(
        margin: EdgeInsets.symmetric(
            horizontal: getProportionateScreenWidth(2),
            vertical: getProportionateScreenHeight(10)),
        child: DefaultButton(
          text: 'Lanjutkan',
          press: _validateInputs,
        ),
      );
    }

    return Scaffold(
      //backgroundColor: Colors.black,
      appBar: AppBar(
        // backgroundColor: Colors.black,
        title: Text(
          'Bayar dengan kartu Kredit/Debit',
          style: secondaryTextStyle.copyWith(
              letterSpacing: 1,
              fontWeight: semiBold,
              fontSize: getProportionateScreenWidth(12)),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(16)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 5),
              Container(
                decoration: BoxDecoration(
                  color: Color(0xFF212121),
                  borderRadius: BorderRadius.circular(8),
                ),
                margin: EdgeInsets.only(
                    left: getProportionateScreenWidth(2),
                    right: getProportionateScreenWidth(2)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(
                          vertical: 10,
                          horizontal: getProportionateScreenWidth(15)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Order ID',
                            style: primaryTextStyle.copyWith(
                              letterSpacing: 1,
                              fontWeight: reguler,
                              fontSize: getProportionateScreenWidth(12),
                              color: secondaryColor,
                            ),
                          ),
                          Text(
                            '1234567890',
                            style: primaryTextStyle.copyWith(
                              letterSpacing: 0.5,
                              fontWeight: reguler,
                              fontSize: getProportionateScreenWidth(12),
                              color: tenthColor,
                            ),
                          ),
                          Divider(color: Color(0xff2D2D2D), thickness: 0.5),
                          Text(
                            'Total Pembayaran',
                            style: primaryTextStyle.copyWith(
                              letterSpacing: 1,
                              fontWeight: reguler,
                              fontSize: getProportionateScreenWidth(12),
                              color: secondaryColor,
                            ),
                          ),
                          Text(
                            'Rp. 150,000',
                            style: secondaryTextStyle.copyWith(
                              letterSpacing: 0.23,
                              fontWeight: reguler,
                              fontSize: getProportionateScreenWidth(18),
                              color: tenthColor,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 18),
              Container(
                decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(color: fourthColor),
                ),
                margin: EdgeInsets.only(
                    left: getProportionateScreenWidth(2),
                    right: getProportionateScreenWidth(2)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(
                          vertical: 7,
                          horizontal: getProportionateScreenWidth(12)),
                      child: Text(
                        'Masukkan Informasi Kartu Kredit/Debit',
                        style: thirdTextStyle.copyWith(
                          letterSpacing: 0.32,
                          fontWeight: semiBold,
                          fontSize: getProportionateScreenWidth(12),
                          color: tenthColor,
                        ),
                      ),
                    ),
                    Divider(
                      thickness: 1,
                      color: fourthColor,
                      height: 1,
                    ),
                    Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 16,
                            horizontal: getProportionateScreenWidth(12)),
                        child: Form(
                          key: _formKey,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              kartuInput(),
                              SizedBox(height: 12),
                              Container(
                                width: getProportionateScreenWidth(111),
                                height: getProportionateScreenWidth(17),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(2),
                                  image: DecorationImage(
                                    fit: BoxFit.fill,
                                    image:
                                        AssetImage("assets/images/kredit.png"),
                                  ),
                                ),
                              ),
                              SizedBox(height: 18),

                              Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  //mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          "Masa Berlaku",
                                          style: secondaryTextStyle.copyWith(
                                            letterSpacing: 1,
                                            fontWeight: semiBold,
                                            fontSize:
                                                getProportionateScreenWidth(12),
                                            color: tenthColor,
                                          ),
                                        ),
                                        Container(
                                            width:
                                                getProportionateScreenWidth(75),
                                            child: masaInput()),
                                      ],
                                    ),
                                    Spacer(),
                                    Container(
                                        width: getProportionateScreenWidth(150),
                                        child: kodeInput()),
                                    // masaInput(),
                                  ]),
                              SizedBox(height: 20),
                              ingat(),
                              // Row(mainAxisAlignment: MainAxisAlignment.start,mainAxisSize: MainAxisSize.min,
                              // children: [

                              // ],
                              // ),
                              // Divider(
                              //     height: 1, thickness: 0.5, color: fourthColor),
                            ],
                          ),
                        )),
                    // SizedBox(height: 12),
                  ],
                ),
              ),

              SizedBox(
                height: getProportionateScreenHeight(20),
              ),
              bottomNav(),
              SizedBox(
                height: getProportionateScreenHeight(20),
              ),
              // form(),
            ],
          ),
        ),
      ),
    );
  }
}
