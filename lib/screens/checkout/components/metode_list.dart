import 'package:flutter/material.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';

class MetodeList extends StatelessWidget {
  const MetodeList({
    Key? key,
    required this.onPress,
    required this.image,
    required this.title1,
    required this.title2,
    required this.width,
    required this.height,
    this.sizedbox = 12,
    this.scale = 1,
  }) : super(key: key);
  final GestureTapCallback onPress;
  final String image;
  final String title1;
  final String title2;
  final double width;
  final double height;
  final double sizedbox;
  final double scale;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPress,
      child: Container(
        decoration: BoxDecoration(
          color: Color(0xFF212121),
          borderRadius: BorderRadius.circular(8),
        ),
        margin: EdgeInsets.only(
            //bottom: getProportionateScreenWidth(26),
            left: getProportionateScreenWidth(2),
            right: getProportionateScreenWidth(2)),
        child: Container(
          padding: EdgeInsets.symmetric(
              vertical: getProportionateScreenWidth(16),
              horizontal: getProportionateScreenWidth(10)),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              // Expanded(
              //   child: Text(
              //     image,
              //     style: primaryTextStyle.copyWith(
              //         fontSize: getProportionateScreenWidth(12),
              //         letterSpacing: 0.5),
              //   ),
              // ),
              Flexible(
                fit: FlexFit.loose,
                flex: 2,
                child: Transform.scale(
                  scale: getProportionateScreenWidth(scale),
                  child: Container(
                    width: getProportionateScreenWidth(width),
                    height: getProportionateScreenWidth(height),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(2),
                      image: DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage(image),
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(width: getProportionateScreenWidth(sizedbox)),
              Flexible(
                fit: FlexFit.tight,
                flex: 8,
                child: SizedBox(
                  height: height,
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          title1,
                          style: primaryTextStyle.copyWith(
                              fontSize: getProportionateScreenWidth(10),
                              fontWeight: reguler,
                              letterSpacing: 0.5),
                        ),
                        Container(
                          child: Text(
                            title2,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 1,
                            style: primaryTextStyle.copyWith(
                                fontSize: getProportionateScreenWidth(7),
                                fontWeight: light,
                                letterSpacing: 0.5),
                          ),
                        ),
                      ]),
                ),
              ),
              Spacer(),
              Flexible(
                fit: FlexFit.tight,
                flex: 1,
                child: Icon(
                  Icons.keyboard_arrow_right,
                  size: getProportionateScreenWidth(24),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
