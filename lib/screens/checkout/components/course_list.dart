import 'package:flutter/material.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';

class CourseList extends StatelessWidget {
  const CourseList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Flexible(
                flex: 10,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: getProportionateScreenWidth(70),
                      height: getProportionateScreenWidth(39),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(
                              'assets/images/course_thumbnail_default_13 1.png'),
                        ),
                      ),
                    ),
                  ],
                )),
            SizedBox(width: getProportionateScreenWidth(10)),
            Flexible(
                flex: 15,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '428 Menit Menjadi Pengusaha Sukses',
                      style: primaryTextStyle.copyWith(
                        letterSpacing: 0.5,
                        fontWeight: reguler,
                        fontSize: getProportionateScreenWidth(12),
                      ),
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ],
                )),
            Spacer(),
            Flexible(
              flex: 10,
              child: Align(
                alignment: Alignment.centerRight,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      'Rp 200.000',
                      style: primaryTextStyle.copyWith(
                        letterSpacing: 0.5,
                        fontWeight: reguler,
                        fontSize: getProportionateScreenWidth(12),
                      ),
                    ),
                    SizedBox(
                      height: getProportionateScreenHeight(2),
                    ),
                    Text(
                      'Rp. 1,000,000',
                      style: primaryTextStyle.copyWith(
                        decoration: TextDecoration.lineThrough,
                        color: secondaryColor,
                        letterSpacing: 0.5,
                        fontWeight: reguler,
                        fontSize: getProportionateScreenWidth(10),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
        SizedBox(
          height: getProportionateScreenHeight(10),
        ),
        Divider(
          color: fourthColor,
        ),
        SizedBox(
          height: getProportionateScreenHeight(5),
        ),
      ],
    );
  }
}
