import 'package:flutter/material.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:styled_text/styled_text.dart';

class ATM extends StatelessWidget {
  final TextStyle baris = primaryTextStyle.copyWith(
    fontWeight: reguler,
    color: secondaryColor,
    fontSize: getProportionateScreenWidth(14),
    letterSpacing: 0.5,
  );
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        StyledText(
          text: '1. Open the <bold>BNI Mobile Banking</bold> app and login',
          style: baris,
          tags: {
            'bold': StyledTextTag(style: TextStyle(fontWeight: bold)),
          },
        ),
        Divider(color: Color(0xff2D2D2D), thickness: 0.5, height: 35),
        StyledText(
          text: '2. Choose menu <bold>Transfer</bold>',
          style: baris,
          tags: {
            'bold': StyledTextTag(style: TextStyle(fontWeight: bold)),
          },
        ),
        Divider(color: Color(0xff2D2D2D), thickness: 0.5, height: 35),
        StyledText(
          text: '3. Choose menu <bold>Virtual Account Billing</bold>',
          style: baris,
          tags: {
            'bold': StyledTextTag(style: TextStyle(fontWeight: bold)),
          },
        ),
        Divider(color: Color(0xff2D2D2D), thickness: 0.5, height: 35),
        StyledText(
          text: '4. Choose the bank account you want to use',
          style: baris,
          tags: {
            'bold': StyledTextTag(style: TextStyle(fontWeight: bold)),
          },
        ),
        Divider(color: Color(0xff2D2D2D), thickness: 0.5, height: 35),
        StyledText(
          text: '5. Enter the 16 digits <bold>virtual account number</bold>',
          style: baris,
          tags: {
            'bold': StyledTextTag(style: TextStyle(fontWeight: bold)),
          },
        ),
        Divider(color: Color(0xff2D2D2D), thickness: 0.5, height: 35),
        StyledText(
          text:
              '6. The billing information will appear on the payment validation page',
          style: baris,
          tags: {
            'bold': StyledTextTag(style: TextStyle(fontWeight: bold)),
          },
        ),
        Divider(color: Color(0xff2D2D2D), thickness: 0.5, height: 35),
        StyledText(
          text:
              '7. If the information is correct, enter your password to proceed the payment',
          style: baris,
          tags: {
            'bold': StyledTextTag(style: TextStyle(fontWeight: bold)),
          },
        ),
        Divider(color: Color(0xff2D2D2D), thickness: 0.5, height: 35),
        StyledText(
          text: '8. Your transaction will be processed',
          style: baris,
          tags: {
            'bold': StyledTextTag(style: TextStyle(fontWeight: bold)),
          },
        ),
        SizedBox(height: 4)
      ],
    );
  }
}
