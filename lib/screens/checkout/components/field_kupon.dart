import 'package:flutter/material.dart';
import '../../../../theme.dart';
import '../../../../size_config.dart';

class FieldKupon extends StatelessWidget {
  const FieldKupon({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    OutlineInputBorder outlineInputBorder = OutlineInputBorder(
      borderRadius: BorderRadius.circular(12),
      borderSide: BorderSide(color: secondaryColor),
      gapPadding: 10,
    );
    // width: SizeConfig.screenWidth * 0.9,
    //   height: 33,
    //   decoration: BoxDecoration(
    //     color: Colors.white,
    //     borderRadius: BorderRadius.circular(15),
    //     border: Border.all(
    //       color: kSecondaryColor.withOpacity(0.5),
    //       width: 2,
    return Column(
      children: [
        Container(
          height: 36,
          //padding: EdgeInsets.only(top: getProportionateScreenWidth(20)),
          child: TextField(
            cursorColor: secondaryColor,
            enabled: true,
            //obscureText: true,
            textAlignVertical: TextAlignVertical.center,
            style: TextStyle(fontSize: 12, color: Colors.white),
            onChanged: (value) => print(value),
            decoration: InputDecoration(
              //isDense: true,
              contentPadding: EdgeInsets.only(top: 10, left: 15),
              filled: true,
              fillColor: Colors.transparent,
              enabledBorder: outlineInputBorder,
              focusedBorder: outlineInputBorder,
              border: outlineInputBorder,
              hintText: "Masukkan kode kupon",
              hintStyle: primaryTextStyle.copyWith(
                  fontSize: 12,
                  color: fourthColor,
                  fontWeight: reguler,
                  letterSpacing: 0.5),
            ),
          ),
        ),
        SizedBox(height: getProportionateScreenHeight(16)),
      ],
    );
  }
}
