import 'package:flutter/material.dart';
import 'package:initial_folder/providers/tab_provider.dart';
import 'package:initial_folder/screens/checkout/components/atm.dart';
import 'package:initial_folder/screens/detail_course/components/tab_bar_items.dart';
import 'package:initial_folder/size_config.dart';
import 'package:provider/provider.dart';

class TabBarBatasBayar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    TabProvider tab = Provider.of<TabProvider>(context);
    Widget buildContent(int currentIndex) {
      switch (currentIndex) {
        case 0:
          return ATM();
        case 1:
          return ATM();
        case 2:
          return ATM();
        default:
          return ATM();
      }
    }

    return Column(
      children: [
        Container(
          // color: Colors.blue,
          width: SizeConfig.screenWidth,
          height: getProportionateScreenHeight(40),
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                TabBarItems(
                  index: 0,
                  title: 'ATM',
                ),
                TabBarItems(
                  index: 1,
                  title: 'Internet Banking',
                ),
                TabBarItems(
                  index: 2,
                  title: 'Mobile Banking',
                ),
              ],
            ),
          ),
        ),
        SizedBox(
          height: getProportionateScreenHeight(14),
        ),
        Container(
          child: buildContent(tab.currentIndex),
        )
      ],
    );
  }
}
