import 'package:flutter/material.dart';
import 'package:initial_folder/screens/checkout/components/tab_bar_batas_bayar.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/widgets/login_regist/default_button.dart';

class BatasBayar extends StatelessWidget {
  const BatasBayar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: Colors.black,
      appBar: AppBar(
        //backgroundColor: Colors.black,
        title: Text(
          'Selesaikan Pembayaran',
          style: secondaryTextStyle.copyWith(
              letterSpacing: 1,
              fontWeight: semiBold,
              fontSize: getProportionateScreenWidth(14)),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(16)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: getProportionateScreenHeight(10)),
              Center(
                child: Column(
                  children: [
                    Text(
                      'Batas akhir pembayaran sampai',
                      style: primaryTextStyle.copyWith(
                        color: secondaryColor,
                        letterSpacing: 0.5,
                        fontWeight: reguler,
                        fontSize: getProportionateScreenWidth(10),
                      ),
                    ),
                    SizedBox(height: getProportionateScreenHeight(5)),
                    Text(
                      'Rabu, 13 Oktober 2021 (Pukul 19:32)',
                      style: secondaryTextStyle.copyWith(
                        letterSpacing: 1,
                        fontWeight: semiBold,
                        color: tenthColor,
                        fontSize: SizeConfig.blockHorizontal! * 3,
                      ),
                    ),
                    SizedBox(height: getProportionateScreenHeight(5)),
                    Text(
                      'Segera selesaikan pembayaran atau pesananmu',
                      style: primaryTextStyle.copyWith(
                        color: secondaryColor,
                        letterSpacing: 0.5,
                        fontWeight: reguler,
                        fontSize: getProportionateScreenWidth(10),
                      ),
                      //maxLines: 2,
                    ),
                    Text(
                      'akan dibatalkan secara otomatis',
                      style: primaryTextStyle.copyWith(
                        color: secondaryColor,
                        letterSpacing: 0.5,
                        fontWeight: reguler,
                        fontSize: getProportionateScreenWidth(10),
                      ),
                      //maxLines: 2,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: getProportionateScreenHeight(30),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Color(0xFF212121),
                  borderRadius: BorderRadius.circular(8),
                ),
                margin: EdgeInsets.only(
                    left: getProportionateScreenWidth(2),
                    right: getProportionateScreenWidth(2)),
                child: Container(
                  padding: EdgeInsets.symmetric(
                      vertical: 16,
                      horizontal: getProportionateScreenWidth(10)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Metode Pembayaran',
                        style: primaryTextStyle.copyWith(
                          color: Colors.white,
                          letterSpacing: 0.5,
                          fontWeight: reguler,
                          fontSize: getProportionateScreenWidth(10),
                        ),
                        //maxLines: 2,
                      ),
                      SizedBox(
                        height: getProportionateScreenHeight(8),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            'Mandiri Virtual Account',
                            style: primaryTextStyle.copyWith(
                              color: Colors.white,
                              letterSpacing: 1,
                              fontWeight: semiBold,
                              fontSize: getProportionateScreenWidth(13),
                            ),
                            //maxLines: 2,
                          ),
                          Container(
                            width: getProportionateScreenWidth(50),
                            height: getProportionateScreenWidth(17),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(2),
                              image: DecorationImage(
                                fit: BoxFit.fill,
                                image: AssetImage("assets/images/mandiri.png"),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: getProportionateScreenHeight(16),
                      ),
                      Text(
                        'Nomor Virtual Akun',
                        style: primaryTextStyle.copyWith(
                          color: Colors.white,
                          letterSpacing: 0.5,
                          fontWeight: reguler,
                          fontSize: getProportionateScreenWidth(10),
                        ),
                        //maxLines: 2,
                      ),
                      SizedBox(
                        height: getProportionateScreenHeight(8),
                      ),
                      Row(
                        children: [
                          Text(
                            '123456789101112',
                            style: primaryTextStyle.copyWith(
                              color: Colors.white,
                              letterSpacing: 1,
                              fontWeight: semiBold,
                              fontSize: getProportionateScreenWidth(14),
                            ),
                            //maxLines: 2,
                          ),
                          Spacer(),
                          GestureDetector(
                            onTap: () {},
                            child: Text(
                              "Salin",
                              style: primaryTextStyle.copyWith(
                                  letterSpacing: 0.5,
                                  color: primaryColor,
                                  fontSize: getProportionateScreenWidth(10),
                                  fontWeight: reguler),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: getProportionateScreenHeight(16),
                      ),
                      Text(
                        'Total Pembayaran',
                        style: primaryTextStyle.copyWith(
                          color: Colors.white,
                          letterSpacing: 0.5,
                          fontWeight: reguler,
                          fontSize: getProportionateScreenWidth(10),
                        ),
                        //maxLines: 2,
                      ),
                      SizedBox(
                        height: getProportionateScreenHeight(8),
                      ),
                      Row(
                        children: [
                          Text(
                            'Rp. 1,299,000',
                            style: primaryTextStyle.copyWith(
                              color: Colors.white,
                              letterSpacing: 1,
                              fontWeight: semiBold,
                              fontSize: getProportionateScreenWidth(14),
                            ),
                            //maxLines: 2,
                          ),
                          Spacer(),
                          GestureDetector(
                            onTap: () {},
                            child: Text(
                              "Detail Pembayaran",
                              style: primaryTextStyle.copyWith(
                                  letterSpacing: 0.5,
                                  color: primaryColor,
                                  fontSize: getProportionateScreenWidth(10),
                                  fontWeight: reguler),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: getProportionateScreenHeight(24),
              ),
              SizedBox(
                width: double.infinity,
                height: getProportionateScreenWidth(44),
                child: TextButton(
                  style: TextButton.styleFrom(
                    shape: RoundedRectangleBorder(
                        side: BorderSide(color: primaryColor),
                        borderRadius: BorderRadius.circular(
                            getProportionateScreenWidth(10))),
                    primary: Colors.white,
                    backgroundColor: Colors.transparent,
                  ),
                  onPressed: () {},
                  child: Text(
                    "Lihat status pembayaran",
                    style: thirdTextStyle.copyWith(
                        fontSize: getProportionateScreenWidth(13),
                        fontWeight: reguler,
                        color: primaryColor,
                        letterSpacing: 0.5),
                  ),
                ),
              ),
              SizedBox(
                height: getProportionateScreenHeight(16),
              ),
              DefaultButton(
                text: 'Belanja kursus lainnya',
                weight: reguler,
                press: () {
                  // Navigator.of(context)
                  //     .push(MaterialPageRoute(builder: (context) => BatasBayar()));
                },
              ),
              SizedBox(
                height: getProportionateScreenHeight(32),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Color(0xFF212121),
                  borderRadius: BorderRadius.circular(8),
                ),
                margin: EdgeInsets.only(
                    left: getProportionateScreenWidth(2),
                    right: getProportionateScreenWidth(2)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(
                          vertical: 16,
                          horizontal: getProportionateScreenWidth(16)),
                      child: Text(
                        'Cara Pembayaran',
                        style: secondaryTextStyle.copyWith(
                          color: tenthColor,
                          letterSpacing: 1,
                          fontWeight: semiBold,
                          fontSize: getProportionateScreenWidth(14),
                        ),
                        //maxLines: 2,
                      ),
                    ),
                    Divider(
                        color: Color(0xff2D2D2D), thickness: 0.5, height: 0.5),
                    Container(
                        padding: EdgeInsets.symmetric(
                            vertical: 16,
                            horizontal: getProportionateScreenWidth(16)),
                        child: TabBarBatasBayar()),
                  ],
                ),
              ),
              SizedBox(
                height: getProportionateScreenHeight(30),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
