import 'package:flutter/material.dart';
import 'package:initial_folder/screens/checkout/components/course_list.dart';
import 'package:initial_folder/screens/checkout/components/field_kupon.dart';
import 'package:initial_folder/screens/checkout/metode_page.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';

class CheckoutPage extends StatelessWidget {
  const CheckoutPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget bottomNav() {
      return Container(
        width: double.infinity,
        height: getProportionateScreenHeight(64),
        decoration: BoxDecoration(
            border: Border(top: BorderSide(color: fourthColor)),
            boxShadow: [
              BoxShadow(
                  offset: Offset(0, -2),
                  blurRadius: 50,
                  color: fourthColor.withOpacity(0.15))
            ]),
        child: Row(
          children: [
            SizedBox(
              width: 13,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: getProportionateScreenHeight(6),
                ),
                Text(
                  "Total",
                  style: primaryTextStyle.copyWith(
                      fontSize: getProportionateScreenHeight(12),
                      letterSpacing: 1),
                ),
                SizedBox(
                  height: 2,
                ),
                Text(
                  "Rp. 150,000",
                  style: primaryTextStyle.copyWith(
                      fontSize: SizeConfig.blockVertical! * 2.5,
                      letterSpacing: 0.23),
                ),
                SizedBox(
                  height: getProportionateScreenHeight(2),
                ),
              ],
            ),
            Spacer(),
            GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => MetodePage()));
              },
              child: Container(
                margin: EdgeInsets.only(right: getProportionateScreenWidth(15)),
                width: getProportionateScreenWidth(188),
                height: getProportionateScreenHeight(41),
                decoration: BoxDecoration(
                    color: primaryColor,
                    borderRadius: BorderRadius.circular(5)),
                child: Center(
                  child: Text(
                    'Lengkapi Pembayaran',
                    style: thirdTextStyle.copyWith(
                      fontSize: SizeConfig.blockVertical! * 1.7,
                      fontWeight: semiBold,
                      letterSpacing: 0.32,
                      color: Color(0xff050505),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      );
    }

    return Scaffold(
      //backgroundColor: Color(0xff181818),
      appBar: AppBar(
        title: Text(
          'Checkout',
          style: secondaryTextStyle.copyWith(
              letterSpacing: 1,
              fontWeight: semiBold,
              fontSize: getProportionateScreenWidth(14)),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(16)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Detail Pesanan',
                style: secondaryTextStyle.copyWith(
                  letterSpacing: 1,
                  fontWeight: semiBold,
                  fontSize: getProportionateScreenWidth(14),
                ),
              ),
              SizedBox(
                height: getProportionateScreenHeight(20),
              ),
              CourseList(),
              SizedBox(
                height: getProportionateScreenHeight(5),
              ),
              Text(
                'Tukar Kupon',
                style: secondaryTextStyle.copyWith(
                  letterSpacing: 1,
                  fontWeight: semiBold,
                  fontSize: getProportionateScreenWidth(14),
                ),
              ),
              SizedBox(
                height: getProportionateScreenHeight(10),
              ),
              FieldKupon(),
              Align(
                alignment: AlignmentDirectional.center,
                child: SizedBox(
                  width: getProportionateScreenWidth(152),
                  height: getProportionateScreenWidth(32),
                  child: TextButton(
                    style: TextButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          side: BorderSide(color: primaryColor),
                          borderRadius: BorderRadius.circular(
                              getProportionateScreenWidth(6))),
                      primary: Colors.white,
                      backgroundColor: Colors.transparent,
                    ),
                    onPressed: () {},
                    child: Text(
                      "Masukkan kupon",
                      style: thirdTextStyle.copyWith(
                          fontSize: SizeConfig.blockHorizontal! * 3,
                          fontWeight: reguler,
                          color: primaryColor,
                          letterSpacing: 0.5),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: getProportionateScreenHeight(28),
              ),
              Text(
                'Rincian Harga',
                style: secondaryTextStyle.copyWith(
                  letterSpacing: 1,
                  fontWeight: semiBold,
                  fontSize: getProportionateScreenWidth(14),
                ),
              ),
              SizedBox(
                height: getProportionateScreenHeight(15),
              ),
              Row(
                children: [
                  Text(
                    'Subtotal Harga Kursus',
                    style: primaryTextStyle.copyWith(
                      letterSpacing: 1,
                      color: secondaryColor,
                      fontWeight: reguler,
                      fontSize: getProportionateScreenWidth(12),
                    ),
                  ),
                  Spacer(),
                  Text(
                    'Rp. 200,000',
                    style: primaryTextStyle.copyWith(
                      letterSpacing: 0.5,
                      color: tenthColor,
                      fontWeight: reguler,
                      fontSize: getProportionateScreenWidth(12),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: getProportionateScreenHeight(8),
              ),
              Row(
                children: [
                  Text(
                    'Potongan Kupon',
                    style: primaryTextStyle.copyWith(
                      letterSpacing: 1,
                      color: secondaryColor,
                      fontWeight: reguler,
                      fontSize: getProportionateScreenWidth(12),
                    ),
                  ),
                  Spacer(),
                  Text(
                    'Rp. 50,000',
                    style: primaryTextStyle.copyWith(
                      letterSpacing: 0.5,
                      color: tenthColor,
                      fontWeight: reguler,
                      fontSize: getProportionateScreenWidth(12),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: getProportionateScreenHeight(5),
              ),
              Divider(
                color: tenthColor,
                thickness: 0.5,
              ),
              SizedBox(
                height: getProportionateScreenHeight(5),
              ),
              Row(
                children: [
                  Text(
                    'Total',
                    style: secondaryTextStyle.copyWith(
                      letterSpacing: 1,
                      fontWeight: semiBold,
                      fontSize: getProportionateScreenWidth(14),
                    ),
                  ),
                  Spacer(),
                  Text(
                    'Rp. 150,000',
                    style: secondaryTextStyle.copyWith(
                      letterSpacing: 1,
                      color: primaryColor,
                      fontWeight: semiBold,
                      fontSize: getProportionateScreenWidth(14),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: bottomNav(),
    );
  }
}
