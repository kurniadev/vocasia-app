import 'package:flutter/material.dart';
//<<<<<<< HEAD
//=======
import 'package:initial_folder/screens/checkout/bayargopay.dart';
//>>>>>>> 1cd2a8b55be8bb9db99a2bbe731c88ae6717f4bd
import 'package:initial_folder/screens/checkout/debit_kredit.dart';
import 'package:initial_folder/screens/checkout/detail_bayar.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/providers/metode_provider.dart';
import 'package:provider/provider.dart';
import 'package:initial_folder/screens/checkout/components/metode_list.dart';

class MetodePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //RadioProvider radioProvider = Provider.of<RadioProvider>(context);
    MetodeProvider metodeProvider = Provider.of<MetodeProvider>(context);
    // Widget bottomNav() {
    //   return Container(
    //     margin: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
    //     child: DefaultButton(
    //       text: 'Lanjutkan',
    //       press: () {
    //         if (metodeProvider.character == MetodePembayaran.kreditdebit) {
    //           Navigator.of(context).push(
    //               MaterialPageRoute(builder: (context) => DebitCreditPage()));
    //         } else {
    //           Navigator.of(context)
    //               .push(MaterialPageRoute(builder: (context) => DetailBayar()));
    //         }
    //       },
    //     ),
    //   );
    // }

    return Scaffold(
      //backgroundColor: Colors.black,
      appBar: AppBar(
        title: Text(
          'Metode Pembayaran',
          style: secondaryTextStyle.copyWith(
              letterSpacing: 1,
              fontWeight: semiBold,
              fontSize: getProportionateScreenWidth(14)),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(16)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(height: 5),
              Text(
                'ATM/Bank Transfer/Internet Banking',
                style: primaryTextStyle.copyWith(
                  letterSpacing: 0.5,
                  fontWeight: reguler,
                  fontSize: getProportionateScreenWidth(14),
                ),
              ),
              SizedBox(
                height: 12,
              ),
              MetodeList(
                  width: 62.5,
                  height: 27.5,
                  onPress: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => DetailBayar()));
                  },
                  image: 'assets/images/mandiri.png',
                  title1: "Bank Mandiri",
                  title2: "Bayar melalui ATM Mandiri atau Internet Banking"),
              SizedBox(
                height: 10,
              ),
              MetodeList(
                  width: 62.5,
                  height: 27.5,
                  onPress: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => DetailBayar()));
                  },
                  image: 'assets/images/bni.png',
                  title1: "Bank BNI",
                  title2: "Bayar melalui ATM BNI atau Internet Banking"),
              SizedBox(
                height: 10,
              ),
              MetodeList(
                  width: 62.5,
                  height: 27.5,
                  onPress: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => DetailBayar()));
                  },
                  image: 'assets/images/permata.png',
                  title1: "Bank Permata",
                  title2:
                      "Bayar melalui ATM Permata Bank atau Internet Banking"),
              SizedBox(
                height: 10,
              ),
              MetodeList(
                  width: 62.5,
                  height: 27.5,
                  onPress: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => DetailBayar()));
                  },
                  image: 'assets/images/bca.png',
                  title1: "Bank BCA",
                  title2: "Bayar melalui ATM BCA atau Internet Banking"),
              SizedBox(
                height: 10,
              ),
              MetodeList(
                  width: 62.5,
                  height: 27.5,
                  onPress: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => DetailBayar()));
                  },
                  image: 'assets/images/bri.png',
                  title1: "Bank BRI",
                  title2: "Bayar melalui ATM BRI atau Internet Banking"),
              SizedBox(
                height: 10,
              ),
              MetodeList(
                  sizedbox: 8,
                  width: 40.5,
                  height: 27.5,
                  onPress: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => DetailBayar()));
                  },
                  image: 'assets/images/banklain.png',
                  title1: "Bank Lainnya",
                  title2: "Bayar menggunakan Bank Lain"),

              SizedBox(
                height: 24,
              ),
              Text(
                'E-Wallet',
                style: primaryTextStyle.copyWith(
                  letterSpacing: 0.5,
                  fontWeight: reguler,
                  fontSize: getProportionateScreenWidth(14),
                ),
              ),
              SizedBox(
                height: 12,
              ),
              MetodeList(
                  scale: 1.5,
                  width: 62.5,
                  height: 27.5,
                  onPress: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => BayarGopay()));
                  },
                  image: 'assets/images/gopay.png',
                  title1: "Gopay",
                  title2: "Bayar dengan menggunakan Gopay"),
              SizedBox(
                height: 10,
              ),
              MetodeList(
                  scale: 1.5,
                  width: 62.5,
                  height: 27.5,
                  onPress: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => DetailBayar()));
                  },
                  image: 'assets/images/shopee.png',
                  title1: "ShoopeePay",
                  title2: "Bayar dengan menggunakan ShopeePay"),
              SizedBox(
                height: 24,
              ),
              Text(
                'Kartu Kredit/Debit',
                style: primaryTextStyle.copyWith(
                  letterSpacing: 0.5,
                  fontWeight: reguler,
                  fontSize: getProportionateScreenWidth(14),
                ),
              ),
              SizedBox(
                height: 12,
              ),
              MetodeList(
                  width: 62.5,
                  height: 27.5,
                  onPress: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => DebitCreditPage()));
                  },
                  image: 'assets/images/visacard.png',
                  title1: "Kartu Kredit/Debit",
                  title2: "Bayar dengan VISA, Mastercard atau JBC"),
              SizedBox(
                height: 24,
              ),
              Text(
                'Gerai',
                style: primaryTextStyle.copyWith(
                  letterSpacing: 0.5,
                  fontWeight: reguler,
                  fontSize: getProportionateScreenWidth(14),
                ),
              ),
              SizedBox(
                height: 12,
              ),
              MetodeList(
                  width: 62.5,
                  height: 27.5,
                  onPress: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => DetailBayar()));
                  },
                  image: 'assets/images/alfamart.png',
                  title1: "Alfamart",
                  title2: "Bayar melalui Alfamart terdekat"),
              SizedBox(
                height: 10,
              ),
              MetodeList(
                  width: 62.5,
                  height: 27.5,
                  onPress: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => DetailBayar()));
                  },
                  image: 'assets/images/indomaret.png',
                  title1: "Indomaret",
                  title2: "Bayar melalui Indomaret terdekat"),
              SizedBox(
                height: 24,
              ),
              //Divider(thickness: 0.5, color: tenthColor),
              // Text(
              //   'ATM/Bank Transfer/Internet Banking',
              //   style: primaryTextStyle.copyWith(
              //     letterSpacing: 0.5,
              //     fontWeight: reguler,
              //     fontSize: getProportionateScreenWidth(12),
              //   ),
              // ),
              // SizedBox(
              //   height: getProportionateScreenHeight(10),
              // ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.start,
              //   children: [
              //     Radio<MetodePembayaran>(
              //       value: MetodePembayaran.mandiri,
              //       groupValue: metodeProvider.character,
              //       onChanged: (MetodePembayaran? value) {
              //         metodeProvider.character = value;
              //       },
              //     ),
              //     Container(
              //       width: getProportionateScreenWidth(48.83),
              //       height: getProportionateScreenWidth(14),
              //       decoration: BoxDecoration(
              //         borderRadius: BorderRadius.circular(2),
              //         image: DecorationImage(
              //           fit: BoxFit.cover,
              //           image: AssetImage('assets/images/mandiri.png'),
              //         ),
              //       ),
              //     ),
              //     SizedBox(width: getProportionateScreenWidth(15)),
              //     Text(
              //       'Bank Mandiri',
              //       style: primaryTextStyle.copyWith(
              //         letterSpacing: 0.5,
              //         fontWeight: reguler,
              //         fontSize: getProportionateScreenWidth(12),
              //       ),
              //     ),
              //   ],
              // ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.start,
              //   children: [
              //     Radio<MetodePembayaran>(
              //       value: MetodePembayaran.bni,
              //       groupValue: metodeProvider.character,
              //       onChanged: (MetodePembayaran? value) {
              //         //setState(() {
              //         metodeProvider.character = value;
              //         //});
              //       },
              //     ),
              //     Container(
              //       width: getProportionateScreenWidth(37.62),
              //       height: getProportionateScreenWidth(14),
              //       decoration: BoxDecoration(
              //         borderRadius: BorderRadius.circular(2),
              //         image: DecorationImage(
              //           fit: BoxFit.cover,
              //           image: AssetImage('assets/images/bni.png'),
              //         ),
              //       ),
              //     ),
              //     SizedBox(width: getProportionateScreenWidth(15)),
              //     Text(
              //       'Bank BNI',
              //       style: primaryTextStyle.copyWith(
              //         letterSpacing: 0.5,
              //         fontWeight: reguler,
              //         fontSize: getProportionateScreenWidth(12),
              //       ),
              //     ),
              //   ],
              // ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.start,
              //   children: [
              //     Radio<MetodePembayaran>(
              //       value: MetodePembayaran.permata,
              //       groupValue: metodeProvider.character,
              //       onChanged: (MetodePembayaran? value) {
              //         //setState(() {
              //         metodeProvider.character = value;
              //         //});
              //       },
              //     ),
              //     Container(
              //       width: getProportionateScreenWidth(40.4),
              //       height: getProportionateScreenWidth(14),
              //       decoration: BoxDecoration(
              //         borderRadius: BorderRadius.circular(2),
              //         image: DecorationImage(
              //           fit: BoxFit.cover,
              //           image: AssetImage('assets/images/permata.png'),
              //         ),
              //       ),
              //     ),
              //     SizedBox(width: getProportionateScreenWidth(15)),
              //     Text(
              //       'Bank Permata',
              //       style: primaryTextStyle.copyWith(
              //         letterSpacing: 0.5,
              //         fontWeight: reguler,
              //         fontSize: getProportionateScreenWidth(12),
              //       ),
              //     ),
              //   ],
              // ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.start,
              //   children: [
              //     Radio<MetodePembayaran>(
              //       value: MetodePembayaran.lainnya,
              //       groupValue: metodeProvider.character,
              //       onChanged: (MetodePembayaran? value) {
              //         //setState(() {
              //         metodeProvider.character = value;
              //         //});
              //       },
              //     ),
              //     Text(
              //       'Bank Lainnya',
              //       style: primaryTextStyle.copyWith(
              //         letterSpacing: 0.5,
              //         fontWeight: reguler,
              //         fontSize: getProportionateScreenWidth(12),
              //       ),
              //     ),
              //   ],
              // ),
              // SizedBox(
              //   height: getProportionateScreenHeight(5),
              // ),
              // Divider(thickness: 0.5, color: tenthColor),
              // SizedBox(
              //   height: getProportionateScreenHeight(10),
              // ),
              // Text(
              //   'E-wallet',
              //   style: primaryTextStyle.copyWith(
              //     letterSpacing: 0.5,
              //     fontWeight: reguler,
              //     fontSize: getProportionateScreenWidth(12),
              //   ),
              // ),
              // SizedBox(
              //   height: getProportionateScreenHeight(10),
              // ),
              // Row(
              //   mainAxisAlignment: MainAxisAlignment.start,
              //   children: [
              //     Radio<MetodePembayaran>(
              //       value: MetodePembayaran.gopay,
              //       groupValue: metodeProvider.character,
              //       onChanged: (MetodePembayaran? value) {
              //         //setState(() {
              //         metodeProvider.character = value;
              //         //});
              //       },
              //     ),
              //     Container(
              //       width: getProportionateScreenWidth(84.93),
              //       height: getProportionateScreenWidth(28),
              //       decoration: BoxDecoration(
              //         borderRadius: BorderRadius.circular(2),
              //         image: DecorationImage(
              //           fit: BoxFit.cover,
              //           image: AssetImage('assets/images/gopay.png'),
              //         ),
              //       ),
              //     ),
              //     SizedBox(width: getProportionateScreenWidth(15)),
              //     Container(
              //       width: getProportionateScreenWidth(75.87),
              //       height: getProportionateScreenWidth(28),
              //       decoration: BoxDecoration(
              //         borderRadius: BorderRadius.circular(2),
              //         image: DecorationImage(
              //           fit: BoxFit.cover,
              //           image: AssetImage('assets/images/qris.png'),
              //         ),
              //       ),
              //     ),
              //   ],
              // ),
            ],
          ),
        ),
      ),
      // bottomNavigationBar: bottomNav(),
    );
  }
}
