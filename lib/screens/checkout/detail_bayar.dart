import 'package:flutter/material.dart';
import 'package:initial_folder/screens/checkout/batas_bayar.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/widgets/login_regist/default_button.dart';

class DetailBayar extends StatelessWidget {
  // final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  // final TextEditingController _nameController = TextEditingController(text: '');

  // final TextEditingController _emailController =
  //     TextEditingController(text: '');
  @override
  Widget build(BuildContext context) {
    // void _validateInputs() {
    //   if (this._formKey.currentState!.validate()) {
    //     Navigator.of(context)
    //         .push(MaterialPageRoute(builder: (context) => BatasBayar()));
    //   }
    // }

    // Widget form() {
    //   Widget nameInput() {
    //     return CustomTextField(
    //       pad: getProportionateScreenWidth(2),
    //       controler: _nameController,
    //       hinttext: 'Masukkan nama lengkap',
    //       title: 'Nama Lengkap',
    //       validate: validateName,
    //     );
    //   }

    //   Widget emailInput() {
    //     return CustomTextField(
    //       pad: getProportionateScreenWidth(2),
    //       controler: _emailController,
    //       hinttext: 'Masukkan email',
    //       title: 'Email',
    //       validate: validateEmail,
    //     );
    //   }

    //   Widget bottomNav() {
    //     return Container(
    //       margin: EdgeInsets.symmetric(
    //           horizontal: getProportionateScreenWidth(90),
    //           vertical: getProportionateScreenHeight(10)),
    //       child: DefaultButton(
    //         text: 'Lanjutkan',
    //         press: _validateInputs,
    //       ),
    //     );
    //   }

    //   return Form(
    //     key: _formKey,
    //     child: Column(
    //       children: [
    //         nameInput(),
    //         SizedBox(height: getProportionateScreenHeight(15)),
    //         emailInput(),
    //         SizedBox(height: getProportionateScreenHeight(15)),
    //         bottomNav(),
    //       ],
    //     ),
    //   );
    // }
    Widget bottomNav() {
      return Container(
        margin: EdgeInsets.symmetric(
            horizontal: getProportionateScreenWidth(90),
            vertical: getProportionateScreenHeight(10)),
        child: DefaultButton(
          text: 'Lanjutkan',
          press: () {
            Navigator.of(context)
                .push(MaterialPageRoute(builder: (context) => BatasBayar()));
          },
        ),
      );
    }

    return Scaffold(
      //backgroundColor: Colors.black,
      appBar: AppBar(
        title: Text(
          'Checkout',
          style: secondaryTextStyle.copyWith(
              letterSpacing: 1,
              fontWeight: semiBold,
              fontSize: getProportionateScreenWidth(14)),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(16)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 10),
              Container(
                decoration: BoxDecoration(
                  color: Color(0xFF212121),
                  borderRadius: BorderRadius.circular(8),
                ),
                margin: EdgeInsets.only(
                    left: getProportionateScreenWidth(2),
                    right: getProportionateScreenWidth(2)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 15,
                                horizontal: getProportionateScreenWidth(15)),
                            child: Text(
                              'Informasi Pembeli',
                              style: secondaryTextStyle.copyWith(
                                letterSpacing: 1,
                                fontWeight: semiBold,
                                fontSize: getProportionateScreenWidth(14),
                                color: tenthColor,
                              ),
                            ),
                          ),
                          Divider(
                              color: Color(0xff2D2D2D),
                              thickness: 0.5,
                              height: 1),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 10,
                                horizontal: getProportionateScreenWidth(15)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Order ID',
                                  style: primaryTextStyle.copyWith(
                                    letterSpacing: 0.5,
                                    fontWeight: reguler,
                                    fontSize: getProportionateScreenWidth(10),
                                    color: secondaryColor,
                                  ),
                                ),
                                Text(
                                  'ABCXYZ12345678',
                                  style: primaryTextStyle.copyWith(
                                    letterSpacing: 0.5,
                                    fontWeight: reguler,
                                    fontSize: getProportionateScreenWidth(12),
                                    color: tenthColor,
                                  ),
                                ),
                                SizedBox(height: 16),
                                Text(
                                  'Nama Lengkap',
                                  style: primaryTextStyle.copyWith(
                                    letterSpacing: 0.5,
                                    fontWeight: reguler,
                                    fontSize: getProportionateScreenWidth(10),
                                    color: secondaryColor,
                                  ),
                                ),
                                Text(
                                  'Ariana Grande',
                                  style: primaryTextStyle.copyWith(
                                    letterSpacing: 0.5,
                                    fontWeight: reguler,
                                    fontSize: getProportionateScreenWidth(12),
                                    color: tenthColor,
                                  ),
                                ),
                                SizedBox(height: 16),
                                Text(
                                  'Email',
                                  style: primaryTextStyle.copyWith(
                                    letterSpacing: 0.5,
                                    fontWeight: reguler,
                                    fontSize: getProportionateScreenWidth(10),
                                    color: secondaryColor,
                                  ),
                                ),
                                Text(
                                  'ariana@gmail.com',
                                  style: primaryTextStyle.copyWith(
                                    letterSpacing: 0.5,
                                    fontWeight: reguler,
                                    fontSize: getProportionateScreenWidth(12),
                                    color: tenthColor,
                                  ),
                                ),
                                SizedBox(height: 12),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: getProportionateScreenHeight(20),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Color(0xFF212121),
                  borderRadius: BorderRadius.circular(8),
                ),
                margin: EdgeInsets.only(
                    left: getProportionateScreenWidth(2),
                    right: getProportionateScreenWidth(2)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 15,
                                horizontal: getProportionateScreenWidth(15)),
                            child: Text(
                              'Informasi Pembeli',
                              style: secondaryTextStyle.copyWith(
                                letterSpacing: 1,
                                fontWeight: semiBold,
                                fontSize: getProportionateScreenWidth(14),
                                color: tenthColor,
                              ),
                            ),
                          ),
                          Divider(
                              color: Color(0xff2D2D2D),
                              thickness: 0.5,
                              height: 1),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 15,
                                horizontal: getProportionateScreenWidth(15)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Metode Pembayaran',
                                      style: primaryTextStyle.copyWith(
                                        letterSpacing: 0.5,
                                        fontWeight: reguler,
                                        fontSize:
                                            getProportionateScreenWidth(10),
                                        color: secondaryColor,
                                      ),
                                    ),
                                    Text(
                                      'Mandiri Virtual Account',
                                      style: primaryTextStyle.copyWith(
                                        letterSpacing: 0.5,
                                        fontWeight: reguler,
                                        fontSize:
                                            getProportionateScreenWidth(11),
                                        color: tenthColor,
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 20),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Total Harga',
                                      style: primaryTextStyle.copyWith(
                                        letterSpacing: 0.5,
                                        fontWeight: reguler,
                                        fontSize:
                                            getProportionateScreenWidth(10),
                                        color: secondaryColor,
                                      ),
                                    ),
                                    Text(
                                      'Rp. 1,250,000',
                                      style: primaryTextStyle.copyWith(
                                        letterSpacing: 0.5,
                                        fontWeight: reguler,
                                        fontSize:
                                            getProportionateScreenWidth(12),
                                        color: tenthColor,
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 12),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Potongan Kupon',
                                      style: primaryTextStyle.copyWith(
                                        letterSpacing: 0.5,
                                        fontWeight: reguler,
                                        fontSize:
                                            getProportionateScreenWidth(10),
                                        color: secondaryColor,
                                      ),
                                    ),
                                    Text(
                                      'Rp. 50,000',
                                      style: primaryTextStyle.copyWith(
                                        letterSpacing: 0.5,
                                        fontWeight: reguler,
                                        fontSize:
                                            getProportionateScreenWidth(12),
                                        color: tenthColor,
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 16),
                                Divider(
                                    color: Color(0xff2D2D2D),
                                    thickness: 0.5,
                                    height: 1),
                                SizedBox(height: 16),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Total Bayar',
                                      style: primaryTextStyle.copyWith(
                                        letterSpacing: 0.5,
                                        fontWeight: reguler,
                                        fontSize:
                                            getProportionateScreenWidth(12),
                                        color: secondaryColor,
                                      ),
                                    ),
                                    Text(
                                      'Rp. 1,200,000',
                                      style: secondaryTextStyle.copyWith(
                                        letterSpacing: 1,
                                        fontWeight: semiBold,
                                        fontSize:
                                            getProportionateScreenWidth(14),
                                        color: tenthColor,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: getProportionateScreenHeight(20),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Color(0xFF212121),
                  borderRadius: BorderRadius.circular(8),
                ),
                margin: EdgeInsets.only(
                    left: getProportionateScreenWidth(2),
                    right: getProportionateScreenWidth(2)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 15,
                                horizontal: getProportionateScreenWidth(15)),
                            child: Text(
                              'Kursus yang dibeli',
                              style: secondaryTextStyle.copyWith(
                                letterSpacing: 1,
                                fontWeight: semiBold,
                                fontSize: getProportionateScreenWidth(14),
                                color: tenthColor,
                              ),
                            ),
                          ),
                          Divider(
                              color: Color(0xff2D2D2D),
                              thickness: 0.5,
                              height: 1),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 15,
                                horizontal: getProportionateScreenWidth(15)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Flexible(
                                      flex: 7,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            '428 Menit Menjadi Pengusaha',
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                            style: primaryTextStyle.copyWith(
                                              letterSpacing: 0.5,
                                              fontWeight: reguler,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      11),
                                              color: tenthColor,
                                            ),
                                          ),
                                          SizedBox(height: 4),
                                          Text(
                                            'Oleh Farid Subkhan',
                                            style: primaryTextStyle.copyWith(
                                              letterSpacing: 0.5,
                                              fontWeight: reguler,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      9.5),
                                              color: secondaryColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Flexible(
                                      flex: 3,
                                      child: Text(
                                        'Rp. 50,000',
                                        style: primaryTextStyle.copyWith(
                                          letterSpacing: 0.5,
                                          fontWeight: reguler,
                                          fontSize:
                                              getProportionateScreenWidth(12),
                                          color: tenthColor,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(height: 16),
                                Divider(
                                    color: Color(0xff2D2D2D),
                                    thickness: 0.5,
                                    height: 1),
                                SizedBox(height: 16),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Flexible(
                                      flex: 7,
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            '428 Menit Menjadi Pengusaha Sukses',
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                            style: primaryTextStyle.copyWith(
                                              letterSpacing: 0.5,
                                              fontWeight: reguler,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      11),
                                              color: tenthColor,
                                            ),
                                          ),
                                          SizedBox(height: 4),
                                          Text(
                                            'Oleh Farid Subkhan',
                                            style: primaryTextStyle.copyWith(
                                              letterSpacing: 0.5,
                                              fontWeight: reguler,
                                              fontSize:
                                                  getProportionateScreenWidth(
                                                      9.5),
                                              color: secondaryColor,
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Flexible(
                                      flex: 3,
                                      child: Text(
                                        'Rp. 500,000',
                                        style: primaryTextStyle.copyWith(
                                          letterSpacing: 0.5,
                                          fontWeight: reguler,
                                          fontSize:
                                              getProportionateScreenWidth(12),
                                          color: tenthColor,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              // Row(
              //   children: [
              //     Text(
              //       'Order ID',
              //       style: primaryTextStyle.copyWith(
              //         letterSpacing: 1,
              //         color: tenthColor,
              //         fontWeight: reguler,
              //         fontSize: SizeConfig.blockHorizontal! * 2.5,
              //       ),
              //     ),
              //     Spacer(),
              //     Text(
              //       '1234567890',
              //       style: primaryTextStyle.copyWith(
              //         letterSpacing: 0.5,
              //         color: primaryColor,
              //         fontWeight: reguler,
              //         fontSize: SizeConfig.blockHorizontal! * 2.5,
              //       ),
              //     ),
              //   ],
              // ),
              // SizedBox(
              //   height: getProportionateScreenHeight(8),
              // ),
              // Row(
              //   children: [
              //     Text(
              //       'Total Pembayaran',
              //       style: primaryTextStyle.copyWith(
              //         letterSpacing: 1,
              //         color: tenthColor,
              //         fontWeight: reguler,
              //         fontSize: SizeConfig.blockHorizontal! * 2.5,
              //       ),
              //     ),
              //     Spacer(),
              //     Text(
              //       'Rp. 250,000',
              //       style: secondaryTextStyle.copyWith(
              //         letterSpacing: 1,
              //         color: primaryColor,
              //         fontWeight: semiBold,
              //         fontSize: SizeConfig.blockHorizontal! * 3,
              //       ),
              //     ),
              //   ],
              // ),
              // SizedBox(
              //   height: getProportionateScreenHeight(8),
              // ),
              // Row(
              //   children: [
              //     Text(
              //       'Metode Pembayaran',
              //       style: primaryTextStyle.copyWith(
              //         letterSpacing: 1,
              //         color: tenthColor,
              //         fontWeight: reguler,
              //         fontSize: SizeConfig.blockHorizontal! * 2.5,
              //       ),
              //     ),
              //     Spacer(),
              //     Text(
              //       'Transfer Bank Mandiri',
              //       style: primaryTextStyle.copyWith(
              //         letterSpacing: 0.5,
              //         color: primaryColor,
              //         fontWeight: reguler,
              //         fontSize: SizeConfig.blockHorizontal! * 2.5,
              //       ),
              //     ),
              //   ],
              // ),
              SizedBox(
                height: getProportionateScreenHeight(15),
              ),
              bottomNav(),
              SizedBox(
                height: getProportionateScreenHeight(15),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
