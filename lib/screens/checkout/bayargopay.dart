import 'package:flutter/material.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/widgets/login_regist/default_button.dart';
import 'package:styled_text/styled_text.dart';

class BayarGopay extends StatelessWidget {
  // final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  // final TextEditingController _nameController = TextEditingController(text: '');

  // final TextEditingController _emailController =
  //     TextEditingController(text: '');
  @override
  Widget build(BuildContext context) {
    final TextStyle baris = primaryTextStyle.copyWith(
      fontWeight: reguler,
      color: secondaryColor,
      fontSize: getProportionateScreenWidth(14),
      letterSpacing: 0.5,
    );
    // void _validateInputs() {
    //   if (this._formKey.currentState!.validate()) {
    //     Navigator.of(context)
    //         .push(MaterialPageRoute(builder: (context) => BatasBayar()));
    //   }
    // }

    // Widget form() {
    //   Widget nameInput() {
    //     return CustomTextField(
    //       pad: getProportionateScreenWidth(2),
    //       controler: _nameController,
    //       hinttext: 'Masukkan nama lengkap',
    //       title: 'Nama Lengkap',
    //       validate: validateName,
    //     );
    //   }

    //   Widget emailInput() {
    //     return CustomTextField(
    //       pad: getProportionateScreenWidth(2),
    //       controler: _emailController,
    //       hinttext: 'Masukkan email',
    //       title: 'Email',
    //       validate: validateEmail,
    //     );
    //   }

    //   Widget bottomNav() {
    //     return Container(
    //       margin: EdgeInsets.symmetric(
    //           horizontal: getProportionateScreenWidth(90),
    //           vertical: getProportionateScreenHeight(10)),
    //       child: DefaultButton(
    //         text: 'Lanjutkan',
    //         press: _validateInputs,
    //       ),
    //     );
    //   }

    //   return Form(
    //     key: _formKey,
    //     child: Column(
    //       children: [
    //         nameInput(),
    //         SizedBox(height: getProportionateScreenHeight(15)),
    //         emailInput(),
    //         SizedBox(height: getProportionateScreenHeight(15)),
    //         bottomNav(),
    //       ],
    //     ),
    //   );
    // }
    Widget bottomNav() {
      return DefaultButton(
        text: 'Bayar dengan Gopay',
        press: () {
          // Navigator.of(context)
          //     .push(MaterialPageRoute(builder: (context) => BatasBayar()));
        },
      );
    }

    return Scaffold(
      //backgroundColor: Colors.black,
      appBar: AppBar(
        title: Text(
          'Bayar dengan Gopay',
          style: secondaryTextStyle.copyWith(
              letterSpacing: 1,
              fontWeight: semiBold,
              fontSize: getProportionateScreenWidth(14)),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          margin:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(16)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: 10),
              Container(
                decoration: BoxDecoration(
                  color: Color(0xFF212121),
                  borderRadius: BorderRadius.circular(8),
                ),
                margin: EdgeInsets.only(
                    left: getProportionateScreenWidth(2),
                    right: getProportionateScreenWidth(2)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 15,
                                horizontal: getProportionateScreenWidth(15)),
                            child: Text(
                              'Informasi Pembayaran',
                              style: secondaryTextStyle.copyWith(
                                letterSpacing: 1,
                                fontWeight: semiBold,
                                fontSize: getProportionateScreenWidth(14),
                                color: tenthColor,
                              ),
                            ),
                          ),
                          Divider(
                              color: Color(0xff2D2D2D),
                              thickness: 0.5,
                              height: 1),
                          Container(
                            padding: EdgeInsets.symmetric(
                                vertical: 10,
                                horizontal: getProportionateScreenWidth(15)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Order ID',
                                  style: primaryTextStyle.copyWith(
                                    letterSpacing: 0.5,
                                    fontWeight: reguler,
                                    fontSize: getProportionateScreenWidth(10),
                                    color: secondaryColor,
                                  ),
                                ),
                                Text(
                                  'ABCXYZ12345678',
                                  style: primaryTextStyle.copyWith(
                                    letterSpacing: 0.5,
                                    fontWeight: reguler,
                                    fontSize: getProportionateScreenWidth(12),
                                    color: tenthColor,
                                  ),
                                ),
                                SizedBox(height: 16),
                                Text(
                                  'Total Pembayaran',
                                  style: primaryTextStyle.copyWith(
                                    letterSpacing: 0.5,
                                    fontWeight: reguler,
                                    fontSize: getProportionateScreenWidth(10),
                                    color: secondaryColor,
                                  ),
                                ),
                                Text(
                                  'Rp. 1,200,000',
                                  style: primaryTextStyle.copyWith(
                                    letterSpacing: 0.5,
                                    fontWeight: reguler,
                                    fontSize: getProportionateScreenWidth(12),
                                    color: tenthColor,
                                  ),
                                ),
                                SizedBox(height: 12),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: getProportionateScreenHeight(20),
              ),
              Container(
                decoration: BoxDecoration(
                  color: Color(0xFF212121),
                  borderRadius: BorderRadius.circular(8),
                ),
                margin: EdgeInsets.only(
                    left: getProportionateScreenWidth(2),
                    right: getProportionateScreenWidth(2)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: EdgeInsets.symmetric(
                          vertical: 16,
                          horizontal: getProportionateScreenWidth(16)),
                      child: Text(
                        'Cara Pembayaran',
                        style: secondaryTextStyle.copyWith(
                          color: tenthColor,
                          letterSpacing: 1,
                          fontWeight: semiBold,
                          fontSize: getProportionateScreenWidth(14),
                        ),
                        //maxLines: 2,
                      ),
                    ),
                    Divider(
                        color: Color(0xff2D2D2D), thickness: 0.5, height: 0.5),
                    Container(
                      padding: EdgeInsets.symmetric(
                          vertical: 16,
                          horizontal: getProportionateScreenWidth(16)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          StyledText(
                            text: '1. Tekan <bold>Bayar dengan Gopay</bold>',
                            style: baris,
                            tags: {
                              'bold': StyledTextTag(
                                  style: TextStyle(fontWeight: bold)),
                            },
                          ),
                          Divider(
                              color: Color(0xff2D2D2D),
                              thickness: 0.5,
                              height: 35),
                          StyledText(
                            text: '2. Aplikasi <bold>Gojek</bold> akan terbuka',
                            style: baris,
                            tags: {
                              'bold': StyledTextTag(
                                  style: TextStyle(fontWeight: bold)),
                            },
                          ),
                          Divider(
                              color: Color(0xff2D2D2D),
                              thickness: 0.5,
                              height: 35),
                          StyledText(
                            text:
                                '3. Cek detail transaksi Anda, kemudian tekan <bold>Lanjutkan</bold>',
                            style: baris,
                            tags: {
                              'bold': StyledTextTag(
                                  style: TextStyle(fontWeight: bold)),
                            },
                          ),
                          Divider(
                              color: Color(0xff2D2D2D),
                              thickness: 0.5,
                              height: 35),
                          StyledText(
                            text:
                                '4. Cek saldo Gopay Anda, kemudian tekan <bold>Bayar</bold>',
                            style: baris,
                            tags: {
                              'bold': StyledTextTag(
                                  style: TextStyle(fontWeight: bold)),
                            },
                          ),
                          Divider(
                              color: Color(0xff2D2D2D),
                              thickness: 0.5,
                              height: 35),
                          StyledText(
                            text: '5. Masukkan <bold>PIN</bold> Anda',
                            style: baris,
                            tags: {
                              'bold': StyledTextTag(
                                  style: TextStyle(fontWeight: bold)),
                            },
                          ),
                          Divider(
                              color: Color(0xff2D2D2D),
                              thickness: 0.5,
                              height: 35),
                          StyledText(
                            text: '6. Transaksi Anda telah selesai',
                            style: baris,
                            tags: {
                              'bold': StyledTextTag(
                                  style: TextStyle(fontWeight: bold)),
                            },
                          ),
                          SizedBox(height: 4)
                        ],
                      ),
                    ),
                  ],
                ),
              ),

              // Row(
              //   children: [
              //     Text(
              //       'Order ID',
              //       style: primaryTextStyle.copyWith(
              //         letterSpacing: 1,
              //         color: tenthColor,
              //         fontWeight: reguler,
              //         fontSize: SizeConfig.blockHorizontal! * 2.5,
              //       ),
              //     ),
              //     Spacer(),
              //     Text(
              //       '1234567890',
              //       style: primaryTextStyle.copyWith(
              //         letterSpacing: 0.5,
              //         color: primaryColor,
              //         fontWeight: reguler,
              //         fontSize: SizeConfig.blockHorizontal! * 2.5,
              //       ),
              //     ),
              //   ],
              // ),
              // SizedBox(
              //   height: getProportionateScreenHeight(8),
              // ),
              // Row(
              //   children: [
              //     Text(
              //       'Total Pembayaran',
              //       style: primaryTextStyle.copyWith(
              //         letterSpacing: 1,
              //         color: tenthColor,
              //         fontWeight: reguler,
              //         fontSize: SizeConfig.blockHorizontal! * 2.5,
              //       ),
              //     ),
              //     Spacer(),
              //     Text(
              //       'Rp. 250,000',
              //       style: secondaryTextStyle.copyWith(
              //         letterSpacing: 1,
              //         color: primaryColor,
              //         fontWeight: semiBold,
              //         fontSize: SizeConfig.blockHorizontal! * 3,
              //       ),
              //     ),
              //   ],
              // ),
              // SizedBox(
              //   height: getProportionateScreenHeight(8),
              // ),
              // Row(
              //   children: [
              //     Text(
              //       'Metode Pembayaran',
              //       style: primaryTextStyle.copyWith(
              //         letterSpacing: 1,
              //         color: tenthColor,
              //         fontWeight: reguler,
              //         fontSize: SizeConfig.blockHorizontal! * 2.5,
              //       ),
              //     ),
              //     Spacer(),
              //     Text(
              //       'Transfer Bank Mandiri',
              //       style: primaryTextStyle.copyWith(
              //         letterSpacing: 0.5,
              //         color: primaryColor,
              //         fontWeight: reguler,
              //         fontSize: SizeConfig.blockHorizontal! * 2.5,
              //       ),
              //     ),
              //   ],
              // ),
              SizedBox(
                height: getProportionateScreenHeight(15),
              ),
              bottomNav(),
              SizedBox(
                height: getProportionateScreenHeight(15),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
