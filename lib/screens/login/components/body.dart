import 'package:flutter/material.dart';

import 'package:initial_folder/screens/home/home_screen.dart';
import 'package:initial_folder/screens/registrasi/registrasi_screen.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/widgets/login_regist/header.dart';
import 'package:initial_folder/widgets/terms_and_privacy.dart';
import '../login_email/login_email_screen.dart';
import '../../../widgets/login_regist/default_button.dart';
import '../../../widgets/login_regist/default_icon_button.dart';
import 'package:flutter/widgets.dart';
import 'package:initial_folder/providers/google_sign_in.dart';
import 'package:provider/provider.dart';
import 'package:flutter/gestures.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:initial_folder/screens/splash/splash_screen_login.dart';
//import 'package:sizer/sizer.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    imageCache!.clear();

    return Center(
      child: SafeArea(
        child: Scaffold(
          body: StreamBuilder(
              stream: FirebaseAuth.instance.authStateChanges(),
              builder: (context, snapshot) {
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return Center(child: CircularProgressIndicator());
                } else if (snapshot.hasData) {
                  Condition.loginFirebase = true;
                  return HomeScreen();
                } else if (snapshot.hasError) {
                  return Center(child: Text('Something Went Wrong!'));
                } else {
                  return SingleChildScrollView(
                    child: Column(children: [
                      Container(
                          padding: EdgeInsets.only(
                              left: getProportionateScreenWidth(12),
                              top: getProportionateScreenHeight(8)),
                          alignment: Alignment.topLeft,
                          child: TextButton(
                              child: Text('Login nanti',
                                  style: primaryTextStyle.copyWith(
                                      color: primaryColor,
                                      fontWeight: reguler,
                                      fontSize:
                                          getProportionateScreenWidth(14))),
                              onPressed: () {
                                Navigator.of(context).pushNamedAndRemoveUntil(
                                    HomeScreen.routeName,
                                    (Route<dynamic> route) => false);
                                print('nanti');
                              })),
                      SizedBox(height: getProportionateScreenHeight(35)),
                      Container(
                        alignment: Alignment.center,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Header(
                              jarak: 16,
                              text:
                                  'Masuk Untuk Melanjutkan Belajar dan Upgrade Skill Bersama Vocasia',
                              style: secondaryTextStyle.copyWith(
                                color: tenthColor,
                                fontWeight: semiBold,
                                fontSize: getProportionateScreenWidth(14),
                              ),
                            ),
                            SizedBox(height: getProportionateScreenHeight(32)),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal:
                                      getProportionateScreenWidth(15.0)),
                              child: DefaultIconButton(
                                text: "Lanjutkan dengan Google",
                                press: () async {
                                  final provider =
                                      Provider.of<GoogleSignInProvider>(context,
                                          listen: false);
                                  print('Login Gmail');
                                  await provider.googleLogin().then((value) {
                                    if (snapshot.hasData) {
                                      Condition.loginFirebase = true;
                                      Navigator.of(context)
                                          .pushNamedAndRemoveUntil(
                                              HomeScreen.routeName,
                                              (Route<dynamic> route) => false);
                                    }
                                  });
                                },
                                icon: 'assets/icons/google.png',
                                iconWidth: 25,
                                iconHeight: 25,
                              ),
                            ),
                            SizedBox(height: getProportionateScreenWidth(16)),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal:
                                      getProportionateScreenWidth(15.0)),
                              child: DefaultIconButton(
                                text: "Lanjutkan dengan Facebook",
                                press: () {
                                  //Navigator.pushNamed(context, SignInScreen.routeName);
                                  print('Login FB');
                                },
                                icon: 'assets/icons/facebook.png',
                                iconWidth: 25,
                                iconHeight: 25,
                              ),
                            ),
                            SizedBox(height: getProportionateScreenWidth(16)),
                            Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal:
                                      getProportionateScreenWidth(15.0)),
                              child: DefaultButton(
                                text: "Login dengan Email",
                                press: () {
                                  //loginEmail = true;
                                  Navigator.pushNamed(
                                      context, LoginEmail.routeName);
                                  //print('Login Manual');
                                },
                              ),
                            ),
                            SizedBox(height: getProportionateScreenHeight(32)),
                            Container(
                                // padding: EdgeInsets.only(
                                //     left: getProportionateScreenWidth(5),
                                //     top: getProportionateScreenWidth(5)),
                                alignment: Alignment.center,
                                child: RichText(
                                    text: TextSpan(
                                        children: <TextSpan>[
                                      TextSpan(text: 'Baru di Vocasia? '),
                                      TextSpan(
                                          text: 'Buat Akun',
                                          style: primaryTextStyle.copyWith(
                                              color: primaryColor),
                                          recognizer: TapGestureRecognizer()
                                            ..onTap = () {
                                              Navigator.of(context)
                                                  .pushNamedAndRemoveUntil(
                                                      RegistrationScreen
                                                          .routeName,
                                                      (Route<dynamic> route) =>
                                                          false);
                                            }),
                                    ],
                                        style: primaryTextStyle.copyWith(
                                            color: tenthColor,
                                            fontWeight: reguler,
                                            fontSize:
                                                getProportionateScreenWidth(
                                                    14))))),
                            SizedBox(height: getProportionateScreenHeight(48)),
                            Container(
                                // padding: EdgeInsets.only(
                                //     left: getProportionateScreenWidth(5),
                                //     top: getProportionateScreenWidth(5)),
                                alignment: Alignment.center,
                                padding: EdgeInsets.symmetric(
                                    horizontal:
                                        getProportionateScreenWidth(30)),
                                child: RichText(
                                    textAlign: TextAlign.center,
                                    maxLines: 2,
                                    text: TextSpan(
                                        children: <TextSpan>[
                                          TextSpan(
                                              text:
                                                  'Dengan membuat akun, anda menyetujui '),
                                          TextSpan(
                                              text: 'S&K',
                                              style: primaryTextStyle.copyWith(
                                                  color: primaryColor),
                                              recognizer: TapGestureRecognizer()
                                                ..onTap = () {
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          TermsAndCondition(
                                                              url:
                                                                  'https://vocasia.id/home/terms_and_condition',
                                                              id: 'sk'),
                                                    ),
                                                  );
                                                }),
                                          TextSpan(text: ' dan '),
                                          TextSpan(
                                              text: 'Kebijakan Privasi',
                                              style: primaryTextStyle.copyWith(
                                                  color: primaryColor),
                                              recognizer: TapGestureRecognizer()
                                                ..onTap = () {
                                                  Navigator.push(
                                                    context,
                                                    MaterialPageRoute(
                                                      builder: (context) =>
                                                          TermsAndCondition(
                                                              url:
                                                                  'https://vocasia.id/home/privacy_policy',
                                                              id: 'prv'),
                                                    ),
                                                  );
                                                }),
                                        ],
                                        style: primaryTextStyle.copyWith(
                                            height: 2,
                                            color: tenthColor,
                                            fontWeight: reguler,
                                            fontSize:
                                                getProportionateScreenWidth(
                                                    14))))),
                            SizedBox(height: getProportionateScreenHeight(90)),
                          ],
                        ),
                      ),
                      // Padding(
                      //   padding: EdgeInsets.symmetric(horizontal: 50),
                      //   child: Text(
                      //       'Dengan membuat akun, anda menyetujui Kebijakan dan Persyaratan Privasi',
                      //       maxLines: 2,
                      //       textAlign: TextAlign.center,
                      //       style: TextStyle(
                      //           height: 2,
                      //           fontSize: getProportionateScreenWidth(14),
                      //           fontWeight: FontWeight.w400,
                      //           color: Colors.white)),
                      // ),
                    ]),
                  );
                }
              }),
        ),
      ),
    );
  }
}
