import 'package:flutter/material.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/widgets/login_regist/default_button.dart';
import 'package:initial_folder/widgets/login_regist/header.dart';
import 'success_screen.dart';
import 'package:initial_folder/helper/validator.dart';
import 'package:initial_folder/widgets/login_regist/custom_text_form_field.dart';

class ResetScreen extends StatefulWidget {
  static String routeName = "/reset";
  ResetScreen({Key? key, this.email = ''}) : super(key: key);
  final String email;

  @override
  State<ResetScreen> createState() => _ResetScreenState();
}

class _ResetScreenState extends State<ResetScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final TextEditingController emailController =
        TextEditingController(text: widget.email);
    SizeConfig().init(context);
    void _validateInputs() {
      if (this._formKey.currentState!.validate()) {
        Navigator.pushNamed(context, ResetSuccess.routeName);
      }
    }

    Widget form() {
      Widget resetEmailInput() {
        return CustomTextField(
          pad: getProportionateScreenWidth(16),
          controler: emailController,
          hinttext: 'Masukkan email',
          title: 'Email',
          validate: validateEmail,
        );
      }

      Widget button() {
        return Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(15)),
          child: DefaultButton(
            text: 'Reset password',
            press: _validateInputs,
          ),
        );
      }

      return Form(
        key: _formKey,
        child: Column(
          children: [resetEmailInput(), button()],
        ),
      );
    }

    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              IconButton(
                onPressed: () => Navigator.of(context).pop(),
                icon: Icon(Icons.arrow_back),
                iconSize: getProportionateScreenWidth(18),
                padding: EdgeInsets.all(getProportionateScreenWidth(8)),
              ),
              Container(
                child: Align(
                  alignment: Alignment.center,
                  child: Header(
                      title: false,
                      text2: "Reset Password",
                      text:
                          'Masukkan email yang terhubung ke akunmu dan kami akan mengirimkan link untuk mereset password',
                      style: primaryTextStyle.copyWith(
                          color: tenthColor,
                          fontWeight: reguler,
                          fontSize: getProportionateScreenWidth(12),
                          letterSpacing: 0.5)),
                ),
              ),
              SizedBox(
                height: getProportionateScreenHeight(32),
              ),
              form(),
              SizedBox(height: getProportionateScreenHeight(32)),
              // Container(
              //   // padding: EdgeInsets.only(
              //   //     left: getProportionateScreenWidth(5),
              //   //     top: getProportionateScreenWidth(5)),
              //   alignment: Alignment.center,
              //   child: RichText(
              //     text: TextSpan(
              //       children: <TextSpan>[
              //         TextSpan(text: 'Belum punya akun? '),
              //         TextSpan(
              //             text: 'Daftar',
              //             style: primaryTextStyle.copyWith(color: primaryColor),
              //             recognizer: TapGestureRecognizer()
              //               ..onTap = () {
              //                 Navigator.pushNamed(
              //                     context, RegistrationScreen.routeName);
              //               }),
              //       ],
              //       style: primaryTextStyle.copyWith(
              //         color: tenthColor,
              //         fontWeight: reguler,
              //         fontSize: getProportionateScreenWidth(14),
              //       ),
              //     ),
              //   ),
              // ),
              SizedBox(height: getProportionateScreenHeight(32)),
              // Container(
              //     // padding: EdgeInsets.only(
              //     //     left: getProportionateScreenWidth(5),
              //     //     top: getProportionateScreenWidth(5)),
              //     alignment: Alignment.center,
              //     padding: EdgeInsets.symmetric(
              //         horizontal: getProportionateScreenWidth(30)),
              //     child: RichText(
              //         textAlign: TextAlign.center,
              //         maxLines: 2,
              //         text: TextSpan(
              //             children: <TextSpan>[
              //               TextSpan(
              //                   text: 'Dengan membuat akun, anda menyetujui '),
              //               TextSpan(
              //                   text: 'Kebijakan',
              //                   style: primaryTextStyle.copyWith(
              //                       color: primaryColor),
              //                   recognizer: TapGestureRecognizer()
              //                     ..onTap = () {
              //                       print('Kebijakan');
              //                     }),
              //               TextSpan(text: ' dan '),
              //               TextSpan(
              //                   text: 'Persyaratan Privasi',
              //                   style: primaryTextStyle.copyWith(
              //                       color: primaryColor),
              //                   recognizer: TapGestureRecognizer()
              //                     ..onTap = () {
              //                       print('Persyaratan Privasi');
              //                     }),
              //             ],
              //             style: primaryTextStyle.copyWith(
              //                 height: 2,
              //                 color: tenthColor,
              //                 fontWeight: reguler,
              //                 fontSize: getProportionateScreenWidth(14))))),
            ],
          ),
        ),
      ),
    );
  }
}
