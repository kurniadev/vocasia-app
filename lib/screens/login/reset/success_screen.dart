import 'package:flutter/material.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/widgets/login_regist/header.dart';
import 'package:flutter/gestures.dart';

class ResetSuccess extends StatelessWidget {
  static String routeName = "/reset_success";
  ResetSuccess({Key? key}) : super(key: key);

  final TextEditingController nameController = TextEditingController(text: '');
  final TextEditingController emailController = TextEditingController(text: '');
  final TextEditingController passwordController =
      TextEditingController(text: '');
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
            Icon(
              Icons.check_rounded,
              color: Colors.greenAccent[700],
              size: getProportionateScreenWidth(60),
            ),
            SizedBox(height: getProportionateScreenHeight(24)),
            Header(
                title: false,
                text2: "Sukses",
                text:
                    'Kami telah mengirimkan link untuk mereset password ke email anda',
                style: primaryTextStyle.copyWith(
                    color: tenthColor,
                    fontWeight: reguler,
                    fontSize: getProportionateScreenWidth(14),
                    letterSpacing: 0.5)),
            SizedBox(height: getProportionateScreenHeight(44)),
            Container(
              // padding: EdgeInsets.only(
              //     left: getProportionateScreenWidth(5),
              //     top: getProportionateScreenWidth(5)),
              alignment: Alignment.center,
              child: RichText(
                text: TextSpan(
                  children: <TextSpan>[
                    TextSpan(text: 'Tidak menerima link? '),
                    TextSpan(
                        text: 'Kirim Ulang',
                        style: primaryTextStyle.copyWith(color: primaryColor),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            //Navigator.pushNamed(
                            //    context, RegistrationScreen.routeName);
                            print('resend');
                          }),
                  ],
                  style: primaryTextStyle.copyWith(
                    color: tenthColor,
                    fontWeight: reguler,
                    fontSize: getProportionateScreenWidth(14),
                  ),
                ),
              ),
            ),
            SizedBox(height: getProportionateScreenHeight(8)),
            Container(
              // padding: EdgeInsets.only(
              //     left: getProportionateScreenWidth(5),
              //     top: getProportionateScreenWidth(5)),
              alignment: Alignment.center,
              child: Text(
                "atau",
                style: primaryTextStyle.copyWith(
                  color: fourthColor,
                  fontWeight: reguler,
                  fontSize: getProportionateScreenWidth(12),
                ),
              ),
            ),
            SizedBox(height: getProportionateScreenHeight(8)),
            Container(
              // padding: EdgeInsets.only(
              //     left: getProportionateScreenWidth(5),
              //     top: getProportionateScreenWidth(5)),
              alignment: Alignment.center,
              child: RichText(
                text: TextSpan(
                  children: <TextSpan>[
                    TextSpan(
                        text: 'Masuk',
                        style: primaryTextStyle.copyWith(
                          color: primaryColor,
                          fontWeight: reguler,
                          fontSize: getProportionateScreenWidth(14),
                        ),
                        recognizer: TapGestureRecognizer()
                          ..onTap = () {
                            Navigator.of(context).pop();
                            Navigator.of(context).pop();
                            // Navigator.of(context).pushNamedAndRemoveUntil(
                            //     LoginEmail.routeName,
                            //     (Route<dynamic> route) => false);
                            //Navigator.pushNamed(context, LoginEmail.routeName);
                            print('masuk');
                          }),
                  ],
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
