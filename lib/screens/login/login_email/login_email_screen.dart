import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:initial_folder/helper/user_info.dart';
import 'package:initial_folder/helper/validator.dart';
import 'package:initial_folder/providers/auth_provider.dart';
import 'package:initial_folder/providers/carts_provider.dart';
import 'package:initial_folder/providers/my_course_provider.dart';
import 'package:initial_folder/providers/user_info_provider.dart';
import 'package:initial_folder/providers/whislist_provider.dart';
import 'package:initial_folder/screens/home/home_screen.dart';
import 'package:initial_folder/screens/login/login_screen.dart';
import 'package:initial_folder/screens/registrasi/registrasi_screen.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/widgets/login_regist/custom_text_form_field.dart';
import 'package:initial_folder/widgets/login_regist/default_button.dart';
import 'package:initial_folder/widgets/login_regist/failed_login.dart';
import 'package:initial_folder/widgets/login_regist/footer.dart';
import 'package:initial_folder/widgets/login_regist/header.dart';
import 'package:initial_folder/widgets/login_regist/loading_button.dart';
import 'package:provider/provider.dart';
import '../reset/reset_screen.dart';
import 'package:initial_folder/screens/splash/splash_screen_login.dart';

class LoginEmail extends StatefulWidget {
  final _LoginEmailState loginEmailState = _LoginEmailState();

  static String routeName = "/login_email";
  LoginEmail({Key? key}) : super(key: key);

  @override
  _LoginEmailState createState() => loginEmailState;
}

class _LoginEmailState extends State<LoginEmail> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  final TextEditingController emailController = TextEditingController(text: '');

  final TextEditingController passwordController =
      TextEditingController(text: '');
  bool isLoading = false;
  bool failed = false;
  bool _isObscure = true;
  @override
  void dispose() {
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);

    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    // WishlistProvider wishlistProvider = Provider.of<WishlistProvider>(context);

    //ShowHidePassword showHidePassword = Provider.of<ShowHidePassword>(context);

    handleSignIn() async {
      setState(() {
        isLoading = true;
      });

      if (await authProvider.login(
        email: emailController.text,
        password: passwordController.text,
      )) {
        Condition.loginEmail = true;

        await Provider.of<UserInfoProvider>(context, listen: false)
            .getUserInfo(emailController.text);

        await Provider.of<CartsProvider>(context, listen: false).getCarts();
        await Provider.of<WishlistProvider>(context, listen: false)
            .getWishlist();
        await Provider.of<MyCourseProvider>(context, listen: false)
            .getMyCourse();
        await UsersInfo().setEmail(emailController.text);

        // await Provider.of<WishlistDatabaseProvider>(context, listen: false)
        //     .getWishlistDatabase();
        Navigator.of(context).pushNamedAndRemoveUntil(
            HomeScreen.routeName, (Route<dynamic> route) => false);
      } else {
        setState(() {
          failed = true;
        });
      }

      setState(() {
        isLoading = false;
      });
    }

    void _validateInputs() {
      if (this._formKey.currentState!.validate()) {
        handleSignIn();
      }
    }

    Widget form() {
      Widget emailInput() {
        return CustomTextField(
          pad: getProportionateScreenWidth(16),
          controler: emailController,
          hinttext: 'Masukkan email',
          title: 'Email',
          validate: validateEmail,
        );
      }

      Widget passwordInput() {
        return CustomTextField(
          pad: getProportionateScreenWidth(16),
          controler: passwordController,
          hinttext: 'Masukkan password',
          title: 'Password',
          obscuretext: _isObscure,
          validate: validatePassword,
          suffix: GestureDetector(
              onTap: () => setState(() {
                    _isObscure = !_isObscure;
                  }),
              child: _isObscure
                  ? Icon(
                      FeatherIcons.eyeOff,
                      color: secondaryColor,
                      size: 18,
                    )
                  : Icon(
                      FeatherIcons.eye,
                      color: secondaryColor,
                      size: 18,
                    )),
        );
      }

      Widget forgotPassword() {
        return Column(
          children: [
            //SizedBox(height: getProportionateScreenHeight(4)),
            Container(
              padding: EdgeInsets.only(right: getProportionateScreenWidth(12)),
              alignment: AlignmentDirectional.topEnd,
              child: TextButton(
                child: Text('Lupa password?',
                    style: primaryTextStyle.copyWith(
                        color: secondaryColor,
                        fontWeight: reguler,
                        fontSize: getProportionateScreenWidth(14))),
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (context) => ResetScreen(
                        email: emailController.text,
                      ),
                    ),
                  );
                },
              ),
            ),
            SizedBox(height: getProportionateScreenHeight(12)),
          ],
        );
      }

      Widget button() {
        return Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(15)),
          child: isLoading
              ? LoadingButton()
              : DefaultButton(
                  text: 'Masuk',
                  press: _validateInputs,
                ),
        );
      }

      return Form(
        key: _formKey,
        child: Column(
          children: [emailInput(), passwordInput(), forgotPassword(), button()],
        ),
      );
    }

    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              IconButton(
                color: Colors.white,
                onPressed: () => Navigator.of(context).pushNamedAndRemoveUntil(
                    LoginScreen.routeName, (Route<dynamic> route) => false),
                //Navigator.pushNamed(context, LoginScreen.routeName),
                icon: Icon(Icons.arrow_back),
                iconSize: getProportionateScreenWidth(18),
                padding: EdgeInsets.all(getProportionateScreenWidth(10)),
              ),
              Container(
                child: Align(
                  alignment: Alignment.center,
                  child: failed
                      ? FailedLogin(
                          style: primaryTextStyle.copyWith(
                              color: sevenColor, letterSpacing: 0.5),
                          text:
                              'Terdapat kendala dalam login, periksa kembali email dan password anda',
                        )
                      : Header(
                          text: 'Lanjutkan belajar bersama Vocasia',
                          style: primaryTextStyle.copyWith(
                              color: tenthColor,
                              fontWeight: reguler,
                              fontSize: getProportionateScreenWidth(14),
                              letterSpacing: 0.5)),
                ),
              ),
              SizedBox(
                height: getProportionateScreenHeight(32),
              ),
              form(),
              SizedBox(height: getProportionateScreenHeight(32)),
              Footer(
                textOne: 'Belum punya Akun? ',
                textTwo: 'Daftar',
                route: RegistrationScreen.routeName,
              )
            ],
          ),
        ),
      ),
    );
  }
}
