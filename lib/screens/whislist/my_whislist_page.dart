import 'package:flutter/material.dart';
import 'package:initial_folder/providers/whislist_provider.dart';
import 'package:initial_folder/screens/home/components/body_comp/latest_course.dart';
import 'package:initial_folder/screens/home/components/body_comp/populer_course.dart';
import 'package:initial_folder/screens/splash/splash_screen_login.dart';
import 'package:initial_folder/widgets/wishlist_page.dart';
import 'package:provider/provider.dart';
import '../../size_config.dart';
import '../../theme.dart';

class WishlistPage extends StatelessWidget {
  const WishlistPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget noWishlist() {
      return Center(
        child: Container(
          margin: EdgeInsets.symmetric(
            horizontal: getProportionateScreenWidth(40),
          ),
          child: Text(
            'Kamu belum login, silahkan login untuk melihat wishlist',
            textAlign: TextAlign.center,
            style: primaryTextStyle,
          ),
        ),
      );
    }

    Widget wishlist() {
      return Consumer<WishlistProvider>(
        builder: (BuildContext context, state, _) {
          if (state.state == ResultState.Loading) {
            return Center(
              child: CircularProgressIndicator(
                color: secondaryColor,
                strokeWidth: 2,
              ),
            );
          } else if (state.state == ResultState.NoData) {
            return SingleChildScrollView(
              child: Center(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(height: 48),
                    Container(
                      width: getProportionateScreenWidth(100),
                      height: getProportionateScreenWidth(100),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(2),
                        image: DecorationImage(
                          fit: BoxFit.fill,
                          image: AssetImage("assets/images/kursuskosong.png"),
                        ),
                      ),
                    ),
                    SizedBox(height: 16),
                    Text(
                      "Tidak ada wishlist",
                      style: secondaryTextStyle.copyWith(
                        letterSpacing: 1,
                        fontWeight: semiBold,
                        fontSize: getProportionateScreenWidth(14),
                        color: tenthColor,
                      ),
                    ),
                    SizedBox(height: 4),
                    Container(
                      padding: EdgeInsets.symmetric(
                          horizontal: getProportionateScreenWidth(16)),
                      child: Text(
                        "Kamu belum memiliki wishlist, cari kursus sekarang untuk menyimpan kursus yang ingin dibeli nanti",
                        textAlign: TextAlign.center,
                        style: primaryTextStyle.copyWith(
                          letterSpacing: 0.5,
                          fontWeight: reguler,
                          fontSize: getProportionateScreenWidth(12),
                          color: secondaryColor,
                        ),
                      ),
                    ),
                    SizedBox(height: 16),
                    PopulerCourse(),
                    LatestCourse(),
                  ],
                ),
              ),
            );
          } else if (state.state == ResultState.HasData) {
            return RefreshIndicator(
              displacement: 40,
              color: primaryColor,
              onRefresh: () async {
                await Provider.of<WishlistProvider>(context, listen: false)
                    .getWishlist();
              },
              child: Container(
                padding: EdgeInsets.only(
                  right: getProportionateScreenWidth(20),
                ),
                child: GridView.builder(
                    shrinkWrap: true,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      childAspectRatio: 2.8 / 4,
                    ),
                    itemCount: state.result!.data[0].length,
                    itemBuilder: (context, index) {
                      var wishlistCourse = state.result!.data[0][index];
                      return Column(
                        children: [
                          MyWishlistPage(
                            wishlistDataModel: wishlistCourse,
                          ),
                        ],
                      );
                    }),
              ),
            );
          } else if (state.state == ResultState.Error) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('Coba Lagi'),
                IconButton(
                    onPressed: () async {
                      await Provider.of<WishlistProvider>(context,
                              listen: false)
                          .getWishlist();
                    },
                    icon: Icon(Icons.refresh))
              ],
            );
          } else {
            return Center(child: Text(''));
          }
        },
      );
    }

    return Scaffold(
      appBar: AppBar(
        title: Text('Wishlist',
            style: secondaryTextStyle.copyWith(
                fontWeight: semiBold,
                letterSpacing: 2.3,
                fontSize: getProportionateScreenWidth(16))),
      ),
      body: Container(
          child: (Condition.loginEmail || Condition.loginFirebase)
              ? wishlist()
              : noWishlist()),
    );
  }
}
