import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:initial_folder/helper/validator.dart';
import 'package:shimmer/shimmer.dart';

import '../../size_config.dart';
import '../../theme.dart';

class WishlistCard extends StatelessWidget {
  const WishlistCard({
    Key? key,
    this.width = 120,
    this.pad = 20,
    required this.thumbnail,
    required this.id,
    required this.isTopCourse,
    required this.title,
    required this.instructorName,
    required this.rating,
    required this.specificRating,
    required this.numberOfRatings,
    required this.price,
    required this.realPrice,
    required this.press,
  }) : super(key: key);

  final double width;
  final double pad;
  final String thumbnail,
      title,
      instructorName,
      price,
      realPrice,
      rating,
      specificRating,
      numberOfRatings,
      id,
      isTopCourse;
  final VoidCallback press;
  // final VoidCallback? whislistPress;
  // final Widget? iconWishlist;

  @override
  Widget build(BuildContext context) {
    //CourseProvider courseProvider = Provider.of<CourseProvider>(context);
    // final formatCurrency =
    //     new NumberFormat.simpleCurrency(decimalDigits: 0, locale: 'id_ID');
    return Container(
      // decoration: BoxDecoration(
      //   borderRadius: BorderRadius.circular(30),
      // ),
      //border: Border.all(width: 2.0, color: secondaryColor)),
      width: getProportionateScreenWidth(170),
      padding: EdgeInsets.only(left: getProportionateScreenWidth(pad)),
      child: InkWell(
        onTap: press,
        child: Container(
          width: getProportionateScreenWidth(width),
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Stack(children: [
              AspectRatio(
                aspectRatio: 1.7,
                child: CachedNetworkImage(
                  imageUrl: thumbnail,
                  imageBuilder: (context, imageProvider) => Container(
                    decoration: BoxDecoration(
                      borderRadius:
                          BorderRadius.vertical(top: Radius.circular(5)),
                      image: DecorationImage(
                        image: imageProvider,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  placeholder: (context, url) => Shimmer(
                      child: Container(
                        color: thirdColor,
                      ),
                      gradient: LinearGradient(stops: [
                        0.4,
                        0.5,
                        0.6
                      ], colors: [
                        secondaryColor,
                        thirdColor,
                        secondaryColor
                      ])),
                  errorWidget: (context, url, error) => Icon(Icons.error),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: Align(
                  alignment: Alignment.topRight,
                  child: CircleAvatar(
                    maxRadius: 10,
                    backgroundColor: Color(0xffFFFFFF).withOpacity(0.6),
                  ),
                ),
              ),
            ]),
            SizedBox(height: getProportionateScreenWidth(4)),
            Container(
              padding: EdgeInsets.only(left: 2),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    title,
                    style: secondaryTextStyle.copyWith(
                        letterSpacing: 1,
                        fontSize: getProportionateScreenWidth(12),
                        color: tenthColor,
                        fontWeight: semiBold),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  SizedBox(height: getProportionateScreenWidth(6)),
                  // Row(
                  //   children: [product.description],
                  // ),
                  RichText(
                    text: new TextSpan(
                      // Note: Styles for TextSpans must be explicitly defined.
                      // Child text spans will inherit styles from parent
                      style: primaryTextStyle.copyWith(
                          fontSize: getProportionateScreenWidth(10),
                          color: secondaryColor),
                      children: <TextSpan>[
                        new TextSpan(
                          text: 'Oleh ',
                          style: TextStyle(
                            fontWeight: reguler,
                          ),
                        ),
                        new TextSpan(
                          text: instructorName,
                          style: TextStyle(fontWeight: semiBold),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: getProportionateScreenWidth(6)),
                  Row(
                    children: [
                      RatingBarIndicator(
                          itemSize: getProportionateScreenWidth(11),
                          rating: double.parse(rating),
                          direction: Axis.horizontal,
                          itemCount: 5,
                          //itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                          itemBuilder: (context, _) => FaIcon(
                              FontAwesomeIcons.solidStar,
                              color: primaryColor)),
                      SizedBox(
                        width: getProportionateScreenWidth(3),
                      ),
                      Text(
                        specificRating,
                        style: primaryTextStyle.copyWith(
                          fontSize: getProportionateScreenWidth(10),
                          color: secondaryColor,
                          fontWeight: reguler,
                        ),
                      ),
                      SizedBox(
                        width: getProportionateScreenWidth(6),
                      ),
                      Text(
                        '(' + numberOfRatings + ')',
                        style: primaryTextStyle.copyWith(
                          fontSize: getProportionateScreenWidth(10),
                          color: secondaryColor,
                          fontWeight: reguler,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: getProportionateScreenWidth(6),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text(
                        // "${formatCurrency.format(product.price)}",
                        // "${formatCurrency.format(1000000)}",
                        price,
                        style: primaryTextStyle.copyWith(
                          letterSpacing: 0.5,
                          fontSize: SizeConfig.blockHorizontal! * 3,
                          fontWeight: reguler,
                          color: tenthColor,
                        ),
                      ),
                      (isTopCourse == 1.toString())
                          ? Container(
                              alignment: Alignment.center,
                              width: getProportionateScreenWidth(48),
                              //height: getProportionateScreenWidth(28),
                              child: Text(
                                'Populer',
                                style: primaryTextStyle.copyWith(
                                    letterSpacing: 0.5,
                                    color: Colors.black,
                                    fontSize: SizeConfig.blockHorizontal! * 2.5,
                                    fontWeight: reguler),
                              ),
                              decoration: BoxDecoration(
                                color: Color(0xffECEB98),
                                borderRadius: BorderRadius.circular(5),
                                //border: Border.all(color: kPrimaryColor),
                              ),
                            )
                          : SizedBox(height: 0, width: 0),
                      // InkWell(
                      //   borderRadius: BorderRadius.circular(50),
                      //   onTap: () {},
                      //   child: Container(
                      //     padding: EdgeInsets.all(getProportionateScreenWidth(8)),
                      //     height: getProportionateScreenWidth(28),
                      //     width: getProportionateScreenWidth(28),

                      //     child: Icon(Icons.favorite,color: sevenColor, size: 10,)
                      //     ),
                      //   ),
                    ],
                  ),
                  SizedBox(height: getProportionateScreenWidth(6)),
                  (realPrice != 0.toString())
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              // "${formatCurrency.format(product.realPrice)}",
                              realPrice,
                              style: primaryTextStyle.copyWith(
                                decoration: TextDecoration.lineThrough,
                                fontSize: getProportionateScreenWidth(10),
                                fontWeight: reguler,
                                color: secondaryColor,
                              ),
                            ),
                            // IconButton(
                            //     onPressed: whislistPress,
                            //     icon: iconWishlist)
                          ],
                        )
                      : SizedBox(height: 13, width: 1),
                ],
              ),
            )
          ]),
        ),
      ),
    );
  }
}
