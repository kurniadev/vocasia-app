import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:initial_folder/providers/page_provider.dart';
import 'package:initial_folder/screens/course/my_course_page.dart';
import 'package:initial_folder/screens/profile/profile_page.dart';
import 'package:initial_folder/screens/search_course/search_page.dart';
import 'package:initial_folder/screens/whislist/my_whislist_page.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:provider/provider.dart';
import 'components/home_page.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);
  static String routeName = "/home";

  @override
  Widget build(BuildContext context) {
    // final bodyList = [
    //   HomePage(),
    //   SearchPage(),
    //   MyCoursePage(),
    //   WishlistPage(),
    //   ProfilePage()
    // ];
    PageProvider pageProvider = Provider.of<PageProvider>(context);
    DateTime backButtonPressTime = DateTime.now();

    SizeConfig().init(context);
    Future<bool> handleWillPop(BuildContext context) async {
      final now = DateTime.now();
      final backButtonHasNotBeenPressedOrSnackBarHasBeenClosed =
          DateTime.now().difference(backButtonPressTime) >=
              Duration(milliseconds: 500);

      if (backButtonHasNotBeenPressedOrSnackBarHasBeenClosed) {
        backButtonPressTime = now;
        ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(
            width: 240,
            duration: Duration(seconds: 2),
            backgroundColor: secondaryColor,
            content: Text(
              'Tekan sekali lagi untuk keluar',
              style: primaryTextStyle.copyWith(
                  color: backgroundColor, fontSize: 12),
              textAlign: TextAlign.center,
            ),
            behavior: SnackBarBehavior.floating,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
          ),
        );
        return false;
      }

      return true;
    }

    Widget customBottomNavigation() {
      final Color inActiveIconColor = fourthColor;
      final Color activeIconColor = tenthColor;
      return Container(
        decoration: BoxDecoration(
          border: Border(top: BorderSide(color: fourthColor)),
          boxShadow: [
            BoxShadow(blurRadius: 11, offset: Offset(-3, 10), spreadRadius: 2)
          ],
        ),
        child: BottomNavigationBar(
            fixedColor: Colors.white,
            unselectedLabelStyle:
                primaryTextStyle.copyWith(letterSpacing: 0.5, fontSize: 10),
            selectedLabelStyle: primaryTextStyle.copyWith(
                letterSpacing: 0.5, fontSize: 10, fontWeight: semiBold),
            type: BottomNavigationBarType.fixed,
            currentIndex: pageProvider.currentIndex,
            onTap: (value) {
              pageProvider.currentIndex = value;
            },
            items: [
              BottomNavigationBarItem(
                  icon: Container(
                      margin: EdgeInsets.only(bottom: 4, top: 4),
                      height: 20,
                      width: 20,
                      child: Icon(FeatherIcons.home,
                          color: pageProvider.currentIndex == 0
                              ? activeIconColor
                              : inActiveIconColor,
                          size: 22)),
                  label: 'Beranda'),
              BottomNavigationBarItem(
                  icon: Container(
                    margin: EdgeInsets.only(bottom: 4, top: 4),
                    height: 20,
                    width: 20,
                    child: Icon(FeatherIcons.search,
                        color: pageProvider.currentIndex == 1
                            ? activeIconColor
                            : inActiveIconColor,
                        size: 22),
                  ),
                  label: 'Pencarian'),
              BottomNavigationBarItem(
                icon: Container(
                  margin: EdgeInsets.only(bottom: 4, top: 4),
                  height: 20,
                  width: 20,
                  child: Icon(FeatherIcons.playCircle,
                      color: pageProvider.currentIndex == 2
                          ? activeIconColor
                          : inActiveIconColor,
                      size: 22),
                ),
                label: 'Kursusku',
              ),
              BottomNavigationBarItem(
                icon: Container(
                  margin: EdgeInsets.only(bottom: 4, top: 4),
                  height: 20,
                  width: 20,
                  child: Icon(
                    FeatherIcons.heart,
                    color: pageProvider.currentIndex == 3
                        ? activeIconColor
                        : inActiveIconColor,
                    size: 22,
                  ),
                ),
                label: 'Wishlist',
              ),
              BottomNavigationBarItem(
                icon: Container(
                  margin: EdgeInsets.only(bottom: 4, top: 4),
                  height: 20,
                  width: 20,
                  child: Icon(FeatherIcons.user,
                      color: pageProvider.currentIndex == 4
                          ? activeIconColor
                          : inActiveIconColor,
                      size: 22),
                ),
                label: 'Profile',
              ),
            ]),
      );
    }

    Widget body() {
      switch (pageProvider.currentIndex) {
        case 0:
          return HomePage(
            key: PageStorageKey<String>('homepage'),
          );
        case 1:
          return SearchPage();
        case 2:
          return MyCoursePage();
        case 3:
          return WishlistPage();
        case 4:
          return ProfilePage();
        default:
          return HomePage(
            key: PageStorageKey<String>('homepage'),
          );
      }
    }

    return WillPopScope(
      onWillPop: () => handleWillPop(context),
      child: Scaffold(
        body: body(),
        bottomNavigationBar: customBottomNavigation(),
      ),
    );
  }
}
