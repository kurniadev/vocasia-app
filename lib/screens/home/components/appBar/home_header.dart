import 'package:flutter/material.dart';
import 'package:flutter_feather_icons/flutter_feather_icons.dart';
import 'package:initial_folder/providers/carts_provider.dart';
import 'package:initial_folder/screens/cart/cart_page.dart';
import 'package:initial_folder/screens/home/components/notifikasi.dart';
import 'package:initial_folder/theme.dart';
import 'package:provider/provider.dart';
import '../../../../size_config.dart';
import 'icon_btn_with_counter.dart';
import 'package:initial_folder/helper/user_info.dart';
import 'package:initial_folder/screens/login/login_screen.dart';
import 'package:initial_folder/screens/splash/splash_screen_login.dart';

class HomeHeader extends StatefulWidget {
  const HomeHeader({
    Key? key,
  }) : super(key: key);

  @override
  State<HomeHeader> createState() => _HomeHeaderState();
}

class _HomeHeaderState extends State<HomeHeader> {
  // @override
  // void initState() {
  //   // TODO: implement initState
  //   init();
  //   super.initState();
  // }

  // init() async {
  //   await Provider.of<CartsProvider>(context, listen: false).getCarts();
  // }

  @override
  Widget build(BuildContext context) {
    double StatusBarHeight = MediaQuery.of(context).padding.top;
    Future _showDialogNotLogin(String teks) {
      return showDialog(
        context: context,
        builder: (context) => AlertDialog(
          contentPadding: EdgeInsets.fromLTRB(12, 20, 12, 1),
          content: Text(
            'Mohon login terlebih dahulu sebelum $teks',
            style: primaryTextStyle.copyWith(
                fontSize: getProportionateScreenWidth(12), letterSpacing: 1),
          ),
          actions: [
            GestureDetector(
              onTap: () {
                Navigator.of(context).pop();
              },
              child: Text('Batal',
                  style: primaryTextStyle.copyWith(
                      fontSize: getProportionateScreenWidth(12),
                      letterSpacing: 1,
                      color: primaryColor)),
            ),
            SizedBox(
              width: getProportionateScreenWidth(5),
            ),
            GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamedAndRemoveUntil(
                    LoginScreen.routeName, (Route<dynamic> route) => false);
              },
              child: Text('Login',
                  style: primaryTextStyle.copyWith(
                      fontSize: getProportionateScreenWidth(12),
                      letterSpacing: 1,
                      color: primaryColor)),
            ),
          ],
        ),
      );
    }

    handleNotLoginCart() async {
      var token = await UsersInfo().getToken();
      if (token != null || Condition.loginFirebase == true) {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => CartPage(),
          ),
        );
      } else {
        String teks = 'dapat mengakses keranjang';
        return _showDialogNotLogin(teks);
      }
    }

    handleNotLoginNotif() async {
      var token = await UsersInfo().getToken();
      if (token != null || Condition.loginFirebase == true) {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => Notifikasi(),
          ),
        );
      } else {
        String teks = 'dapat mengakses notifikasi';
        return _showDialogNotLogin(teks);
      }
    }

    return Container(
      // color: primaryColor,
      height: getProportionateScreenHeight(60),
      // padding: EdgeInsets.only(top: StatusBarHeight),
      //color: Color(0XFF071014),
      child: Center(
        child: Row(
          //crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //backgroundColor: Color(0XFFcfe9ee),
          //backgroundColor: Colors.white,
          //elevation: 0.0,
          //automaticallyImplyLeading: false,
          //title: Padding(
          //   padding: EdgeInsets.only(left: getProportionateScreenHeight(4)),
          //   child: Transform.scale(
          //     origin: Offset(-50, 0),
          //     scale: getProportionateScreenHeight(1),
          //     child: Image.asset(
          //       'assets/images/Logo2.png',
          //       width: 100,
          //       height: 50,
          //     ),
          //   ),
          // ),
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(left: getProportionateScreenWidth(23)),
              child: Transform.scale(
                origin: Offset(-50, 0),
                scale: getProportionateScreenHeight(1),
                child: Image.asset(
                  'assets/images/VOCASIA logo.png',
                  width: 100,
                  height: 50,
                ),
              ),
            ),
            Row(
              children: [
                !Condition.loginEmail
                    ? Transform.scale(
                        origin: Offset(0, 0),
                        scale: getProportionateScreenHeight(1),
                        child: Container(
                          padding: EdgeInsets.fromLTRB(
                              getProportionateScreenHeight(3),
                              0,
                              getProportionateScreenHeight(3),
                              0),
                          child: IconBtnWithCounter(
                            numOfitem: 0,
                            icon: FeatherIcons.shoppingCart,
                            press: () => handleNotLoginCart(),
                          ),
                        ),
                      )
                    : Transform.scale(
                        origin: Offset(0, 0),
                        scale: getProportionateScreenHeight(1),
                        child: Container(
                          padding: EdgeInsets.fromLTRB(
                              getProportionateScreenHeight(3),
                              0,
                              getProportionateScreenHeight(3),
                              0),
                          child: Consumer<CartsProvider>(
                              builder: (context, state, _) {
                            return IconBtnWithCounter(
                              numOfitem:
                                  state.result == null ? 0 : state.lenght,
                              icon: FeatherIcons.shoppingCart,
                              press: () => handleNotLoginCart(),
                            );
                          }),
                        ),
                      ),
                SizedBox(width: SizeConfig.blockHorizontal! * 0.5),
                Transform.scale(
                  origin: Offset(-11, 0),
                  scale: getProportionateScreenHeight(0.9),
                  child: Container(
                    padding: EdgeInsets.fromLTRB(
                        getProportionateScreenHeight(3),
                        0,
                        getProportionateScreenHeight(4),
                        0),
                    child: IconBtnWithCounter(
                      icon: FeatherIcons.bell,
                      press: () {
                        handleNotLoginNotif();
                      },
                    ),
                  ),
                ),
                Padding(
                  padding:
                      EdgeInsets.only(left: SizeConfig.blockHorizontal! * 4),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
//  Consumer<CartsProvider>(
//                         builder: (context, state, _) {
//                           if (state.state == ResultState.Loading) {
//                             return Transform.scale(
//                               origin: Offset(0, 0),
//                               scale: getProportionateScreenHeight(1),
//                               child: Container(
//                                 padding: EdgeInsets.fromLTRB(
//                                     getProportionateScreenHeight(3),
//                                     0,
//                                     getProportionateScreenHeight(3),
//                                     0),
//                                 child: ClipRRect(
//                                   child: Container(
//                                     width: 20,
//                                     height: 20,
//                                     color: secondaryColor,
//                                   ),
//                                 ),
//                               ),
//                             );
//                           } else if (state.state == ResultState.NoData) {
//                             return Transform.scale(
//                               origin: Offset(0, 0),
//                               scale: getProportionateScreenHeight(1),
//                               child: Container(
//                                 padding: EdgeInsets.fromLTRB(
//                                     getProportionateScreenHeight(3),
//                                     0,
//                                     getProportionateScreenHeight(3),
//                                     0),
//                                 child: IconBtnWithCounter(
//                                   numOfitem: 0,
//                                   icon: Icons.shopping_cart_outlined,
//                                   press: () => handleNotLoginCart(),
//                                 ),
//                               ),
//                             );
//                           } else if (state.state == ResultState.HasData) {
//                             return Transform.scale(
//                               origin: Offset(0, 0),
//                               scale: getProportionateScreenHeight(1),
//                               child: Container(
//                                 padding: EdgeInsets.fromLTRB(
//                                     getProportionateScreenHeight(3),
//                                     0,
//                                     getProportionateScreenHeight(3),
//                                     0),
//                                 child: Consumer<CartsProvider>(
//                                     builder: (context, state, _) {
//                                   return IconBtnWithCounter(
//                                     numOfitem: state.result!.data.length,
//                                     icon: Icons.shopping_cart_outlined,
//                                     press: () => handleNotLoginCart(),
//                                   );
//                                 }),
//                               ),
//                             );
//                           } else if (state.state == ResultState.Error) {
//                             return Transform.scale(
//                               origin: Offset(0, 0),
//                               scale: getProportionateScreenHeight(1),
//                               child: Container(
//                                 padding: EdgeInsets.fromLTRB(
//                                     getProportionateScreenHeight(3),
//                                     0,
//                                     getProportionateScreenHeight(3),
//                                     0),
//                                 child: IconBtnWithCounter(
//                                   numOfitem: 0,
//                                   icon: Icons.shopping_cart_outlined,
//                                   press: () => handleNotLoginCart(),
//                                 ),
//                               ),
//                             );
//                           } else {
//                             return Transform.scale(
//                               origin: Offset(0, 0),
//                               scale: getProportionateScreenHeight(1),
//                               child: Container(
//                                 padding: EdgeInsets.fromLTRB(
//                                     getProportionateScreenHeight(3),
//                                     0,
//                                     getProportionateScreenHeight(3),
//                                     0),
//                                 child: IconBtnWithCounter(
//                                   numOfitem: 0,
//                                   icon: Icons.shopping_cart_outlined,
//                                   press: () => handleNotLoginCart(),
//                                 ),
//                               ),
//                             );
//                           }
//                         },
//                       ),
