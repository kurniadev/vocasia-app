// import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:initial_folder/theme.dart';

class IconBtnWithCounter extends StatelessWidget {
  const IconBtnWithCounter({
    Key? key,
    required this.icon,
    this.numOfitem = 0,
    required this.press,
  }) : super(key: key);

  final IconData icon;
  final int numOfitem;
  final GestureTapCallback press;

  @override
  Widget build(BuildContext context) {
    return IconButton(
      highlightColor: Colors.transparent,
      hoverColor: Colors.transparent,
      onPressed: press,
      visualDensity: VisualDensity(horizontal: -4.0, vertical: -4.0),
      padding: EdgeInsets.zero,
      icon: Stack(
        clipBehavior: Clip.none,
        children: [
          Container(
            height: 20,
            width: 20,
            child: Icon(
              icon,
              size: 20,
            ),
          ),
          if (numOfitem != 0)
            Positioned(
              top: -6,
              right: -8,
              child: Container(
                height: 14,
                width: 16,
                decoration: BoxDecoration(
                  color: Color(0xffCD2228),
                  shape: BoxShape.circle,
                  // border: Border.all(width: 1.5, color: Colors.red),
                ),
                child: Center(
                  child: Text(
                    "$numOfitem",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 8,
                      height: 1.3,
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            )
        ],
      ),
    );
  }
}
