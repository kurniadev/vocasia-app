import 'package:flutter/material.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:initial_folder/widgets/notifikasi_list.dart';

class Notifikasi extends StatelessWidget {
  const Notifikasi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
            centerTitle: true,
            title: Text(
              'Notifikasi',
              style: secondaryTextStyle.copyWith(
                  letterSpacing: 2,
                  fontWeight: semiBold,
                  fontSize: getProportionateScreenWidth(14)),
            )),
        body: ListView(
          shrinkWrap: true,
          children: [
            NotifikasiList(berhasil: false, baru: true),
            NotifikasiList(berhasil: true, baru: true),
            NotifikasiList(berhasil: false, baru: true),
            NotifikasiList(berhasil: true),
            NotifikasiList(berhasil: true),
          ],
        ),
      ),
    );
  }
}
