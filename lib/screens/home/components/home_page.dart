import 'package:flutter/material.dart';
import 'package:initial_folder/providers/others_course_provider.dart';
import 'package:initial_folder/screens/home/components/appBar/home_header.dart';
import 'package:initial_folder/screens/home/components/body_comp/gopay_voucher.dart';
import 'package:initial_folder/screens/home/components/body_comp/latest_course.dart';
import 'package:initial_folder/screens/home/components/body_comp/populer_course.dart';
import 'package:provider/provider.dart';
import '../../../size_config.dart';
import '../../../theme.dart';
import 'body_comp/list_of_categories.dart';
import 'body_comp/others_course.dart';
import 'body_comp/program.dart';
import 'body_comp/carousel.dart';
import 'package:flutter/widgets.dart';

class HomePage extends StatefulWidget {
  static String routeName = "/homepage";
  HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  ScrollController _controller = ScrollController();
  @override
  void initState() {
    _controller.addListener(() {
      onScrol();
    });
    super.initState();
  }

  void onScrol() async {
    double maxScroll = _controller.position.maxScrollExtent;
    double currentScroll = _controller.position.pixels;
    if (maxScroll == currentScroll) {
      await Provider.of<OthersCourseProvider>(context, listen: false)
          .getOthersCourses();
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        //resizeToAvoidBottomInset: false,
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(getProportionateScreenHeight(60)),
            child: HomeHeader()),
        body: ListView(
          controller: _controller,
          children: [
            // Stack(
            //   children: [
            //     // Container(
            //     //   height: getProportionateScreenWidth(140),
            //     //   decoration: BoxDecoration(
            //     //     image: DecorationImage(
            //     //       image: AssetImage('assets/images/VectorBG.png'),
            //     //       fit: BoxFit.fill,
            //     //       alignment: AlignmentDirectional.topCenter,
            //     //     ),
            //     //   ),
            //     // ),
            //     Container(
            //         padding: EdgeInsets.fromLTRB(
            //             getProportionateScreenWidth(18.0),
            //             getProportionateScreenWidth(11.5),
            //             0,
            //             0),
            //         child: Begin()),
            //     SizedBox(height: getProportionateScreenWidth(24)),
            //     Container(
            //       // padding:
            //       //     EdgeInsets.only(top: getProportionateScreenWidth(110)),
            //       //height: getProportionateScreenWidth(33),
            //       alignment: Alignment.bottomCenter,
            //       child: SearchField(),
            //     ),
            //   ],
            // ),
            // Container(
            //     padding: EdgeInsets.fromLTRB(getProportionateScreenWidth(18.0),
            //         getProportionateScreenWidth(11.5), 0, 0),
            //     child: Begin()),
            // SizedBox(height: getProportionateScreenWidth(36)),

            //SearchField(),

            // SizedBox(height: getProportionateScreenWidth(12)),
            CarouselWithIndicatorDemo(),

            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: getProportionateScreenWidth(14)),
              child: GopayVoucher(),
            ),
            // SizedBox(height: 18),
            // Program(),

            SizedBox(height: 20),
            Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: getProportionateScreenWidth(20)),
              child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                Text('Pencarian Terpopuler',
                    textAlign: TextAlign.left,
                    style: secondaryTextStyle.copyWith(
                        letterSpacing: 1,
                        color: tenthColor,
                        fontSize: getProportionateScreenWidth(14),
                        fontWeight: semiBold)),
              ]),
            ),
            SizedBox(height: 10),
            Categories(),
            SizedBox(height: 20),
            PopulerCourse(),
            LatestCourse(),
            OthersCourse(
              key: PageStorageKey<String>('homepage'),
            ),
            Provider.of<OthersCourseProvider>(context).loading
                ? Center(
                    child: SizedBox(
                      height: 30,
                      width: 30,
                      child: CircularProgressIndicator(
                        color: secondaryColor,
                        strokeWidth: 2,
                      ),
                    ),
                  )
                : SizedBox(height: 30),
            SizedBox(height: 25),
            // ChangeNotifierProvider<CourseProvider>.value(
            //     value: CourseProvider(courseService: CourseService()),
            //     child: Course()),

            // Recommendation(),
          ],
        ),
      ),
    );
  }
}
