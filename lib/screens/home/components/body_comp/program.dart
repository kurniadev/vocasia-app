import 'package:flutter/material.dart';
import 'product_card/product_card_program.dart';
import 'package:initial_folder/models/ProductProgram.dart';

import '../../../../size_config.dart';
import '../../../../theme.dart';

class Program extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
            Text('Promo Terkini',
                textAlign: TextAlign.left,
                style: secondaryTextStyle.copyWith(
                    letterSpacing: 1,
                    color: tenthColor,
                    fontSize: getProportionateScreenWidth(14),
                    fontWeight: semiBold)),
          ]),
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: getProportionateScreenWidth(20)),
            child: Row(
              children: [
                ...List.generate(
                  demoProducts.length,
                  (index) => ProductCardProgram(product: demoProducts[index]),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }
}
