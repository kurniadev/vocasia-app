import 'package:flutter/material.dart';

import '../../../../size_config.dart';
import '../../../../theme.dart';

class SectionTitle extends StatelessWidget {
  const SectionTitle({
    Key? key,
    required this.title,
    required this.press,
  }) : super(key: key);

  final String title;
  final GestureTapCallback press;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: secondaryTextStyle.copyWith(
              letterSpacing: 1,
              color: tenthColor,
              fontSize: getProportionateScreenWidth(14),
              fontWeight: semiBold),
        ),
        GestureDetector(
          onTap: press,
          child: Text(
            "Lihat Semua",
            style: primaryTextStyle.copyWith(
                letterSpacing: 0.5,
                color: primaryColor,
                fontSize: getProportionateScreenWidth(12),
                fontWeight: reguler),
          ),
        ),
      ],
    );
  }
}
