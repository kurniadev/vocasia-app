import 'package:flutter/material.dart';
import 'package:initial_folder/helper/validator.dart';
import 'package:initial_folder/providers/latest_course_provider.dart';
import 'package:initial_folder/screens/detail_course/detail_course_screen.dart';
import 'package:initial_folder/screens/home/components/body_comp/product_card/product_card.dart';
import 'package:provider/provider.dart';

import '../../../../size_config.dart';
import '../../../../theme.dart';

class LatestCourse extends StatelessWidget {
  const LatestCourse({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(16)),
          child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
            Text('Kursus Terbaru',
                textAlign: TextAlign.left,
                style: secondaryTextStyle.copyWith(
                    letterSpacing: 1,
                    color: tenthColor,
                    fontSize: getProportionateScreenWidth(14),
                    fontWeight: semiBold)),
          ]),
        ),
        SizedBox(height: 20),
        Consumer<LatestCourseProvider>(builder: (context, state, _) {
          if (state.state == ResultState.Loading) {
            return Center(
              child: CircularProgressIndicator(
                color: secondaryColor,
                strokeWidth: 2,
              ),
            );
          } else if (state.state == ResultState.HasData) {
            return Container(
              margin: EdgeInsets.only(left: getProportionateScreenWidth(4)),
              height: getProportionateScreenHeight(215),
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                // padding:
                //     EdgeInsets.only(right: getProportionateScreenWidth(20)),
                physics: ScrollPhysics(),
                shrinkWrap: true,
                itemCount: state.result.length,
                itemBuilder: (context, index) {
                  var latestCourse = state.result[index];
                  // var finalRating = double.parse(
                  //     (topCourse.specificRating![0] / 20).toStringAsFixed(2));
                  return ProductCard(
                    pad: 12,
                    // padRight: 12,
                    id: latestCourse.idCourse,
                    thumbnail: latestCourse.thumbnail ??
                        'http://api.vocasia.pasia.id/uploads/courses_thumbnail/course_thumbnail_default_57.jpg',
                    title: latestCourse.title,
                    instructorName: latestCourse.instructorName,
                    specificRating: double.parse(
                            latestCourse.rating[0]!.avgRating != null
                                ? '${latestCourse.rating[0]!.avgRating}'
                                : '0')
                        .toString(),
                    rating: latestCourse.rating[0]!.avgRating != null
                        ? '${latestCourse.rating[0]!.avgRating}'
                        : '5.0',
                    numberOfRatings: latestCourse.rating[0]!.totalReview ?? '0',
                    isTopCourse: latestCourse.topCourse!,
                    price: (latestCourse.discountPrice == '0')
                        ? 'Gratis'
                        : numberFormat(latestCourse.discountPrice),
                    realPrice: (latestCourse.price == '0')
                        ? ''
                        : numberFormat(latestCourse.price),
                    press: () {
                      print(latestCourse.idCourse);
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => DetailCourseScreen(
                            idcourse: latestCourse.idCourse,
                          ),
                        ),
                      );
                    },
                  );
                },
              ),
            );
          } else if (state.state == ResultState.NoData) {
            return Center(child: Text(state.message));
          } else if (state.state == ResultState.Error) {
            return Center(
                child: Column(
              children: [
                Text('Terjadi Kesalahan Coba Lagi'),
              ],
            ));
          } else {
            return Center(child: Text(''));
          }
        })
      ],
    );
  }

  // @override
  // Widget build(BuildContext context) {
  //   return Column(
  //     children: [
  //       Padding(
  //         padding:
  //             EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(16)),
  //         child: Row(mainAxisAlignment: MainAxisAlignment.start, children: [
  //           Text('Kursus Terbaru',
  //               textAlign: TextAlign.left,
  //               style: secondaryTextStyle.copyWith(
  //                   letterSpacing: 1,
  //                   color: tenthColor,
  //                   fontSize: getProportionateScreenWidth(14),
  //                   fontWeight: semiBold)),
  //         ]),
  //       ),
  //       SizedBox(height: 20),
  //       Row(
  //         mainAxisAlignment: MainAxisAlignment.center,
  //         children: [
  //           Icon(Icons.construction_outlined),
  //           Text('Under Construction'),
  //         ],
  //       ),
  //     ],
  //   );
  // }
}
