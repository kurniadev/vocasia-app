import 'package:flutter/material.dart';
import 'package:initial_folder/widgets/search_and_filter_course.dart';
import '../../../../theme.dart';
import '../../../../size_config.dart';

class SearchField extends StatelessWidget {
  const SearchField({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // width: SizeConfig.screenWidth * 0.9,
    //   height: 33,
    //   decoration: BoxDecoration(
    //     color: Colors.white,
    //     borderRadius: BorderRadius.circular(15),
    //     border: Border.all(
    //       color: kSecondaryColor.withOpacity(0.5),
    //       width: 2,
    //print(SizeConfig.screenWidth);
    return GestureDetector(
      onTap: () => Navigator.push(context,
          MaterialPageRoute(builder: (context) => SearchAndFilterCourse())),
      child: Container(
        margin:
            EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(16)),
        height: 35,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: secondaryColor)),
        child: Column(
          children: [
            Expanded(
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 13),
                    child: Icon(
                      Icons.search,
                      size: 20,
                      color: secondaryColor,
                    ),
                  ),
                  SizedBox(
                    width: getProportionateScreenWidth(10),
                  ),
                  Text(
                    'Cari kursus',
                    style: secondaryTextStyle.copyWith(
                        color: secondaryColor, fontSize: 12),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
