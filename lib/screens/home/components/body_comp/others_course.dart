import 'package:flutter/material.dart';
import 'package:initial_folder/helper/validator.dart';
import 'package:initial_folder/providers/others_course_provider.dart';
import 'package:initial_folder/screens/detail_course/detail_course_screen.dart';
import 'package:initial_folder/theme.dart';
import 'package:provider/provider.dart';
import 'product_card/product_card.dart';
import '../../../../size_config.dart';
import 'section_title.dart';
import 'package:intl/intl.dart';

class OthersCourse extends StatelessWidget {
  const OthersCourse({Key? key}) : super(key: key);
  // final ScrollController _controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    // _controller.addListener(() {
    //   if (_controller.position.pixels == _controller.position.maxScrollExtent) {
    //     print('daper');
    //     Provider.of<OthersCourseProvider>(context, listen: false)
    //         .getOthersCourses();
    //   }
    // });
    return Column(
      children: [
        Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(16)),
          child: SectionTitle(title: "Kursus Lainnya", press: () {}),
        ),
        SizedBox(height: 20),
        Consumer<OthersCourseProvider>(builder: (context, state, _) {
          if (state.state == ResultState.Loading) {
            return Center(
              child: CircularProgressIndicator(
                color: secondaryColor,
                strokeWidth: 2,
              ),
            );
          } else if (state.state == ResultState.HasData) {
            return GridView.builder(
              // controller: _controller,
              padding: EdgeInsets.only(right: getProportionateScreenWidth(20)),
              physics: ScrollPhysics(),
              shrinkWrap: true,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 2.8 / 4,
                // crossAxisSpacing: 10,
                //mainAxisSpacing: 20,
                //mainAxisSpacing: 0,
              ),
              itemCount: state.result.length,
              itemBuilder: (context, index) {
                var othersCourse = state.result[index];
                // var finalRating = double.parse(
                //     (othersCourse.specificRating![0] / 20).toStringAsFixed(2));
                return ProductCard(
                  id: othersCourse.idCourse,
                  thumbnail: othersCourse.thumbnail ??
                      'http://api.vocasia.pasia.id/uploads/courses_thumbnail/course_thumbnail_default_57.jpg',
                  title: othersCourse.title,
                  instructorName: othersCourse.instructorName,
                  specificRating: double.parse(
                          othersCourse.rating[0]!.avgRating != null
                              ? '${othersCourse.rating[0]!.avgRating}'
                              : '0')
                      .toString(),
                  rating: othersCourse.rating[0]!.avgRating != null
                      ? '${othersCourse.rating[0]!.avgRating}'
                      : '5.0',
                  numberOfRatings: othersCourse.rating[0]!.totalReview ?? '0',
                  isTopCourse: othersCourse.topCourse ?? '0',
                  price: (othersCourse.discountPrice == '0')
                      ? 'Gratis'
                      : numberFormat(othersCourse.discountPrice),
                  realPrice: (othersCourse.price == '0')
                      ? ''
                      : numberFormat(othersCourse.price),
                  press: () {
                    print(othersCourse.idCourse);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => DetailCourseScreen(
                          idcourse: othersCourse.idCourse,
                        ),
                      ),
                    );
                  },
                );
              },
            );
          } else if (state.state == ResultState.NoData) {
            return Center(child: Text(state.message));
          } else if (state.state == ResultState.Error) {
            return Center(
                child: Column(
              children: [
                Text(
                  'Terjadi Kesalahan Coba Lagi',
                  style: thirdTextStyle,
                ),
              ],
            ));
          } else {
            return Center(child: Text(''));
          }
        })
      ],
    );
  }
}

// courseProvider.course
//               .map(
//                 (product) => ProductCard(
//                   product: product,
//                 ),
//               )
//               .toList(),
