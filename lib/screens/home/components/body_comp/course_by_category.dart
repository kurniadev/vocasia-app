import 'package:flutter/material.dart';
import 'package:initial_folder/helper/validator.dart';
import 'package:initial_folder/providers/course_by_category_provider.dart';
import 'package:initial_folder/screens/detail_course/components/app_bar.dart';
import 'package:initial_folder/screens/detail_course/detail_course_screen.dart';
import 'package:initial_folder/screens/home/components/body_comp/latest_course.dart';
import 'package:initial_folder/screens/home/components/body_comp/list_of_categories.dart';
import 'package:initial_folder/screens/home/components/body_comp/populer_course.dart';
import 'package:initial_folder/screens/home/components/body_comp/product_card/product_card.dart';
import 'package:initial_folder/services/course_by_category_service.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:provider/provider.dart';

class CourseByCategory extends StatelessWidget {
  final String id;
  final String name;
  const CourseByCategory({Key? key, required this.id, required this.name})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: ChangeNotifierProvider<CourseByCategoryProvider>(
            create: (context) => CourseByCategoryProvider(
              courseByCategoryService: CourseByCategoryService(),
              id: id,
            ),
            child: Container(
              // padding: EdgeInsets.only(top: getProportionateScreenWidth(20)),
              child: Consumer<CourseByCategoryProvider>(
                  builder: (context, state, _) {
                if (state.state == ResultState.Loading) {
                  return Center(
                    child: CircularProgressIndicator(
                      color: secondaryColor,
                      strokeWidth: 2,
                    ),
                  );
                } else if (state.state == ResultState.HasData) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AppBarHeader(),
                      Container(
                        margin: EdgeInsets.symmetric(
                            horizontal: getProportionateScreenWidth(16)),
                        child: Text(
                          '$name',
                          style: secondaryTextStyle.copyWith(
                            fontSize: getProportionateScreenWidth(30),
                            letterSpacing: -0.5,
                          ),
                          textAlign: TextAlign.start,
                        ),
                      ),
                      SizedBox(
                        height: getProportionateScreenHeight(32),
                      ),
                      PopulerCourse(),
                      LatestCourse(),
                      SizedBox(height: 8),
                      Padding(
                        padding:
                            EdgeInsets.all(getProportionateScreenWidth(16)),
                        child: Text('Subkategori',
                            textAlign: TextAlign.left,
                            style: secondaryTextStyle.copyWith(
                                letterSpacing: 1,
                                color: tenthColor,
                                fontSize: getProportionateScreenWidth(14),
                                fontWeight: semiBold)),
                      ),
                      Categories(),
                      GridView.builder(
                        padding: EdgeInsets.only(
                            right: getProportionateScreenWidth(20)),
                        physics: ScrollPhysics(),
                        shrinkWrap: true,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          childAspectRatio: 2.8 / 4,
                          // crossAxisSpacing: 10,
                          //mainAxisSpacing: 20,
                          //mainAxisSpacing: 0,
                        ),
                        itemCount: state.result.length,
                        itemBuilder: (context, index) {
                          var courses = state.result[index];

                          return ProductCard(
                              id: courses.idCourse,
                              thumbnail: courses.thumbnail ??
                                  'https://vocasia.id/uploads/thumbnails/course_thumbnails/course_thumbnail_default_63.jpg',
                              title: courses.title,
                              instructorName: courses.instructorName,
                              specificRating: double.parse(
                                      courses.rating[0]!.avgRating != null
                                          ? '${courses.rating[0]!.avgRating}'
                                          : '5.0')
                                  .toString(),
                              rating: courses.rating[0]!.avgRating != null
                                  ? '${courses.rating[0]!.avgRating}'
                                  : '5.0',
                              numberOfRatings:
                                  courses.rating[0]!.totalReview ?? '0',
                              isTopCourse: '0',
                              price: (courses.discountPrice == '0')
                                  ? 'Gratis'
                                  : numberFormat(courses.discountPrice),
                              realPrice: (courses.price == '0')
                                  ? ''
                                  : numberFormat(courses.price),
                              press: () async {
                                // await Hive.openBox<Wishlist>("wishlist");
                                // await Hive.openBox('carts');

                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            DetailCourseScreen(
                                              idcourse: courses.idCourse,
                                            )));
                              });
                        },
                      ),
                    ],
                  );
                } else if (state.state == ResultState.NoData) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      AppBarHeader(),
                      Container(
                        margin: EdgeInsets.symmetric(
                            horizontal: getProportionateScreenWidth(16)),
                        child: Text(
                          '$name',
                          style: secondaryTextStyle.copyWith(
                            fontSize: getProportionateScreenWidth(30),
                            letterSpacing: -0.5,
                          ),
                          textAlign: TextAlign.start,
                        ),
                      ),
                      SizedBox(
                        height: getProportionateScreenHeight(32),
                      ),
                      PopulerCourse(),
                      LatestCourse(),
                      SizedBox(height: 8),
                      Padding(
                        padding:
                            EdgeInsets.all(getProportionateScreenWidth(16)),
                        child: Text('Subkategori',
                            textAlign: TextAlign.left,
                            style: secondaryTextStyle.copyWith(
                                letterSpacing: 1,
                                color: tenthColor,
                                fontSize: getProportionateScreenWidth(14),
                                fontWeight: semiBold)),
                      ),
                      Categories(),
                    ],
                  );
                } else if (state.state == ResultState.Error) {
                  return Center(child: Text("No internet connections."));
                } else {
                  return Center(child: Text(''));
                }
              }),
            ),
          ),
        ),
      ),
    );
  }
}

// String decimalpoint(var value, int fractionDigits) {
//   return double.parse(value.toString())
//       .toDouble()
//       .toStringAsFixed(fractionDigits);
// }
