import 'package:flutter/material.dart';
import 'package:initial_folder/helper/validator.dart';
import 'package:initial_folder/providers/categories_provider.dart';
import 'package:initial_folder/screens/home/components/body_comp/course_by_category.dart';
import 'package:initial_folder/size_config.dart';
import 'package:initial_folder/theme.dart';
import 'package:provider/provider.dart';

class Categories extends StatelessWidget {
  const Categories({Key? key, this.scrollable = false}) : super(key: key);
  final bool scrollable;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: getProportionateScreenWidth(16)),
      child: Consumer<CategoriesProvider>(builder: (context, state, _) {
        if (state.state == ResultState.Loading) {
          return Center(
            child: CircularProgressIndicator(
              color: secondaryColor,
              strokeWidth: 2,
            ),
          );
        } else if (state.state == ResultState.HasData) {
          var categori = state.result;
          return scrollable
              ? SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Wrap(
                    children: [
                      for (var categoryModel in categori)
                        GestureDetector(
                          onTap: () {
                            print(categoryModel.id);
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (context) => CourseByCategory(
                                  id: categoryModel.id!,
                                  name: categoryModel.nameCategory ?? '',
                                ),
                              ),
                            );
                          },
                          child: Container(
                            margin: EdgeInsets.only(
                              right: getProportionateScreenWidth(7),
                              bottom: getProportionateScreenWidth(9),
                            ),
                            child: Chip(
                              materialTapTargetSize:
                                  MaterialTapTargetSize.shrinkWrap,
                              padding: EdgeInsets.zero,
                              labelPadding: EdgeInsets.symmetric(
                                horizontal: 7,
                              ),
                              label: Text(
                                  filterAnd(categoryModel.nameCategory ?? ''),
                                  style: primaryTextStyle.copyWith(
                                      color: tenthColor,
                                      fontSize: getProportionateScreenWidth(12),
                                      fontWeight: reguler)),
                              backgroundColor: backgroundColor,
                              side: BorderSide(color: Colors.white),
                            ),
                          ),
                        ),
                    ],
                  ),
                )
              : Wrap(
                  children: [
                    for (var categoryModel in categori)
                      GestureDetector(
                        onTap: () {
                          print(categoryModel.id);
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => CourseByCategory(
                                id: categoryModel.id!,
                                name: categoryModel.nameCategory ?? '',
                              ),
                            ),
                          );
                        },
                        child: Container(
                          margin: EdgeInsets.only(
                            right: getProportionateScreenWidth(7),
                            bottom: getProportionateScreenWidth(9),
                          ),
                          child: Chip(
                            materialTapTargetSize:
                                MaterialTapTargetSize.shrinkWrap,
                            padding: EdgeInsets.zero,
                            labelPadding: EdgeInsets.symmetric(
                              horizontal: 7,
                            ),
                            label: Text(
                                filterAnd(categoryModel.nameCategory ?? ''),
                                style: primaryTextStyle.copyWith(
                                    color: tenthColor,
                                    fontSize: getProportionateScreenWidth(12),
                                    fontWeight: reguler)),
                            backgroundColor: backgroundColor,
                            side: BorderSide(color: Colors.white),
                          ),
                        ),
                      ),
                  ],
                );
        } else if (state.state == ResultState.NoData) {
          return Center(child: Text(state.message));
        } else if (state.state == ResultState.Error) {
          return Center(child: Text("No internet connections."));
        } else {
          print(state.result.length);
          return Center(child: Text(''));
        }
      }),
    );
  }
}
