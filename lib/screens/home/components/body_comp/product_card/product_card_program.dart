import 'package:flutter/material.dart';
import 'package:initial_folder/models/ProductProgram.dart';
import '../../../../../theme.dart';
import '../../../../../size_config.dart';

class ProductCardProgram extends StatelessWidget {
  const ProductCardProgram({
    Key? key,
    this.width = 150,
    this.aspectRetio = 1.02,
    required this.product,
  }) : super(key: key);

  final double width, aspectRetio;
  final ProductProgram product;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(0, getProportionateScreenWidth(10),
          getProportionateScreenWidth(10), getProportionateScreenWidth(10)),
      child: SizedBox(
        width: getProportionateScreenWidth(width),
        child: GestureDetector(
          onTap: () {},
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              AspectRatio(
                aspectRatio: 1.7,
                child: Container(
                  decoration: BoxDecoration(
                    color: tenthColor.withOpacity(0),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Hero(
                    tag: product.id.toString(),
                    child: Image.asset(product.images[0], scale: 0.000000001),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
