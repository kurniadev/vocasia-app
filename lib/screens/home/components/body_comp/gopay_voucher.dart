import 'package:flutter/material.dart';
import '../../../../theme.dart';
import '../../../../size_config.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class GopayVoucher extends StatelessWidget {
  const GopayVoucher({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //EmailProvider emailProvider = Provider.of<EmailProvider>(context);
    double heightSet = getProportionateScreenWidth(20.23);
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: getProportionateScreenWidth(12),
      ),
      decoration: BoxDecoration(
        color: Color(0xFF212121),
        borderRadius: BorderRadius.circular(8),
      ),
      child: IntrinsicHeight(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Flexible(
              flex: 6,
              child: Row(
                children: [
                  Transform.scale(
                    scale: getProportionateScreenWidth(1.5),
                    child: Container(
                      margin:
                          EdgeInsets.only(left: getProportionateScreenWidth(5)),
                      width: getProportionateScreenWidth(60.83),
                      height: heightSet,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(2),
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage('assets/images/gopay.png'),
                        ),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      print("Sukses");
                    },
                    child: Container(
                      margin: EdgeInsets.only(
                        left: getProportionateScreenWidth(7),
                      ),
                      child: Chip(
                        padding: EdgeInsets.zero,
                        label: Text("Link now",
                            style: primaryTextStyle.copyWith(
                                color: Color(0xff181818),
                                fontSize: getProportionateScreenWidth(10),
                                fontWeight: reguler)),
                        backgroundColor: primaryColor,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Flexible(
              flex: 1,
              child: Container(
                height: heightSet,
                child: VerticalDivider(
                    color: Color(0xFF616161), thickness: 1, width: 1),
              ),
            ),
            Flexible(
              flex: 6,
              child: GestureDetector(
                onTap: () {
                  print("Sukses voucher");
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    FaIcon(FontAwesomeIcons.ticketAlt,
                        size: getProportionateScreenWidth(18),
                        color: Color(0xFFBFBFBF)),
                    SizedBox(
                      width: getProportionateScreenWidth(7),
                    ),
                    Text("You have 3 vouchers",
                        style: primaryTextStyle.copyWith(
                          letterSpacing: 0.2,
                          color: Color(0xFFBFBFBF),
                          fontSize: getProportionateScreenWidth(8),
                          fontWeight: reguler,
                        )),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
