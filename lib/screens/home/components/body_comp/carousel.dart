import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:initial_folder/models/banners_model.dart';
import 'package:initial_folder/providers/banners_provider.dart';
import 'package:initial_folder/theme.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import '../../../../size_config.dart';

// final List<String> imgList = [
//   'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
//   'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
//   'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
//   'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
//   'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
//   'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
// ];

class CarouselWithIndicatorDemo extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CarouselWithIndicatorState();
  }
}

class _CarouselWithIndicatorState extends State<CarouselWithIndicatorDemo> {
  int _current = 0;

  @override
  Widget build(BuildContext context) {
    Widget listBannerPicture(BannersModel listBanner) {
      return Container(
        // height: getProportionateScreenHeight(120),
        margin: EdgeInsets.symmetric(
          horizontal: getProportionateScreenWidth(16),
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.all(Radius.circular(10)),
          child: AspectRatio(
            aspectRatio: 2,
            child: CachedNetworkImage(
              fadeInDuration: Duration(microseconds: 1),
              imageUrl: listBanner.img,
              fit: BoxFit.cover,
              // imageBuilder: (context, imageProvider) => Container(
              //   decoration: BoxDecoration(
              //     borderRadius: BorderRadius.circular(5),
              //     image: DecorationImage(
              //       image: imageProvider,
              //       fit: BoxFit.fill,
              //     ),
              //   ),
              // ),
              placeholder: (context, url) => Shimmer(
                  child: Container(
                    decoration: BoxDecoration(
                      color: ninthColor,
                      borderRadius: BorderRadius.circular(5),
                    ),
                  ),
                  gradient: LinearGradient(
                      stops: [0.2, 0.5, 0.6],
                      colors: [ninthColor, fourthColor, ninthColor])),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
        ),
      );
    }

    // Image.network(listBanner
    //             .imageUrl,
    //                 fit: BoxFit.cover, width: 700.0, height: 200),
    return Container(
      child: Consumer<BannersProvider>(builder: (context, state, _) {
        if (state.state == ResultState.Loading) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: AspectRatio(
              aspectRatio: 2,
              child: Container(
                decoration: BoxDecoration(
                  color: ninthColor,
                  borderRadius: BorderRadius.circular(5),
                ),
              ),
            ),
          );
        } else if (state.state == ResultState.HasData) {
          return Column(children: [
            CarouselSlider(
              items: state.result
                  .map((banner) => listBannerPicture(banner))
                  .toList(),
              options: CarouselOptions(
                  disableCenter: true,
                  autoPlay: true,
                  autoPlayInterval: Duration(seconds: 10),
                  enlargeCenterPage: true,
                  viewportFraction: 1,
                  aspectRatio: 2,
                  onPageChanged: (index, reason) {
                    setState(() {
                      _current = index;
                    });
                  }),
            ),
            // SizedBox(height: 5),
            Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: state.result.map((url) {
                  int index = state.result.indexOf(url);
                  return Container(
                    width: getProportionateScreenWidth(6.0),
                    height: getProportionateScreenWidth(6.0),
                    margin: EdgeInsets.symmetric(
                        horizontal: getProportionateScreenWidth(3.0),
                        vertical: getProportionateScreenWidth(16)),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: _current == index ? thirdColor : fourthColor,
                    ),
                  );
                }).toList()),
          ]);
        } else if (state.state == ResultState.NoData) {
          return Center(child: Text(state.message));
        } else if (state.state == ResultState.Error) {
          return Center(child: Text('Terjadi Kesalahan'));
        } else {
          return Center(child: Text(''));
        }
      }),
    );
  }
}
