import 'package:flutter/material.dart';
import '../../../../theme.dart';
import '../../../../size_config.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:initial_folder/screens/splash/splash_screen_login.dart';

class Begin extends StatelessWidget {
  const Begin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //EmailProvider emailProvider = Provider.of<EmailProvider>(context);
    final user = FirebaseAuth.instance.currentUser;
    return Row(
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              height: getProportionateScreenWidth(60),
              width: getProportionateScreenWidth(60),
              decoration: BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.circle,
                  //border: Border.all(width: 2.0, color: Colors.white),
                  image: (Condition.loginFirebase == true)
                      ? DecorationImage(
                          fit: BoxFit.fill,
                          image: NetworkImage(user!.photoURL!))
                      : DecorationImage(
                          image:
                              AssetImage('assets/images/Profile Image.png'))),
            ),
          ],
        ),
        SizedBox(width: getProportionateScreenWidth(24)),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            //SizedBox(height: getProportionateScreenWidth(4)),
            Row(
              children: <Widget>[
                Text(
                  "Hai,",
                  style: primaryTextStyle.copyWith(
                    fontSize: getProportionateScreenWidth(12),
                    fontWeight: reguler,
                    color: tenthColor,
                  ),
                  textAlign: TextAlign.left,
                ),
              ],
            ),
            SizedBox(height: getProportionateScreenWidth(7)),
            Row(
              children: <Widget>[
                Text(
                  'Mau Upgrade Skill Apa?',
                  style: secondaryTextStyle.copyWith(
                    color: tenthColor,
                    fontWeight: semiBold,
                    fontSize: getProportionateScreenWidth(14),
                  ),
                  textAlign: TextAlign.left,
                ),
              ],
            ),
          ],
        ),
      ],
    );
  }
}
