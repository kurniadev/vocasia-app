[1mdiff --cc lib/helper/user_info.dart[m
[1mindex ce783fa,00092b8..0000000[m
[1m--- a/lib/helper/user_info.dart[m
[1m+++ b/lib/helper/user_info.dart[m
[36m@@@ -1,3 -1,3 +1,5 @@@[m
[32m++import 'dart:async';[m
[32m++[m
  import 'package:shared_preferences/shared_preferences.dart';[m
  [m
  class UsersInfo {[m
[1mdiff --cc lib/main.dart[m
[1mindex fa035f3,4908dbf..0000000[m
[1m--- a/lib/main.dart[m
[1m+++ b/lib/main.dart[m
[1mdiff --cc lib/models/user_info_model.dart[m
[1mindex 8f2f56f,8440a16..0000000[m
[1m--- a/lib/models/user_info_model.dart[m
[1m+++ b/lib/models/user_info_model.dart[m
[1mdiff --cc lib/providers/user_info_provider.dart[m
[1mindex 34f2ea8,b408919..0000000[m
[1m--- a/lib/providers/user_info_provider.dart[m
[1m+++ b/lib/providers/user_info_provider.dart[m
[1mdiff --cc lib/screens/checkout/components/field_kupon.dart[m
[1mindex b21b33e,622b9e2..0000000[m
[1m--- a/lib/screens/checkout/components/field_kupon.dart[m
[1m+++ b/lib/screens/checkout/components/field_kupon.dart[m
[1mdiff --cc lib/screens/home/components/body_comp/search_field.dart[m
[1mindex b5b5fb1,ada94d5..0000000[m
[1m--- a/lib/screens/home/components/body_comp/search_field.dart[m
[1m+++ b/lib/screens/home/components/body_comp/search_field.dart[m
[1mdiff --cc lib/screens/home/components/home_page.dart[m
[1mindex ae3ed0f,e0ba03b..0000000[m
[1m--- a/lib/screens/home/components/home_page.dart[m
[1m+++ b/lib/screens/home/components/home_page.dart[m
[1mdiff --cc lib/screens/login/login_email/login_email_screen.dart[m
[1mindex 7e66993,ce3ff7b..0000000[m
[1m--- a/lib/screens/login/login_email/login_email_screen.dart[m
[1m+++ b/lib/screens/login/login_email/login_email_screen.dart[m
[1mdiff --cc lib/screens/profile/account_sign_in/sign_in_screen_email.dart[m
[1mindex 0dd039e,ccfc187..0000000[m
[1m--- a/lib/screens/profile/account_sign_in/sign_in_screen_email.dart[m
[1m+++ b/lib/screens/profile/account_sign_in/sign_in_screen_email.dart[m
[36m@@@ -1,21 -1,24 +1,27 @@@[m
  import 'package:flutter/material.dart';[m
  import 'package:initial_folder/helper/user_info.dart';[m
[32m+ import 'package:initial_folder/providers/email_provider.dart';[m
[32m+ [m
  import 'package:initial_folder/providers/page_provider.dart';[m
[32m +import 'package:initial_folder/providers/user_info_provider.dart';[m
  import 'package:initial_folder/screens/login/login_screen.dart';[m
  import 'package:initial_folder/screens/profile/account_sign_in/data_diri.dart';[m
  import 'package:initial_folder/screens/profile/account_sign_in/setting_akun.dart';[m
[32m +import 'package:initial_folder/services/user_info_service.dart';[m
  import 'package:initial_folder/size_config.dart';[m
  import 'package:initial_folder/theme.dart';[m
[32m+ import 'package:initial_folder/models/user_info_model.dart';[m
  import 'package:initial_folder/screens/profile/components/about_profile_list.dart';[m
  import 'package:initial_folder/providers/google_sign_in.dart';[m
[32m+ import 'package:initial_folder/providers/user_info_provider.dart';[m
[32m+ import 'package:initial_folder/services/user_info_service.dart';[m
  import 'package:provider/provider.dart';[m
  import 'package:initial_folder/screens/splash/splash_screen_login.dart';[m
[32m +import 'package:initial_folder/models/user_info_model.dart';[m
  [m
  class SignInScreenEmail extends StatelessWidget {[m
[31m-   const SignInScreenEmail({Key? key}) : super(key: key);[m
[32m+   const SignInScreenEmail({Key? key, required this.email}) : super(key: key);[m
[32m+   final String email;[m
  [m
    @override[m
    Widget build(BuildContext context) {[m
[1mdiff --cc lib/screens/splash/splash_screen_login.dart[m
[1mindex 8d70828,e4ebd3d..0000000[m
[1m--- a/lib/screens/splash/splash_screen_login.dart[m
[1m+++ b/lib/screens/splash/splash_screen_login.dart[m
[1mdiff --cc lib/services/user_info_service.dart[m
[1mindex a7a75a2,8210133..0000000[m
[1m--- a/lib/services/user_info_service.dart[m
[1m+++ b/lib/services/user_info_service.dart[m
* Unmerged path lib/models/new_course_model/new_course_model.dart
[1mdiff --git a/lib/providers/course_by_category_provider.dart b/lib/providers/course_by_category_provider.dart[m
[1mindex 3c7267e..33d2653 100644[m
[1m--- a/lib/providers/course_by_category_provider.dart[m
[1m+++ b/lib/providers/course_by_category_provider.dart[m
[36m@@ -1,52 +1,52 @@[m
[31m-import 'package:flutter/material.dart';[m
[31m-import 'package:initial_folder/models/new_course_model/new_course_model.dart';[m
[31m-import 'package:initial_folder/services/course_by_category_service.dart';[m
[31m-[m
[31m-enum ResultState { Loading, NoData, HasData, Error }[m
[31m-[m
[31m-class CourseByCategoryProvider with ChangeNotifier {[m
[31m-  final CourseByCategoryService courseByCategoryService;[m
[31m-  final String id;[m
[31m-  CourseByCategoryProvider([m
[31m-      {required this.courseByCategoryService, required this.id}) {[m
[31m-    getCourseByCategory(id);[m
[31m-  }[m
[31m-  List<NewCourseModel> _courseByCategory = [];[m
[31m-[m
[31m-  ResultState? _state;[m
[31m-[m
[31m-  String _message = '';[m
[31m-[m
[31m-  List<NewCourseModel> get result => _courseByCategory;[m
[31m-[m
[31m-  ResultState? get state => _state;[m
[31m-[m
[31m-  String get message => _message;[m
[31m-[m
[31m-  set courseByCategory(List<NewCourseModel> courseByCategory) {[m
[31m-    _courseByCategory = courseByCategory;[m
[31m-    notifyListeners();[m
[31m-  }[m
[31m-[m
[31m-  Future<dynamic> getCourseByCategory(_id) async {[m
[31m-    try {[m
[31m-      _state = ResultState.Loading;[m
[31m-      notifyListeners();[m
[31m-      List<NewCourseModel> courseByCategory =[m
[31m-          await courseByCategoryService.getCourseByCategory(_id);[m
[31m-      if (courseByCategory.isEmpty) {[m
[31m-        _state = ResultState.NoData;[m
[31m-        notifyListeners();[m
[31m-        return _message = 'Empty Data';[m
[31m-      } else {[m
[31m-        _state = ResultState.HasData;[m
[31m-        notifyListeners();[m
[31m-        return _courseByCategory = courseByCategory;[m
[31m-      }[m
[31m-    } catch (e) {[m
[31m-      _state = ResultState.Error;[m
[31m-      notifyListeners();[m
[31m-      return _message = 'Error --> $e';[m
[31m-    }[m
[31m-  }[m
[31m-}[m
[32m+[m[32m// import 'package:flutter/material.dart';[m
[32m+[m[32m// import 'package:initial_folder/models/new_course_model/new_course_model.dart';[m
[32m+[m[32m// import 'package:initial_folder/services/course_by_category_service.dart';[m
[32m+[m
[32m+[m[32m// enum ResultState { Loading, NoData, HasData, Error }[m
[32m+[m
[32m+[m[32m// class CourseByCategoryProvider with ChangeNotifier {[m
[32m+[m[32m//   final CourseByCategoryService courseByCategoryService;[m
[32m+[m[32m//   final String id;[m
[32m+[m[32m//   CourseByCategoryProvider([m
[32m+[m[32m//       {required this.courseByCategoryService, required this.id}) {[m
[32m+[m[32m//     getCourseByCategory(id);[m
[32m+[m[32m//   }[m
[32m+[m[32m//   List<NewCourseModel> _courseByCategory = [];[m
[32m+[m
[32m+[m[32m//   ResultState? _state;[m
[32m+[m
[32m+[m[32m//   String _message = '';[m
[32m+[m
[32m+[m[32m//   List<NewCourseModel> get result => _courseByCategory;[m
[32m+[m
[32m+[m[32m//   ResultState? get state => _state;[m
[32m+[m
[32m+[m[32m//   String get message => _message;[m
[32m+[m
[32m+[m[32m//   set courseByCategory(List<NewCourseModel> courseByCategory) {[m
[32m+[m[32m//     _courseByCategory = courseByCategory;[m
[32m+[m[32m//     notifyListeners();[m
[32m+[m[32m//   }[m
[32m+[m
[32m+[m[32m//   Future<dynamic> getCourseByCategory(_id) async {[m
[32m+[m[32m//     try {[m
[32m+[m[32m//       _state = ResultState.Loading;[m
[32m+[m[32m//       notifyListeners();[m
[32m+[m[32m//       List<NewCourseModel> courseByCategory =[m
[32m+[m[32m//           await courseByCategoryService.getCourseByCategory(_id);[m
[32m+[m[32m//       if (c